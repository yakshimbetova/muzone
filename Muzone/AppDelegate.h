//
//  AppDelegate.h
//  Muzone
//
//  Created by Sukhrob Khakimov on 5/22/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

#import "DownloadManager.h"
#import "TrackManager.h"

#import "REFrostedViewController.h"
#import "MenuViewController.h"
#import "PlayerViewController.h"

#import "NowPlayingView.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (nonnull, nonatomic, strong) UIWindow *window;

@property (nonnull, nonatomic, strong) DownloadManager *downloadManager;
@property (nonnull, nonatomic, strong) TrackManager *trackManager;

@property (nonatomic, retain) REFrostedViewController * _Nullable containerViewController;
@property (nonnull, nonatomic, strong) MenuViewController *menuViewController;
@property (nonnull, nonatomic, strong) PlayerViewController *playerViewController;

@property (nonnull, nonatomic, strong) NowPlayingView *nowPlayingView;
@property (nonatomic, assign, getter = isNowPlayingVisible) BOOL isNowPlayingVisible;
@property (nullable, nonatomic, copy) void(^backgroundTransferCompletionHandler)();

@property (nonnull, nonatomic, strong, readonly) NSManagedObjectContext *masterManagedObjectContext;
@property (nonnull, nonatomic, strong, readonly) NSManagedObjectContext *mainManagedObjectContext;
@property (nonnull, nonatomic, strong, readonly) NSManagedObjectContext *temporaryManagedObjectContext;
@property (nonnull, nonatomic, strong, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonnull, nonatomic, strong, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (UIEdgeInsets)contentInsetForScrollView:(nonnull UIScrollView *)scrollView;
-(void)launch;
-(void)logout;

@end
