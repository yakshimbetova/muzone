//
//  AppDelegate.m
//  Muzone
//
//  Created by Sukhrob Khakimov on 5/22/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//
#import "FavoriteManager.h"
#import <AVFoundation/AVFoundation.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <TSMessages/TSMessageView.h>

#import "AppDelegate.h"
#import "Constants.h"
#import "BaseTheme.h"
#import "SessionManager.h"
#import "ApiClient.h"
#import "TrackRepository.h"
#import "ErrorHandler.h"

#import "App.h"
#import "Album.h"
#import "Artist.h"
#import "Genre.h"
#import "Track.h"

#import "BaseNavigationController.h"

#import "LoginViewController.h"
#import "User.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
@import Firebase;

#import "Muzone-Swift.h" // FIXME: testing

@interface AppDelegate () <UINavigationControllerDelegate, MenuViewControllerDelegate, REFrostedViewControllerDelegate, TSMessageViewProtocol, NowPlayingViewDelegate>

@property (nonatomic, assign) BOOL wasPlaying;
@property (nonnull, nonatomic, strong) NSMutableDictionary *tracksQueue;

@property (nullable, nonatomic, strong) UIView *panView;

@end

@implementation AppDelegate

@synthesize masterManagedObjectContext = _masterManagedObjectContext;
@synthesize mainManagedObjectContext = _mainManagedObjectContext;
@synthesize temporaryManagedObjectContext = _temporaryManagedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

#pragma mark - UIApplication

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Window
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    
#ifndef DEBUG
    // Fabric
    [Fabric with:@[[Crashlytics class], [Answers class]]];
#endif
    
    // Session Manager
    SessionManager *sessionManager = [SessionManager sharedManager];
    
    App *app = sessionManager.app;
    
    if (!app) {
        app = [App new];
    }
    
    app.version = APP_VERSION;
    app.build = APP_BUILD;
    [sessionManager setApp:app];
    [sessionManager saveSession];
    
    //Facebook
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];

    //Firebase
    [FIRApp configure];
    
    
    // Theme
    [BaseTheme currentTheme];
    
    // TSMessage settings
    [TSMessage setDelegate:self];
    
    // NSNotificationCenter
    [self registerForNotificationCenterNotifications];
    
    // UIEvent
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    
    
    if ([User currentUser].accessToken != nil) {
        self.window.rootViewController = self.containerViewController;
    }
    else
    {
        self.window.rootViewController = [LoginViewController new];
    }

    [self.window makeKeyAndVisible];

    return YES;
}

- (void)application:(UIApplication *)application handleEventsForBackgroundURLSession:(NSString *)identifier completionHandler:(void (^)())completionHandler {
    self.downloadManager.savedCompletionHandler = completionHandler;
}

-(void)launch
{
    if ([self.window.rootViewController presentedViewController]) {
        [self.window.rootViewController dismissViewControllerAnimated:NO completion:^{}];
    }
    self.window.rootViewController = self.containerViewController;
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter postNotificationName:UserSignInNotification object:nil];
}

-(void)logout
{
    [[User currentUser] logout];
    self.containerViewController = nil;
    self.window.rootViewController = [LoginViewController new];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self.playerViewController dispose];
}


- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                  openURL:url
                                                        sourceApplication:sourceApplication
                                                               annotation:annotation
                    ];
    return handled;
} 


#pragma mark - UIEvent

- (void)remoteControlReceivedWithEvent:(UIEvent *)event {
    switch (event.subtype) {
        case UIEventSubtypeRemoteControlPlay:
            [self.playerViewController play];
            break;
            
        case UIEventSubtypeRemoteControlPause:
            [self.playerViewController pause];
            break;
            
        case UIEventSubtypeRemoteControlTogglePlayPause:
            if (self.playerViewController.isRunning) {
                [self.playerViewController pause];
            } else {
                [self.playerViewController play];
            }
            
            break;
            
        case UIEventSubtypeRemoteControlPreviousTrack:
            [self.playerViewController previous];
            self.nowPlayingView.track = self.playerViewController.currentTrack;
            break;
            
        case UIEventSubtypeRemoteControlNextTrack:
            [self.playerViewController next];
            self.nowPlayingView.track = self.playerViewController.currentTrack;
            break;
            
        default:
            break;
    }
}

#pragma mark - DownloadManager

- (DownloadManager *)downloadManager {
    if (_downloadManager == nil) {
        _downloadManager = [[DownloadManager alloc] initWithClient:[ApiClient sharedClient]];
    }
    
    return _downloadManager;
}

#pragma mark - TrackManager

- (TrackManager *)trackManager {
    if (_trackManager == nil) {
        _trackManager = [[TrackManager alloc] initWithManagedObjectContext:self.mainManagedObjectContext
                                                             directoryPath:TRACKS_DIRECTORY];
    }
    
    return _trackManager;
}

#pragma mark - REFrostedViewController

- (REFrostedViewController *)containerViewController {
    if (_containerViewController == nil) {
        _containerViewController = [[REFrostedViewController alloc] initWithContentViewController:self.menuViewController.selectedViewController
                                                                               menuViewController:self.menuViewController];
        _containerViewController.delegate = self;
        _containerViewController.direction = REFrostedViewControllerDirectionLeft;
        _containerViewController.animationDuration = 0.25f;
        _containerViewController.limitMenuViewSize = YES;
        
        // Pan view
        [_containerViewController.view addSubview:self.panView];
        
        [_containerViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[panView(width)]"
                                                                                              options:0
                                                                                              metrics:@{@"width": @(5.0f)}
                                                                                                views:@{@"panView": self.panView}]];
        
        [_containerViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[panView]|"
                                                                                              options:0
                                                                                              metrics:nil
                                                                                                views:@{@"panView": self.panView}]];
    }
    
    return _containerViewController;
}

- (UIView *)panView {
    if (_panView == nil) {
        _panView = [[UIView alloc] init];
        _panView.translatesAutoresizingMaskIntoConstraints = NO;
        [_panView addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self
                                                                               action:@selector(panGestureRecognized:)]];
    }
    
    return _panView;
}

- (void)panGestureRecognized:(UIPanGestureRecognizer *)sender {
    UIViewController *viewController = ((BaseNavigationController *)self.menuViewController.selectedViewController).viewControllers.firstObject;
    UIViewController *visibleViewController = [self visibleViewController:self.menuViewController.selectedViewController];
    
    if ([viewController isEqual:visibleViewController]) {
        [self.containerViewController panGestureRecognized:sender];
    }
}

#pragma mark - REFrostedViewControllerDelegate

- (void)frostedViewController:(REFrostedViewController *)frostedViewController willShowMenuViewController:(UIViewController *)menuViewController {
    // Not implemented
}

- (void)frostedViewController:(REFrostedViewController *)frostedViewController didHideMenuViewController:(UIViewController *)menuViewController {
    // Not implemented
}

#pragma mark - UINavigationControllerDelegate

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    UIViewController *rootViewController = ((BaseNavigationController *)self.menuViewController.selectedViewController).viewControllers.firstObject;
    
    if ([viewController isEqual:rootViewController]) {
        self.panView.hidden = NO;
    } else {
        self.panView.hidden = YES;
    }
}

#pragma mark - MenuViewController

- (MenuViewController *)menuViewController {
    if (_menuViewController == nil) {
        _menuViewController = [[MenuViewController alloc] init];
        _menuViewController.delegate = self;
        
        BaseNavigationController *navigationController = (BaseNavigationController *)_menuViewController.selectedViewController;
        navigationController.delegate = self;
    }
    
    return _menuViewController;
}

#pragma mark - MenuViewControllerDelegate

- (void)menuViewControllerDidSelectViewController:(MenuViewController *)menuViewController {
    BaseNavigationController *navigationController = (BaseNavigationController *)self.menuViewController.selectedViewController;
    navigationController.delegate = self;
}

#pragma mark - Visible view controller

- (UIViewController *)visibleViewController:(UIViewController *)viewController {
    if ([viewController isKindOfClass:[UINavigationController class]]) {
        return [self visibleViewController:[((UINavigationController *) viewController) visibleViewController]];
    } else if ([viewController isKindOfClass:[UITabBarController class]]) {
        return [self visibleViewController:[((UITabBarController *) viewController) selectedViewController]];
    } else if (viewController.presentedViewController) {
        return [self visibleViewController:viewController.presentedViewController];
    }
    
    return viewController;
}

#pragma mark - TSMessageViewProtocol

- (void)customizeMessageView:(TSMessageView *)messageView {
    messageView.alpha = 0.9f;
}

#pragma mark - NSNotificationCenter

- (void)registerForNotificationCenterNotifications {
    NSError *error;
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayback
                   error:&error];
    [session setActive:YES
                 error:&error];
    
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    
    [notificationCenter addObserver:self
                           selector:@selector(errorHandlerWillHandleError:)
                               name:ErrorHandlerWillHandleError
                             object:nil];
    
    [notificationCenter addObserver:self
                           selector:@selector(audioSessionRouteChanged:)
                               name:AVAudioSessionRouteChangeNotification
                             object:nil];
    
    [notificationCenter addObserver:self
                           selector:@selector(audioSessionInterrupted:)
                               name:AVAudioSessionInterruptionNotification
                             object:session];
    
    [notificationCenter addObserver:self
                           selector:@selector(playerViewControllerWillPlay:)
                               name:PlayerViewControllerWillPlay
                             object:nil];
    
    [notificationCenter addObserver:self
                           selector:@selector(playerViewControllerWillPause:)
                               name:PlayerViewControllerWillPause
                             object:nil];
    
    [notificationCenter addObserver:self
                           selector:@selector(playerViewControllerWillStop:)
                               name:PlayerViewControllerWillStop
                             object:nil];
    
    [notificationCenter addObserver:self
                           selector:@selector(playerViewControllerDidChangeCurrentTrack:)
                               name:PlayerViewControllerDidChangeCurrentTrack
                             object:nil];
    
    [notificationCenter addObserver:self
                           selector:@selector(playerViewControllerWillAppear:)
                               name:PlayerViewControllerWillAppear
                             object:nil];
    
    [notificationCenter addObserver:self
                           selector:@selector(playerViewControllerDidDisappear:)
                               name:PlayerViewControllerDidDisappear
                             object:nil];
    
    [notificationCenter addObserver:self
                           selector:@selector(playerViewControllerShouldHandleError:)
                               name:PlayerViewControllerShouldHandleError
                             object:nil];
    
    [notificationCenter addObserver:self
                           selector:@selector(trackQueueDidEnqueue:)
                               name:TrackQueueDidEnqueue
                             object:nil];
    
    [notificationCenter addObserver:self
                           selector:@selector(downloadManagerProcessDidEnd:)
                               name:DownloadManagerProcessDidEnd
                             object:nil];
}

- (void)errorHandlerWillHandleError:(NSNotification *)notification {
    NSError *error = notification.userInfo[ErrorHandlerErrorKey];
    
    if (error.code != NSURLErrorCancelled) {
        [TSMessage showNotificationInViewController:[self visibleViewController:self.window.rootViewController]
                                              title:NSLocalizedString(@"error", nil)
                                           subtitle:[ErrorHandler messageFromError:error]
                                               type:TSMessageNotificationTypeError
                                           duration:TSMessageNotificationDurationAutomatic];
    }
}

- (void)audioSessionRouteChanged:(NSNotification *)notification {
    NSInteger reason = [notification.userInfo[AVAudioSessionRouteChangeReasonKey] integerValue];
    
    if (reason == AVAudioSessionRouteChangeReasonOldDeviceUnavailable) {
        if (self.playerViewController.isRunning) {
            [self.playerViewController pause];
        }
    }
}

- (void)audioSessionInterrupted:(NSNotification *)notification {
    if ([notification.name isEqualToString:AVAudioSessionInterruptionNotification]) {
        if ([[notification.userInfo valueForKey:AVAudioSessionInterruptionTypeKey] isEqualToNumber:@(AVAudioSessionInterruptionTypeBegan)]) {
            self.wasPlaying = self.playerViewController.isRunning;
            [self.playerViewController pause];
        } else if ([[notification.userInfo valueForKey:AVAudioSessionInterruptionTypeKey] isEqualToNumber:@(AVAudioSessionInterruptionTypeEnded)]) {
            NSError *error;
            AVAudioSession *session = [AVAudioSession sharedInstance];
            [session setActive:YES
                         error:&error];
            
            if (self.wasPlaying) {
                [self.playerViewController play];
            }
        }
    }
}

- (void)playerViewControllerWillPlay:(NSNotification *)notification {
    self.isNowPlayingVisible = YES;
    self.nowPlayingView.track = self.playerViewController.currentTrack;
    [self.nowPlayingView showInView:self.window.rootViewController.view
                           animated:YES];
}

- (void)playerViewControllerWillPause:(NSNotification *)notification {
    [self.nowPlayingView pause];
}

- (void)playerViewControllerWillStop:(NSNotification *)notification {
    self.isNowPlayingVisible = NO;
    [self.nowPlayingView stop];
}

- (void)playerViewControllerDidChangeCurrentTrack:(NSNotification *)notification {
    self.nowPlayingView.track = self.playerViewController.currentTrack;
}

- (void)playerViewControllerWillAppear:(NSNotification *)notification {
    [self.nowPlayingView hideAnimated:NO];
}

- (void)playerViewControllerDidDisappear:(NSNotification *)notification {
    if (self.playerViewController.isRunning) {
        self.nowPlayingView.track = self.playerViewController.currentTrack;
        [self.nowPlayingView showInView:self.window.rootViewController.view
                               animated:YES];
    }
}

- (void)playerViewControllerShouldHandleError:(NSNotification *)notification {
    [self.nowPlayingView pause];
    
    NSError *error = notification.userInfo[PlayerViewControllerErrorKey];
    [ErrorHandler handleError:error];
}

- (void)trackQueueDidEnqueue:(NSNotification *)notification {
    Track *track = notification.userInfo[TrackQueueKey];
    [self.tracksQueue setObject:track
                         forKey:track.fileUrl];
}

- (void)downloadManagerProcessDidEnd:(NSNotification *)notification {
    NSURL *fileUrl = notification.userInfo[DownloadManagerFileUrlKey];
    NSString *localFileName = notification.userInfo[DownloadManagerLocalFileNameKey];
    NSError *error = notification.userInfo[DownloadManagerErrorKey];
    Track *track = self.tracksQueue[fileUrl.absoluteString];
    
    if (error) {
        if (track) {
            [self.temporaryManagedObjectContext performBlock:^{
                [self.temporaryManagedObjectContext deleteObject:track];
            }];
            
            [self.tracksQueue removeObjectForKey:fileUrl.absoluteString];
        }
        
        [ErrorHandler handleError:error];
    } else {
        if (track) {
            track.localFileName = localFileName;
            [track copyWithContext:self.masterManagedObjectContext];
            
            [self.temporaryManagedObjectContext performBlock:^{
                [self.temporaryManagedObjectContext deleteObject:track];
                
                [self.masterManagedObjectContext performBlock:^{
                    NSError *error;
                    
                    if (![self.masterManagedObjectContext save:&error]) {
                        [self.mainManagedObjectContext performBlock:^{
                            [ErrorHandler handleError:error];
                        }];
                    }
                }];
            }];
            
            [self.tracksQueue removeObjectForKey:fileUrl.absoluteString];
        }
    }
}

#pragma mark -

- (UIEdgeInsets)contentInsetForScrollView:(UIScrollView *)scrollView {
    UIEdgeInsets contentInset = scrollView.contentInset;
    
    if (self.isNowPlayingVisible) {
        contentInset.bottom = self.nowPlayingView.height;
    } else {
        contentInset.bottom = 0.0f;
    }
    
    return contentInset;
}

#pragma mark - NowPlayingView

- (NowPlayingView *)nowPlayingView {
    if (_nowPlayingView == nil) {
        _nowPlayingView = [[NowPlayingView alloc] initWithTrack:self.playerViewController.currentTrack];
        _nowPlayingView.delegate = self;
    }
    
    return _nowPlayingView;
}

#pragma mark - NowPlayingViewDelegate

- (void)nowPlayingViewWillPlay:(NowPlayingView *)nowPlayingView {
    [self.playerViewController play];
}

- (void)nowPlayingViewWillPause:(NowPlayingView *)nowPlayingView {
    [self.playerViewController pause];
}

- (void)nowPlayingViewWillStop:(NowPlayingView *)nowPlayingView {
    [self.playerViewController stop];
}

- (void)nowPlayingViewWillDisappear:(NowPlayingView *)nowPlayingView {
    [self.window.rootViewController presentViewController:self.playerViewController
                                                 animated:YES
                                               completion:nil];
}

#pragma mark - NSMutableDictionary

- (NSMutableDictionary *)tracksQueue {
    if (_tracksQueue == nil) {
        _tracksQueue = [NSMutableDictionary new];
    }
    
    return _tracksQueue;
}

#pragma mark - CoreData

- (NSURL *)applicationDocumentsDirectory {
    return [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                  inDomains:NSUserDomainMask].lastObject;
}

- (NSManagedObjectModel *)managedObjectModel {
    if (_managedObjectModel == nil) {
        NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Muzone"
                                                  withExtension:@"momd"];
        _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    }
    
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    
    NSDictionary *options = @{NSMigratePersistentStoresAutomaticallyOption: @YES,
                              NSInferMappingModelAutomaticallyOption: @YES};
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Muzone.sqlite"];
    // Uncomment to delete the data store
//    [[NSFileManager defaultManager] removeItemAtPath:storeURL.path error:nil];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
        NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
        dictionary[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dictionary[NSLocalizedFailureReasonErrorKey] = failureReason;
        dictionary[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN"
                                    code:9999
                                userInfo:dictionary];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

- (NSManagedObjectContext *)masterManagedObjectContext {
    if (_masterManagedObjectContext != nil) {
        return _masterManagedObjectContext;
    }
    
    NSPersistentStoreCoordinator *persistentStoreCoordinator = self.persistentStoreCoordinator;
    
    if (!persistentStoreCoordinator) {
        return nil;
    }
    
    _masterManagedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    _masterManagedObjectContext.persistentStoreCoordinator = persistentStoreCoordinator;
    
    return _masterManagedObjectContext;
}

- (NSManagedObjectContext *)mainManagedObjectContext {
    if (_mainManagedObjectContext == nil) {
        _mainManagedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        _mainManagedObjectContext.parentContext = self.masterManagedObjectContext;
    }
    
    return _mainManagedObjectContext;
}

- (NSManagedObjectContext *)temporaryManagedObjectContext {
    if (_temporaryManagedObjectContext == nil) {
        _temporaryManagedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        _temporaryManagedObjectContext.parentContext = self.masterManagedObjectContext;
    }
    
    return _temporaryManagedObjectContext;
}

@end
