//
//  Constants.h
//  Muzone
//
//  Created by Sukhrob Khakimov on 7/4/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

#define APP_NAME           [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleName"]
#define APP_VERSION        [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]
#define APP_BUILD          [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey]

#define TERMS_URL          @"https://muzone.uz/terms/m"
#define ABOUT_URL          @"https://muzone.uz/about/m"

#define TRACKS_DIRECTORY   @"Tracks"
#define MOVIES_DIRECTORY   @"Movies"

#define kApiBaseUrlString @"https://api.muzone.uz" // Production
//#define kApiBaseUrlString @"https://devapi.muzone.uz" // Development

#define WEAKIFY_SELF __weak __typeof(self) weakSelf = self
#define STRONGIFY_SELF __strong __typeof(self) self = weakSelf
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>



#define Rgb2UIColor(r, g, b)  [UIColor colorWithRed:((r) / 255.0) green:((g) / 255.0) blue:((b) / 255.0) alpha:1.0]

extern NSString *__nonnull const TrackQueueDidEnqueue;
extern NSString *__nonnull const TrackQueueKey;

@interface Constants : NSObject

@end
