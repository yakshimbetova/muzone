//
//  Constants.m
//  Muzone
//
//  Created by Sukhrob Khakimov on 7/4/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import "Constants.h"

NSString *const TrackQueueDidEnqueue = @"TrackQueueDidEnqueue";
NSString *const TrackQueueKey = @"TrackQueueKey";

@implementation Constants

@end
