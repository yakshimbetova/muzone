//
//  Album+CoreDataProperties.h
//  Muzone
//
//  Created by Sukhrob Khakimov on 10/16/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Album.h"

NS_ASSUME_NONNULL_BEGIN

@interface Album (CoreDataProperties)

@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *imageUrl;
@property (nonatomic, retain) NSNumber *year;
@property (nonatomic, retain) NSNumber *tracksCount;
@property (nonatomic, retain) NSNumber *uid;
@property (nonatomic, retain) Artist *artist;
@property (nonatomic, retain) NSSet<Track *> *tracks;

@end

@interface Album (CoreDataGeneratedAccessors)

- (void)addTracksObject:(Track *)value;
- (void)removeTracksObject:(Track *)value;
- (void)addTracks:(NSSet<Track *> *)values;
- (void)removeTracks:(NSSet<Track *> *)values;

@end

NS_ASSUME_NONNULL_END
