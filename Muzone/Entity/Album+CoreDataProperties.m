//
//  Album+CoreDataProperties.m
//  Muzone
//
//  Created by Sukhrob Khakimov on 10/16/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Album+CoreDataProperties.h"

@implementation Album (CoreDataProperties)

@dynamic title;
@dynamic imageUrl;
@dynamic year;
@dynamic tracksCount;
@dynamic uid;
@dynamic artist;
@dynamic tracks;
@end
