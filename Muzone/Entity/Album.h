//
//  Album.h
//  Muzone
//
//  Created by Sukhrob Khakimov on 10/16/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#import "Model.h"

@class Artist, Track;

NS_ASSUME_NONNULL_BEGIN

@interface Album : NSManagedObject <Model>

@end

NS_ASSUME_NONNULL_END

#import "Album+CoreDataProperties.h"
