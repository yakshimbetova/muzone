//
//  Album.m
//  Muzone
//
//  Created by Sukhrob Khakimov on 10/16/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import "Album.h"
#import "Artist.h"
#import "Track.h"

@implementation Album

#pragma mark - Model

- (instancetype)initWithDictionary:(NSDictionary *)dictionary context:(NSManagedObjectContext *)context {
    NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass(self.class)
                                              inManagedObjectContext:context];
    self = [super initWithEntity:entity insertIntoManagedObjectContext:context];
    
    if (self) {
        [self setWithDictionary:dictionary];
    }
    
    return self;
}

- (void)setWithDictionary:(NSDictionary *)dictionary {
    self.uid = dictionary[@"id"];
    
    if (![dictionary[@"title"] isEqual:[NSNull null]]) {
        self.title = dictionary[@"title"];
    }
    
    if (![dictionary[@"image"] isEqual:[NSNull null]]) {
        self.imageUrl = dictionary[@"image"];
    }
    
    self.year = dictionary[@"year"];
    self.tracksCount = dictionary[@"totalTrackCount"];
    self.artist = [[Artist alloc] initWithDictionary:dictionary[@"album_artist"]
                                             context:self.managedObjectContext];
}

- (id)copyWithContext:(NSManagedObjectContext *)context {
    NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass(self.class)
                                              inManagedObjectContext:context];
    Album *album = [[Album alloc] initWithEntity:entity insertIntoManagedObjectContext:context];
    album.uid = [self.uid copy];
    album.title = [self.title copy];
    album.imageUrl = [self.imageUrl copy];
    album.year = [self.year copy];
    album.tracksCount = [self.tracksCount copy];
    album.artist = [self.artist copyWithContext:context];
    
    return album;
}

@end
