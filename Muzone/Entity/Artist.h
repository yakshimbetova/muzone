//
//  Artist.h
//  Muzone
//
//  Created by Sukhrob Khakimov on 10/16/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#import "Model.h"

@class Album, Track;

NS_ASSUME_NONNULL_BEGIN

@interface Artist : NSManagedObject <Model>

@end

NS_ASSUME_NONNULL_END

#import "Artist+CoreDataProperties.h"
