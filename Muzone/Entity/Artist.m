//
//  Artist.m
//  Muzone
//
//  Created by Sukhrob Khakimov on 10/16/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import "Album.h"
#import "Artist.h"
#import "Track.h"

@implementation Artist

#pragma mark - Model

- (instancetype)initWithDictionary:(NSDictionary *)dictionary context:(NSManagedObjectContext *)context {
    NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass(self.class)
                                              inManagedObjectContext:context];
    self = [super initWithEntity:entity insertIntoManagedObjectContext:context];
    
    if (self) {
        [self setWithDictionary:dictionary];
    }
    
    return self;
}

- (void)setWithDictionary:(NSDictionary *)dictionary {
    if (![dictionary[@"id"] isEqual:[NSNull null]]) {
        self.uid = dictionary[@"id"];
    }
    
    if (![dictionary[@"name"] isEqual:[NSNull null]]) {
        self.name = dictionary[@"name"];
    }
}

- (id)copyWithContext:(NSManagedObjectContext *)context {
    NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass(self.class)
                                              inManagedObjectContext:context];
    Artist *artist = [[Artist alloc] initWithEntity:entity insertIntoManagedObjectContext:context];
    artist.uid = [self.uid copy];
    artist.name = [self.name copy];
    artist.photoUrl = [self.photoUrl copy];
    artist.bio = [self.bio copy];
    
    return artist;
}

@end
