//
//  Author.swift
//  Muzone
//
//  Created by Sukhrob Khakimov on 10/21/17.
//  Copyright © 2017 Muzone LLC. All rights reserved.
//

import CoreData
import Foundation

@objc(Author)
public class Author: NSManagedObject, Model {
    @NSManaged public var uid: String
    @NSManaged public var username: String
    @NSManaged public var firstName: String?
    @NSManaged public var lastName: String?
    @NSManaged public var profileImageUrl: String?
    @NSManaged public var playlists: Set<Playlist>

    private override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }

    public init(uid: String, username: String, context: NSManagedObjectContext) {
        let entityDescription = NSEntityDescription.entity(
            forEntityName: String(describing: type(of: self)),
            in: context
        )

        super.init(entity: entityDescription!, insertInto: context)

        self.uid = uid
        self.username = username
    }

    public required init(dictionary: [AnyHashable: Any], context: NSManagedObjectContext) {
        let entityDescription = NSEntityDescription.entity(
            forEntityName: String(describing: type(of: self)),
            in: context
        )

        super.init(entity: entityDescription!, insertInto: context)

        setWith(dictionary)
    }

    public func setWith(_ dictionary: [AnyHashable: Any]) {
        if let uid = dictionary["id"] as? String {
            self.uid = uid
        } else {
            uid = ""
        }

        if let username = dictionary["username"] as? String {
            self.username = username
        } else {
            username = ""
        }

        if let firstName = dictionary["firstName"] as? String {
            self.firstName = firstName
        }

        if let lastName = dictionary["lastName"] as? String {
            self.lastName = lastName
        }

        if let profileImageUrl = dictionary["profilePicUrl"] as? String {
            self.profileImageUrl = profileImageUrl
        }
    }

    public func copy(with context: NSManagedObjectContext) -> Any {
        let author = Author(uid: uid, username: username, context: context)
        author.firstName = firstName
        author.lastName = lastName
        author.profileImageUrl = profileImageUrl

        return author
    }
}

extension Author {
    func name() -> String {
        if let firstName = firstName , !firstName.isEmpty, let lastName = lastName, !lastName.isEmpty {
            return String(format: "%@ %@", firstName, lastName)
        } else if let firstName = firstName , !firstName.isEmpty {
            return firstName
        } else if let lastName = lastName, !lastName.isEmpty {
            return lastName
        }

        return ""
    }
}

