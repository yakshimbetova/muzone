//
//  Genre.m
//  Muzone
//
//  Created by Sukhrob Khakimov on 10/16/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import "Genre.h"
#import "Track.h"

@implementation Genre

#pragma mark - Model

- (instancetype)initWithDictionary:(NSDictionary *)dictionary context:(NSManagedObjectContext *)context {
    NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass(self.class)
                                              inManagedObjectContext:context];
    self = [super initWithEntity:entity insertIntoManagedObjectContext:context];
    
    if (self) {
        [self setWithDictionary:dictionary];
    }
    
    return self;
}

- (void)setWithDictionary:(NSDictionary *)dictionary {
    if (![dictionary[@"id"] isEqual:[NSNull null]]) {
        self.uid = dictionary[@"id"];
    }
    
    if (![dictionary[@"title"] isEqual:[NSNull null]]) {
        self.title = dictionary[@"title"];
    }
    
    if (![dictionary[@"subtitle"] isEqual:[NSNull null]]) {
        self.subtitle = dictionary[@"subtitle"];
    }
    
    if (![dictionary[@"image"] isEqual:[NSNull null]]) {
        self.imageUrl = dictionary[@"image"];
    }
}

- (id)copyWithContext:(NSManagedObjectContext *)context {
    NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass(self.class)
                                              inManagedObjectContext:context];
    Genre *genre = [[Genre alloc] initWithEntity:entity insertIntoManagedObjectContext:context];
    genre.uid = [self.uid copy];
    genre.title = [self.title copy];
    genre.subtitle = [self.subtitle copy];
    genre.imageUrl = [self.imageUrl copy];
    
    return genre;
}

@end
