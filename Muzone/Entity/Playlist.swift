//
//  Playlist.swift
//  Muzone
//
//  Created by Sukhrob Khakimov on 10/21/17.
//  Copyright © 2017 Muzone LLC. All rights reserved.
//

import CoreData
import Foundation

@objc(Playlist)
public class Playlist: NSManagedObject, Model {
    @NSManaged public var uid: NSNumber?
    @NSManaged public var title: String
    @NSManaged public var coverImageUrl: String?
    @NSManaged public var isFavorite: Bool
    @NSManaged public var isPublic: Bool
    @NSManaged public var followersCount: NSNumber
    @NSManaged public var tracksCount: NSNumber
    @NSManaged public var author: Author
    @NSManaged public var tracks: Set<Track>

    private override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }

    public init(title: String, context: NSManagedObjectContext) {
        let entityDescription = NSEntityDescription.entity(
            forEntityName: String(describing: type(of: self)),
            in: context
        )

        super.init(entity: entityDescription!, insertInto: context)

        self.title = title
        isFavorite = false
        isPublic = false
        followersCount = 0
        tracksCount = 0
    }

    public required init(dictionary: [AnyHashable: Any], context: NSManagedObjectContext) {
        let entityDescription = NSEntityDescription.entity(
            forEntityName: String(describing: type(of: self)),
            in: context
        )

        super.init(entity: entityDescription!, insertInto: context)

        setWith(dictionary)
    }

    public func setWith(_ dictionary: [AnyHashable: Any]) {
        if let uid = dictionary["id"] as? NSNumber {
            self.uid = uid
        }

        if let title = dictionary["title"] as? String {
            self.title = title
        } else {
            title = ""
        }

        if let coverImageUrl = dictionary["image"] as? String {
            self.coverImageUrl = coverImageUrl
        }

        if let isFavorite = dictionary["is_favorite"] as? Bool {
            self.isFavorite = isFavorite
        } else {
            isFavorite = false
        }

        if let isPublic = dictionary["is_public"] as? Bool {
            self.isPublic = isPublic
        } else {
            isPublic = false
        }

        if let followersCount = dictionary["followers"] as? NSNumber {
            self.followersCount = followersCount
        } else {
            followersCount = NSNumber(value: 0)
        }

        if let tracksCount = dictionary["track_count"] as? NSNumber {
            self.tracksCount = tracksCount
        } else {
            tracksCount = NSNumber(value: 0)
        }

        if let authorDictionary = dictionary["owner"] as? [String: Any] {
            author = Author(dictionary: authorDictionary, context: managedObjectContext!)
        } else {
            author = Author(uid: "", username: "", context: managedObjectContext!)
        }
    }

    public func copy(with context: NSManagedObjectContext) -> Any {
        let playlist = Playlist(title: title, context: context)
        playlist.uid = uid
        playlist.coverImageUrl = coverImageUrl
        playlist.isFavorite = isFavorite
        playlist.isPublic = isPublic
        playlist.followersCount = followersCount
        playlist.tracksCount = tracksCount

        if let author = author.copy(with: context) as? Author {
            playlist.author = author
        } else {
            playlist.author = Author(uid: "", username: "", context: context)
        }

        return playlist
    }
}
