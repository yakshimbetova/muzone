//
//  SearchHistory+CoreDataProperties.h
//  Muzone
//
//  Created by Sukhrob Khakimov on 6/22/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "SearchHistory.h"

NS_ASSUME_NONNULL_BEGIN

@interface SearchHistory (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *key;
@property (nullable, nonatomic, retain) NSDate *createdAt;

@end

NS_ASSUME_NONNULL_END
