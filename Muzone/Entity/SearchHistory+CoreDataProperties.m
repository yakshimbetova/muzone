//
//  SearchHistory+CoreDataProperties.m
//  Muzone
//
//  Created by Sukhrob Khakimov on 6/22/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "SearchHistory+CoreDataProperties.h"

@implementation SearchHistory (CoreDataProperties)

@dynamic key;
@dynamic createdAt;

@end
