//
//  Track+CoreDataProperties.h
//  Muzone
//
//  Created by Sukhrob Khakimov on 10/16/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Track.h"

NS_ASSUME_NONNULL_BEGIN

@interface Track (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *title;
@property (nullable, nonatomic, retain) NSString *fileUrl;
@property (nullable, nonatomic, retain) NSString *localFileName;
@property (nullable, nonatomic, retain) NSString *imageUrl;
@property (nullable, nonatomic, retain) NSNumber *duration;
@property (nullable, nonatomic, retain) NSNumber *number;
@property (nullable, nonatomic, retain) NSNumber *year;
@property (nullable, nonatomic, retain) NSString *lyrics;
@property (nullable, nonatomic, retain) NSNumber *explicit;
@property (nullable, nonatomic, retain) NSNumber *state;
@property (nullable, nonatomic, retain) NSNumber *progress;
@property (nullable, nonatomic, retain) NSNumber *uid;
@property (nullable, nonatomic, retain) Album *album;
@property (nullable, nonatomic, retain) Artist *artist;
@property (nullable, nonatomic, retain) Genre *genre;

@end

NS_ASSUME_NONNULL_END
