//
//  Track+CoreDataProperties.m
//  Muzone
//
//  Created by Sukhrob Khakimov on 10/16/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Track+CoreDataProperties.h"

@implementation Track (CoreDataProperties)

@dynamic title;
@dynamic fileUrl;
@dynamic localFileName;
@dynamic imageUrl;
@dynamic duration;
@dynamic number;
@dynamic year;
@dynamic lyrics;
@dynamic explicit;
@dynamic state;
@dynamic progress;
@dynamic uid;
@dynamic album;
@dynamic artist;
@dynamic genre;

@end
