//
//  Track.m
//  Muzone
//
//  Created by Sukhrob Khakimov on 10/16/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import "TimeHelper.h"

#import "Album.h"
#import "Artist.h"
#import "Genre.h"
#import "Track.h"

@implementation Track

#pragma mark - Model

- (instancetype)initWithDictionary:(NSDictionary *)dictionary context:(NSManagedObjectContext *)context {
    NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass(self.class)
                                              inManagedObjectContext:context];
    self = [super initWithEntity:entity insertIntoManagedObjectContext:context];
    
    if (self) {
        [self setWithDictionary:dictionary];
        
        self.state = @(TrackStateNone);
        self.progress = @(0.0f);
    }
    
    return self;
}

- (void)setWithDictionary:(NSDictionary *)dictionary {
    if (![dictionary[@"id"] isEqual:[NSNull null]]) {
        self.uid = dictionary[@"id"];
    }
    
    if (![dictionary[@"title"] isEqual:[NSNull null]]) {
        self.title = dictionary[@"title"];
    }
    
    if (![dictionary[@"file_name"] isEqual:[NSNull null]]) {
        self.fileUrl = dictionary[@"file_name"];
    }
    
    if (![dictionary[@"image"] isEqual:[NSNull null]]) {
        self.imageUrl = dictionary[@"image"];
    }
    
    if (![dictionary[@"duration"] isEqual:[NSNull null]]) {
        self.duration = [TimeHelper totalSecondsForTimeString:dictionary[@"duration"]];
    }
    
    if (![dictionary[@"number"] isEqual:[NSNull null]]) {
        self.number = dictionary[@"number"];
    }
    
    if (![dictionary[@"year"] isEqual:[NSNull null]]) {
        self.year = dictionary[@"year"];
    }
    
    if (![dictionary[@"is_explicit"] isEqual:[NSNull null]]) {
        self.explicit = dictionary[@"is_explicit"];
    }
    
    if (![dictionary[@"lyrics"] isEqual:[NSNull null]]) {
        self.lyrics = dictionary[@"lyrics"];
    }
    
    if (![dictionary[@"album"] isEqual:[NSNull null]]) {
        self.album = [[Album alloc] initWithDictionary:dictionary[@"album"]
                                               context:self.managedObjectContext];
    }
    
    if (![dictionary[@"artist"] isEqual:[NSNull null]]) {
        self.artist = [[Artist alloc] initWithDictionary:dictionary[@"artist"]
                                                 context:self.managedObjectContext];
    }
    
    if (![dictionary[@"genre"] isEqual:[NSNull null]]) {
        self.genre = [[Genre alloc] initWithDictionary:dictionary[@"genre"]
                                               context:self.managedObjectContext];
    }
}

- (id)copyWithContext:(NSManagedObjectContext *)context {
    NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass(self.class)
                                              inManagedObjectContext:context];
    Track *track = [[Track alloc] initWithEntity:entity insertIntoManagedObjectContext:context];
    track.uid = [self.uid copy];
    track.title = [self.title copy];
    track.fileUrl = [self.fileUrl copy];
    track.localFileName = [self.localFileName copy];
    track.imageUrl = [self.imageUrl copy];
    track.duration = [self.duration copy];
    track.number = [self.number copy];
    track.year = [self.year copy];
    track.explicit = [self.explicit copy];
    track.state = [self.state copy];
    track.progress = [self.progress copy];
    track.album = [self.album copyWithContext:context];
    track.artist = [self.artist copyWithContext:context];
    track.genre = [self.genre copyWithContext:context];
    
    return track;
}

@end
