//
//  App.h
//  Muzone
//
//  Created by Sukhrob Khakimov on 7/14/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface App : NSObject <NSCoding>

@property (nonnull, nonatomic, copy) NSString *version;
@property (nonnull, nonatomic, copy) NSString *build;

@end
