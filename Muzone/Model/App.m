//
//  App.m
//  Muzone
//
//  Created by Sukhrob Khakimov on 7/14/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import "App.h"

@implementation App

#pragma mark - NSCoding

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    
    if (self) {
        self.version = [aDecoder decodeObjectForKey:@"version"];
        self.build = [aDecoder decodeObjectForKey:@"build"];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.version
                  forKey:@"version"];
    [aCoder encodeObject:self.build
                  forKey:@"build"];
}

@end
