//
//  Model.h
//  Muzone
//
//  Created by Sukhrob Khakimov on 6/6/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NSManagedObjectContext;

@protocol Model <NSObject>

- (nonnull instancetype)initWithDictionary:(nonnull NSDictionary *)dictionary context:(nonnull NSManagedObjectContext *)context;
- (void)setWithDictionary:(nonnull NSDictionary *)dictionary;

- (nonnull id)copyWithContext:(nonnull NSManagedObjectContext *)context;

@end
