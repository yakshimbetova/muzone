//
//  User.h
//  Muzone
//
//  Created by Macbook on 3/6/17.
//  Copyright © 2017 Muzone LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

+(User*)currentUser;

@property (nonatomic, copy) NSString* facebookToken;
@property (nonatomic, copy) NSString* accessToken;
@property (nonatomic, copy) NSString* firstName;
@property (nonatomic, copy) NSString* lastName;
@property (nonatomic, copy) NSString* email;
@property (nonatomic, copy) NSString* fbid;
@property (nonatomic, copy) NSString* idString;
@property (nonatomic, copy) NSString* profilePicUrl;
@property (nonatomic, copy) NSString* username;

-(void)logout;
-(void)showLoginForm;
@end
