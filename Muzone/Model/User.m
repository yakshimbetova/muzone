//
//  User.m
//  Muzone
//
//  Created by Macbook on 3/6/17.
//  Copyright © 2017 Muzone LLC. All rights reserved.
//

#import "User.h"
#import "LoginViewController.h"
#import "AppDelegate.h"

@implementation User

+(User *)currentUser
{
    static id sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[[self class] alloc] init];
    });
    return sharedInstance;
}
-(NSString *)facebookToken { return [self getString:@"facebookToken"]; }
-(NSString *)accessToken {return [self getString:@"accessToken"]; }
-(NSString *)firstName { return [self getString:@"firstName"]; }
-(NSString *)lastName { return [self getString:@"lastName"]; }

-(NSString *)email { return [self getString:@"email"]; }
-(NSString *)fbid { return [self getString:@"fbid"]; }
-(NSString *)idString { return [self getString:@"idString"]; }
-(NSString *)profilePicUrl { return [self getString:@"profilePicUrl"]; }
-(NSString *)username { return [self getString:@"username"]; }


-(void)setFacebookToken:(NSString *)facebookToken { [self setString:facebookToken key:@"facebookToken"];}
-(void)setAccessToken:(NSString *)accessToken { [self setString:accessToken key:@"accessToken"];}
-(void)setFirstName:(NSString *)firstName { [self setString:firstName key:@"firstName"];}
-(void)setLastName:(NSString *)lastName { [self setString:lastName key:@"lastName"];}

-(void)setEmail:(NSString *)email { [self setString:email key:@"email"];}
-(void)setFbid:(NSString *)fbid { [self setString:fbid key:@"fbid"]; }
-(void)setIdString:(NSString *)idString { [self setString:idString key:@"idString"]; }
-(void)setProfilePicUrl:(NSString *)profilePicUrl { [self setString:profilePicUrl key:@"profilePicUrl"]; }
-(void)setUsername:(NSString *)username { [self setString:username key:@"username"]; }


#pragma mark -

-(NSString*)getString:(NSString*)key {
    return [[NSUserDefaults standardUserDefaults] stringForKey:key];
}

-(void)setString:(NSString*)val key:(NSString*)key {
    [[NSUserDefaults standardUserDefaults] setObject:val forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)logout
{
    [User currentUser].facebookToken = nil;
    [User currentUser].accessToken = nil;
    [User currentUser].firstName = nil;
    [User currentUser].lastName = nil;
}

-(void)showLoginForm
{
    LoginViewController *vc = [LoginViewController new];
    AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    if ([delegate.window.rootViewController presentedViewController]) {
        [delegate.window.rootViewController dismissViewControllerAnimated:NO completion:^{}];
    }
    [delegate.window.rootViewController presentViewController:vc animated:YES completion:^{}];
}

@end
