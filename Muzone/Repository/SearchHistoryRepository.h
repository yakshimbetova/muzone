//
//  SearchHistoryRepository.h
//  Muzone
//
//  Created by Sukhrob Khakimov on 6/22/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NSManagedObjectContext;
@class SearchHistory;

@interface SearchHistoryRepository : NSObject

@property (nonnull, nonatomic, strong, readonly) NSManagedObjectContext *managedObjectContext;

- (nonnull instancetype)initWithManagedObjectContext:(nonnull NSManagedObjectContext *)managedObjectContext;
- (nullable NSArray<SearchHistory *> *)fetchAllByKey:(nullable NSString *)key offset:(NSUInteger)offset limit:(NSUInteger)limit;
- (nullable SearchHistory *)fetchOneByKey:(nonnull NSString *)key;

@end
