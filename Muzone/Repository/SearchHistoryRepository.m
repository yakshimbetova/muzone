//
//  SearchHistoryRepository.m
//  Muzone
//
//  Created by Sukhrob Khakimov on 6/22/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import <CoreData/CoreData.h>

#import "SearchHistoryRepository.h"
#import "SearchHistory.h"

@implementation SearchHistoryRepository

- (instancetype)initWithManagedObjectContext:(NSManagedObjectContext *)managedObjectContext {
    self = [super init];
    
    if (self) {
        _managedObjectContext = managedObjectContext;
    }
    
    return self;
}

- (NSArray<SearchHistory *> *)fetchAllByKey:(NSString *)key offset:(NSUInteger)offset limit:(NSUInteger)limit {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    fetchRequest.entity = [NSEntityDescription entityForName:NSStringFromClass([SearchHistory class])
                                      inManagedObjectContext:_managedObjectContext];
    fetchRequest.fetchOffset = offset;
    fetchRequest.fetchLimit = limit;
    
    if (key.length > 0) {
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"key CONTAINS[cd] %@", key];
    } else {
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"createdAt"
                                                                       ascending:NO];
        fetchRequest.sortDescriptors = @[sortDescriptor];
    }
    
    return [_managedObjectContext executeFetchRequest:fetchRequest
                                                error:nil];
}

- (SearchHistory *)fetchOneByKey:(NSString *)key {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    fetchRequest.entity = [NSEntityDescription entityForName:NSStringFromClass([SearchHistory class])
                                      inManagedObjectContext:_managedObjectContext];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"key ==[c] %@", key];
    
    NSArray *result = [_managedObjectContext executeFetchRequest:fetchRequest
                                                           error:nil];
    
    if (result.count > 0) {
        return result[0];
    }
    
    return nil;
}

@end
