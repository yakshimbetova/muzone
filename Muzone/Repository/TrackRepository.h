//
//  TrackRepository.h
//  Muzone
//
//  Created by Sukhrob Khakimov on 10/17/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NSManagedObjectContext;
@class Track;

@interface TrackRepository : NSObject

@property (nonnull, nonatomic, strong, readonly) NSManagedObjectContext *managedObjectContext;

- (nonnull instancetype)initWithManagedObjectContext:(nonnull NSManagedObjectContext *)managedObjectContext;
- (nullable NSArray<Track *> *)fetchAllWithText:(nullable NSString *)text offset:(NSUInteger)offset limit:(NSUInteger)limit;
- (nullable NSArray<Track *> *)fetchAllTracks;
- (nullable Track *)fetchOneByUid:(nonnull NSNumber *)uid;
@end
