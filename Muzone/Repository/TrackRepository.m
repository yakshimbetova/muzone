//
//  TrackRepository.m
//  Muzone
//
//  Created by Sukhrob Khakimov on 10/17/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import <CoreData/CoreData.h>

#import "TrackRepository.h"
#import "Track.h"

@implementation TrackRepository

- (instancetype)initWithManagedObjectContext:(NSManagedObjectContext *)managedObjectContext {
    self = [super init];
    
    if (self) {
        _managedObjectContext = managedObjectContext;
    }
    
    return self;
}

- (NSArray<Track *> *)fetchAllWithText:(NSString *)text offset:(NSUInteger)offset limit:(NSUInteger)limit {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    fetchRequest.entity = [NSEntityDescription entityForName:NSStringFromClass([Track class])
                                      inManagedObjectContext:_managedObjectContext];
    fetchRequest.fetchOffset = offset;
    fetchRequest.fetchLimit = limit;
    
    if (text.length > 0) {
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"title CONTAINS[cd] %@ OR album.artist.name CONTAINS[cd] %@", text, text];
    }
    NSSortDescriptor *titleSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"title"
                                                                        ascending:YES];
    NSSortDescriptor *artistNameSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"album.artist.name"
                                                                             ascending:YES];
    fetchRequest.sortDescriptors = @[titleSortDescriptor, artistNameSortDescriptor];
    
    return [_managedObjectContext executeFetchRequest:fetchRequest
                                                error:nil];
}

- (NSArray<Track *> *)fetchAllTracks {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    fetchRequest.entity = [NSEntityDescription entityForName:NSStringFromClass([Track class])
                                      inManagedObjectContext:_managedObjectContext];
    return [_managedObjectContext executeFetchRequest:fetchRequest
                                                error:nil];
}


- (Track *)fetchOneByUid:(NSNumber *)uid {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    fetchRequest.entity = [NSEntityDescription entityForName:NSStringFromClass([Track class])
                                      inManagedObjectContext:_managedObjectContext];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"uid == %@", uid];
    
    NSArray *result = [_managedObjectContext executeFetchRequest:fetchRequest
                                                           error:nil];
    
    if (result.count > 0) {
        return result.lastObject;
    }
    
    return nil;
}


@end
