//
//  ApiClient.h
//  Muzone
//
//  Created by Sukhrob Khakimov on 5/26/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPSessionManager.h"

@interface ApiClient : AFHTTPSessionManager

+ (nonnull ApiClient *)sharedClient;

#pragma mark - Album

- (nullable NSURLSessionDataTask *)albumsWithParameters:(nullable id)parameters
                                                   type:(nullable NSString *)type
                                                success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                                failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure;

- (nullable NSURLSessionDataTask *)albumWithParameters:(nullable id)parameters
                                               success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                               failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure;

#pragma mark - Artist

- (nullable NSURLSessionDataTask *)artistsWithParameters:(nullable id)parameters
                                                 success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                                 failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure;

- (nullable NSURLSessionDataTask *)artistWithParameters:(nullable id)parameters
                                                success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                                failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure;

#pragma mark - Genre

- (nullable NSURLSessionDataTask *)genresWithParameters:(nullable id)parameters
                                                   type:(nullable NSString *)type
                                                success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                                failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure;

- (nullable NSURLSessionDataTask *)genreWithParameters:(nullable id)parameters
                                               success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                               failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure;

#pragma mark - Track

- (nullable NSURLSessionDataTask *)tracksWithParameters:(nullable id)parameters
                                                   type:(nullable NSString *)type
                                                success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                                failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure;

- (nullable NSURLSessionDataTask *)trackWithParameters:(nullable id)parameters
                                               success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                               failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure;

#pragma mark - Playlist

- (nullable NSURLSessionDataTask *)myPlaylistsWithParameters:(nullable id)parameters
                                                     success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                                     failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure;

- (nullable NSURLSessionDataTask *)followingPlaylistsWithParameters:(nullable id)parameters
                                                            success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                                            failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure;

- (nullable NSURLSessionDataTask *)publicPlaylistsWithParameters:(nullable id)parameters
                                                         success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                                         failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure;

#pragma mark - Search

- (nullable NSURLSessionDataTask *)searchWithParameters:(nullable id)parameters
                                                   type:(nullable NSString *)type
                                                success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                                failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure;

#pragma mark - Login

- (nullable NSURLSessionDataTask *)loginWithParameters:(nullable id)parameters
                                               success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                               failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure;

- (nullable NSURLSessionDataTask *)createAccountWithParameters:(nullable id)parameters
                                                       success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                                       failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure;

#pragma mark - Favorite

//FavoriteTracks

- (nullable NSURLSessionDataTask *)addFavoriteTrackWithParameters:(nullable id)parameters
                                                          success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                                          failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure;

- (nullable NSURLSessionDataTask *)deleteFavoriteTrackWithParameters:(nullable id)parameters
                                                             success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                                             failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure;

- (nullable NSURLSessionDataTask *)getFavoriteTracksWithParameters:(nullable id)parameters
                                                           success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                                           failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure;

//FavoriteAlbums

- (nullable NSURLSessionDataTask *)addFavoriteAlbumWithParameters:(nullable id)parameters
                                                          success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                                          failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure;

- (nullable NSURLSessionDataTask *)deleteFavoriteAlbumWithParameters:(nullable id)parameters
                                                             success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                                             failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure;

- (nullable NSURLSessionDataTask *)getFavoriteAlbumsWithParameters:(nullable id)parameters
                                                           success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                                           failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure;

//FavoriteArtists

- (nullable NSURLSessionDataTask *)addFavoriteArtistWithParameters:(nullable id)parameters
                                                           success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                                           failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure;

- (nullable NSURLSessionDataTask *)deleteFavoriteArtistWithParameters:(nullable id)parameters
                                                              success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                                              failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure;

- (nullable NSURLSessionDataTask *)getFavoriteArtistssWithParameters:(nullable id)parameters
                                                             success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                                             failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure;




#pragma mark - Playlist

//create playlist

- (nullable NSURLSessionDataTask *)createPlaylistWithParameters:(nullable id)parameters
                                                        success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                                        failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure;


// rename playlist

- (nullable NSURLSessionDataTask *)renamePlaylistWithParameters:(nullable id)parameters
                                                        success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                                        failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure;


// delete playlist

- (nullable NSURLSessionDataTask *)deletePlaylistWithParameters:(nullable id)parameters
                                                        success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                                        failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure;
// add track to playlist

- (nullable NSURLSessionDataTask *)addTrackToPlaylistWithParameters:(nullable id)parameters
                                                            success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                                            failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure;

// delete track from playlist

- (nullable NSURLSessionDataTask *)deleteTrackFromPlaylistWithParameters:(nullable id)parameters
                                                                 success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                                                 failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure;


// get all playlists

- (nullable NSURLSessionDataTask *)getAllPlaylistWithParameters:(nullable id)parameters
                                                        success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                                        failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure;

//get all tracks in playlist

- (nullable NSURLSessionDataTask *)getAllTracksInPlaylistWithParameters:(nullable id)parameters
                                                                success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                                                failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure;

@end
