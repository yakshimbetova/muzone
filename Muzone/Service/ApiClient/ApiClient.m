//
//  ApiClient.m
//  Muzone
//
//  Created by Sukhrob Khakimov on 5/26/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import "ApiClient.h"
#import "Constants.h"
#import "User.h"

@implementation ApiClient

+ (ApiClient *)sharedClient {
    static ApiClient *sharedClient;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedClient = [[ApiClient alloc] initWithBaseURL:[NSURL URLWithString:kApiBaseUrlString]];
        sharedClient.requestSerializer = [AFHTTPRequestSerializer serializer];
        sharedClient.responseSerializer = [AFJSONResponseSerializer serializer];

        NSString *accessToken = [User currentUser].accessToken;

        if (accessToken != nil) {
            [sharedClient.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", accessToken]
                                  forHTTPHeaderField:@"Authorization"];
        }
    });
    
    return sharedClient;
}

- (NSString *)basePath {
    return @"api";
}

#pragma mark - Album

- (NSURLSessionDataTask *)albumsWithParameters:(id)parameters
                                          type:(NSString *)type
                                       success:(void (^)(NSURLSessionDataTask * _Nullable, id _Nullable))success
                                       failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nullable))failure {
    NSString *urlString = [NSString stringWithFormat:@"%@/albums/%@", [self basePath], type];
    
    return [self GET:urlString parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(task, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

- (NSURLSessionDataTask *)albumWithParameters:(id)parameters
                                      success:(void (^)(NSURLSessionDataTask * _Nullable, id _Nullable))success
                                      failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nullable))failure {
    NSString *urlString = [NSString stringWithFormat:@"%@/albums/details", [self basePath]];
    
    return [self GET:urlString parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(task, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

#pragma mark - Artist

- (NSURLSessionDataTask *)artistsWithParameters:(id)parameters
                                        success:(void (^)(NSURLSessionDataTask * _Nullable, id _Nullable))success
                                        failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nullable))failure {
    NSString *type;
    
    if ([parameters containsObject:@"l"]) {
        type = @"startingwith";
    } else {
        type = @"all";
    }
    
    NSString *urlString = [NSString stringWithFormat:@"%@/artists/%@", [self basePath], type];
    
    return [self GET:urlString parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(task, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

- (NSURLSessionDataTask *)artistWithParameters:(id)parameters
                                       success:(void (^)(NSURLSessionDataTask * _Nullable, id _Nullable))success
                                       failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nullable))failure {
    NSString *urlString = [NSString stringWithFormat:@"%@/artists/details", [self basePath]];
    
    return [self GET:urlString parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(task, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

#pragma mark - Genre

- (NSURLSessionDataTask *)genresWithParameters:(id)parameters
                                          type:(NSString *)type
                                       success:(void (^)(NSURLSessionDataTask * _Nullable, id _Nullable))success
                                       failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nullable))failure {
    NSString *urlString = [NSString stringWithFormat:@"%@/genres/%@", [self basePath], type];
    
    return [self GET:urlString parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(task, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

- (NSURLSessionDataTask *)genreWithParameters:(id)parameters
                                      success:(void (^)(NSURLSessionDataTask * _Nullable, id _Nullable))success
                                      failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nullable))failure {
    NSString *urlString = [NSString stringWithFormat:@"%@/genres/details", [self basePath]];
    
    return [self GET:urlString parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(task, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

#pragma mark - Track

- (NSURLSessionDataTask *)tracksWithParameters:(id)parameters
                                          type:(NSString *)type
                                       success:(void (^)(NSURLSessionDataTask * _Nullable, id _Nullable))success
                                       failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nullable))failure {
    NSString *urlString = [NSString stringWithFormat:@"%@/tracks/%@", [self basePath], type];
    
    return [self GET:urlString parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(task, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

- (NSURLSessionDataTask *)trackWithParameters:(id)parameters
                                      success:(void (^)(NSURLSessionDataTask * _Nullable, id _Nullable))success
                                      failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nullable))failure {
    NSString *urlString = [NSString stringWithFormat:@"%@/tracks/details", [self basePath]];
    
    return [self GET:urlString parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(task, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

#pragma mark - Playlist

- (NSURLSessionDataTask *)myPlaylistsWithParameters:(id)parameters
                                            success:(void (^)(NSURLSessionDataTask * _Nullable, id _Nullable))success
                                            failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nullable))failure {
    NSString *urlString = [NSString stringWithFormat:@"%@/playlists/getallowned", [self basePath]];

    return [self GET:urlString parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(task, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

- (NSURLSessionDataTask *)followingPlaylistsWithParameters:(id)parameters
                                                   success:(void (^)(NSURLSessionDataTask * _Nullable, id _Nullable))success
                                                   failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nullable))failure {
    NSString *urlString = [NSString stringWithFormat:@"%@/playlists/getfollowing", [self basePath]];

    return [self GET:urlString parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(task, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

- (NSURLSessionDataTask *)publicPlaylistsWithParameters:(id)parameters
                                                success:(void (^)(NSURLSessionDataTask * _Nullable, id _Nullable))success
                                                failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nullable))failure {
    NSString *urlString = [NSString stringWithFormat:@"%@/playlists/getallpublic", [self basePath]];

    return [self GET:urlString parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(task, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

#pragma mark - Search

- (NSURLSessionDataTask *)searchWithParameters:(id)parameters
                                          type:(NSString *)type
                                       success:(void (^)(NSURLSessionDataTask * _Nullable, id _Nullable))success
                                       failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nullable))failure {
    NSString *urlString = [NSString stringWithFormat:@"%@/search/%@", [self basePath], type];
    
    return [self GET:urlString parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(task, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

#pragma mark - Login



- (nullable NSURLSessionDataTask *)loginWithParameters:(nullable id)parameters
                                               success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                               failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure
{
    NSString *urlString = [NSString stringWithFormat:@"%@/accounts/FacebookAuthenticate", [self basePath]];
    return [self POST:urlString parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(task, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

- (nullable NSURLSessionDataTask *)createAccountWithParameters:(nullable id)parameters
                                                       success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                                       failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure
{
    NSString *urlString = [NSString stringWithFormat:@"%@/accounts/create", [self basePath]];

    return [self POST:urlString parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(task, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

#pragma mark - Favorite

//FavoriteTracks

- (nullable NSURLSessionDataTask *)addFavoriteTrackWithParameters:(nullable id)parameters
                                                          success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                                          failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure
{
    NSString *urlString = [NSString stringWithFormat:@"%@/favorites/addtrack", [self basePath]];

    return [self PUT:urlString parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(task, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

- (nullable NSURLSessionDataTask *)deleteFavoriteTrackWithParameters:(nullable id)parameters
                                                             success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                                             failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure
{
    NSString *urlString = [NSString stringWithFormat:@"%@/favorites/deletetrack", [self basePath]];
    self.requestSerializer.HTTPMethodsEncodingParametersInURI = [NSSet setWithObjects:@"GET", @"HEAD", nil];
    return [self DELETE:urlString parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(task, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

- (nullable NSURLSessionDataTask *)getFavoriteTracksWithParameters:(nullable id)parameters
                                                           success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                                           failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure
{
    NSString *urlString = [NSString stringWithFormat:@"%@/favorites/gettracks", [self basePath]];

    return [self GET:urlString parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(task, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

//FavoriteAlbums

- (nullable NSURLSessionDataTask *)addFavoriteAlbumWithParameters:(nullable id)parameters
                                                          success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                                          failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure
{
    NSString *urlString = [NSString stringWithFormat:@"%@/favorites/addalbum", [self basePath]];

    return [self PUT:urlString parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(task, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];

}

- (nullable NSURLSessionDataTask *)deleteFavoriteAlbumWithParameters:(nullable id)parameters
                                                             success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                                             failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure
{
    NSString *urlString = [NSString stringWithFormat:@"%@/favorites/deletealbum", [self basePath]];
    self.requestSerializer.HTTPMethodsEncodingParametersInURI = [NSSet setWithObjects:@"GET", @"HEAD", nil];

    return [self DELETE:urlString parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(task, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

- (nullable NSURLSessionDataTask *)getFavoriteAlbumsWithParameters:(nullable id)parameters
                                                           success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                                           failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure
{
    NSString *urlString = [NSString stringWithFormat:@"%@/favorites/getalbums", [self basePath]];

    return [self GET:urlString parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(task, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];

}

//FavoriteArtists

- (nullable NSURLSessionDataTask *)addFavoriteArtistWithParameters:(nullable id)parameters
                                                           success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                                           failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure
{
    NSString *urlString = [NSString stringWithFormat:@"%@/favorites/addartist", [self basePath]];

    return [self PUT:urlString parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(task, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];

}

- (nullable NSURLSessionDataTask *)deleteFavoriteArtistWithParameters:(nullable id)parameters
                                                              success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                                              failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure
{
    NSString *urlString = [NSString stringWithFormat:@"%@/favorites/deleteartist", [self basePath]];
    self.requestSerializer.HTTPMethodsEncodingParametersInURI = [NSSet setWithObjects:@"GET", @"HEAD", nil];

    return [self DELETE:urlString parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(task, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

- (nullable NSURLSessionDataTask *)getFavoriteArtistssWithParameters:(nullable id)parameters
                                                             success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                                             failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure
{
    NSString *urlString = [NSString stringWithFormat:@"%@/favorites/getartists", [self basePath]];

    return [self GET:urlString parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(task, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}


#pragma mark - Playlist

//create playlist

- (nullable NSURLSessionDataTask *)createPlaylistWithParameters:(nullable id)parameters
                                                        success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                                        failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure

{
    NSString *urlString = [NSString stringWithFormat:@"%@/playlists/create", [self basePath]];

    return [self PUT:urlString parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(task, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];

}

// rename playlist

- (nullable NSURLSessionDataTask *)renamePlaylistWithParameters:(nullable id)parameters
                                                        success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                                        failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure

{
    NSString *urlString = [NSString stringWithFormat:@"%@/playlists/rename", [self basePath]];

    return [self PUT:urlString parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(task, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

// delete playlist

- (nullable NSURLSessionDataTask *)deletePlaylistWithParameters:(nullable id)parameters
                                                        success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                                        failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure
{
    NSString *urlString = [NSString stringWithFormat:@"%@/playlists/delete", [self basePath]];
    self.requestSerializer.HTTPMethodsEncodingParametersInURI = [NSSet setWithObjects:@"GET", @"HEAD", nil];

    return [self DELETE:urlString parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(task, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

// add track to playlist

- (nullable NSURLSessionDataTask *)addTrackToPlaylistWithParameters:(nullable id)parameters
                                                            success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                                            failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure
{
    NSString *urlString = [NSString stringWithFormat:@"%@/playlists/addtrack", [self basePath]];

    return [self PUT:urlString parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(task, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];

}

// delete track from playlist

- (nullable NSURLSessionDataTask *)deleteTrackFromPlaylistWithParameters:(nullable id)parameters
                                                                 success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                                                 failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure
{
    NSString *urlString = [NSString stringWithFormat:@"%@/playlists/deletetrack", [self basePath]];

    self.requestSerializer.HTTPMethodsEncodingParametersInURI = [NSSet setWithObjects:@"GET", @"HEAD", nil];

    return [self DELETE:urlString parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(task, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

// get all playlists

- (nullable NSURLSessionDataTask *)getAllPlaylistWithParameters:(nullable id)parameters
                                                        success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                                        failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure
{
    NSString *urlString = [NSString stringWithFormat:@"%@/playlists/getallowned", [self basePath]];

    return [self GET:urlString parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(task, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];

}

// get tracks in playlist

- (nullable NSURLSessionDataTask *)getAllTracksInPlaylistWithParameters:(nullable id)parameters
                                                                success:(nullable void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                                                failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure
{
    NSString *urlString = [NSString stringWithFormat:@"%@/playlists/gettracks", [self basePath]];

    return [self GET:urlString parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(task, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
    
}

@end
