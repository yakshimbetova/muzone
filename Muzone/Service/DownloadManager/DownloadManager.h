//
//  DownloadManager.h
//  Muzone
//
//  Created by Sukhrob Khakimov on 10/15/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *__nonnull const DownloadManagerProcessWillStart;
extern NSString *__nonnull const DownloadManagerProcessInProgress;
extern NSString *__nonnull const DownloadManagerProcessDidEnd;

extern NSString *__nonnull const DownloadManagerTargetPathKey;
extern NSString *__nonnull const DownloadManagerFileUrlKey;
extern NSString *__nonnull const DownloadManagerLocalFileNameKey;
extern NSString *__nonnull const DownloadManagerProgressKey;
extern NSString *__nonnull const DownloadManagerErrorKey;

@class AFHTTPSessionManager;

@interface DownloadManager : NSObject

- (nonnull instancetype)initWithClient:(nonnull AFHTTPSessionManager *)client;

@property (nonatomic, copy) void (^ _Nullable savedCompletionHandler)();

- (void)downloadFileWithUrl:(nonnull NSURL *)fileUrl toTargetPath:(nonnull NSURL *)path;
- (void)cancelFileWithUrl:(nonnull NSURL *)fileUrl;
- (void)cancelAll;

@end
