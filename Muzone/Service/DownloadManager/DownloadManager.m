//
//  DownloadManager.m
//  Muzone
//
//  Created by Sukhrob Khakimov on 10/15/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import "AFHTTPSessionManager.h"
#import "DownloadManager.h"

NSString *const DownloadManagerProcessWillStart = @"DownloadManagerProcessWillStart";
NSString *const DownloadManagerProcessInProgress = @"DownloadManagerProcessInProgress";
NSString *const DownloadManagerProcessDidEnd = @"DownloadManagerProcessDidEnd";

NSString *const DownloadManagerTargetPathKey = @"DownloadManagerTargetPathKey";
NSString *const DownloadManagerFileUrlKey = @"DownloadManagerFileUrlKey";
NSString *const DownloadManagerLocalFileNameKey = @"DownloadManagerLocalFileNameKey";
NSString *const DownloadManagerProgressKey = @"DownloadManagerProgressKey";
NSString *const DownloadManagerErrorKey = @"DownloadManagerErrorKey";

@interface DownloadManager ()<NSURLSessionDownloadDelegate, NSURLSessionDelegate>

@property (nonnull, nonatomic, strong) AFHTTPSessionManager *client;
@property (nonnull, nonatomic, strong) NSMutableDictionary *tasks;

@end

@implementation DownloadManager

- (instancetype)initWithClient:(AFHTTPSessionManager *)client {
    self = [super init];
    
    if (self) {
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:@"uz.muzone.backgroundDownloadSession"];
        self.client = [[AFHTTPSessionManager alloc] initWithBaseURL:client.baseURL sessionConfiguration:configuration];
        self.client.requestSerializer = client.requestSerializer;
        self.client.responseSerializer = client.responseSerializer;
    }
    
    return self;
}

- (void)downloadFileWithUrl:(NSURL *)fileUrl toTargetPath:(NSURL *)path {
    if (!self.tasks[fileUrl]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:DownloadManagerProcessWillStart
                                                            object:self
                                                          userInfo:@{DownloadManagerTargetPathKey: path,
                                                                     DownloadManagerFileUrlKey: fileUrl}];
        
        NSURLSessionDownloadTask *task = [self.client downloadTaskWithRequest:[NSURLRequest requestWithURL:fileUrl] progress:^(NSProgress * _Nonnull downloadProgress) {
            [[NSNotificationCenter defaultCenter] postNotificationName:DownloadManagerProcessInProgress
                                                                object:self
                                                              userInfo:@{DownloadManagerTargetPathKey: path,
                                                                         DownloadManagerFileUrlKey: fileUrl,
                                                                         DownloadManagerProgressKey: @(downloadProgress.fractionCompleted)}];
        } destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response) {
            return [path URLByAppendingPathComponent:response.suggestedFilename];
        } completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nullable filePath, NSError * _Nullable error) {
            NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithDictionary:@{DownloadManagerTargetPathKey: path,
                                                                                            DownloadManagerFileUrlKey: fileUrl}];
            
            if (self.savedCompletionHandler) {
                self.savedCompletionHandler();
                self.savedCompletionHandler = nil;
            }

            if (error) {
                userInfo[DownloadManagerErrorKey] = error;
            } else {
                userInfo[DownloadManagerLocalFileNameKey] = response.suggestedFilename;
            }
            
            [self.tasks removeObjectForKey:fileUrl];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:DownloadManagerProcessDidEnd
                                                                object:self
                                                              userInfo:userInfo];
        }];
        
        self.tasks[fileUrl] = task;
        [task resume];
    }
}

- (void)cancelFileWithUrl:(NSURL *)fileUrl {
    NSURLSessionDownloadTask *task = self.tasks[fileUrl];
    [task cancel];
}

- (void)cancelAll {
    NSArray *tasksCopy = [self.tasks copy];
    
    for (NSURL *fileUrl in tasksCopy) {
        NSURLSessionDownloadTask *task = self.tasks[fileUrl];
        [task cancel];
    }
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location {
    NSLog(@"%s: %@", __FUNCTION__, downloadTask.originalRequest.URL.lastPathComponent);
    
    NSError *error;
    NSURL *documents = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:false error:&error];
    NSAssert(!error, @"Docs failed %@", error);
    
    NSURL *localPath = [documents URLByAppendingPathComponent:downloadTask.originalRequest.URL.lastPathComponent];
    if (![[NSFileManager defaultManager] moveItemAtURL:location toURL:localPath error:&error]) {
        NSLog(@"move failed: %@", error);
    }
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error {
    NSLog(@"%s: %@ %@", __FUNCTION__, error, task.originalRequest.URL.lastPathComponent);
}

- (void)URLSessionDidFinishEventsForBackgroundURLSession:(NSURLSession *)session {
    NSLog(@"%s", __FUNCTION__);
        
    if (self.savedCompletionHandler) {
        self.savedCompletionHandler();
        self.savedCompletionHandler = nil;
    }
}

#pragma mark - NSMutableDictionary

- (NSMutableDictionary *)tasks {
    if (_tasks == nil) {
        _tasks = [NSMutableDictionary new];
    }
    
    return _tasks;
}


- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


@end
