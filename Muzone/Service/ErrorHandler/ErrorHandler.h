//
//  ErrorHandler.h
//  Muzone
//
//  Created by Sukhrob Khakimov on 6/8/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const ErrorHandlerWillHandleError;
extern NSString *const ErrorHandlerDidHandleError;
extern NSString *const ErrorHandlerErrorKey;

@interface ErrorHandler : NSObject

+ (NSError *)errorWithUserInfo:(NSDictionary *)userInfo;
+ (NSError *)errorWithCode:(NSInteger)code userInfo:(NSDictionary *)userInfo;
+ (NSString *)messageFromError:(NSError *)error;
+ (void)handleError:(NSError *)error;
+ (void)didHandleError;

@end
