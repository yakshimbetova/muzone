//
//  ErrorHandler.m
//  Muzone
//
//  Created by Sukhrob Khakimov on 6/8/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import "ErrorHandler.h"

NSString *const ErrorHandlerWillHandleError = @"ErrorHandlerWillHandleError";
NSString *const ErrorHandlerDidHandleError = @"ErrorHandlerDidHandleError";
NSString *const ErrorHandlerErrorKey = @"ErrorHandlerErrorKey";

@implementation ErrorHandler

+ (NSError *)errorWithUserInfo:(NSDictionary *)userInfo {
    return [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
                               code:0
                           userInfo:userInfo];
}

+ (NSError *)errorWithCode:(NSInteger)code userInfo:(NSDictionary *)userInfo {
    return [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
                               code:code
                           userInfo:userInfo];
}

+ (NSString *)messageFromError:(NSError *)error {
    return error.userInfo[NSLocalizedDescriptionKey];
}

+ (void)handleError:(NSError *)error {
    [[NSNotificationCenter defaultCenter] postNotificationName:ErrorHandlerWillHandleError
                                                        object:nil
                                                      userInfo:@{ErrorHandlerErrorKey: error}];
}

+ (void)didHandleError {
    [[NSNotificationCenter defaultCenter] postNotificationName:ErrorHandlerDidHandleError
                                                        object:nil
                                                      userInfo:nil];
}

@end
