//
//  String+Localization.swift
//  Muzone
//
//  Created by Sukhrob Khakimov on 10/21/17.
//  Copyright © 2017 Muzone LLC. All rights reserved.
//

import Foundation

private class Localizer {
    static let shared = Localizer()

    lazy var localizableDictionary: NSDictionary! = {
        if let path = Bundle.main.path(forResource: "Localizable", ofType: "plist") {
            return NSDictionary(contentsOfFile: path)
        }

        fatalError("The Localizable.plist file is not found")
    }()

    func localize(string: String) -> String {
        guard let dictionary = localizableDictionary.value(forKey: string) as? NSDictionary,
            let localizedString = dictionary.value(forKey: "value") as? String else {
                assertionFailure("Missing translation for \"\(string)\"")

                return ""
        }

        return localizedString
    }
}

extension String {
    var localized: String {
        return Localizer.shared.localize(string: self)
    }
}
