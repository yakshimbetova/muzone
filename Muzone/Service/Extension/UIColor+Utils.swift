//
//  UIColor+Utils.swift
//  Muzone
//
//  Created by Sukhrob Khakimov on 10/18/17.
//  Copyright © 2017 Muzone LLC. All rights reserved.
//

import UIKit

extension UIColor {
    static func rgb(value: CGFloat) -> UIColor {
        return rgb(
            red: value,
            green: value,
            blue: value,
            alpha: 1.0
        )
    }

    static func rgb(value: CGFloat, alpha: CGFloat) -> UIColor {
        return rgb(
            red: value,
            green: value,
            blue: value,
            alpha: alpha
        )
    }

    static func rgb(red: CGFloat, green: CGFloat, blue: CGFloat) -> UIColor {
        return rgb(
            red: red,
            green: green,
            blue: blue,
            alpha: 1.0
        )
    }

    static func rgb(red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) -> UIColor {
        return UIColor(
            red: red / 255.0,
            green: green / 255.0,
            blue: blue / 255.0,
            alpha: min(max(alpha, 0.0), 1.0)
        )
    }
}
