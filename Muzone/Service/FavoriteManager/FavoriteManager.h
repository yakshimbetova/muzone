//
//  FavoriteManager.h
//  Muzone
//
//  Created by Macbook on 4/9/17.
//  Copyright © 2017 Muzone LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Track;
@class Album;
@class Artist;
@class Genre;

extern NSString *__nonnull const FavoriteManagerUpdateTracksNotification;
extern NSString *__nonnull const FavoriteManagerUpdateArtistsNotification;
extern NSString *__nonnull const FavoriteManagerUpdateAlbumsNotification;
extern NSString *__nonnull const  UserSignInNotification;

@interface FavoriteManager : NSObject

@property (nonnull, nonatomic, readonly) NSMutableArray *genres;
@property (nonnull, nonatomic, readonly) NSMutableArray *tracks;
@property (nonnull, nonatomic, readonly) NSMutableArray *albums;
@property (nonnull, nonatomic, readonly) NSMutableArray *artists;

+ (nonnull FavoriteManager *)sharedManager;

- (void)getFavoriteAlbums:(nullable void (^)(BOOL success, NSArray* _Nullable result))complete;
- (void)getFavoriteTracks:(nullable void (^)(BOOL success, NSArray* _Nullable result))complete;
- (void)getFavoriteGenres:(nullable void (^)(BOOL success, NSArray* _Nullable result))complete;
- (void)getFavoriteArtists:(nullable void (^)(BOOL success, NSArray* _Nullable result))complete;

-(void)addToFavoriteTrack:(Track*_Nonnull)track complete:(nullable void (^)(BOOL success))complete;;
-(void)deleteFromFavoriteTrack:(Track*_Nonnull)track complete:(nullable void (^)(BOOL success))complete;

-(void)addToFavoriteAlbum:(Album*_Nonnull)album complete:(nullable void (^)(BOOL success))complete;
-(void)deleteFromFavoriteAlbum:(Album*_Nonnull)album complete:(nullable void (^)(BOOL success))complete;

-(void)addToFavoriteArtist:(Artist*_Nonnull)artist complete:(nullable void (^)(BOOL success))complete;
-(void)deleteFromFavoriteArtist:(Artist*_Nonnull)artist complete:(nullable void (^)(BOOL success))complete;

-(BOOL)isFavoriteTrack:(Track*_Nonnull)track;
-(BOOL)isFavoriteGenre:(Genre*_Nonnull)genre;
-(BOOL)isFavoriteAlbum:(Album*_Nonnull)album;
-(BOOL)isFavoriteArtist:(Artist*_Nonnull)artist;


//not ony for favorites
-(NSMutableArray*_Nonnull)getUniqueGenresFromTracks:(NSMutableArray*_Nonnull)tracks;
-(NSMutableArray*_Nonnull)getUniqueArtistsFromTracks:(NSMutableArray*_Nonnull)tracks;
-(NSMutableArray*_Nonnull)getUniqueAlbumsFromTracks:(NSMutableArray*_Nonnull)tracks;


@end
