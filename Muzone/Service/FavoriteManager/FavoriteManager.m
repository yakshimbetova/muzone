//
//  FavoriteManager.m
//  Muzone
//
//  Created by Macbook on 4/9/17.
//  Copyright © 2017 Muzone LLC. All rights reserved.
//

#import "FavoriteManager.h"
#import "ApiClient.h"
#import "Track.h"
#import "Genre.h"
#import "Album.h"
#import "Artist.h"

#import "TrackRepository.h"
#import "TrackManager.h"
#import "AppDelegate.h"

NSString * const FavoriteManagerUpdateTracksNotification = @"FavoriteManagerUpdateTracksNotification";
NSString * const FavoriteManagerUpdateArtistsNotification = @"FavoriteManagerUpdateArtistsNotification";
NSString * const FavoriteManagerUpdateAlbumsNotification = @"FavoriteManagerUpdateAlbumsNotification";
NSString * const UserSignInNotification = @"UserSignInNotification";

@interface FavoriteManager()
@property (nonatomic, retain) NSMutableArray *genres;
@property (nonatomic, retain) NSMutableArray *tracks;
@property (nonatomic, retain) NSMutableArray *albums;
@property (nonatomic, retain) NSMutableArray *artists;

@property (nonnull, nonatomic, strong) NSManagedObjectContext *mainManagedObjectContext;
@property (nonnull, nonatomic, strong) NSManagedObjectContext *workerManagedObjectContext;
@property (nonnull, nonatomic, strong) TrackRepository *trackRepository;
@property (nonnull, nonatomic, strong) TrackManager *trackManager;

@end

@implementation FavoriteManager

+ (FavoriteManager *)sharedManager {
    static FavoriteManager *sharedManager;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedManager = [[FavoriteManager alloc] initByDefault];
    });
    
    return sharedManager;
}

- (instancetype)initByDefault{
    self = [super init];
    
    if (self) {
        [self commonInit];
    }
    
    return self;
}
- (void)commonInit {
    self.albums  = [NSMutableArray new];
    self.genres  = [NSMutableArray new];
    self.artists  = [NSMutableArray new];
    self.tracks = [NSMutableArray new];
}

-(void)getFavoriteAlbums:(void (^)(BOOL, NSArray * _Nullable))complete
{
    ApiClient *client = [ApiClient sharedClient];
    [client getFavoriteAlbumsWithParameters:@{} success:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        NSArray *albums = (NSArray*)responseObject;
        NSMutableArray *savedAlbums = [NSMutableArray new];
        for (NSInteger index = 0; index < albums.count; index++) {
            NSDictionary *albumData = albums[index];
            Album *album = [[Album alloc] initWithDictionary:albumData
                                                     context:self.workerManagedObjectContext];
            if (album != nil) {
                [savedAlbums addObject:album];
            }
        }
        self.albums = savedAlbums;
        complete(YES, savedAlbums);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error) {
        complete(NO, @[]);
    }];

}

-(void)getFavoriteTracks:(void (^)(BOOL, NSArray * _Nullable))complete
{
    ApiClient *client = [ApiClient sharedClient];
    [client getFavoriteTracksWithParameters:@{} success:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        NSArray *tracks = (NSArray*)responseObject;
        NSMutableArray *savedTracks = [NSMutableArray new];
        for (NSInteger index = 0; index < tracks.count; index++) {
            NSDictionary *trackData = tracks[index];
            Track *track = [[Track alloc] initWithDictionary:trackData
                                                     context:self.workerManagedObjectContext];
            if (track != nil) {
                [savedTracks addObject:track];
            }
        }
        self.tracks = savedTracks;
        complete(YES, savedTracks);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error) {
        complete(NO, @[]);
    }];


}

-(void)getFavoriteGenres:(void (^)(BOOL, NSArray * _Nullable))complete
{
    ApiClient *client = [ApiClient sharedClient];
    [client getFavoriteTracksWithParameters:@{} success:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        NSArray *tracks = (NSArray*)responseObject;
        NSMutableArray *savedTracks = [NSMutableArray new];
        for (NSInteger index = 0; index < tracks.count; index++) {
            NSDictionary *trackData = tracks[index];
            Track *track = [[Track alloc] initWithDictionary:trackData
                                                     context:self.workerManagedObjectContext];
            if (track != nil) {
                [savedTracks addObject:track];
            }
        }
        self.genres = [self getUniqueGenresFromTracks:savedTracks];
        complete(YES, self.genres);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error) {
        complete(NO, @[]);
    }];
    
}

-(NSMutableArray*)getUniqueGenresFromTracks:(NSMutableArray*)tracks
{
    NSMutableArray *uniqueGenres = [NSMutableArray new];
    for (Track *track in tracks)
    {
        BOOL appeared = false;
        if (track.genre == nil) {
            continue;
        }
        for (Genre *unique in uniqueGenres ) {
            if (track.genre.uid == unique.uid) {
                appeared = true;
            }
        }
        if (appeared == false) {
            [uniqueGenres addObject:track.genre];
        }
    }
    return uniqueGenres;
}

-(NSMutableArray*)getUniqueArtistsFromTracks:(NSMutableArray*)tracks
{
    NSMutableArray *uniqueArtists = [NSMutableArray new];
    for (Track *track in tracks)
    {
        BOOL appeared = false;
        if (track.artist == nil) {
            continue;
        }
        for (Artist *unique in uniqueArtists ) {
            if (track.artist.uid == unique.uid) {
                appeared = true;
            }
        }
        if (appeared == false) {
            [uniqueArtists addObject:track.artist];
        }
    }
    return uniqueArtists;
}

-(NSMutableArray*)getUniqueAlbumsFromTracks:(NSMutableArray*)tracks
{
    NSMutableArray *uniqueAlbums = [NSMutableArray new];
    for (Track *track in tracks)
    {
        BOOL appeared = false;
        if (track.album == nil) {
            continue;
        }
        for (Album *unique in uniqueAlbums ) {
            if (track.album.uid == unique.uid) {
                appeared = true;
            }
        }
        if (appeared == false) {
            [uniqueAlbums addObject:track.album];
        }
    }
    return uniqueAlbums;
}



-(void)getFavoriteArtists:(void (^)(BOOL, NSArray * _Nullable))complete
{
    ApiClient *client = [ApiClient sharedClient];
    [client getFavoriteArtistssWithParameters:@{} success:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        NSArray *artists = (NSArray*)responseObject;
        NSMutableArray *savedArtists = [NSMutableArray new];
        for (NSInteger index = 0; index < artists.count; index++) {
            NSDictionary *artistData = artists[index];
            Artist *artist = [[Artist alloc] initWithDictionary:artistData
                                                        context:self.workerManagedObjectContext];
            if (artist != nil) {
                [savedArtists addObject:artist];
            }
        }
        self.artists = savedArtists;
        complete(YES, savedArtists);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error) {
        complete(NO, @[]);
    }];
    

}


-(void)addToFavoriteTrack:(Track*)track complete:(void (^)(BOOL))complete
{
    ApiClient *client = [ApiClient sharedClient];
    if (track.uid != nil || track.uid.integerValue > 0) {
        [client addFavoriteTrackWithParameters:@{@"id":track.uid} success:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
            [self.tracks addObject:track];
            [[NSNotificationCenter defaultCenter] postNotificationName:FavoriteManagerUpdateTracksNotification object:nil];
            complete(YES);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error) {
            complete(NO);
        }];
    }
    else
    {
        complete(NO);
    }
    
}


-(void)deleteFromFavoriteTrack:(Track*)track complete:(void (^)(BOOL))complete
{
    ApiClient *client = [ApiClient sharedClient];
    [client deleteFavoriteTrackWithParameters:@{@"id":track.uid} success:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        [self.tracks removeObject:track];
        [[NSNotificationCenter defaultCenter] postNotificationName:FavoriteManagerUpdateTracksNotification object:nil];
        complete(YES);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error) {
        complete(NO);
    }];

}



-(void)addToFavoriteAlbum:(Album*)album complete:(void (^)(BOOL))complete
{
    ApiClient *client = [ApiClient sharedClient];
    [client addFavoriteAlbumWithParameters:@{@"id":album.uid} success:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        [self.albums addObject:album];
        [[NSNotificationCenter defaultCenter] postNotificationName:FavoriteManagerUpdateAlbumsNotification object:nil];

        complete(YES);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error) {
        complete(NO);
    }];
}
     
-(void)deleteFromFavoriteAlbum:(Album*)album complete:(void (^)(BOOL))complete
{
    ApiClient *client = [ApiClient sharedClient];
    [client deleteFavoriteAlbumWithParameters:@{@"id":album.uid} success:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        [self.albums removeObject:album];
        [[NSNotificationCenter defaultCenter] postNotificationName:FavoriteManagerUpdateAlbumsNotification object:nil];
        
        complete(YES);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error) {
        complete(NO);
    }];

}

-(void)addToFavoriteArtist:(Artist*)artist complete:(void (^)(BOOL))complete
{
    ApiClient *client = [ApiClient sharedClient];
    [client addFavoriteArtistWithParameters:@{@"id":artist.uid} success:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        [self.artists addObject:artist];
        [[NSNotificationCenter defaultCenter] postNotificationName:FavoriteManagerUpdateArtistsNotification object:self];
        complete(YES);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error) {
        complete(NO);
    }];

}


-(void)deleteFromFavoriteArtist:(Artist*)artist complete:(void (^)(BOOL))complete
{
    ApiClient *client = [ApiClient sharedClient];
    [client deleteFavoriteArtistWithParameters:@{@"id":artist.uid} success:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        [self.artists removeObject:artist];
        [[NSNotificationCenter defaultCenter] postNotificationName:FavoriteManagerUpdateArtistsNotification object:self];
        complete(YES);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error) {
        complete(NO);
    }];

}


-(BOOL)isFavoriteTrack:(Track*)track
{
    for (Track *tr in self.tracks)
    {
        if (track.uid == tr.uid) {
            return YES;
        }
    }
    return NO;
}
-(BOOL)isFavoriteGenre:(Genre*)genre
{
    for (Genre *gr in self.genres)
    {
        if (genre.uid == gr.uid) {
            return YES;
        }
    }
    return NO;

}
-(BOOL)isFavoriteAlbum:(Album*)album
{
    for (Album *alb in self.albums)
    {
        if (album.uid == alb.uid) {
            return YES;
        }
    }
    return NO;

}
-(BOOL)isFavoriteArtist:(Artist*)artist
{
    for (Artist *ar in self.artists)
    {
        if (artist.uid == ar.uid) {
            return YES;
        }
    }
    return NO;

}

#pragma mark - CoreData

- (NSManagedObjectContext *)mainManagedObjectContext {
    if (_mainManagedObjectContext == nil) {
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        _mainManagedObjectContext = appDelegate.mainManagedObjectContext;
    }
    
    return _mainManagedObjectContext;
}

- (NSManagedObjectContext *)workerManagedObjectContext {
    if (_workerManagedObjectContext == nil) {
        _workerManagedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        _workerManagedObjectContext.parentContext = self.mainManagedObjectContext;
    }
    
    return _workerManagedObjectContext;
}

- (TrackRepository *)trackRepository {
    if (_trackRepository == nil) {
        _trackRepository = [[TrackRepository alloc] initWithManagedObjectContext:self.workerManagedObjectContext];
    }
    
    return _trackRepository;
}

- (TrackManager *)trackManager {
    if (_trackManager == nil) {
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        _trackManager = appDelegate.trackManager;
    }
    
    return _trackManager;
}


@end
