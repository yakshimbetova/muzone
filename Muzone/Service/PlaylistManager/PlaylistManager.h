//
//  PlaylistManager.h
//  Muzone
//
//  Created by Macbook on 4/29/17.
//  Copyright © 2017 Muzone LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Track.h"

#import "Muzone-Swift.h"

extern NSString *__nonnull const PlaylistManagerUpdatePlaylistsNotification;

@interface PlaylistManager : NSObject

+ (nonnull PlaylistManager *)sharedManager;

@property (nonnull, nonatomic, readonly) NSMutableArray *playlists;


- (void)getAllPlaylists:(nullable void (^)(BOOL success, NSArray* _Nullable result))complete;

- (void)createNewPlaylist:(NSString*_Nonnull)name complete:(nullable void (^)(BOOL success))complete;

- (void)addTrackToPlaylist:(Track*_Nonnull)track playlist:(Playlist*_Nonnull)playlist complete:(nullable void (^)(BOOL success))complete;

- (void)deleteTrackFromPlaylist:(Track*_Nonnull)track playlist:(Playlist*_Nonnull)playlist complete:(nullable void (^)(BOOL success))complete;

- (void)getAllTracksInPlaylist:(Playlist*_Nonnull)playlist complete:(nullable void (^)(BOOL success, NSArray * _Nullable result))complete;

- (void)deletePlaylist:(Playlist*_Nonnull)playlist complete:(nullable void (^)(BOOL success))complete;

- (void)renamePlaylist:(NSString*_Nonnull)title playlist:(Playlist*_Nonnull)playlist complete:(nullable void (^)(BOOL success))complete;

@end
