//
//  PlaylistManager.m
//  Muzone
//
//  Created by Macbook on 4/29/17.
//  Copyright © 2017 Muzone LLC. All rights reserved.
//

#import "PlaylistManager.h"
#import "ApiClient.h"
#import "AppDelegate.h"

#import "Muzone-Swift.h"

NSString * const PlaylistManagerUpdatePlaylistsNotification = @"PlaylistManagerUpdatePlaylistsNotification";

@interface PlaylistManager ()
@property (nonnull, nonatomic, strong) NSManagedObjectContext *mainManagedObjectContext;
@property (nonnull, nonatomic, strong) NSManagedObjectContext *workerManagedObjectContext;

@property (nonatomic, retain) NSMutableArray *playlists;

@end

@implementation PlaylistManager


+ (PlaylistManager *)sharedManager {
    static PlaylistManager *sharedManager;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedManager = [[PlaylistManager alloc] initByDefault];
    });
    
    return sharedManager;
}

- (instancetype)initByDefault{
    self = [super init];
    
    if (self) {
        [self commonInit];
    }
    
    return self;
}
- (void)commonInit {
    self.playlists  = [NSMutableArray new];
}

-(void)getAllPlaylists:(void (^)(BOOL, NSArray * _Nullable))complete
{
    ApiClient *client = [ApiClient sharedClient];
    [client getAllPlaylistWithParameters:@{} success:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        NSArray *playlists = (NSArray*)responseObject;
        NSMutableArray *savedPlaylists = [NSMutableArray new];
        for (NSInteger index = 0; index < playlists.count; index++) {
            NSDictionary *data = playlists[index];
            Playlist *playlist = [[Playlist alloc] initWithDictionary:data
                                                              context:self.workerManagedObjectContext];
            if (playlist != nil) {
                [savedPlaylists addObject:playlist];
            }
        }
        self.playlists = savedPlaylists;
        complete(YES, self.playlists);

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error) {
        complete(NO, @[]);
    }];
    
}

-(void)createNewPlaylist:(NSString*)name complete:(void (^)(BOOL))complete
{
    ApiClient *client = [ApiClient sharedClient];
    [client createPlaylistWithParameters:@{@"title":name} success:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        NSDictionary *playlistData = responseObject;
        Playlist *playlist = [[Playlist alloc] initWithDictionary:playlistData
                                                          context:self.workerManagedObjectContext];

        [self.playlists addObject:playlist];
        [[NSNotificationCenter defaultCenter] postNotificationName:PlaylistManagerUpdatePlaylistsNotification object:self];
        complete(YES);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error) {
        complete(NO);
    }];
}

- (void)addTrackToPlaylist:(Track*)track playlist:(Playlist*)playlist complete:(void (^)(BOOL))complete
{
    ApiClient *client = [ApiClient sharedClient];
    [client addTrackToPlaylistWithParameters:@{@"trackId" : track.uid, @"playlistId" : playlist.uid} success:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        complete(YES);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error) {
        complete(NO);
    }];
}

- (void)deleteTrackFromPlaylist:(Track*)track playlist:(Playlist*)playlist complete:(void (^)(BOOL))complete
{
    ApiClient *client = [ApiClient sharedClient];
    [client deleteTrackFromPlaylistWithParameters:@{@"trackId" : track.uid, @"playlistId" : playlist.uid} success:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        complete(YES);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error) {
        complete(NO);
    }];
}


-(void)getAllTracksInPlaylist:(Playlist *)playlist complete:(void (^)(BOOL, NSArray * _Nullable))complete
{
    ApiClient *client = [ApiClient sharedClient];
    [client getAllTracksInPlaylistWithParameters:@{@"playlistId":playlist.uid} success:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        NSArray *tracks = (NSArray*)responseObject;
        NSMutableArray *savedTracks = [NSMutableArray new];
        for (NSInteger index = 0; index < tracks.count; index++) {
            NSDictionary *data = tracks[index];
            Track *track = [[Track alloc] initWithDictionary:data
                                                     context:self.workerManagedObjectContext];
            if (track != nil) {
                [savedTracks addObject:track];
            }
        }
        complete(YES, savedTracks);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error) {
        complete(NO, @[]);
    }];
}

- (void)deletePlaylist:(Playlist*)playlist complete:(void (^)(BOOL))complete
{
    ApiClient *client = [ApiClient sharedClient];
    [client deletePlaylistWithParameters:@{@"id":playlist.uid} success:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        complete(YES);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error) {
        complete(NO);
    }];
}

- (void)renamePlaylist:(NSString*)title playlist:(Playlist*)playlist complete:(void (^)(BOOL success))complete
{
    ApiClient *client = [ApiClient sharedClient];
    [client renamePlaylistWithParameters:@{@"id" : playlist.uid, @"newTitle" : title} success:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        complete(YES);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error) {
        complete(NO);
    }];
}

#pragma mark - CoreData

- (NSManagedObjectContext *)mainManagedObjectContext {
    if (_mainManagedObjectContext == nil) {
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        _mainManagedObjectContext = appDelegate.mainManagedObjectContext;
    }
    
    return _mainManagedObjectContext;
}

- (NSManagedObjectContext *)workerManagedObjectContext {
    if (_workerManagedObjectContext == nil) {
        _workerManagedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        _workerManagedObjectContext.parentContext = self.mainManagedObjectContext;
    }
    
    return _workerManagedObjectContext;
}



@end
