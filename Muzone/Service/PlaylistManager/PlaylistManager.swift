//
//  PlaylistManager.swift
//  Muzone
//
//  Created by Sukhrob Khakimov on 10/30/17.
//  Copyright © 2017 Muzone LLC. All rights reserved.
//

import Foundation

public class PlaylistManager {
    public let client: ApiClient

    public init(client: ApiClient) {
        self.client = client
    }

    public func createPlaylist(_ playlist: Playlist, completionHandler: @escaping (_ error: Error?) -> Void) {
        let parameters: [String: Any] = [
            "title": playlist.title,
            "is_public": playlist.isPublic
        ]

        client.createPlaylist(withParameters: parameters, success: { (task, responseObject) in
            if let responseData = responseObject as? [String: Any] {
                playlist.setWith(responseData)
            }

            completionHandler(nil)
        }) { (task, error) in
            completionHandler(error)
        }
    }

    public func updatePlaylist(_ playlist: Playlist, completionHandler: @escaping (_ error: Error?) -> Void) {
        let parameters: [String: Any] = [
            "title": playlist.title,
            "is_public": playlist.isPublic
        ]

        client.renamePlaylist(withParameters: parameters, success: { (task, responseObject) in
            if let responseData = responseObject as? [String: Any] {
                playlist.setWith(responseData)
            }

            completionHandler(nil)
        }) { (task, error) in
            completionHandler(error)
        }
    }

    public func deletePlaylist(_ playlist: Playlist, completionHandler: @escaping (_ error: Error?) -> Void) {
        client.deletePlaylist(withParameters: nil, success: { (task, responseObject) in
            completionHandler(nil)
        }) { (task, error) in
            completionHandler(error)
        }
    }
}
