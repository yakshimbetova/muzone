//
//  SessionManager.h
//  Muzone
//
//  Created by Sukhrob Khakimov on 7/14/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SessionManager : NSObject

#pragma mark - SessionManager

+ (SessionManager *)sharedManager;
- (void)saveSession;
- (void)refreshSession;

#pragma mark - App

- (void)setApp:(id)app;
- (id)app;

#pragma mark - Preferences

- (void)setRepeatMode:(NSInteger)repeatMode;
- (NSInteger)repeatMode;

- (void)setShuffle:(BOOL)shuffle;
- (BOOL)shuffle;

@end
