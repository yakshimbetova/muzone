//
//  SessionManager.m
//  Muzone
//
//  Created by Sukhrob Khakimov on 7/14/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import "UICKeyChainStore.h"
#import "SessionManager.h"

static NSString *const kSessionKeysKey = @"MUZSessionKeysKey";

@interface SessionManager ()

@property (nonnull, nonatomic, strong) NSUserDefaults *session;
@property (nonnull, nonatomic, strong) NSMutableSet *sessionKeys;
@property (nonnull, nonatomic, strong) UICKeyChainStore *keyChainStore;

@end

@implementation SessionManager

#pragma mark - SessionManager

+ (SessionManager *)sharedManager {
    static SessionManager *sharedManager;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        [self createSaveSession];
        sharedManager = [[SessionManager alloc] init];
        sharedManager.session = [NSUserDefaults standardUserDefaults];
        sharedManager.sessionKeys = [NSMutableSet setWithArray:[sharedManager.session objectForKey:kSessionKeysKey]];
        sharedManager.keyChainStore = [UICKeyChainStore keyChainStore];
    });
    
    return sharedManager;
}

+(void)createSaveSession
{
    NSString* path = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    path = [path stringByAppendingPathComponent:@"Caches/com.apple.nsurlsessiond/Downloads"];
    
    
    NSString *appInfoPlist = [[NSBundle mainBundle] pathForResource:@"Info" ofType:@"plist"];
    NSDictionary *dictionary = [[NSDictionary alloc]initWithContentsOfFile:appInfoPlist];
    
    path = [path stringByAppendingPathComponent:dictionary[@"CFBundleIdentifier"]];
    [[NSFileManager defaultManager] setAttributes:@{NSFileProtectionKey: NSFileProtectionCompleteUntilFirstUserAuthentication} ofItemAtPath:path error:nil];
}

- (void)setSession:(id)value forKey:(NSString *)key {
    [self.session setObject:value
                     forKey:key];
    [self.sessionKeys addObject:key];
}

- (void)saveSession {
    [self.session setObject:[self.sessionKeys allObjects]
                     forKey:kSessionKeysKey];
    [self.session synchronize];
    [self refreshSession];
}

- (void)refreshSession {
    self.session = [NSUserDefaults standardUserDefaults];
    self.sessionKeys = [NSMutableSet setWithArray:[self.session objectForKey:kSessionKeysKey]];
}

#pragma mark - App

- (void)setApp:(id)app {
    [self.keyChainStore setData:[NSKeyedArchiver archivedDataWithRootObject:app]
                         forKey:@"app"];
}

- (id)app {
    NSData *data = [self.keyChainStore dataForKey:@"app"];
    
    if (data) {
        @try {
            return [NSKeyedUnarchiver unarchiveObjectWithData:data];
        } @catch (NSException *exception) {
            return nil;
        }
    }
    
    return nil;
}

#pragma mark - Preferences

- (void)setRepeatMode:(NSInteger)repeatMode {
    [self.keyChainStore setString:[NSString stringWithFormat:@"%@", @(repeatMode)]
                           forKey:@"repeat_mode"];
}

- (NSInteger)repeatMode {
    return [[self.keyChainStore stringForKey:@"repeat_mode"] integerValue];
}

- (void)setShuffle:(BOOL)shuffle {
    [self.keyChainStore setString:[NSString stringWithFormat:@"%@", @(shuffle)]
                           forKey:@"shuffle"];
}

- (BOOL)shuffle {
    return [[self.keyChainStore stringForKey:@"shuffle"] boolValue];
}

@end
