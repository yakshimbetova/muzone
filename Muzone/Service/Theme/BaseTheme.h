//
//  BaseTheme.h
//  Muzone
//
//  Created by Sukhrob Khakimov on 5/22/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import "Theme.h"

@interface BaseTheme : NSObject

+ (id <Theme>)currentTheme;

@end
