//
//  BaseTheme.m
//  Muzone
//
//  Created by Sukhrob Khakimov on 5/22/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import "BaseTheme.h"
#import "DefaultTheme.h"

@implementation BaseTheme

+ (id <Theme>)currentTheme {
    static DefaultTheme *currentTheme;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        currentTheme = [[DefaultTheme alloc] init];
    });
    
    return currentTheme;
}

@end
