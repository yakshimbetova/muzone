//
//  DefaultTheme.m
//  Muzone
//
//  Created by Sukhrob Khakimov on 5/22/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import "DefaultTheme.h"

@implementation DefaultTheme

- (instancetype)init {
    self = [super init];
    
    if (self) {
        // UINavigationBar
        [[UINavigationBar appearance] setTranslucent:NO];
        [[UINavigationBar appearance] setBarTintColor:[self tintColor]];
        [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"bg_nav.png"]
                                           forBarMetrics:UIBarMetricsDefault];
        [[UINavigationBar appearance] setShadowImage:[UIImage new]];
        [[UINavigationBar appearance] setBackIndicatorImage:[UIImage imageNamed:@"ic_back.png"]];
        [[UINavigationBar appearance] setBackIndicatorTransitionMaskImage:[UIImage imageNamed:@"ic_back.png"]];
        [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
        [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor],
                                                               NSFontAttributeName: [UIFont boldSystemFontOfSize:17.0f]}];
        
        // UITextField
        [[UITextField appearance] setTintColor:[self tintColor]];
        
        // UISearchBar
        [[UISearchBar appearance] setTintColor:[self tintColor]];
    }
    
    return self;
}

- (UIColor *)tintColor {
    return [self tintColorWithAlpha:1.0f];
}

- (UIColor *)tintColorWithAlpha:(CGFloat)alpha {
    return [UIColor colorWithRed:65.0f / 255
                           green:77.0f / 255
                            blue:180.0f / 255
                           alpha:alpha];
}

- (UIColor *)controlTintColor {
    return [UIColor colorWithRed:162.0f / 255
                           green:194.0f / 255
                            blue:65.0f / 255
                           alpha:1.0f];
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithTitle:@""
                                            style:UIBarButtonItemStylePlain
                                           target:nil
                                           action:nil];
}

- (CGFloat)separatorHeight {
    return 1.0f;
}

- (UIColor *)separatorColor {
    return [UIColor colorWithRed:224.0f / 255
                           green:224.0f / 255
                            blue:224.0f / 255
                           alpha:1.0f];
}

- (UIColor *)sectionBackgroundColor {
    return [UIColor colorWithRed:236.0f / 255
                           green:236.0f / 255
                            blue:236.0f / 255
                           alpha:1.0f];
}

@end
