//
//  Theme.h
//  Muzone
//
//  Created by Sukhrob Khakimov on 5/22/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol Theme <NSObject>

- (UIColor *)tintColor;
- (UIColor *)tintColorWithAlpha:(CGFloat)alpha;
- (UIColor *)controlTintColor;
- (UIBarButtonItem *)backBarButtonItem;

- (CGFloat)separatorHeight;
- (UIColor *)separatorColor;

- (UIColor *)sectionBackgroundColor;

@end
