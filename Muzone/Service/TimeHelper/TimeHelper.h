//
//  TimeHelper.h
//  Muzone
//
//  Created by Sukhrob Khakimov on 6/14/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TimeHelper : NSObject

+ (NSNumber *)totalSecondsForTimeString:(NSString *)timeString;
+ (NSString *)timeStringFromTotalSeconds:(NSNumber *)totalSeconds;

@end
