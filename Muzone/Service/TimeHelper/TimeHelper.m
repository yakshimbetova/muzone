//
//  TimeHelper.m
//  Muzone
//
//  Created by Sukhrob Khakimov on 6/14/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import "TimeHelper.h"

@implementation TimeHelper

+ (NSNumber *)totalSecondsForTimeString:(NSString *)timeString {
    NSArray *components = [timeString componentsSeparatedByString:@":"];
    
    NSInteger hours = [components[0] integerValue];
    NSInteger minutes = [components[1] integerValue];
    NSInteger seconds = [components[2] integerValue];
    
    return @(hours * 60 * 60 + minutes * 60 + seconds);
}

+ (NSString *)timeStringFromTotalSeconds:(NSNumber *)totalSeconds {
    long seconds = [totalSeconds integerValue] % 60;
    long minutes = ([totalSeconds integerValue] / 60) % 60;
    long hours = [totalSeconds integerValue] / 3600;
    
    if ([totalSeconds integerValue] >= 3600) {
        return [NSString stringWithFormat:@"%02ld:%02ld:%02ld", hours, minutes, seconds];
    }
    
    return [NSString stringWithFormat:@"%02ld:%02ld", minutes, seconds];
}

@end
