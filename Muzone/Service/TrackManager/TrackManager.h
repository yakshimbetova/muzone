//
//  TrackManager.h
//  Muzone
//
//  Created by Sukhrob Khakimov on 11/13/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *__nonnull const TrackManagerDidDeleteTrack;

extern NSString *__nonnull const TrackManagerTrackUidKey;
extern NSString *__nonnull const TrackManagerErrorKey;

@class NSManagedObjectContext;
@class Track;

@interface TrackManager : NSObject

@property (nonnull, nonatomic, strong, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonnull, nonatomic, copy) NSString *directoryPath;

- (nonnull instancetype)initWithManagedObjectContext:(nonnull NSManagedObjectContext *)managedObjectContext directoryPath:(nonnull NSString *)directoryPath;
- (void)deleteTrack:(nonnull Track *)track;

@end
