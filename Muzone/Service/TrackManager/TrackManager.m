//
//  TrackManager.m
//  Muzone
//
//  Created by Sukhrob Khakimov on 11/13/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import "TrackManager.h"
#import "TrackRepository.h"
#import "Track.h"

NSString *const TrackManagerDidDeleteTrack = @"TrackManagerDidDeleteTrack";

NSString *const TrackManagerTrackUidKey = @"TrackManagerTrackUidKey";
NSString *const TrackManagerErrorKey = @"TrackManagerErrorKey";

@interface TrackManager ()

@property (nonnull, nonatomic, strong) TrackRepository *repository;

@end

@implementation TrackManager

- (instancetype)initWithManagedObjectContext:(NSManagedObjectContext *)managedObjectContext directoryPath:(NSString *)directoryPath {
    self = [super init];
    
    if (self) {
        _managedObjectContext = managedObjectContext;
        _directoryPath = directoryPath;
    }
    
    return self;
}

- (void)deleteTrack:(Track *)track {
    NSNumber *uid = [track.uid copy];
    NSString *fileName = track.localFileName;
    
    track = [self.repository fetchOneByUid:track.uid];
    
    [_managedObjectContext.parentContext performBlock:^{
        [_managedObjectContext.parentContext deleteObject:track];
        
        NSError *error;
        
        if (![_managedObjectContext.parentContext save:&error]) {
            [_managedObjectContext performBlock:^{
                [[NSNotificationCenter defaultCenter] postNotificationName:TrackManagerDidDeleteTrack
                                                                    object:self
                                                                  userInfo:@{TrackManagerErrorKey: error}];
            }];
        } else {
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = paths.lastObject;
            NSString *sourcePath = [documentsDirectory stringByAppendingPathComponent:self.directoryPath];
            NSString *filePath = [sourcePath stringByAppendingPathComponent:fileName];
            
            NSFileManager *fileManager = [NSFileManager defaultManager];
            [fileManager removeItemAtPath:filePath
                                    error:&error];
            
            [_managedObjectContext performBlock:^{
                if (error) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:TrackManagerDidDeleteTrack
                                                                        object:self
                                                                      userInfo:@{TrackManagerErrorKey: error}];
                } else {
                    [[NSNotificationCenter defaultCenter] postNotificationName:TrackManagerDidDeleteTrack
                                                                        object:self
                                                                      userInfo:@{TrackManagerTrackUidKey: uid}];
                }
            }];
        }
    }];
}

#pragma mark - TrackRepository

- (TrackRepository *)repository {
    if (_repository == nil) {
        _repository = [[TrackRepository alloc] initWithManagedObjectContext:_managedObjectContext.parentContext];
    }
    
    return _repository;
}

@end
