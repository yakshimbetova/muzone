//
//  SZKMessageView.h
//  Muzone
//
//  Created by Sukhrob Khakimov on 6/7/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SZKMessageViewDelegate;

@interface SZKMessageView : UIView <UITableViewDataSource, UITableViewDelegate>

@property (nullable, nonatomic, weak) id<SZKMessageViewDelegate> delegate;
@property (nonatomic, assign) CGSize size;

- (void)showInView:(nonnull UIView *)view iconImage:(nullable UIImage *)iconImage title:(nullable NSString *)title message:(nullable NSString *)message actionButtonTitle:(nullable NSString *)actionButtonTitle;
- (void)showInView:(nonnull UIView *)view iconImage:(nullable UIImage *)iconImage title:(nullable NSString *)title message:(nullable NSString *)message actionButtonImage:(nullable UIImage *)actionButtonImage;
- (void)hide;

@end

@protocol SZKMessageViewDelegate <NSObject>

@optional
- (void)messageView:(nonnull SZKMessageView *)messageView didTapActionButton:(nonnull UIButton *)actionButton;
- (void)messageViewSizeDidChange:(nonnull SZKMessageView *)messageView;

@end
