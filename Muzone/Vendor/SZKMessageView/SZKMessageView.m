//
//  SZKMessageView.m
//  Muzone
//
//  Created by Sukhrob Khakimov on 6/7/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import "SZKMessageView.h"
#import "SZKMessageViewTableViewCell.h"

@interface SZKMessageView ()

@property (nullable, nonatomic, weak) UIView *containerView;
@property (nullable, nonatomic, strong) UITableView *tableView;

@property (nonnull, nonatomic, copy) NSString *cellReuseIdentifier;
@property (nonatomic, assign) CGSize constrainedSize;

@property (nullable, nonatomic, strong) UIImage *iconImage;
@property (nullable, nonatomic, copy) NSString *title;
@property (nullable, nonatomic, copy) NSString *message;
@property (nullable, nonatomic, copy) NSString *actionButtonTitle;
@property (nullable, nonatomic, strong) UIImage *actionButtonImage;

@end

@implementation SZKMessageView

- (instancetype)init {
    self = [super init];
    
    if (self) {
        self.translatesAutoresizingMaskIntoConstraints = NO;
        self.cellReuseIdentifier = NSStringFromClass([SZKMessageViewTableViewCell class]);
    }
    
    return self;
}

- (void)showInView:(UIView *)view iconImage:(UIImage *)iconImage title:(NSString *)title message:(NSString *)message actionButtonTitle:(NSString *)actionButtonTitle {
    self.iconImage = iconImage;
    self.title = title;
    self.message = message;
    self.actionButtonTitle = actionButtonTitle;
    
    self.containerView = view;
    [self show];
}

- (void)showInView:(UIView *)view iconImage:(UIImage *)iconImage title:(NSString *)title message:(NSString *)message actionButtonImage:(UIImage *)actionButtonImage {
    self.iconImage = iconImage;
    self.title = title;
    self.message = message;
    self.actionButtonImage = actionButtonImage;
    
    self.containerView = view;
    [self show];
}

- (void)show {
    [self hide];
    
    self.constrainedSize = CGSizeMake(self.containerView.bounds.size.width - kSZKMessageViewTableViewCellHorizontalSpace * 2, CGFLOAT_MAX);
    self.size = CGSizeMake(self.containerView.bounds.size.width, ceilf([self calculateHeight]));
    
    if (self.containerView.bounds.size.height > 0 &&
        self.size.height > self.containerView.bounds.size.height) {
        self.size = self.containerView.bounds.size;
    }
    
    if ([self.delegate respondsToSelector:@selector(messageViewSizeDidChange:)]) {
        [self.delegate messageViewSizeDidChange:self];
    }
    
    [self.containerView addSubview:self];
    [self addSubview:self.tableView];
    
    [self.containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|"
                                                                               options:0
                                                                               metrics:nil
                                                                                 views:@{@"view": self}]];
    
    [self.containerView addConstraint:[NSLayoutConstraint constraintWithItem:self
                                                                   attribute:NSLayoutAttributeCenterY
                                                                   relatedBy:NSLayoutRelationEqual
                                                                      toItem:self.containerView
                                                                   attribute:NSLayoutAttributeCenterY
                                                                  multiplier:1.0f
                                                                    constant:0.0f]];

    [self.containerView addConstraint:[NSLayoutConstraint constraintWithItem:self
                                                                   attribute:NSLayoutAttributeHeight
                                                                   relatedBy:NSLayoutRelationEqual
                                                                      toItem:nil
                                                                   attribute:NSLayoutAttributeNotAnAttribute
                                                                  multiplier:1.0f
                                                                    constant:self.size.height]];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[tableView]|"
                                                                 options:0
                                                                 metrics:nil
                                                                   views:@{@"tableView": self.tableView}]];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[tableView]|"
                                                                 options:0
                                                                 metrics:nil
                                                                   views:@{@"tableView": self.tableView}]];
}

- (void)hide {
    for (UIView *subview in self.subviews) {
        [subview removeFromSuperview];
    }
    
    [self removeFromSuperview];
    self.tableView = nil;
}

#pragma mark - UIButton

- (void)actionButtonTapped:(UIButton *)button {
    [self hide];
    
    if ([self.delegate respondsToSelector:@selector(messageView:didTapActionButton:)]) {
        [self.delegate messageView:self
                didTapActionButton:button];
    }
}

#pragma mark - CGRect

- (CGFloat)calculateHeight {
    CGFloat height = 0.0f;
    
    if (self.iconImage) {
        height += self.iconImage.size.height;
    }
    
    if (self.title.length > 0) {
        height += [self titleLabelRect].size.height;
        
        if (self.iconImage) {
            height += kSZKMessageViewTableViewCellVerticalSpace;
        }
    }
    
    if (self.message.length > 0) {
        height += [self messageLabelRect].size.height;
        
        if (self.iconImage ||
            self.title.length > 0) {
            height += kSZKMessageViewTableViewCellVerticalSpace;
        }
    }
    
    if (self.actionButtonTitle.length > 0) {
        height += [self actionButtonRect].size.height;
        
        if (self.iconImage ||
            self.title.length > 0 ||
            self.message.length > 0) {
            height += kSZKMessageViewTableViewCellVerticalSpace;
        }
    } else if (self.actionButtonImage) {
        height += self.actionButtonImage.size.height;
        
        if (self.iconImage ||
            self.title.length > 0 ||
            self.message.length > 0) {
            height += kSZKMessageViewTableViewCellVerticalSpace;
        }
    }
    
    if (height > 0) {
        height += kSZKMessageViewTableViewCellVerticalSpace * 2;
    }
    
    return ceilf(height);
}

- (CGRect)titleLabelRect {
    return [self.title boundingRectWithSize:self.constrainedSize
                                    options:NSStringDrawingUsesLineFragmentOrigin
                                 attributes:@{NSFontAttributeName: kSZKMessageViewTableViewCellTitleLabelFont}
                                    context:nil];
}

- (CGRect)messageLabelRect {
    return [self.message boundingRectWithSize:self.constrainedSize
                                      options:NSStringDrawingUsesLineFragmentOrigin
                                   attributes:@{NSFontAttributeName: kSZKMessageViewTableViewCellMessageLabelFont}
                                      context:nil];
}

- (CGRect)actionButtonRect {
    return [self.actionButtonTitle boundingRectWithSize:self.constrainedSize
                                                options:NSStringDrawingUsesLineFragmentOrigin
                                             attributes:@{NSFontAttributeName: kSZKMessageViewTableViewCellActionButtonTitleLabelFont}
                                                context:nil];
}

#pragma mark - UITableView

- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero
                                                  style:UITableViewStylePlain];
        _tableView.translatesAutoresizingMaskIntoConstraints = NO;
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.allowsSelection = NO;
        _tableView.bounces = NO;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = self.backgroundColor;
        [_tableView registerClass:[SZKMessageViewTableViewCell class]
           forCellReuseIdentifier:self.cellReuseIdentifier];
    }
    
    return _tableView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SZKMessageViewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:self.cellReuseIdentifier
                                                                        forIndexPath:indexPath];
    cell.iconImageView.image = self.iconImage;
    cell.titleLabel.text = self.title;
    cell.messageLabel.text = self.message;
    [cell.actionButton addTarget:self
                          action:@selector(actionButtonTapped:)
                forControlEvents:UIControlEventTouchUpInside];
    
    if (self.actionButtonTitle.length > 0) {
        [cell.actionButton setTitle:self.actionButtonTitle
                           forState:UIControlStateNormal];
    } else if (self.actionButtonImage) {
        [cell.actionButton setImage:self.actionButtonImage
                           forState:UIControlStateNormal];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self calculateHeight];
}

@end
