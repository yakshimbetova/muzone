//
//  SZKMessageViewTableViewCell.h
//  Muzone
//
//  Created by Sukhrob Khakimov on 6/7/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kSZKMessageViewTableViewCellHorizontalSpace             15.0f
#define kSZKMessageViewTableViewCellVerticalSpace               10.0f
#define kSZKMessageViewTableViewCellTitleLabelFont              [UIFont systemFontOfSize:20.0f]
#define kSZKMessageViewTableViewCellMessageLabelFont            [UIFont systemFontOfSize:17.0f]
#define kSZKMessageViewTableViewCellActionButtonTitleLabelFont  [UIFont systemFontOfSize:17.0f]

@interface SZKMessageViewTableViewCell : UITableViewCell

@property (nonnull, nonatomic, strong) UIImageView *iconImageView;
@property (nonnull, nonatomic, strong) UILabel *titleLabel;
@property (nonnull, nonatomic, strong) UILabel *messageLabel;
@property (nonnull, nonatomic, strong) UIButton *actionButton;

@end
