//
//  SZKMessageViewTableViewCell.m
//  Muzone
//
//  Created by Sukhrob Khakimov on 6/7/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import "BaseTheme.h"

#import "SZKMessageViewTableViewCell.h"

@implementation SZKMessageViewTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        self.backgroundColor = [UIColor clearColor];
    }
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (NSString *)reuseIdentifier {
    return NSStringFromClass(self.class);
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    if (![self.contentView.subviews containsObject:self.iconImageView] &&
        self.iconImageView.image) {
        [self.contentView addSubview:self.iconImageView];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.iconImageView
                                                                     attribute:NSLayoutAttributeCenterX
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.contentView
                                                                     attribute:NSLayoutAttributeCenterX
                                                                    multiplier:1.0f
                                                                      constant:0.0f]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-space-[iconImageView]"
                                                                                 options:0
                                                                                 metrics:@{@"space": @(kSZKMessageViewTableViewCellVerticalSpace)}
                                                                                   views:@{@"iconImageView": self.iconImageView}]];
    }
    
    if (![self.contentView.subviews containsObject:self.titleLabel] &&
        self.titleLabel.text.length > 0) {
        [self.contentView addSubview:self.titleLabel];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-space-[titleLabel]-space-|"
                                                                                 options:0
                                                                                 metrics:@{@"space": @(kSZKMessageViewTableViewCellHorizontalSpace)}
                                                                                   views:@{@"titleLabel": self.titleLabel}]];
        
        if (self.iconImageView.image) {
            [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.titleLabel
                                                                         attribute:NSLayoutAttributeTop
                                                                         relatedBy:NSLayoutRelationEqual
                                                                            toItem:self.iconImageView
                                                                         attribute:NSLayoutAttributeBottom
                                                                        multiplier:1.0f
                                                                          constant:kSZKMessageViewTableViewCellVerticalSpace]];
        } else {
            [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.titleLabel
                                                                         attribute:NSLayoutAttributeTop
                                                                         relatedBy:NSLayoutRelationEqual
                                                                            toItem:self.contentView
                                                                         attribute:NSLayoutAttributeTop
                                                                        multiplier:1.0f
                                                                          constant:kSZKMessageViewTableViewCellVerticalSpace]];
        }
    }
    
    if (![self.contentView.subviews containsObject:self.messageLabel] &&
        self.messageLabel.text.length > 0) {
        [self.contentView addSubview:self.messageLabel];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-space-[messageLabel]-space-|"
                                                                                 options:0
                                                                                 metrics:@{@"space": @(kSZKMessageViewTableViewCellHorizontalSpace)}
                                                                                   views:@{@"messageLabel": self.messageLabel}]];
        
        if (self.titleLabel.text.length > 0) {
            [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.messageLabel
                                                                         attribute:NSLayoutAttributeTop
                                                                         relatedBy:NSLayoutRelationEqual
                                                                            toItem:self.titleLabel
                                                                         attribute:NSLayoutAttributeBottom
                                                                        multiplier:1.0f
                                                                          constant:kSZKMessageViewTableViewCellVerticalSpace]];
        } else if (self.iconImageView.image) {
            [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.messageLabel
                                                                         attribute:NSLayoutAttributeTop
                                                                         relatedBy:NSLayoutRelationEqual
                                                                            toItem:self.iconImageView
                                                                         attribute:NSLayoutAttributeBottom
                                                                        multiplier:1.0f
                                                                          constant:kSZKMessageViewTableViewCellVerticalSpace]];
        } else {
            [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.messageLabel
                                                                         attribute:NSLayoutAttributeTop
                                                                         relatedBy:NSLayoutRelationEqual
                                                                            toItem:self.contentView
                                                                         attribute:NSLayoutAttributeTop
                                                                        multiplier:1.0f
                                                                          constant:kSZKMessageViewTableViewCellVerticalSpace]];
        }
    }
    
    if (![self.contentView.subviews containsObject:self.actionButton] &&
        (self.actionButton.titleLabel.text.length > 0 || self.actionButton.imageView.image)) {
        CGSize size;
        
        if (self.actionButton.titleLabel.text.length > 0) {
            size = [self actionButtonRect].size;
            size.width = ceilf(size.width);
            size.height = ceilf(size.height);
            
            CGSize constrainedSize = [self constrainedSize];
            
            if (size.width > constrainedSize.width) {
                size.width = constrainedSize.width;
            }
        } else {
            size = self.actionButton.imageView.image.size;
        }
        
        [self.contentView addSubview:self.actionButton];
        
        [self.actionButton addConstraint:[NSLayoutConstraint constraintWithItem:self.actionButton
                                                                      attribute:NSLayoutAttributeWidth
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:nil
                                                                      attribute:NSLayoutAttributeNotAnAttribute
                                                                     multiplier:1.0f
                                                                       constant:size.width]];
        
        [self.actionButton addConstraint:[NSLayoutConstraint constraintWithItem:self.actionButton
                                                                      attribute:NSLayoutAttributeHeight
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:nil
                                                                      attribute:NSLayoutAttributeNotAnAttribute
                                                                     multiplier:1.0f
                                                                       constant:size.height]];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.actionButton
                                                                     attribute:NSLayoutAttributeCenterX
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.contentView
                                                                     attribute:NSLayoutAttributeCenterX
                                                                    multiplier:1.0f
                                                                      constant:0.0f]];
        
        if (self.messageLabel.text.length > 0) {
            [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.actionButton
                                                                         attribute:NSLayoutAttributeTop
                                                                         relatedBy:NSLayoutRelationEqual
                                                                            toItem:self.messageLabel
                                                                         attribute:NSLayoutAttributeBottom
                                                                        multiplier:1.0f
                                                                          constant:kSZKMessageViewTableViewCellVerticalSpace]];
        } else if (self.titleLabel.text.length > 0) {
            [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.actionButton
                                                                         attribute:NSLayoutAttributeTop
                                                                         relatedBy:NSLayoutRelationEqual
                                                                            toItem:self.titleLabel
                                                                         attribute:NSLayoutAttributeBottom
                                                                        multiplier:1.0f
                                                                          constant:kSZKMessageViewTableViewCellVerticalSpace]];
        } else if (self.iconImageView.image) {
            [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.actionButton
                                                                         attribute:NSLayoutAttributeTop
                                                                         relatedBy:NSLayoutRelationEqual
                                                                            toItem:self.iconImageView
                                                                         attribute:NSLayoutAttributeBottom
                                                                        multiplier:1.0f
                                                                          constant:kSZKMessageViewTableViewCellVerticalSpace]];
        } else {
            [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.actionButton
                                                                         attribute:NSLayoutAttributeTop
                                                                         relatedBy:NSLayoutRelationEqual
                                                                            toItem:self.contentView
                                                                         attribute:NSLayoutAttributeTop
                                                                        multiplier:1.0f
                                                                          constant:kSZKMessageViewTableViewCellVerticalSpace]];
        }
    }
}

- (CGSize)constrainedSize {
    return CGSizeMake(self.frame.size.width - kSZKMessageViewTableViewCellHorizontalSpace * 2, CGFLOAT_MAX);
}

- (CGRect)actionButtonRect {
    return [self.actionButton.titleLabel.text boundingRectWithSize:[self constrainedSize]
                                                           options:NSStringDrawingUsesLineFragmentOrigin
                                                        attributes:@{NSFontAttributeName: kSZKMessageViewTableViewCellActionButtonTitleLabelFont}
                                                           context:nil];
}

#pragma mark - UIImageView

- (UIImageView *)iconImageView {
    if (_iconImageView == nil) {
        _iconImageView = [[UIImageView alloc] init];
        _iconImageView.translatesAutoresizingMaskIntoConstraints = NO;
    }
    
    return _iconImageView;
}

#pragma mark - UILabel

- (UILabel *)titleLabel {
    if (_titleLabel == nil) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _titleLabel.font = kSZKMessageViewTableViewCellTitleLabelFont;
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.textColor = [UIColor darkGrayColor];
        _titleLabel.numberOfLines = 0;
    }
    
    return _titleLabel;
}

- (UILabel *)messageLabel {
    if (_messageLabel == nil) {
        _messageLabel = [[UILabel alloc] init];
        _messageLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _messageLabel.font = kSZKMessageViewTableViewCellMessageLabelFont;
        _messageLabel.textAlignment = NSTextAlignmentCenter;
        _messageLabel.textColor = [UIColor grayColor];
        _messageLabel.numberOfLines = 0;
    }
    
    return _messageLabel;
}

#pragma mark - UIButton

- (UIButton *)actionButton {
    if (_actionButton == nil) {
        _actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _actionButton.translatesAutoresizingMaskIntoConstraints = NO;
        _actionButton.titleLabel.font = kSZKMessageViewTableViewCellActionButtonTitleLabelFont;
        _actionButton.titleLabel.numberOfLines = 0;
        [_actionButton setTitleColor:[[BaseTheme currentTheme] tintColor]
                            forState:UIControlStateNormal];
        [_actionButton setTitleColor:[[BaseTheme currentTheme] tintColorWithAlpha:0.5f]
                            forState:UIControlStateHighlighted];
    }
    
    return _actionButton;
}

@end
