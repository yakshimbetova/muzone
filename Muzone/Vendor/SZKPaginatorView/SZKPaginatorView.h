//
//  SZKPaginatorView.h
//  Muzone
//
//  Created by Sukhrob Khakimov on 6/2/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SZKPaginatorViewDataSource;
@protocol SZKPaginatorViewDelegate;

@interface SZKPaginatorView : UIView <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (nullable, nonatomic, weak) id<SZKPaginatorViewDataSource> dataSource;
@property (nullable, nonatomic, weak) id<SZKPaginatorViewDelegate> delegate;

@property (nonatomic, assign) NSUInteger selectedPage;

- (void)selectPage:(NSInteger)page animated:(BOOL)animated completion:(void (^ __nullable)(BOOL finished))completion;

@end

@protocol SZKPaginatorViewDataSource <NSObject>

@required
- (NSInteger)numberOfPagesInPaginatorView:(nonnull SZKPaginatorView *)paginatorView;
- (nonnull UIView *)paginatorView:(nonnull SZKPaginatorView *)paginatorView viewForPageAtIndex:(NSInteger)index selected:(BOOL)selected;

@end

@protocol SZKPaginatorViewDelegate <NSObject>

@required
- (CGSize)paginatorView:(nonnull SZKPaginatorView *)paginatorView sizeForPageAtIndex:(NSInteger)index;

@optional
- (void)paginatorView:(nonnull SZKPaginatorView *)paginatorView willSelectPageAtIndex:(NSInteger)index;
- (void)paginatorView:(nonnull SZKPaginatorView *)paginatorView didSelectPageAtIndex:(NSInteger)index;

@end
