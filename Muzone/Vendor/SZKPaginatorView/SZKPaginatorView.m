//
//  SZKPaginatorView.m
//  Muzone
//
//  Created by Sukhrob Khakimov on 6/2/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import "SZKPaginatorView.h"

@interface SZKPaginatorView ()

@property (nonnull, nonatomic, strong) NSMutableArray *pageSizes;
@property (nonnull, nonatomic, copy) NSString *cellReuseIdentifier;

@property (nonnull, nonatomic, strong) UICollectionViewFlowLayout *collectionViewFlowLayout;
@property (nonnull, nonatomic, strong) UICollectionView *collectionView;

@property (nonnull, nonatomic, strong) UIView *selectionIndicatorView;
@property (nonnull, nonatomic, strong) NSLayoutConstraint *selectionIndicatorViewLeadingSpaceConstraint;
@property (nonnull, nonatomic, strong) NSLayoutConstraint *selectionIndicatorViewWidthConstraint;

@end

@implementation SZKPaginatorView

- (instancetype)init {
    self = [super init];
    
    if (self) {
        self.selectedPage = 0;
        self.cellReuseIdentifier = NSStringFromClass([UICollectionViewCell class]);
        [self setupViews];
    }
    
    return self;
}

#pragma mark -

- (void)setupViews {
    [self addSubview:self.collectionView];
    [self addSubview:self.selectionIndicatorView];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[collectionView]|"
                                                                 options:0
                                                                 metrics:nil
                                                                   views:@{@"collectionView": self.collectionView}]];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[collectionView]|"
                                                                 options:0
                                                                 metrics:nil
                                                                   views:@{@"collectionView": self.collectionView}]];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[selectionIndicatorView(height)]|"
                                                                 options:0
                                                                 metrics:@{@"height": @(2.0f)}
                                                                   views:@{@"selectionIndicatorView": self.selectionIndicatorView}]];
    
    self.selectionIndicatorViewLeadingSpaceConstraint = [NSLayoutConstraint constraintWithItem:self.selectionIndicatorView
                                                                                     attribute:NSLayoutAttributeLeading
                                                                                     relatedBy:NSLayoutRelationEqual
                                                                                        toItem:self
                                                                                     attribute:NSLayoutAttributeLeading
                                                                                    multiplier:1.0f
                                                                                      constant:0.0f];
    [self addConstraint:self.selectionIndicatorViewLeadingSpaceConstraint];
    
    self.selectionIndicatorViewWidthConstraint = [NSLayoutConstraint constraintWithItem:self.selectionIndicatorView
                                                                              attribute:NSLayoutAttributeWidth
                                                                              relatedBy:NSLayoutRelationEqual
                                                                                 toItem:nil
                                                                              attribute:NSLayoutAttributeNotAnAttribute
                                                                             multiplier:1.0f
                                                                               constant:0.0f];
    [self.selectionIndicatorView addConstraint:self.selectionIndicatorViewWidthConstraint];
}

#pragma mark -

- (void)selectPage:(NSInteger)page animated:(BOOL)animated completion:(void (^)(BOOL))completion {
    if (page != self.selectedPage && page >= 0 && page <= self.pageSizes.count - 1) {
        NSInteger previousPage = self.selectedPage;
        self.selectedPage = page;
        [self.collectionView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:previousPage inSection:0],
                                                       [NSIndexPath indexPathForItem:self.selectedPage inSection:0]]];
        
        NSArray *visibleIndexPaths = [self.collectionView indexPathsForVisibleItems];
        visibleIndexPaths = [visibleIndexPaths sortedArrayUsingComparator:^NSComparisonResult(id _Nonnull obj1, id _Nonnull obj2) {
            return [(NSIndexPath *)obj1 compare:(NSIndexPath *)obj2];
        }];
        
        NSIndexPath *firstVisibleIndexPath = visibleIndexPaths.firstObject;
        NSIndexPath *lastVisibleIndexPath = visibleIndexPaths.lastObject;
        
        CGFloat width = CGSizeFromString(self.pageSizes[self.selectedPage]).width;
        CGFloat space = 0.0f;
        
        if (firstVisibleIndexPath.item == self.selectedPage || firstVisibleIndexPath.item + 1 == self.selectedPage || lastVisibleIndexPath.item == self.selectedPage || lastVisibleIndexPath.item - 1 == self.selectedPage) {
            if (firstVisibleIndexPath.item == self.selectedPage) {
                if (self.selectedPage > 0) {
                    space = CGSizeFromString(self.pageSizes[self.selectedPage - 1]).width;
                    
                    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:self.selectedPage - 1 inSection:0]
                                                atScrollPosition:UICollectionViewScrollPositionLeft
                                                        animated:animated];
                } else {
                    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:self.selectedPage inSection:0]
                                                atScrollPosition:UICollectionViewScrollPositionLeft
                                                        animated:animated];
                }
            } else if (firstVisibleIndexPath.item + 1 == self.selectedPage) {
                space = CGSizeFromString(self.pageSizes[self.selectedPage - 1]).width;
                
                if (previousPage < self.selectedPage) {
                    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:self.selectedPage + 1 inSection:0]
                                                atScrollPosition:UICollectionViewScrollPositionRight
                                                        animated:animated];
                } else {
                    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:self.selectedPage - 1 inSection:0]
                                                atScrollPosition:UICollectionViewScrollPositionLeft
                                                        animated:animated];
                }
            } else if (lastVisibleIndexPath.item == self.selectedPage) {
                if (self.selectedPage == self.pageSizes.count - 1) {
                    space = self.bounds.size.width - CGSizeFromString(self.pageSizes[self.selectedPage]).width;
                    
                    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:self.selectedPage inSection:0]
                                                atScrollPosition:UICollectionViewScrollPositionRight
                                                        animated:animated];
                } else {
                    space = self.bounds.size.width - CGSizeFromString(self.pageSizes[self.selectedPage]).width - CGSizeFromString(self.pageSizes[self.selectedPage + 1]).width;
                    
                    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:self.selectedPage + 1 inSection:0]
                                                atScrollPosition:UICollectionViewScrollPositionRight
                                                        animated:animated];
                }
            } else {
                space = self.bounds.size.width - CGSizeFromString(self.pageSizes[self.selectedPage]).width - CGSizeFromString(self.pageSizes[self.selectedPage + 1]).width;
                
                if (previousPage < self.selectedPage) {
                    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:self.selectedPage + 1 inSection:0]
                                                atScrollPosition:UICollectionViewScrollPositionRight
                                                        animated:animated];
                } else {
                    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:self.selectedPage - 1 inSection:0]
                                                atScrollPosition:UICollectionViewScrollPositionLeft
                                                        animated:animated];
                }
            }
        } else {
            if (previousPage < self.selectedPage) {
                for (NSInteger index = previousPage; index < self.selectedPage; index++) {
                    space += CGSizeFromString(self.pageSizes[index]).width;
                }
                
                space += self.selectionIndicatorViewLeadingSpaceConstraint.constant;
            } else {
                for (NSInteger index = self.selectedPage; index < previousPage; index++) {
                    space += CGSizeFromString(self.pageSizes[index]).width;
                }
                
                space = self.selectionIndicatorViewLeadingSpaceConstraint.constant - space;
            }
        }
        
        self.selectionIndicatorViewWidthConstraint.constant = width;
        self.selectionIndicatorViewLeadingSpaceConstraint.constant = space;
        [self setNeedsUpdateConstraints];
        
        if (animated) {
            [UIView animateWithDuration:0.25f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^{
                [self layoutIfNeeded];
            } completion:completion];
        } else {
            [self layoutIfNeeded];
            completion(YES);
        }
    }
}

#pragma mark - NSMutableArray

- (NSMutableArray *)pageSizes {
    if (_pageSizes == nil) {
        _pageSizes = [NSMutableArray new];
    }
    
    return _pageSizes;
}

#pragma mark - UICollectionView

- (UICollectionView *)collectionView {
    if (_collectionView == nil) {
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero
                                             collectionViewLayout:self.collectionViewFlowLayout];
        _collectionView.translatesAutoresizingMaskIntoConstraints = NO;
        _collectionView.backgroundColor = [UIColor clearColor];
        _collectionView.showsHorizontalScrollIndicator = NO;
        _collectionView.bounces = NO;
        _collectionView.scrollEnabled = NO;
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        [_collectionView registerClass:[UICollectionViewCell class]
            forCellWithReuseIdentifier:self.cellReuseIdentifier];
    }
    
    return _collectionView;
}

#pragma mark - UICollectionViewFlowLayout

- (UICollectionViewFlowLayout *)collectionViewFlowLayout {
    if (_collectionViewFlowLayout == nil) {
        _collectionViewFlowLayout = [[UICollectionViewFlowLayout alloc] init];
        _collectionViewFlowLayout.sectionInset = UIEdgeInsetsZero;
        _collectionViewFlowLayout.minimumLineSpacing = 0.0f;
        _collectionViewFlowLayout.minimumInteritemSpacing = 0.0f;
        _collectionViewFlowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    }
    
    return _collectionViewFlowLayout;
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.dataSource numberOfPagesInPaginatorView:self];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:self.cellReuseIdentifier
                                                                           forIndexPath:indexPath];
    cell.backgroundColor = [UIColor clearColor];
    
    for (UIView *subview in cell.contentView.subviews) {
        [subview removeFromSuperview];
    }
    
    UIView *view = [self.dataSource paginatorView:self
                               viewForPageAtIndex:indexPath.item
                                         selected:indexPath.item == self.selectedPage];
    view.translatesAutoresizingMaskIntoConstraints = NO;
    
    [cell.contentView addSubview:view];
    
    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|"
                                                                             options:0
                                                                             metrics:nil
                                                                               views:@{@"view": view}]];
    
    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|"
                                                                             options:0
                                                                             metrics:nil
                                                                               views:@{@"view": view}]];
    
    return cell;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGSize size = [self.delegate paginatorView:self
                            sizeForPageAtIndex:indexPath.item];
    self.pageSizes[indexPath.item] = NSStringFromCGSize(size);
    
    if (indexPath.item == self.selectedPage) {
        self.selectionIndicatorViewWidthConstraint.constant = size.width;
        [self.selectionIndicatorView setNeedsUpdateConstraints];
        [self.selectionIndicatorView layoutIfNeeded];
    }
    
    return size;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger page = indexPath.item;
    
    if (page != self.selectedPage) {
        if ([self.delegate respondsToSelector:@selector(paginatorView:willSelectPageAtIndex:)]) {
            [self.delegate paginatorView:self
                   willSelectPageAtIndex:page];
        }
        
        [self selectPage:page animated:YES completion:^(BOOL finished) {
            if ([self.delegate respondsToSelector:@selector(paginatorView:didSelectPageAtIndex:)]) {
                [self.delegate paginatorView:self
                        didSelectPageAtIndex:page];
            }
        }];
    }
}

#pragma mark - UIView

- (UIView *)selectionIndicatorView {
    if (_selectionIndicatorView == nil) {
        _selectionIndicatorView = [[UIView alloc] init];
        _selectionIndicatorView.translatesAutoresizingMaskIntoConstraints = NO;
        _selectionIndicatorView.backgroundColor = [UIColor whiteColor];
    }
    
    return _selectionIndicatorView;
}

@end
