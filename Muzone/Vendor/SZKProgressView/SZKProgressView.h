//
//  SZKProgressView.h
//  Muzone
//
//  Created by Sukhrob Khakimov on 6/7/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SZKProgressView : UIView

- (void)showInView:(nonnull UIView *)view;
- (void)showInView:(nonnull UIView *)view message:(nonnull NSString *)message;
- (void)hide;

@end
