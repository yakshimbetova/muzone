//
//  SZKProgressView.m
//  Muzone
//
//  Created by Sukhrob Khakimov on 6/7/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import "SZKProgressView.h"

#define kSize CGSizeMake(32.0f, 32.0f)

static CGFloat const horizontalSpace = 15.0f;
static CGFloat const verticalSpace = 15.0f;

@interface SZKProgressView ()

@property (nullable, nonatomic, weak) UIView *containerView;
@property (nonnull, nonatomic, strong) UILabel *messageLabel;
@property (nonnull, nonatomic, strong) UIActivityIndicatorView *activityIndicatorView;

@end

@implementation SZKProgressView

- (instancetype)init {
    self = [super init];
    
    if (self) {
        self.translatesAutoresizingMaskIntoConstraints = NO;
    }
    
    return self;
}

- (void)setupConstraints {
    [self.containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|"
                                                                               options:0
                                                                               metrics:nil
                                                                                 views:@{@"view": self}]];
    
    [self.containerView addConstraint:[NSLayoutConstraint constraintWithItem:self
                                                                   attribute:NSLayoutAttributeCenterY
                                                                   relatedBy:NSLayoutRelationEqual
                                                                      toItem:self.containerView
                                                                   attribute:NSLayoutAttributeCenterY
                                                                  multiplier:1.0f
                                                                    constant:0.0f]];
    
    [self.containerView addConstraint:[NSLayoutConstraint constraintWithItem:self
                                                                   attribute:NSLayoutAttributeHeight
                                                                   relatedBy:NSLayoutRelationEqual
                                                                      toItem:nil
                                                                   attribute:NSLayoutAttributeNotAnAttribute
                                                                  multiplier:1.0f
                                                                    constant:[self height]]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.activityIndicatorView
                                                     attribute:NSLayoutAttributeCenterX
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeCenterX
                                                    multiplier:1.0f
                                                      constant:0.0f]];
    
    [self.activityIndicatorView addConstraint:[NSLayoutConstraint constraintWithItem:self.activityIndicatorView
                                                                           attribute:NSLayoutAttributeWidth
                                                                           relatedBy:NSLayoutRelationEqual
                                                                              toItem:nil
                                                                           attribute:NSLayoutAttributeNotAnAttribute
                                                                          multiplier:1.0f
                                                                            constant:kSize.width]];
    
    [self.activityIndicatorView addConstraint:[NSLayoutConstraint constraintWithItem:self.activityIndicatorView
                                                                           attribute:NSLayoutAttributeHeight
                                                                           relatedBy:NSLayoutRelationEqual
                                                                              toItem:nil
                                                                           attribute:NSLayoutAttributeNotAnAttribute
                                                                          multiplier:1.0f
                                                                            constant:kSize.height]];
}

- (void)showInView:(UIView *)view {
    [self hide];
    self.containerView = view;
    
    [self.containerView addSubview:self];
    [self addSubview:self.activityIndicatorView];
    
    [self setupConstraints];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.activityIndicatorView
                                                     attribute:NSLayoutAttributeCenterY
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeCenterY
                                                    multiplier:1.0f
                                                      constant:0.0f]];
    
    [self.activityIndicatorView startAnimating];
}

- (void)showInView:(UIView *)view message:(NSString *)message {
    [self hide];
    self.containerView = view;
    self.messageLabel.text = message;
    
    [self.containerView addSubview:self];
    [self addSubview:self.messageLabel];
    [self addSubview:self.activityIndicatorView];
    
    [self setupConstraints];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-space-[messageLabel]-space-|"
                                                                 options:0
                                                                 metrics:@{@"space": @(horizontalSpace)}
                                                                   views:@{@"messageLabel": self.messageLabel}]];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[messageLabel]-space-[activityIndicatorView]|"
                                                                 options:0
                                                                 metrics:@{@"space": @(verticalSpace)}
                                                                   views:@{@"messageLabel": self.messageLabel,
                                                                           @"activityIndicatorView": self.activityIndicatorView}]];
    
    [self.activityIndicatorView startAnimating];
}

- (void)hide {
    [self.activityIndicatorView stopAnimating];
    [self removeFromSuperview];
    
    for (UIView *subview in self.subviews) {
        [subview removeFromSuperview];
    }
}

- (CGFloat)height {
    CGRect rect = [self.messageLabel.text boundingRectWithSize:CGSizeMake(self.containerView.frame.size.width - horizontalSpace * 2, CGFLOAT_MAX)
                                                       options:NSStringDrawingUsesLineFragmentOrigin
                                                    attributes:@{NSFontAttributeName: self.messageLabel.font}
                                                       context:nil];
    
    return kSize.height + ceilf(rect.size.height) + verticalSpace;
}

#pragma mark - UILabel

- (UILabel *)messageLabel {
    if (_messageLabel == nil) {
        _messageLabel = [[UILabel alloc] init];
        _messageLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _messageLabel.font = [UIFont systemFontOfSize:15.0f];
        _messageLabel.textAlignment = NSTextAlignmentCenter;
        _messageLabel.textColor = [UIColor grayColor];
        _messageLabel.numberOfLines = 0;
    }
    
    return _messageLabel;
}

#pragma mark - UIActivityIndicatorView

- (UIActivityIndicatorView *)activityIndicatorView {
    if (_activityIndicatorView == nil) {
        _activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        _activityIndicatorView.translatesAutoresizingMaskIntoConstraints = NO;
    }
    
    return _activityIndicatorView;
}

@end
