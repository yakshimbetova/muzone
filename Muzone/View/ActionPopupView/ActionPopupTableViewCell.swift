//
//  ActionPopupTableViewCell.swift
//  Muzone
//
//  Created by Sukhrob Khakimov on 10/22/17.
//  Copyright © 2017 Muzone LLC. All rights reserved.
//

import UIKit

class ActionPopupTableViewCell: UITableViewCell {
    static let horizontalSpace = CGFloat(15.0)
    static let iconSize = CGSize(width: 24.0, height: 24.0)

    lazy var iconImageView: UIImageView = {
        var iconImageView = UIImageView()
        iconImageView.translatesAutoresizingMaskIntoConstraints = false
        iconImageView.contentMode = .scaleAspectFit

        return iconImageView
    }()

    lazy var titleLabel: UILabel = {
        var titleLabel = UILabel()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.font = UIFont(name: "YanoneKaffeesatz-Regular", size: 14.0) // FIXME: load from theme
        titleLabel.textColor = .black

        return titleLabel
    }()

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        let bindings: [String: AnyObject] = [
            "iconImageView": iconImageView,
            "titleLabel": titleLabel
        ]

        contentView.addSubview(iconImageView)
        contentView.addSubview(titleLabel)

        contentView.addConstraints(
            NSLayoutConstraint.constraints(
                withVisualFormat: "H:|-space-[iconImageView(width)]-space-[titleLabel]-space-|",
                options: NSLayoutFormatOptions(rawValue: 0),
                metrics: [
                    "space": ActionPopupTableViewCell.horizontalSpace,
                    "width": ActionPopupTableViewCell.iconSize.width,
                ],
                views: bindings
            )
        )

        iconImageView.addConstraint(
            NSLayoutConstraint(
                item: iconImageView,
                attribute: .height,
                relatedBy: .equal,
                toItem: nil,
                attribute: .notAnAttribute,
                multiplier: 1.0,
                constant: ActionPopupTableViewCell.iconSize.height
            )
        )

        contentView.addConstraint(
            NSLayoutConstraint(
                item: iconImageView,
                attribute: .centerY,
                relatedBy: .equal,
                toItem: contentView,
                attribute: .centerY,
                multiplier: 1.0,
                constant: 0.0
            )
        )

        contentView.addConstraint(
            NSLayoutConstraint(
                item: titleLabel,
                attribute: .centerY,
                relatedBy: .equal,
                toItem: contentView,
                attribute: .centerY,
                multiplier: 1.0,
                constant: 0.0
            )
        )
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
