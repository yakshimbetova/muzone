//
//  ActionPopupView.swift
//  Muzone
//
//  Created by Sukhrob Khakimov on 10/22/17.
//  Copyright © 2017 Muzone LLC. All rights reserved.
//

import UIKit

protocol ActionPopupViewDataSource: class {
    func numberOfActionItems(in actionPopupView: ActionPopupView) -> Int
    func actionPopupView(_ actionPopupView: ActionPopupView, iconImageForActionItemAt index: Int) -> UIImage?
    func actionPopupView(_ actionPopupView: ActionPopupView, titleForActionItemAt index: Int) -> String?
}

protocol ActionPopupViewDelegate: class {
    func actionPopupView(_ actionPopupView: ActionPopupView, heightForActionItemAt index: Int) -> CGFloat
    func actionPopupView(_ actionPopupView: ActionPopupView, didSelectActionItemAt index: Int)
}

extension ActionPopupViewDataSource {
    func numberOfActionItems(in actionPopupView: ActionPopupView) -> Int {
        return 1
    }
}

extension ActionPopupViewDelegate {
    func actionPopupView(_ actionPopupView: ActionPopupView, heightForActionItemAt index: Int) -> CGFloat {
        return ActionPopupView.defaultRowHeight
    }

    func actionPopupView(_ actionPopupView: ActionPopupView, didSelectActionItemAt index: Int) {}
}

class ActionPopupView: UIView {
    static let defaultRowHeight = CGFloat(48.0)
    static let cellReuseIdentifier = String(describing: ActionPopupTableViewCell.self)

    weak var dataSource: ActionPopupViewDataSource?
    weak var delegate: ActionPopupViewDelegate?

    var animationDuration: TimeInterval = 0.2
    var animationDelay: TimeInterval = 0.2

    fileprivate var containerViewSize: CGSize!
    fileprivate var containerViewTopSpaceConstraint: NSLayoutConstraint!
    fileprivate var containerViewWidthConstraint: NSLayoutConstraint!
    fileprivate var containerViewHeightConstraint: NSLayoutConstraint!

    lazy fileprivate var tapGestureRecognizer: UITapGestureRecognizer = {
        var tapGestureRecognizer = UITapGestureRecognizer()
        tapGestureRecognizer.delegate = self
        tapGestureRecognizer.addTarget(
            self, action:
            #selector(handle(tapGestureRecognizer:))
        )

        return tapGestureRecognizer
    }()

    lazy fileprivate var containerView: UIView = {
        var containerView = UIView()
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.backgroundColor = .white
        containerView.layer.anchorPoint = CGPoint(x: 1.0, y: 0.0)
        containerView.layer.shadowColor = UIColor.black.cgColor
        containerView.layer.shadowOpacity = 0.5
        containerView.layer.shadowRadius = 12.0
        containerView.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)

        return containerView
    }()

    lazy fileprivate var tableView: UITableView = {
        var tableView = UITableView(
            frame: .zero,
            style: .plain
        )
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = .white
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        tableView.isScrollEnabled = false
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(
            ActionPopupTableViewCell.self,
            forCellReuseIdentifier: ActionPopupView.cellReuseIdentifier
        )

        return tableView
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)

        translatesAutoresizingMaskIntoConstraints = false

        addGestureRecognizer(tapGestureRecognizer)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - UITapGestureRecognizer

    func handle(tapGestureRecognizer: UITapGestureRecognizer) {
        hide()
    }

    // MARK: -

    fileprivate func setupViews(superView: UIView) {
        let numberOfActionItems: Int
        var containerViewHeight = CGFloat(0.0)

        if let dataSource = dataSource {
            numberOfActionItems = dataSource.numberOfActionItems(in: self)
        } else {
            numberOfActionItems = 0
        }

        if let delegate = delegate {
            for index in 0..<numberOfActionItems {
                containerViewHeight += delegate.actionPopupView(self, heightForActionItemAt: index)
            }
        } else {
            containerViewHeight = ActionPopupView.defaultRowHeight
        }

        containerViewSize = CGSize(width: superView.bounds.size.width / 2, height: containerViewHeight)

        let bindings: [String: AnyObject] = [
            "view": self,
            "containerView": containerView,
            "tableView": tableView
        ]

        superView.addSubview(self)
        addSubview(containerView)
        containerView.addSubview(tableView)

        superView.addConstraints(
            NSLayoutConstraint.constraints(
                withVisualFormat: "H:|[view]|",
                options: NSLayoutFormatOptions(rawValue: 0),
                metrics: nil,
                views: bindings
            )
        )

        superView.addConstraints(
            NSLayoutConstraint.constraints(
                withVisualFormat: "V:|[view]|",
                options: NSLayoutFormatOptions(rawValue: 0),
                metrics: nil,
                views: bindings
            )
        )

        addConstraints(
            NSLayoutConstraint.constraints(
                withVisualFormat: "H:[containerView]-space-|",
                options: NSLayoutFormatOptions(rawValue: 0),
                metrics: ["space": -containerViewSize.width / 2],
                views: bindings
            )
        )

        containerViewTopSpaceConstraint = NSLayoutConstraint(
            item: containerView,
            attribute: .top,
            relatedBy: .equal,
            toItem: self,
            attribute: .top,
            multiplier: 1.0,
            constant: -containerViewSize.height / 2
        )
        addConstraint(containerViewTopSpaceConstraint)

        containerViewWidthConstraint = NSLayoutConstraint(
            item: containerView,
            attribute: .width,
            relatedBy: .equal,
            toItem: nil,
            attribute: .notAnAttribute,
            multiplier: 1.0,
            constant: containerViewSize.width
        )
        containerView.addConstraint(containerViewWidthConstraint)

        containerViewHeightConstraint = NSLayoutConstraint(
            item: containerView,
            attribute: .height,
            relatedBy: .equal,
            toItem: nil,
            attribute: .notAnAttribute,
            multiplier: 1.0,
            constant: containerViewSize.height
        )
        containerView.addConstraint(containerViewHeightConstraint)

        containerView.addConstraints(
            NSLayoutConstraint.constraints(
                withVisualFormat: "H:|[tableView]|",
                options: NSLayoutFormatOptions(rawValue: 0),
                metrics: nil,
                views: bindings
            )
        )

        containerView.addConstraints(
            NSLayoutConstraint.constraints(
                withVisualFormat: "V:|[tableView]|",
                options: NSLayoutFormatOptions(rawValue: 0),
                metrics: nil,
                views: bindings
            )
        )
    }

    func show(in superView: UIView, for view: UIView? = nil) {
        setupViews(superView: superView)

        if let view = view {
            let position = view.convert(CGPoint.zero, to: superView)
            containerViewTopSpaceConstraint.constant = position.y - containerViewSize.height / 2
            setNeedsUpdateConstraints()
            layoutIfNeeded()
        }

        containerView.transform = CGAffineTransform(scaleX: 1 / containerViewSize.width, y: 1 / containerViewSize.height)

        UIView.animate(withDuration: animationDuration, delay: 0.0, options: .curveEaseInOut, animations: {
            self.containerView.transform = CGAffineTransform.identity
        }) { (finished) in
            // FIXME: implement
        }
    }

    func hide() {
        hide(delay: 0.0)
    }

    func hide(delay: TimeInterval) {
        containerView.transform = CGAffineTransform.identity

        UIView.animate(withDuration: animationDuration, delay: delay, options: .curveEaseInOut, animations: {
            self.containerView.transform = CGAffineTransform(scaleX: 1 / self.containerViewSize.width, y: 1 / self.containerViewSize.height)
        }) { (finished) in
            self.removeFromSuperview()
        }
    }
}

// MARK: - UIGestureRecognizerDelegate

extension ActionPopupView: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if let view = touch.view, view.isDescendant(of: tableView) {
            return false
        }

        return true
    }
}

// MARK: - UITableViewDataSource

extension ActionPopupView: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let dataSource = dataSource {
            return dataSource.numberOfActionItems(in: self)
        }

        return 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(
            withIdentifier: ActionPopupView.cellReuseIdentifier,
            for: indexPath
        ) as! ActionPopupTableViewCell

        cell.iconImageView.image = dataSource?.actionPopupView(self, iconImageForActionItemAt: indexPath.row)
        cell.titleLabel.text = dataSource?.actionPopupView(self, titleForActionItemAt: indexPath.row)

        return cell
    }
}

// MARK: - UITableViewDelegate

extension ActionPopupView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let delegate = delegate {
            return delegate.actionPopupView(self, heightForActionItemAt: indexPath.row)
        }

        return ActionPopupView.defaultRowHeight
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.actionPopupView(self, didSelectActionItemAt: indexPath.row)
        hide(delay: animationDelay)
    }
}
