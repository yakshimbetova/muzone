//
//  AlbumTableViewCell.h
//  Muzone
//
//  Created by Sukhrob Khakimov on 6/12/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import "MRActivityIndicatorView.h"
#import "MRCircularProgressView.h"

#import "BaseTableViewCell.h"

@interface AlbumTableViewCell : BaseTableViewCell

@property (nonnull, nonatomic, strong) UIImageView *iconImageView;
@property (nonnull, nonatomic, strong) UILabel *titleLabel;
@property (nonnull, nonatomic, strong) UILabel *artistNameLabel;
@property (nonnull, nonatomic, strong) UIButton *actionButton;
@property (nonnull, nonatomic, strong) MRActivityIndicatorView *circularActivityIndicatorView;
@property (nonnull, nonatomic, strong) MRCircularProgressView *circularProgressView;
@property (nonnull, nonatomic, strong) UITapGestureRecognizer *tapGestureRecognizer;

@end
