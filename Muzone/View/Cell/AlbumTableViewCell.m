//
//  AlbumTableViewCell.m
//  Muzone
//
//  Created by Sukhrob Khakimov on 6/12/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import "BaseTheme.h"

#import "AlbumTableViewCell.h"

@interface AlbumTableViewCell ()

@property (nonnull, nonatomic, strong) UIView *stateContainerView;

@end

@implementation AlbumTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        [self.stateContainerView addGestureRecognizer:self.tapGestureRecognizer];
        
        [self.contentView addSubview:self.iconImageView];
        [self.contentView addSubview:self.titleLabel];
        [self.contentView addSubview:self.artistNameLabel];
        [self.contentView addSubview:self.stateContainerView];
        [self.stateContainerView addSubview:self.actionButton];
        [self.stateContainerView addSubview:self.circularActivityIndicatorView];
        [self.stateContainerView addSubview:self.circularProgressView];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-space-[iconImageView]-space-[titleLabel]-space-[stateContainerView(width)]|"
                                                                                 options:0
                                                                                 metrics:@{@"space": @(15.0f),
                                                                                           @"width": @(60.0f)}
                                                                                   views:@{@"iconImageView": self.iconImageView,
                                                                                           @"titleLabel": self.titleLabel,
                                                                                           @"stateContainerView": self.stateContainerView}]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-space1-[titleLabel]-space2-[artistNameLabel]"
                                                                                 options:0
                                                                                 metrics:@{@"space1": @(10.0f),
                                                                                           @"space2": @(2.0f)}
                                                                                   views:@{@"titleLabel": self.titleLabel,
                                                                                           @"artistNameLabel": self.artistNameLabel}]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[stateContainerView]|"
                                                                                 options:0
                                                                                 metrics:nil
                                                                                   views:@{@"stateContainerView": self.stateContainerView}]];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.artistNameLabel
                                                                     attribute:NSLayoutAttributeLeading
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.titleLabel
                                                                     attribute:NSLayoutAttributeLeading
                                                                    multiplier:1.0f
                                                                      constant:0.0f]];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.artistNameLabel
                                                                     attribute:NSLayoutAttributeTrailing
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.titleLabel
                                                                     attribute:NSLayoutAttributeTrailing
                                                                    multiplier:1.0f
                                                                      constant:0.0f]];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.iconImageView
                                                                     attribute:NSLayoutAttributeCenterY
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.contentView
                                                                     attribute:NSLayoutAttributeCenterY
                                                                    multiplier:1.0f
                                                                      constant:0.0f]];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.iconImageView
                                                                     attribute:NSLayoutAttributeCenterY
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.contentView
                                                                     attribute:NSLayoutAttributeCenterY
                                                                    multiplier:1.0f
                                                                      constant:0.0f]];
        
        [self.stateContainerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[actionButton]|"
                                                                                        options:0
                                                                                        metrics:nil
                                                                                          views:@{@"actionButton": self.actionButton}]];
        
        [self.stateContainerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[actionButton]|"
                                                                                        options:0
                                                                                        metrics:nil
                                                                                          views:@{@"actionButton": self.actionButton}]];
        
        [self.stateContainerView addConstraint:[NSLayoutConstraint constraintWithItem:self.circularActivityIndicatorView
                                                                            attribute:NSLayoutAttributeCenterX
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:self.stateContainerView
                                                                            attribute:NSLayoutAttributeCenterX
                                                                           multiplier:1.0f
                                                                             constant:0.0f]];
        
        [self.stateContainerView addConstraint:[NSLayoutConstraint constraintWithItem:self.circularActivityIndicatorView
                                                                            attribute:NSLayoutAttributeCenterY
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:self.stateContainerView
                                                                            attribute:NSLayoutAttributeCenterY
                                                                           multiplier:1.0f
                                                                             constant:0.0f]];
        
        [self.circularActivityIndicatorView addConstraint:[NSLayoutConstraint constraintWithItem:self.circularActivityIndicatorView
                                                                                       attribute:NSLayoutAttributeWidth
                                                                                       relatedBy:NSLayoutRelationEqual
                                                                                          toItem:nil
                                                                                       attribute:NSLayoutAttributeNotAnAttribute
                                                                                      multiplier:1.0f
                                                                                        constant:24.0f]];
        
        [self.circularActivityIndicatorView addConstraint:[NSLayoutConstraint constraintWithItem:self.circularActivityIndicatorView
                                                                                       attribute:NSLayoutAttributeHeight
                                                                                       relatedBy:NSLayoutRelationEqual
                                                                                          toItem:nil
                                                                                       attribute:NSLayoutAttributeNotAnAttribute
                                                                                      multiplier:1.0f
                                                                                        constant:24.0f]];
        
        [self.stateContainerView addConstraint:[NSLayoutConstraint constraintWithItem:self.circularProgressView
                                                                            attribute:NSLayoutAttributeCenterX
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:self.stateContainerView
                                                                            attribute:NSLayoutAttributeCenterX
                                                                           multiplier:1.0f
                                                                             constant:0.0f]];
        
        [self.stateContainerView addConstraint:[NSLayoutConstraint constraintWithItem:self.circularProgressView
                                                                            attribute:NSLayoutAttributeCenterY
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:self.stateContainerView
                                                                            attribute:NSLayoutAttributeCenterY
                                                                           multiplier:1.0f
                                                                             constant:0.0f]];
        
        [self.circularProgressView addConstraint:[NSLayoutConstraint constraintWithItem:self.circularProgressView
                                                                              attribute:NSLayoutAttributeWidth
                                                                              relatedBy:NSLayoutRelationEqual
                                                                                 toItem:nil
                                                                              attribute:NSLayoutAttributeNotAnAttribute
                                                                             multiplier:1.0f
                                                                               constant:24.0f]];
        
        [self.circularProgressView addConstraint:[NSLayoutConstraint constraintWithItem:self.circularProgressView
                                                                              attribute:NSLayoutAttributeHeight
                                                                              relatedBy:NSLayoutRelationEqual
                                                                                 toItem:nil
                                                                              attribute:NSLayoutAttributeNotAnAttribute
                                                                             multiplier:1.0f
                                                                               constant:24.0f]];
    }
    
    return self;
}

#pragma mark - UIImageView

- (UIImageView *)iconImageView {
    if (_iconImageView == nil) {
        _iconImageView = [[UIImageView alloc] init];
        _iconImageView.translatesAutoresizingMaskIntoConstraints = NO;
        [_iconImageView setContentHuggingPriority:UILayoutPriorityDefaultHigh
                                          forAxis:UILayoutConstraintAxisHorizontal];
        [_iconImageView setContentCompressionResistancePriority:UILayoutPriorityDefaultHigh
                                                        forAxis:UILayoutConstraintAxisHorizontal];
        _iconImageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    
    return _iconImageView;
}

#pragma mark - UILabel

- (UILabel *)titleLabel {
    if (_titleLabel == nil) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [_titleLabel setContentHuggingPriority:UILayoutPriorityDefaultLow
                                       forAxis:UILayoutConstraintAxisHorizontal];
        [_titleLabel setContentCompressionResistancePriority:UILayoutPriorityDefaultLow
                                                     forAxis:UILayoutConstraintAxisHorizontal];
        _titleLabel.font = [UIFont systemFontOfSize:17.0f];
        _titleLabel.textColor = [UIColor blackColor];
    }
    
    return _titleLabel;
}

- (UILabel *)artistNameLabel {
    if (_artistNameLabel == nil) {
        _artistNameLabel = [[UILabel alloc] init];
        _artistNameLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _artistNameLabel.font = [UIFont systemFontOfSize:12.0f];
        _artistNameLabel.textColor = [UIColor grayColor];
    }
    
    return _artistNameLabel;
}

#pragma mark - UIView

- (UIView *)stateContainerView {
    if (_stateContainerView == nil) {
        _stateContainerView = [[UIView alloc] init];
        _stateContainerView.translatesAutoresizingMaskIntoConstraints = NO;
    }
    
    return _stateContainerView;
}

#pragma mark - UIButton

- (UIButton *)actionButton {
    if (_actionButton == nil) {
        _actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _actionButton.translatesAutoresizingMaskIntoConstraints = NO;
    }
    
    return _actionButton;
}

#pragma mark - MRActivityIndicatorView

- (MRActivityIndicatorView *)circularActivityIndicatorView {
    if (_circularActivityIndicatorView == nil) {
        _circularActivityIndicatorView = [[MRActivityIndicatorView alloc] init];
        _circularActivityIndicatorView.translatesAutoresizingMaskIntoConstraints = NO;
        _circularActivityIndicatorView.tintColor = [[BaseTheme currentTheme] tintColor];
        _circularActivityIndicatorView.lineWidth = 1.0f;
    }
    
    return _circularActivityIndicatorView;
}

#pragma mark - MRCircularProgressView

- (MRCircularProgressView *)circularProgressView {
    if (_circularProgressView == nil) {
        _circularProgressView = [[MRCircularProgressView alloc] init];
        _circularProgressView.translatesAutoresizingMaskIntoConstraints = NO;
        _circularProgressView.tintColor = [[BaseTheme currentTheme] tintColor];
        _circularProgressView.lineWidth = 2.0f;
        _circularProgressView.borderWidth = 1.0f;
        _circularProgressView.mayStop = YES;
    }
    
    return _circularProgressView;
}

#pragma mark - UITapGestureRecognizer

- (UITapGestureRecognizer *)tapGestureRecognizer {
    if (_tapGestureRecognizer == nil) {
        _tapGestureRecognizer = [[UITapGestureRecognizer alloc] init];
    }
    
    return _tapGestureRecognizer;
}

@end
