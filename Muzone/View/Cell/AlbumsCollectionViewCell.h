//
//  AlbumsCollectionViewCell.h
//  Muzone
//
//  Created by Sukhrob Khakimov on 6/9/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlbumsCollectionViewCell : UICollectionViewCell

@property (nonnull, nonatomic, strong) UIActivityIndicatorView *activityIndicatorView;
@property (nonnull, nonatomic, strong) UIImageView *coverImageView;
@property (nonnull, nonatomic, strong) UILabel *titleLabel;
@property (nonnull, nonatomic, strong) UILabel *yearLabel;
@property (nonnull, nonatomic, strong) UILabel *artistNameLabel;

@end
