//
//  AlbumsCollectionViewCell.m
//  Muzone
//
//  Created by Sukhrob Khakimov on 6/9/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import "AlbumsCollectionViewCell.h"

#define kPadding 8.0f

@implementation AlbumsCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        self.layer.borderWidth = 1.0f;
        self.layer.borderColor = [UIColor colorWithRed:223.0f / 255
                                                 green:223.0f / 255
                                                  blue:223.0f / 255
                                                 alpha:1.0f].CGColor;
        self.layer.cornerRadius = 3.0f;
        self.layer.shadowColor = self.layer.borderColor;
        self.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
        self.layer.shadowOpacity = 0.8f;
        self.layer.shadowRadius = 1.0f;
        
        [self.contentView addSubview:self.coverImageView];
        [self.contentView addSubview:self.activityIndicatorView];
        [self.contentView addSubview:self.titleLabel];
        [self.contentView addSubview:self.yearLabel];
        [self.contentView addSubview:self.artistNameLabel];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.activityIndicatorView
                                                                     attribute:NSLayoutAttributeCenterX
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.coverImageView
                                                                     attribute:NSLayoutAttributeCenterX
                                                                    multiplier:1.0f
                                                                      constant:0.0f]];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.activityIndicatorView
                                                                     attribute:NSLayoutAttributeCenterY
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.coverImageView
                                                                     attribute:NSLayoutAttributeCenterY
                                                                    multiplier:1.0f
                                                                      constant:0.0f]];
    
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-space-[coverImageView]-space-|"
                                                                                 options:0
                                                                                 metrics:@{@"space": @(kPadding)}
                                                                                   views:@{@"coverImageView": self.coverImageView}]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-space-[titleLabel]-space-|"
                                                                                 options:0
                                                                                 metrics:@{@"space": @(kPadding)}
                                                                                   views:@{@"titleLabel": self.titleLabel}]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-space-[artistNameLabel]-space-[yearLabel]-space-|"
                                                                                 options:0
                                                                                 metrics:@{@"space": @(kPadding)}
                                                                                   views:@{@"artistNameLabel": self.artistNameLabel,
                                                                                           @"yearLabel": self.yearLabel}]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-space1-[coverImageView]-space1-[titleLabel]-space2-[artistNameLabel]"
                                                                                 options:0
                                                                                 metrics:@{@"space1": @(kPadding),
                                                                                           @"space2": @(2.0f)}
                                                                                   views:@{@"coverImageView": self.coverImageView,
                                                                                           @"titleLabel": self.titleLabel,
                                                                                           @"artistNameLabel": self.artistNameLabel}]];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.coverImageView
                                                                     attribute:NSLayoutAttributeHeight
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.coverImageView
                                                                     attribute:NSLayoutAttributeWidth
                                                                    multiplier:1.0f
                                                                      constant:0.0f]];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.yearLabel
                                                                     attribute:NSLayoutAttributeBottom
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.artistNameLabel
                                                                     attribute:NSLayoutAttributeBottom
                                                                    multiplier:1.0f
                                                                      constant:0.0f]];
    }
    
    return self;
}

- (NSString *)reuseIdentifier {
    return NSStringFromClass(self.class);
}

#pragma mark - UIActivityIndicatorView

- (UIActivityIndicatorView *)activityIndicatorView {
    if (_activityIndicatorView == nil) {
        _activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        _activityIndicatorView.translatesAutoresizingMaskIntoConstraints = NO;
    }
    
    return _activityIndicatorView;
}

#pragma mark - UIImageView

- (UIImageView *)coverImageView {
    if (_coverImageView == nil) {
        _coverImageView = [[UIImageView alloc] init];
        _coverImageView.translatesAutoresizingMaskIntoConstraints = NO;
        _coverImageView.contentMode = UIViewContentModeScaleAspectFill;
        _coverImageView.layer.cornerRadius = 3.0f;
        _coverImageView.clipsToBounds = YES;
    }
    
    return _coverImageView;
}

#pragma mark - UILabel

- (UILabel *)titleLabel {
    if (_titleLabel == nil) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _titleLabel.font = [UIFont systemFontOfSize:15.0f];
        _titleLabel.textColor = [UIColor blackColor];
    }
    
    return _titleLabel;
}

- (UILabel *)yearLabel {
    if (_yearLabel == nil) {
        _yearLabel = [[UILabel alloc] init];
        _yearLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [_yearLabel setContentHuggingPriority:UILayoutPriorityDefaultHigh
                                      forAxis:UILayoutConstraintAxisHorizontal];
        [_yearLabel setContentCompressionResistancePriority:UILayoutPriorityDefaultHigh
                                                    forAxis:UILayoutConstraintAxisHorizontal];
        _yearLabel.font = [UIFont systemFontOfSize:12.0f];
        _yearLabel.textColor = [UIColor darkGrayColor];
    }
    
    return _yearLabel;
}

- (UILabel *)artistNameLabel {
    if (_artistNameLabel == nil) {
        _artistNameLabel = [[UILabel alloc] init];
        _artistNameLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [_artistNameLabel setContentHuggingPriority:UILayoutPriorityDefaultLow
                                            forAxis:UILayoutConstraintAxisHorizontal];
        [_artistNameLabel setContentCompressionResistancePriority:UILayoutPriorityDefaultLow
                                                          forAxis:UILayoutConstraintAxisHorizontal];
        _artistNameLabel.font = [UIFont systemFontOfSize:12.0f];
        _artistNameLabel.textColor = [UIColor grayColor];
    }
    
    return _artistNameLabel;
}

@end
