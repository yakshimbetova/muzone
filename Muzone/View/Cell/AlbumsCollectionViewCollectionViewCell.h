//
//  AlbumsCollectionViewCollectionViewCell.h
//  Muzone
//
//  Created by Sukhrob Khakimov on 6/11/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlbumsCollectionViewCollectionViewCell : UICollectionViewCell

@property (nonnull, nonatomic, strong) UICollectionView *collectionView;
@property (nonnull, nonatomic, strong) UICollectionViewFlowLayout *collectionViewFlowLayout;

@end
