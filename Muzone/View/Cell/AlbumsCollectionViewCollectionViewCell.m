//
//  AlbumsCollectionViewCollectionViewCell.m
//  Muzone
//
//  Created by Sukhrob Khakimov on 6/11/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import "AlbumsCollectionViewCollectionViewCell.h"
#import "AlbumsCollectionViewCell.h"

@implementation AlbumsCollectionViewCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        [self.contentView addSubview:self.collectionView];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[collectionView]|"
                                                                                 options:0
                                                                                 metrics:nil
                                                                                   views:@{@"collectionView": self.collectionView}]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[collectionView]|"
                                                                                 options:0
                                                                                 metrics:nil
                                                                                   views:@{@"collectionView": self.collectionView}]];
    }
    
    return self;
}

- (NSString *)reuseIdentifier {
    return NSStringFromClass(self.class);
}

#pragma mark - UICollectionView

- (UICollectionView *)collectionView {
    if (_collectionView == nil) {
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero
                                             collectionViewLayout:self.collectionViewFlowLayout];
        _collectionView.translatesAutoresizingMaskIntoConstraints = NO;
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.alwaysBounceHorizontal = YES;
        _collectionView.showsHorizontalScrollIndicator = NO;
        [_collectionView registerClass:[AlbumsCollectionViewCell class]
            forCellWithReuseIdentifier:NSStringFromClass([AlbumsCollectionViewCell class])];
    }
    
    return _collectionView;
}

#pragma mark - UICollectionViewFlowLayout

- (UICollectionViewFlowLayout *)collectionViewFlowLayout {
    if (_collectionViewFlowLayout == nil) {
        _collectionViewFlowLayout = [[UICollectionViewFlowLayout alloc] init];
        _collectionViewFlowLayout.minimumLineSpacing = 8.0f;
        _collectionViewFlowLayout.minimumInteritemSpacing = 0.0f;
        _collectionViewFlowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    }
    
    return _collectionViewFlowLayout;
}

@end
