//
//  ArtistsTableViewCell.h
//  Muzone
//
//  Created by Sukhrob Khakimov on 6/9/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArtistsTableViewCell : UITableViewCell

@property (nonnull, nonatomic, strong) UILabel *titleLabel;

@end
