//
//  BaseTableViewCell.h
//  Muzone
//
//  Created by Sukhrob Khakimov on 10/23/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseTableViewCell : UITableViewCell

@end
