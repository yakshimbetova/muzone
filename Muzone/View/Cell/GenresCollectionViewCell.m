//
//  GenresCollectionViewCell.m
//  Muzone
//
//  Created by Sukhrob Khakimov on 6/9/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import "GenresCollectionViewCell.h"

@implementation GenresCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        self.layer.cornerRadius = 3.0f;
        self.clipsToBounds = YES;
        
        [self.contentView addSubview:self.coverImageView];
        [self.contentView addSubview:self.activityIndicatorView];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[coverImageView]|"
                                                                                 options:0
                                                                                 metrics:nil
                                                                                   views:@{@"coverImageView": self.coverImageView}]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[coverImageView]|"
                                                                                 options:0
                                                                                 metrics:nil
                                                                                   views:@{@"coverImageView": self.coverImageView}]];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.activityIndicatorView
                                                                     attribute:NSLayoutAttributeCenterX
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.coverImageView
                                                                     attribute:NSLayoutAttributeCenterX
                                                                    multiplier:1.0f
                                                                      constant:0.0f]];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.activityIndicatorView
                                                                     attribute:NSLayoutAttributeCenterY
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.coverImageView
                                                                     attribute:NSLayoutAttributeCenterY
                                                                    multiplier:1.0f
                                                                      constant:0.0f]];
    }
    
    return self;
}

- (NSString *)reuseIdentifier {
    return NSStringFromClass(self.class);
}

#pragma mark - UIImageView

- (UIImageView *)coverImageView {
    if (_coverImageView == nil) {
        _coverImageView = [[UIImageView alloc] init];
        _coverImageView.translatesAutoresizingMaskIntoConstraints = NO;
        _coverImageView.contentMode = UIViewContentModeScaleAspectFill;
        _coverImageView.layer.cornerRadius = 3.0f;
        _coverImageView.clipsToBounds = YES;
    }
    
    return _coverImageView;
}

#pragma mark - UIActivityIndicatorView

- (UIActivityIndicatorView *)activityIndicatorView {
    if (_activityIndicatorView == nil) {
        _activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        _activityIndicatorView.translatesAutoresizingMaskIntoConstraints = NO;
    }
    
    return _activityIndicatorView;
}

@end
