//
//  MenuTableViewCell.h
//  Muzone
//
//  Created by Sukhrob Khakimov on 5/22/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuTableViewCell : UITableViewCell

@property (nonnull, nonatomic, strong) UIImageView *iconImageView;
@property (nonnull, nonatomic, strong) UILabel *titleLabel;

@end
