//
//  PageCollectionViewCollectionViewCell.h
//  Muzone
//
//  Created by Sukhrob Khakimov on 5/30/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SZKProgressView.h"
#import "SZKMessageView.h"

@interface PageCollectionViewCollectionViewCell : UICollectionViewCell

@property (nonnull, nonatomic, strong) UICollectionView *collectionView;
@property (nonnull, nonatomic, strong) UICollectionViewFlowLayout *collectionViewFlowLayout;

@property (nonnull, nonatomic, strong) UIRefreshControl *refreshControl;
@property (nonnull, nonatomic, strong) SZKProgressView *progressView;
@property (nonnull, nonatomic, strong) SZKMessageView *messageView;

@end
