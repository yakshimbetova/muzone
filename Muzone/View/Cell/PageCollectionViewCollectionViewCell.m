//
//  PageCollectionViewCollectionViewCell.m
//  Muzone
//
//  Created by Sukhrob Khakimov on 5/30/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import "PageCollectionViewCollectionReusableView.h"
#import "PageCollectionViewCollectionViewCell.h"
#import "AlbumsCollectionViewCollectionViewCell.h"
#import "AlbumsCollectionViewCell.h"
#import "GenresCollectionViewCell.h"

@implementation PageCollectionViewCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        [self.contentView addSubview:self.collectionView];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[collectionView]|"
                                                                                 options:0
                                                                                 metrics:nil
                                                                                   views:@{@"collectionView": self.collectionView}]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[collectionView]|"
                                                                                 options:0
                                                                                 metrics:nil
                                                                                   views:@{@"collectionView": self.collectionView}]];
    }
    
    return self;
}

- (NSString *)reuseIdentifier {
    return NSStringFromClass(self.class);
}

#pragma mark - UICollectionView

- (UICollectionView *)collectionView {
    if (_collectionView == nil) {
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero
                                             collectionViewLayout:self.collectionViewFlowLayout];
        _collectionView.translatesAutoresizingMaskIntoConstraints = NO;
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.alwaysBounceVertical = YES;
        [_collectionView registerClass:[PageCollectionViewCollectionReusableView class]
            forSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                   withReuseIdentifier:NSStringFromClass([PageCollectionViewCollectionReusableView class])];
        [_collectionView registerClass:[AlbumsCollectionViewCollectionViewCell class]
            forCellWithReuseIdentifier:NSStringFromClass([AlbumsCollectionViewCollectionViewCell class])];
        [_collectionView registerClass:[AlbumsCollectionViewCell class]
            forCellWithReuseIdentifier:NSStringFromClass([AlbumsCollectionViewCell class])];
        [_collectionView registerClass:[GenresCollectionViewCell class]
            forCellWithReuseIdentifier:NSStringFromClass([GenresCollectionViewCell class])];
    }
    
    return _collectionView;
}

#pragma mark - UICollectionViewFlowLayout

- (UICollectionViewFlowLayout *)collectionViewFlowLayout {
    if (_collectionViewFlowLayout == nil) {
        _collectionViewFlowLayout = [[UICollectionViewFlowLayout alloc] init];
        _collectionViewFlowLayout.minimumLineSpacing = 8.0f;
        _collectionViewFlowLayout.minimumInteritemSpacing = 0.0f;
        _collectionViewFlowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    }
    
    return _collectionViewFlowLayout;
}

#pragma mark - UIRefreshControl

- (UIRefreshControl *)refreshControl {
    if (_refreshControl == nil) {
        _refreshControl = [[UIRefreshControl alloc] init];
        _refreshControl.backgroundColor = [UIColor whiteColor];
    }
    
    return _refreshControl;
}

#pragma mark - SZKProgressView

- (SZKProgressView *)progressView {
    if (_progressView == nil) {
        _progressView = [[SZKProgressView alloc] init];
    }
    
    return _progressView;
}

#pragma mark - SZKMessageView

- (SZKMessageView *)messageView {
    if (_messageView == nil) {
        _messageView = [[SZKMessageView alloc] init];
    }
    
    return _messageView;
}

@end
