//
//  PageTableViewCollectionViewCell.h
//  Muzone
//
//  Created by Sukhrob Khakimov on 6/9/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SZKProgressView.h"
#import "SZKMessageView.h"

@interface PageTableViewCollectionViewCell : UICollectionViewCell

@property (nonnull, nonatomic, strong) UITableView *tableView;

@property (nonnull, nonatomic, strong) UIRefreshControl *refreshControl;
@property (nonnull, nonatomic, strong) SZKProgressView *progressView;
@property (nonnull, nonatomic, strong) SZKMessageView *messageView;

@end
