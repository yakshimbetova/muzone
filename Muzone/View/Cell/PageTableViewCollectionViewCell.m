//
//  PageTableViewCollectionViewCell.m
//  Muzone
//
//  Created by Sukhrob Khakimov on 6/9/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import "PageTableViewCollectionViewCell.h"
#import "ArtistsTableViewCell.h"

#import "Muzone-Swift.h"

@implementation PageTableViewCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        [self.contentView addSubview:self.tableView];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[tableView]|"
                                                                                 options:0
                                                                                 metrics:nil
                                                                                   views:@{@"tableView": self.tableView}]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[tableView]|"
                                                                                 options:0
                                                                                 metrics:nil
                                                                                   views:@{@"tableView": self.tableView}]];
    }
    
    return self;
}

- (NSString *)reuseIdentifier {
    return NSStringFromClass(self.class);
}

#pragma mark - UITableView

- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero
                                                  style:UITableViewStylePlain];
        _tableView.translatesAutoresizingMaskIntoConstraints = NO;
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        [_tableView registerClass:[ArtistsTableViewCell class]
           forCellReuseIdentifier:NSStringFromClass([ArtistsTableViewCell class])];
        [_tableView registerClass:[PlaylistTableViewCell class]
           forCellReuseIdentifier:NSStringFromClass([PlaylistTableViewCell class])];
    }
    
    return _tableView;
}

#pragma mark - UIRefreshControl

- (UIRefreshControl *)refreshControl {
    if (_refreshControl == nil) {
        _refreshControl = [[UIRefreshControl alloc] init];
        _refreshControl.backgroundColor = [UIColor whiteColor];
    }
    
    return _refreshControl;
}

#pragma mark - SZKProgressView

- (SZKProgressView *)progressView {
    if (_progressView == nil) {
        _progressView = [[SZKProgressView alloc] init];
    }
    
    return _progressView;
}

#pragma mark - SZKMessageView

- (SZKMessageView *)messageView {
    if (_messageView == nil) {
        _messageView = [[SZKMessageView alloc] init];
    }
    
    return _messageView;
}

@end
