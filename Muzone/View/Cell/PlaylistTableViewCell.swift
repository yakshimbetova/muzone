//
//  PlaylistTableViewCell.swift
//  Muzone
//
//  Created by Sukhrob Khakimov on 10/18/17.
//  Copyright © 2017 Muzone LLC. All rights reserved.
//

import UIKit

class PlaylistTableViewCell: BaseTableViewCell {
    static let horizontalSpace = CGFloat(15.0)
    static let verticalSpace = CGFloat(15.0)
    static let coverImageSize = CGSize(width: 40.0, height: 40.0)
    static let authorNameLabelTopSpace = CGFloat(4.0)
    static let favoriteIconSize = CGSize(width: 18.0, height: 18.0)
    static let favoriteIconLeadingSpace = CGFloat(5.0)
    static let visibilityLabelHeight = CGFloat(20.0)
    static let visibilityLabelPadding = CGFloat(15.0)
    static let actionButtonWidth = CGFloat(36.0)

    static let coverImageViewBackgroundColor = UIColor.rgb(value: 212.0)
    static let privateVisibilityBackgroundColor = UIColor.rgb(value: 117.0)
    static let publicVisibilityBackgroundColor = UIColor.rgb(red: 22.0, green: 167.0, blue: 101.0)

    fileprivate var titleLabelTopSpaceConstraint: NSLayoutConstraint!
    fileprivate var visibilityLabelWidthSpaceConstraint: NSLayoutConstraint!
    fileprivate var visibilityLabelBackgroundColor: UIColor

    lazy var coverImageView: UIImageView = {
        var coverImageView = UIImageView()
        coverImageView.translatesAutoresizingMaskIntoConstraints = false
        coverImageView.backgroundColor = .rgb(value: 212.0)
        coverImageView.contentMode = .scaleAspectFill
        coverImageView.layer.cornerRadius = 5.0
        coverImageView.clipsToBounds = true

        return coverImageView
    }()

    lazy var titleLabel: UILabel = {
        var titleLabel = UILabel()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.font = UIFont(name: "YanoneKaffeesatz-Regular", size: 16.0) // FIXME: load from theme
        titleLabel.textColor = .black
        titleLabel.setContentHuggingPriority(
            UILayoutPriorityDefaultHigh,
            for: .horizontal
        )
        titleLabel.setContentCompressionResistancePriority(
            UILayoutPriorityDefaultLow,
            for: .horizontal
        )

        return titleLabel
    }()

    lazy var favoriteIconImageView: UIImageView = {
        var favoriteIconImageView = UIImageView(image: UIImage(named: "ic_star.png")?.withRenderingMode(.alwaysTemplate))
        favoriteIconImageView.translatesAutoresizingMaskIntoConstraints = false
        favoriteIconImageView.contentMode = .scaleAspectFill
        favoriteIconImageView.tintColor = .rgb(red: 255.0, green: 193.0, blue: 7.0)

        return favoriteIconImageView
    }()

    lazy var authorNameLabel: UILabel = {
        var authorNameLabel = UILabel()
        authorNameLabel.translatesAutoresizingMaskIntoConstraints = false
        authorNameLabel.font = UIFont(name: "YanoneKaffeesatz-Regular", size: 14.0) // FIXME: load from theme
        authorNameLabel.textColor = .rgb(value: 0.0, alpha: 0.54)
        authorNameLabel.setContentHuggingPriority(
            UILayoutPriorityDefaultHigh,
            for: .horizontal
        )
        authorNameLabel.setContentCompressionResistancePriority(
            UILayoutPriorityDefaultLow,
            for: .horizontal
        )

        return authorNameLabel
    }()

    lazy var tracksCountLabel: UILabel = {
        var tracksCountLabel = UILabel()
        tracksCountLabel.translatesAutoresizingMaskIntoConstraints = false
        tracksCountLabel.font = UIFont(name: "YanoneKaffeesatz-Regular", size: 14.0) // FIXME: load from theme
        tracksCountLabel.textColor = .black
        tracksCountLabel.textAlignment = .right
        tracksCountLabel.setContentHuggingPriority(
            UILayoutPriorityDefaultHigh,
            for: .horizontal
        )
        tracksCountLabel.setContentCompressionResistancePriority(
            UILayoutPriorityDefaultHigh,
            for: .horizontal
        )

        return tracksCountLabel
    }()

    lazy var visibilityLabel: UILabel = {
        var visibilityLabel = UILabel()
        visibilityLabel.translatesAutoresizingMaskIntoConstraints = false
        visibilityLabel.backgroundColor = self.visibilityLabelBackgroundColor
        visibilityLabel.font = UIFont(name: "YanoneKaffeesatz-Light", size: 10.0) // FIXME: load from theme
        visibilityLabel.textColor = .white
        visibilityLabel.textAlignment = .center
        visibilityLabel.layer.cornerRadius = 2.0
        visibilityLabel.clipsToBounds = true
        visibilityLabel.setContentHuggingPriority(
            UILayoutPriorityDefaultHigh,
            for: .horizontal
        )
        visibilityLabel.setContentCompressionResistancePriority(
            UILayoutPriorityDefaultHigh,
            for: .horizontal
        )

        return visibilityLabel
    }()

    lazy var actionButton: UIButton = {
        var actionButton = UIButton(type: .custom)
        actionButton.translatesAutoresizingMaskIntoConstraints = false
        actionButton.setImage(
            UIImage(named: "ic_more.png")?.withRenderingMode(.alwaysTemplate),
            for: .normal
        )
        actionButton.imageView?.tintColor = .rgb(value: 117.0)

        return actionButton
    }()

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        visibilityLabelBackgroundColor = PlaylistTableViewCell.privateVisibilityBackgroundColor

        super.init(style: style, reuseIdentifier: reuseIdentifier)

        let bindings: [String: AnyObject] = [
            "coverImageView": coverImageView,
            "titleLabel": titleLabel,
            "favoriteIconImageView": favoriteIconImageView,
            "authorNameLabel": authorNameLabel,
            "tracksCountLabel": tracksCountLabel,
            "visibilityLabel": visibilityLabel,
            "actionButton": actionButton
        ]

        contentView.addSubview(coverImageView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(favoriteIconImageView)
        contentView.addSubview(authorNameLabel)
        contentView.addSubview(tracksCountLabel)
        contentView.addSubview(visibilityLabel)
        contentView.addSubview(actionButton)

        contentView.addConstraints(
            NSLayoutConstraint.constraints(
                withVisualFormat: "H:|-space1-[coverImageView(width1)]-space1-[titleLabel]-space2-[favoriteIconImageView(width2)]-(>=space1)-[tracksCountLabel][actionButton(width3)]|",
                options: NSLayoutFormatOptions(rawValue: 0),
                metrics: [
                    "space1": PlaylistTableViewCell.horizontalSpace,
                    "space2": PlaylistTableViewCell.favoriteIconLeadingSpace,
                    "width1": PlaylistTableViewCell.coverImageSize.width,
                    "width2": PlaylistTableViewCell.favoriteIconSize.width,
                    "width3": PlaylistTableViewCell.actionButtonWidth
                ],
                views: bindings
            )
        )

        contentView.addConstraints(
            NSLayoutConstraint.constraints(
                withVisualFormat: "H:[coverImageView]-space-[authorNameLabel]-(>=space)-[visibilityLabel][actionButton]|",
                options: NSLayoutFormatOptions(rawValue: 0),
                metrics: ["space": PlaylistTableViewCell.horizontalSpace],
                views: bindings
            )
        )

        coverImageView.addConstraint(
            NSLayoutConstraint(
                item: coverImageView,
                attribute: .height,
                relatedBy: .equal,
                toItem: nil,
                attribute: .notAnAttribute,
                multiplier: 1.0,
                constant: PlaylistTableViewCell.coverImageSize.height
            )
        )

        contentView.addConstraint(
            NSLayoutConstraint(
                item: coverImageView,
                attribute: .centerY,
                relatedBy: .equal,
                toItem: contentView,
                attribute: .centerY,
                multiplier: 1.0,
                constant: 0.0
            )
        )

        contentView.addConstraints(
            NSLayoutConstraint.constraints(
                withVisualFormat: "V:[titleLabel]-space-[authorNameLabel]",
                options: NSLayoutFormatOptions(rawValue: 0),
                metrics: ["space": PlaylistTableViewCell.authorNameLabelTopSpace],
                views: bindings
            )
        )

        titleLabelTopSpaceConstraint = NSLayoutConstraint(
            item: titleLabel,
            attribute: .top,
            relatedBy: .equal,
            toItem: contentView,
            attribute: .top,
            multiplier: 1.0,
            constant: 0.0
        )
        contentView.addConstraint(titleLabelTopSpaceConstraint)

        favoriteIconImageView.addConstraint(
            NSLayoutConstraint(
                item: favoriteIconImageView,
                attribute: .height,
                relatedBy: .equal,
                toItem: nil,
                attribute: .notAnAttribute,
                multiplier: 1.0,
                constant: PlaylistTableViewCell.favoriteIconSize.height
            )
        )

        contentView.addConstraint(
            NSLayoutConstraint(
                item: favoriteIconImageView,
                attribute: .centerY,
                relatedBy: .equal,
                toItem: titleLabel,
                attribute: .centerY,
                multiplier: 1.0,
                constant: 0.0
            )
        )

        contentView.addConstraint(
            NSLayoutConstraint(
                item: tracksCountLabel,
                attribute: .centerY,
                relatedBy: .equal,
                toItem: titleLabel,
                attribute: .centerY,
                multiplier: 1.0,
                constant: 0.0
            )
        )

        visibilityLabelWidthSpaceConstraint = NSLayoutConstraint(
            item: visibilityLabel,
            attribute: .width,
            relatedBy: .equal,
            toItem: nil,
            attribute: .notAnAttribute,
            multiplier: 1.0,
            constant: 0.0
        )
        visibilityLabel.addConstraint(visibilityLabelWidthSpaceConstraint)

        visibilityLabel.addConstraint(
            NSLayoutConstraint(
                item: visibilityLabel,
                attribute: .height,
                relatedBy: .equal,
                toItem: nil,
                attribute: .notAnAttribute,
                multiplier: 1.0,
                constant: PlaylistTableViewCell.visibilityLabelHeight
            )
        )

        contentView.addConstraint(
            NSLayoutConstraint(
                item: visibilityLabel,
                attribute: .centerY,
                relatedBy: .equal,
                toItem: authorNameLabel,
                attribute: .centerY,
                multiplier: 1.0,
                constant: 0.0
            )
        )

        contentView.addConstraints(
            NSLayoutConstraint.constraints(
                withVisualFormat: "V:|[actionButton]|",
                options: NSLayoutFormatOptions(rawValue: 0),
                metrics: nil,
                views: bindings
            )
        )

        contentView.addConstraint(
            NSLayoutConstraint(
                item: actionButton,
                attribute: .centerY,
                relatedBy: .equal,
                toItem: contentView,
                attribute: .centerY,
                multiplier: 1.0,
                constant: 0.0
            )
        )
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)

        coverImageView.backgroundColor = PlaylistTableViewCell.coverImageViewBackgroundColor
        visibilityLabel.backgroundColor = visibilityLabelBackgroundColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        coverImageView.backgroundColor = PlaylistTableViewCell.coverImageViewBackgroundColor
        visibilityLabel.backgroundColor = visibilityLabelBackgroundColor
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        var visibilityLabelWidth = CGFloat(0.0)

        if let text = visibilityLabel.text {
            let constrainedSize = CGSize(
                width: bounds.size.width,
                height: .greatestFiniteMagnitude
            )
            let visibilityLabelRect = text.boundingRect(
                with: constrainedSize,
                options: .usesLineFragmentOrigin,
                attributes: [NSFontAttributeName: visibilityLabel.font],
                context: nil
            )
            visibilityLabelWidth = ceil(visibilityLabelRect.size.width) + PlaylistTableViewCell.visibilityLabelPadding * 2
        }

        titleLabelTopSpaceConstraint.constant = (bounds.size.height - titleLabel.font.lineHeight - authorNameLabel.font.lineHeight - PlaylistTableViewCell.authorNameLabelTopSpace) / 2
        visibilityLabelWidthSpaceConstraint.constant = visibilityLabelWidth
    }

    func updateContent(playlist: Playlist) {
        if let urlString = playlist.coverImageUrl, let url = URL(string: urlString) {
            coverImageView.sd_setImage(with: url)
        }

        titleLabel.text = playlist.title
        authorNameLabel.text = String(format: "by".localized, playlist.author.name())
        tracksCountLabel.text = String(format: "%@ %@", playlist.tracksCount, "tracks".localized)
        favoriteIconImageView.isHidden = !playlist.isFavorite

        if playlist.isPublic {
            visibilityLabelBackgroundColor = PlaylistTableViewCell.publicVisibilityBackgroundColor
            visibilityLabel.text = String(format: "%@ %@", playlist.followersCount, "followers".localized)
        } else {
            visibilityLabelBackgroundColor = PlaylistTableViewCell.privateVisibilityBackgroundColor
            visibilityLabel.text = "private".localized
        }

        visibilityLabel.backgroundColor = visibilityLabelBackgroundColor
    }
}
