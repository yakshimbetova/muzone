//
//  PlaylistTableViewCellObjc.h
//  Muzone
//
//  Created by Macbook on 5/8/17.
//  Copyright © 2017 Muzone LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlaylistTableViewCellObjc : UITableViewCell

@property (nonnull, nonatomic, strong) UILabel *titleLabel;
@property (nonnull, nonatomic, strong) UIButton *deleteButton;

- (void)styleAnimated:(BOOL)animated;


@end
