//
//  PlaylistTableViewCellObjc.m
//  Muzone
//
//  Created by Macbook on 5/8/17.
//  Copyright © 2017 Muzone LLC. All rights reserved.
//

#import "PlaylistTableViewCellObjc.h"
#define kDeleteButtonWidth  60.0f

@interface PlaylistTableViewCellObjc ()
@property (nonnull, nonatomic, strong) NSLayoutConstraint *deleteButtonTrailingSpaceConstraint;
@end

@implementation PlaylistTableViewCellObjc

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        [self.contentView addSubview:self.titleLabel];
        [self.contentView addSubview:self.deleteButton];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-space-[titleLabel]-space-[deleteButton(width)]"
                                                                                 options:0
                                                                                 metrics:@{@"space": @(15.0f),
                                                                                           @"width": @(kDeleteButtonWidth)}
                                                                                   views:@{@"titleLabel": self.titleLabel,
                                                                                           @"deleteButton": self.deleteButton}]];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.titleLabel
                                                                     attribute:NSLayoutAttributeCenterY
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.contentView
                                                                     attribute:NSLayoutAttributeCenterY
                                                                    multiplier:1.0f
                                                                      constant:0.0f]];
        
        
        
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[deleteButton]|"
                                                                                 options:0
                                                                                 metrics:nil
                                                                                   views:@{@"deleteButton": self.deleteButton}]];


        
        self.deleteButtonTrailingSpaceConstraint = [NSLayoutConstraint constraintWithItem:self.contentView
                                                                                attribute:NSLayoutAttributeTrailing
                                                                                relatedBy:NSLayoutRelationEqual
                                                                                   toItem:self.deleteButton
                                                                                attribute:NSLayoutAttributeTrailing
                                                                               multiplier:1.0f
                                                                                 constant:0.0f];

        [self.contentView addConstraint:self.deleteButtonTrailingSpaceConstraint];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.deleteButton
                                                                     attribute:NSLayoutAttributeCenterY
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.contentView
                                                                     attribute:NSLayoutAttributeCenterY
                                                                    multiplier:1.0f
                                                                      constant:0.0f]];
        
    }
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (NSString *)reuseIdentifier {
    return NSStringFromClass(self.class);
}

#pragma mark - UILabel

- (UILabel *)titleLabel {
    if (_titleLabel == nil) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [_titleLabel setContentHuggingPriority:UILayoutPriorityDefaultLow
                                       forAxis:UILayoutConstraintAxisHorizontal];
        [_titleLabel setContentCompressionResistancePriority:UILayoutPriorityDefaultLow
                                                     forAxis:UILayoutConstraintAxisHorizontal];
        _titleLabel.font = [UIFont systemFontOfSize:17.0f];
        _titleLabel.textColor = [UIColor blackColor];
    }
    
    return _titleLabel;
}

#pragma mark - UIButton

- (UIButton *)deleteButton {
    if (_deleteButton == nil) {
        _deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _deleteButton.translatesAutoresizingMaskIntoConstraints = NO;
    }
    
    return _deleteButton;
}

#pragma mark -

- (void)styleAnimated:(BOOL)animated {
    if (self.editing) {
        self.deleteButtonTrailingSpaceConstraint.constant = 0.0f;
    } else {
        self.deleteButtonTrailingSpaceConstraint.constant = -kDeleteButtonWidth;
    }
    
    [self.contentView setNeedsUpdateConstraints];
    
    if (animated) {
        [UIView animateWithDuration:0.25f animations:^{
            if (self.editing) {
                self.deleteButton.alpha = 1.0f;
            } else {
                self.deleteButton.alpha = 0.0f;
            }
            
            [self.contentView layoutIfNeeded];
        }];
    } else {
        if (self.editing) {
            self.deleteButton.alpha = 1.0f;
        } else {
            self.deleteButton.alpha = 0.0f;
        }
        
        [self.contentView layoutIfNeeded];
    }
}


@end
