//
//  SearchResultsTableViewCell.h
//  Muzone
//
//  Created by Sukhrob Khakimov on 6/18/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchResultsTableViewCell : UITableViewCell

@property (nonnull, nonatomic, strong) UIActivityIndicatorView *activityIndicatorView;
@property (nonnull, nonatomic, strong) UIImageView *albumCoverImageView;
@property (nonnull, nonatomic, strong) UILabel *albumTitleLabel;
@property (nonnull, nonatomic, strong) UILabel *artistNameLabel;
@property (nonnull, nonatomic, strong) UILabel *trackTitleLabel;

@end
