//
//  SearchResultsTableViewCell.m
//  Muzone
//
//  Created by Sukhrob Khakimov on 6/18/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import "SearchResultsTableViewCell.h"

#define kImageSize CGSizeMake(44.0f, 44.0f)

@implementation SearchResultsTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        if ([reuseIdentifier isEqualToString:[NSString stringWithFormat:@"%@0", self.reuseIdentifier]]) {
            self.artistNameLabel.font = [UIFont systemFontOfSize:12.0f];
            self.artistNameLabel.textColor = [UIColor grayColor];
            
            [self.contentView addSubview:self.albumCoverImageView];
            [self.contentView addSubview:self.activityIndicatorView];
            [self.contentView addSubview:self.albumTitleLabel];
            [self.contentView addSubview:self.artistNameLabel];
            
            [self setupBaseConstraints];
            
            [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.artistNameLabel
                                                                         attribute:NSLayoutAttributeLeading
                                                                         relatedBy:NSLayoutRelationEqual
                                                                            toItem:self.albumTitleLabel
                                                                         attribute:NSLayoutAttributeLeading
                                                                        multiplier:1.0f
                                                                          constant:0.0f]];
            
            [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.artistNameLabel
                                                                         attribute:NSLayoutAttributeTrailing
                                                                         relatedBy:NSLayoutRelationEqual
                                                                            toItem:self.albumTitleLabel
                                                                         attribute:NSLayoutAttributeTrailing
                                                                        multiplier:1.0f
                                                                          constant:0.0f]];
            
            [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-space1-[albumTitleLabel]-space2-[artistNameLabel]"
                                                                                     options:0
                                                                                     metrics:@{@"space1": @(10.0f),
                                                                                               @"space2": @(2.0f)}
                                                                                       views:@{@"albumTitleLabel": self.albumTitleLabel,
                                                                                               @"artistNameLabel": self.artistNameLabel}]];
        } else if ([reuseIdentifier isEqualToString:[NSString stringWithFormat:@"%@1", self.reuseIdentifier]]) {
            [self.contentView addSubview:self.artistNameLabel];
            
            [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-space-[artistNameLabel]-space-|"
                                                                                     options:0
                                                                                     metrics:@{@"space": @(15.0f)}
                                                                                       views:@{@"artistNameLabel": self.artistNameLabel}]];
            
            [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.artistNameLabel
                                                                         attribute:NSLayoutAttributeCenterY
                                                                         relatedBy:NSLayoutRelationEqual
                                                                            toItem:self.contentView
                                                                         attribute:NSLayoutAttributeCenterY
                                                                        multiplier:1.0f
                                                                          constant:0.0f]];
        } else {
            self.albumTitleLabel.font = [UIFont systemFontOfSize:12.0f];
            self.albumTitleLabel.textColor = [UIColor grayColor];
            
            [self.contentView addSubview:self.albumCoverImageView];
            [self.contentView addSubview:self.activityIndicatorView];
            [self.contentView addSubview:self.trackTitleLabel];
            [self.contentView addSubview:self.albumTitleLabel];
            
            [self setupBaseConstraints];
            
            [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.trackTitleLabel
                                                                         attribute:NSLayoutAttributeLeading
                                                                         relatedBy:NSLayoutRelationEqual
                                                                            toItem:self.albumTitleLabel
                                                                         attribute:NSLayoutAttributeLeading
                                                                        multiplier:1.0f
                                                                          constant:0.0f]];
            
            [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.trackTitleLabel
                                                                         attribute:NSLayoutAttributeTrailing
                                                                         relatedBy:NSLayoutRelationEqual
                                                                            toItem:self.albumTitleLabel
                                                                         attribute:NSLayoutAttributeTrailing
                                                                        multiplier:1.0f
                                                                          constant:0.0f]];
            
            [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-space1-[trackTitleLabel]-space2-[albumTitleLabel]"
                                                                                     options:0
                                                                                     metrics:@{@"space1": @(10.0f),
                                                                                               @"space2": @(2.0f)}
                                                                                       views:@{@"trackTitleLabel": self.trackTitleLabel,
                                                                                               @"albumTitleLabel": self.albumTitleLabel}]];
        }
    }
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (NSString *)reuseIdentifier {
    return NSStringFromClass(self.class);
}

- (void)setupBaseConstraints {
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-space-[albumCoverImageView(width)]-space-[albumTitleLabel]-space-|"
                                                                             options:0
                                                                             metrics:@{@"space": @(15.0f),
                                                                                       @"width": @(kImageSize.width)}
                                                                               views:@{@"albumCoverImageView": self.albumCoverImageView,
                                                                                       @"albumTitleLabel": self.albumTitleLabel}]];
    
    [self.albumCoverImageView addConstraint:[NSLayoutConstraint constraintWithItem:self.albumCoverImageView
                                                                         attribute:NSLayoutAttributeHeight
                                                                         relatedBy:NSLayoutRelationEqual
                                                                            toItem:nil
                                                                         attribute:NSLayoutAttributeNotAnAttribute
                                                                        multiplier:1.0f
                                                                          constant:kImageSize.height]];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.albumCoverImageView
                                                                 attribute:NSLayoutAttributeCenterY
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.contentView
                                                                 attribute:NSLayoutAttributeCenterY
                                                                multiplier:1.0f
                                                                  constant:0.0f]];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.activityIndicatorView
                                                                 attribute:NSLayoutAttributeCenterX
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.albumCoverImageView
                                                                 attribute:NSLayoutAttributeCenterX
                                                                multiplier:1.0f
                                                                  constant:0.0f]];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.activityIndicatorView
                                                                 attribute:NSLayoutAttributeCenterY
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.albumCoverImageView
                                                                 attribute:NSLayoutAttributeCenterY
                                                                multiplier:1.0f
                                                                  constant:0.0f]];
}

#pragma mark - UIActivityIndicatorView

- (UIActivityIndicatorView *)activityIndicatorView {
    if (_activityIndicatorView == nil) {
        _activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        _activityIndicatorView.translatesAutoresizingMaskIntoConstraints = NO;
    }
    
    return _activityIndicatorView;
}

#pragma mark - UIImageView

- (UIImageView *)albumCoverImageView {
    if (_albumCoverImageView == nil) {
        _albumCoverImageView = [[UIImageView alloc] init];
        _albumCoverImageView.translatesAutoresizingMaskIntoConstraints = NO;
        _albumCoverImageView.contentMode = UIViewContentModeScaleAspectFill;
        _albumCoverImageView.layer.cornerRadius = 3.0f;
        _albumCoverImageView.clipsToBounds = YES;
    }
    
    return _albumCoverImageView;
}

#pragma mark - UILabel

- (UILabel *)albumTitleLabel {
    if (_albumTitleLabel == nil) {
        _albumTitleLabel = [[UILabel alloc] init];
        _albumTitleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _albumTitleLabel.font = [UIFont systemFontOfSize:17.0f];
        _albumTitleLabel.textColor = [UIColor blackColor];
    }
    
    return _albumTitleLabel;
}

- (UILabel *)artistNameLabel {
    if (_artistNameLabel == nil) {
        _artistNameLabel = [[UILabel alloc] init];
        _artistNameLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _artistNameLabel.font = [UIFont systemFontOfSize:17.0f];
        _artistNameLabel.textColor = [UIColor blackColor];
    }
    
    return _artistNameLabel;
}

- (UILabel *)trackTitleLabel {
    if (_trackTitleLabel == nil) {
        _trackTitleLabel = [[UILabel alloc] init];
        _trackTitleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _trackTitleLabel.font = [UIFont systemFontOfSize:17.0f];
        _trackTitleLabel.textColor = [UIColor blackColor];
    }
    
    return _trackTitleLabel;
}

@end
