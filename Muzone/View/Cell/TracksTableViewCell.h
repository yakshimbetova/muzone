//
//  TracksTableViewCell.h
//  Muzone
//
//  Created by Sukhrob Khakimov on 10/23/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface TracksTableViewCell : BaseTableViewCell

@property (nonnull, nonatomic, strong) UIImageView *iconImageView;
@property (nonnull, nonatomic, strong) UILabel *titleLabel;
@property (nonnull, nonatomic, strong) UILabel *artistNameLabel;
@property (nonnull, nonatomic, strong) UIButton *deleteButton;

- (void)styleAnimated:(BOOL)animated;

@end
