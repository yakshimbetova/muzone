//
//  TracksTableViewCell.m
//  Muzone
//
//  Created by Sukhrob Khakimov on 10/23/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import "TracksTableViewCell.h"

#define kHorizontalSpace    15.0f
#define kDeleteButtonWidth  60.0f

@interface TracksTableViewCell ()

@property (nonnull, nonatomic, strong) NSLayoutConstraint *deleteButtonTrailingSpaceConstraint;

@end

@implementation TracksTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        [self.contentView addSubview:self.iconImageView];
        [self.contentView addSubview:self.titleLabel];
        [self.contentView addSubview:self.artistNameLabel];
        [self.contentView addSubview:self.deleteButton];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-space-[iconImageView]-space-[titleLabel]-space-[deleteButton(width)]"
                                                                                 options:0
                                                                                 metrics:@{@"space": @(kHorizontalSpace),
                                                                                           @"width": @(kDeleteButtonWidth)}
                                                                                   views:@{@"iconImageView": self.iconImageView,
                                                                                           @"titleLabel": self.titleLabel,
                                                                                           @"deleteButton": self.deleteButton}]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-space1-[titleLabel]-space2-[artistNameLabel]"
                                                                                 options:0
                                                                                 metrics:@{@"space1": @(10.0f),
                                                                                           @"space2": @(2.0f)}
                                                                                   views:@{@"titleLabel": self.titleLabel,
                                                                                           @"artistNameLabel": self.artistNameLabel}]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[deleteButton]|"
                                                                                 options:0
                                                                                 metrics:nil
                                                                                   views:@{@"deleteButton": self.deleteButton}]];
        
        self.deleteButtonTrailingSpaceConstraint = [NSLayoutConstraint constraintWithItem:self.contentView
                                                                                attribute:NSLayoutAttributeTrailing
                                                                                relatedBy:NSLayoutRelationEqual
                                                                                   toItem:self.deleteButton
                                                                                attribute:NSLayoutAttributeTrailing
                                                                               multiplier:1.0f
                                                                                 constant:0.0f];
        [self.contentView addConstraint:self.deleteButtonTrailingSpaceConstraint];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.artistNameLabel
                                                                     attribute:NSLayoutAttributeLeading
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.titleLabel
                                                                     attribute:NSLayoutAttributeLeading
                                                                    multiplier:1.0f
                                                                      constant:0.0f]];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.artistNameLabel
                                                                     attribute:NSLayoutAttributeTrailing
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.titleLabel
                                                                     attribute:NSLayoutAttributeTrailing
                                                                    multiplier:1.0f
                                                                      constant:0.0f]];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.iconImageView
                                                                     attribute:NSLayoutAttributeCenterY
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.contentView
                                                                     attribute:NSLayoutAttributeCenterY
                                                                    multiplier:1.0f
                                                                      constant:0.0f]];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.iconImageView
                                                                     attribute:NSLayoutAttributeCenterY
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.contentView
                                                                     attribute:NSLayoutAttributeCenterY
                                                                    multiplier:1.0f
                                                                      constant:0.0f]];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.deleteButton
                                                                     attribute:NSLayoutAttributeCenterY
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.contentView
                                                                     attribute:NSLayoutAttributeCenterY
                                                                    multiplier:1.0f
                                                                      constant:0.0f]];
    }
    
    return self;
}

#pragma mark -

- (void)styleAnimated:(BOOL)animated {
    if (self.editing) {
        self.deleteButtonTrailingSpaceConstraint.constant = 0.0f;
    } else {
        self.deleteButtonTrailingSpaceConstraint.constant = -kDeleteButtonWidth;
    }
    
    [self.contentView setNeedsUpdateConstraints];
    
    if (animated) {
        [UIView animateWithDuration:0.25f animations:^{
            if (self.editing) {
                self.deleteButton.alpha = 1.0f;
            } else {
                self.deleteButton.alpha = 0.0f;
            }
            
            [self.contentView layoutIfNeeded];
        }];
    } else {
        if (self.editing) {
            self.deleteButton.alpha = 1.0f;
        } else {
            self.deleteButton.alpha = 0.0f;
        }
        
        [self.contentView layoutIfNeeded];
    }
}

#pragma mark - UIImageView

- (UIImageView *)iconImageView {
    if (_iconImageView == nil) {
        _iconImageView = [[UIImageView alloc] init];
        _iconImageView.translatesAutoresizingMaskIntoConstraints = NO;
        [_iconImageView setContentHuggingPriority:UILayoutPriorityDefaultHigh
                                          forAxis:UILayoutConstraintAxisHorizontal];
        [_iconImageView setContentCompressionResistancePriority:UILayoutPriorityDefaultHigh
                                                        forAxis:UILayoutConstraintAxisHorizontal];
        _iconImageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    
    return _iconImageView;
}

#pragma mark - UILabel

- (UILabel *)titleLabel {
    if (_titleLabel == nil) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [_titleLabel setContentHuggingPriority:UILayoutPriorityDefaultLow
                                       forAxis:UILayoutConstraintAxisHorizontal];
        [_titleLabel setContentCompressionResistancePriority:UILayoutPriorityDefaultLow
                                                     forAxis:UILayoutConstraintAxisHorizontal];
        _titleLabel.font = [UIFont systemFontOfSize:17.0f];
        _titleLabel.textColor = [UIColor blackColor];
    }
    
    return _titleLabel;
}

- (UILabel *)artistNameLabel {
    if (_artistNameLabel == nil) {
        _artistNameLabel = [[UILabel alloc] init];
        _artistNameLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _artistNameLabel.font = [UIFont systemFontOfSize:12.0f];
        _artistNameLabel.textColor = [UIColor grayColor];
    }
    
    return _artistNameLabel;
}

#pragma mark - UIButton

- (UIButton *)deleteButton {
    if (_deleteButton == nil) {
        _deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _deleteButton.translatesAutoresizingMaskIntoConstraints = NO;
    }
    
    return _deleteButton;
}

@end
