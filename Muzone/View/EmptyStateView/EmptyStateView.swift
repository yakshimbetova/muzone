//
//  EmptyStateView.swift
//  Muzone
//
//  Created by Sukhrob Khakimov on 10/28/17.
//  Copyright © 2017 Muzone LLC. All rights reserved.
//

import UIKit

protocol EmptyStateViewDelegate: class {
    func emptyStateView(_ emptyStateView: EmptyStateView, didTap actionButton: UIButton)
}

class EmptyStateView: UIView {
    static let horizontalSpace = CGFloat(20.0)
    static let verticalSpace = CGFloat(15.0)
    static let iconImageSize = CGSize(width: 64.0, height: 64.0)
    static let actionButtonHeight = CGFloat(44.0)

    weak var delegate: EmptyStateViewDelegate?

    lazy fileprivate var iconImageView: UIImageView = {
        var iconImageView = UIImageView()
        iconImageView.translatesAutoresizingMaskIntoConstraints = false
        iconImageView.contentMode = .scaleAspectFit

        return iconImageView
    }()

    lazy fileprivate var messageLabel: UILabel = {
        var messageLabel = UILabel()
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        messageLabel.font = UIFont(name: "YanoneKaffeesatz-Regular", size: 20.0) // FIXME: load from theme
        messageLabel.textColor = .rgb(value: 0.0, alpha: 0.87)
        messageLabel.textAlignment = .center
        messageLabel.numberOfLines = 0

        return messageLabel
    }()

    lazy fileprivate var actionButton: UIButton = {
        var actionButton = UIButton(type: .custom)
        actionButton.translatesAutoresizingMaskIntoConstraints = false
        actionButton.titleLabel?.font = UIFont(name: "YanoneKaffeesatz-Regular", size: 16.0) // FIXME: load from theme
        actionButton.setTitleColor(.white, for: .normal)
        actionButton.setTitleColor(
            .rgb(value: 255.0, alpha: 0.5),
            for: .highlighted
        )
        actionButton.setBackgroundImage(
            UIImage(color: .rgb(red: 192.0, green: 202.0, blue: 51.0)),
            for: .normal
        )
        actionButton.setBackgroundImage(
            UIImage(color: .rgb(red: 192.0, green: 202.0, blue: 51.0, alpha: 0.5)),
            for: .highlighted
        )
        actionButton.addTarget(
            self,
            action: #selector(touchUpInside(actionButton:)),
            for: .touchUpInside
        )

        return actionButton
    }()

    init() {
        super.init(frame: .zero)

        translatesAutoresizingMaskIntoConstraints = false
    }

    override init(frame: CGRect) {
        fatalError("init(frame:) has not been implemented")
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    fileprivate func setupViews(superView: UIView) {
        let bindings: [String: AnyObject] = [
            "view": self,
            "iconImageView": iconImageView,
            "messageLabel": messageLabel,
            "actionButton": actionButton
        ]

        superView.addSubview(self)
        addSubview(iconImageView)
        addSubview(messageLabel)
        addSubview(actionButton)

        superView.addConstraints(
            NSLayoutConstraint.constraints(
                withVisualFormat: "H:|-space-[view]-space-|",
                options: NSLayoutFormatOptions(rawValue: 0),
                metrics: ["space": EmptyStateView.horizontalSpace],
                views: bindings
            )
        )

        superView.addConstraint(
            NSLayoutConstraint(
                item: self,
                attribute: .centerY,
                relatedBy: .equal,
                toItem: superView,
                attribute: .centerY,
                multiplier: 1.0,
                constant: 0.0
            )
        )

        addConstraint(
            NSLayoutConstraint(
                item: iconImageView,
                attribute: .centerX,
                relatedBy: .equal,
                toItem: self,
                attribute: .centerX,
                multiplier: 1.0,
                constant: 0.0
            )
        )

        iconImageView.addConstraint(
            NSLayoutConstraint(
                item: iconImageView,
                attribute: .width,
                relatedBy: .equal,
                toItem: nil,
                attribute: .notAnAttribute,
                multiplier: 1.0,
                constant: EmptyStateView.iconImageSize.width
            )
        )

        addConstraints(
            NSLayoutConstraint.constraints(
                withVisualFormat: "H:|[messageLabel]|",
                options: NSLayoutFormatOptions(rawValue: 0),
                metrics: nil,
                views: bindings
            )
        )

        addConstraints(
            NSLayoutConstraint.constraints(
                withVisualFormat: "H:|[actionButton]|",
                options: NSLayoutFormatOptions(rawValue: 0),
                metrics: nil,
                views: bindings
            )
        )

        addConstraints(
            NSLayoutConstraint.constraints(
                withVisualFormat: "V:|[iconImageView(height1)]-space-[messageLabel]-space-[actionButton(height2)]|",
                options: NSLayoutFormatOptions(rawValue: 0),
                metrics: [
                    "space": EmptyStateView.verticalSpace,
                    "height1": EmptyStateView.iconImageSize.height,
                    "height2": EmptyStateView.actionButtonHeight
                ],
                views: bindings
            )
        )
    }

    // MARK: - UIButton

    func touchUpInside(actionButton: UIButton) {
        delegate?.emptyStateView(self, didTap: actionButton)
    }

    // MARK: -

    func show(in view: UIView, iconImage: UIImage?, message: String?, actionTitle: String?) {
        iconImageView.image = iconImage
        messageLabel.text = message
        actionButton.setTitle(actionTitle, for: .normal)

        setupViews(superView: view)
    }

    func hide() {
        removeFromSuperview()
    }
}
