//
//  PageCollectionViewCollectionReusableView.h
//  Muzone
//
//  Created by Sukhrob Khakimov on 5/31/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PageCollectionViewCollectionReusableView : UICollectionReusableView

@property (nonnull, nonatomic, strong) UILabel *titleLabel;

@end
