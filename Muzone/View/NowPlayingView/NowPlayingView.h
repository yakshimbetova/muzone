//
//  NowPlayingView.h
//  Muzone
//
//  Created by Sukhrob Khakimov on 7/3/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NowPlayingViewDelegate;

@class Track;

@interface NowPlayingView : UIView

@property (nullable, nonatomic, weak) id<NowPlayingViewDelegate> delegate;
@property (nonnull, nonatomic, strong) Track *track;
@property (nonatomic, assign, readonly) CGFloat height;

- (nonnull instancetype)initWithTrack:(nonnull Track *)track;

- (void)showInView:(nonnull UIView *)view animated:(BOOL)animated;
- (void)hideAnimated:(BOOL)animated;
- (void)play;
- (void)pause;
- (void)stop;

@end

@protocol NowPlayingViewDelegate <NSObject>

@optional
- (void)nowPlayingViewWillPlay:(nonnull NowPlayingView *)nowPlayingView;
- (void)nowPlayingViewWillPause:(nonnull NowPlayingView *)nowPlayingView;
- (void)nowPlayingViewWillStop:(nonnull NowPlayingView *)nowPlayingView;
- (void)nowPlayingViewWillDisappear:(nonnull NowPlayingView *)nowPlayingView;

@end
