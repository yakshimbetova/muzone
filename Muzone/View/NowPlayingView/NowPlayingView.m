//
//  NowPlayingView.m
//  Muzone
//
//  Created by Sukhrob Khakimov on 7/3/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import <SDWebImage/UIImageView+WebCache.h>
#import "BaseTheme.h"

#import "Album.h"
#import "Artist.h"
#import "Track.h"

#import "MarqueeLabel.h"
#import "NowPlayingView.h"

#define kHorizontalSpace            10.0f
#define kArtistNameLabelTopSpace    4.0f
#define kAlbumCoverImageViewSize    CGSizeMake(60.0f, 60.0f)
#define kPlayPauseButtonSize        CGSizeMake(44.0f, 44.0f)

@interface NowPlayingView ()

@property (nonnull, nonatomic, strong) UIView *separatorView;
@property (nonnull, nonatomic, strong) UIImageView *albumCoverImageView;
@property (nonnull, nonatomic, strong) MarqueeLabel *trackTitleLabel;
@property (nonnull, nonatomic, strong) MarqueeLabel *artistNameLabel;
@property (nonnull, nonatomic, strong) UIButton *playPauseButton;

@property (nonnull, nonatomic, strong) NSLayoutConstraint *topSpaceConstraint;

@end

@implementation NowPlayingView

- (instancetype)initWithTrack:(Track *)track {
    self = [super init];
    
    if (self) {
        self.translatesAutoresizingMaskIntoConstraints = NO;
        self.backgroundColor = [UIColor whiteColor];
        
        self.track = track;
        _height = 80.0f;
        
        // Gestures
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                               action:@selector(handleTapGestureRecognizer)];
        [self addGestureRecognizer:tapGestureRecognizer];
        
        UISwipeGestureRecognizer *swipeUpGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                                                                       action:@selector(handleSwipeUpGestureRecognizer)];
        swipeUpGestureRecognizer.direction = UISwipeGestureRecognizerDirectionUp;
        [self addGestureRecognizer:swipeUpGestureRecognizer];
        
        UISwipeGestureRecognizer *swipeDownGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                                                                         action:@selector(handleSwipeDownGestureRecognizer)];
        swipeDownGestureRecognizer.direction = UISwipeGestureRecognizerDirectionDown;
        [self addGestureRecognizer:swipeDownGestureRecognizer];
        
        UISwipeGestureRecognizer *swipeLeftGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                                                                         action:@selector(handleSwipeLeftGestureRecognizer)];
        swipeLeftGestureRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
        [self addGestureRecognizer:swipeLeftGestureRecognizer];
        
        UISwipeGestureRecognizer *swipeRightGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                                                                          action:@selector(handleSwipeRightGestureRecognizer)];
        swipeRightGestureRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
        [self addGestureRecognizer:swipeRightGestureRecognizer];
    }
    
    return self;
}

- (void)setTrack:(Track *)track {
    _track = track;
    
    __weak NowPlayingView *weakSelf = self;
    
    [self.albumCoverImageView sd_setImageWithURL:[NSURL URLWithString:self.track.imageUrl] placeholderImage:[UIImage imageNamed:@"img_placeholder.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (image) {
            if (cacheType == SDImageCacheTypeNone) {
                weakSelf.albumCoverImageView.alpha = 0.0f;
                [UIView animateWithDuration:0.25f animations:^{
                    weakSelf.albumCoverImageView.alpha = 1.0f;
                } completion:^(BOOL finished) {
                    weakSelf.albumCoverImageView.image = image;
                }];
            } else {
                weakSelf.albumCoverImageView.image = image;
            }
        }
    }];
    self.trackTitleLabel.text = self.track.title;
    self.artistNameLabel.text = self.track.artist.name;
}

- (void)showInView:(UIView *)view animated:(BOOL)animated {
    [self play];
    
    if (![view.subviews containsObject:self]) {
        CGFloat topSpace = (_height - self.trackTitleLabel.font.lineHeight - self.artistNameLabel.font.lineHeight - kArtistNameLabelTopSpace) / 2;
        
        [view addSubview:self];
        [self addSubview:self.separatorView];
        [self addSubview:self.albumCoverImageView];
        [self addSubview:self.trackTitleLabel];
        [self addSubview:self.artistNameLabel];
        [self addSubview:self.playPauseButton];
        
        [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|"
                                                                     options:0
                                                                     metrics:nil
                                                                       views:@{@"view": self}]];
        
        [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[view(height)]"
                                                                     options:0
                                                                     metrics:@{@"height": @(_height)}
                                                                       views:@{@"view": self}]];
        
        self.topSpaceConstraint = [NSLayoutConstraint constraintWithItem:self
                                                               attribute:NSLayoutAttributeTop
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:view
                                                               attribute:NSLayoutAttributeBottom
                                                              multiplier:1.0f
                                                                constant:0.0f];
        [view addConstraint:self.topSpaceConstraint];
        
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[separatorView]|"
                                                                     options:0
                                                                     metrics:nil
                                                                       views:@{@"separatorView": self.separatorView}]];
        
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[separatorView(height)]"
                                                                     options:0
                                                                     metrics:@{@"height": @([[BaseTheme currentTheme] separatorHeight])}
                                                                       views:@{@"separatorView": self.separatorView}]];
        
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-space-[albumCoverImageView(width1)]-space-[trackTitleLabel]-space-[playPauseButton(width2)]-space-|"
                                                                     options:0
                                                                     metrics:@{@"space": @(kHorizontalSpace),
                                                                               @"width1": @(kAlbumCoverImageViewSize.width),
                                                                               @"width2": @(kPlayPauseButtonSize.width)}
                                                                       views:@{@"albumCoverImageView": self.albumCoverImageView,
                                                                               @"trackTitleLabel": self.trackTitleLabel,
                                                                               @"playPauseButton": self.playPauseButton}]];
        
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-space1-[trackTitleLabel]-space2-[artistNameLabel]"
                                                                     options:0
                                                                     metrics:@{@"space1": @(topSpace),
                                                                               @"space2": @(kArtistNameLabelTopSpace)}
                                                                       views:@{@"trackTitleLabel": self.trackTitleLabel,
                                                                               @"artistNameLabel": self.artistNameLabel}]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.artistNameLabel
                                                         attribute:NSLayoutAttributeLeading
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self.trackTitleLabel
                                                         attribute:NSLayoutAttributeLeading
                                                        multiplier:1.0f
                                                          constant:0.0f]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.artistNameLabel
                                                         attribute:NSLayoutAttributeTrailing
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self.trackTitleLabel
                                                         attribute:NSLayoutAttributeTrailing
                                                        multiplier:1.0f
                                                          constant:0.0f]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.albumCoverImageView
                                                         attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeCenterY
                                                        multiplier:1.0f
                                                          constant:0.0f]];
        
        [self.albumCoverImageView addConstraint:[NSLayoutConstraint constraintWithItem:self.albumCoverImageView
                                                                             attribute:NSLayoutAttributeHeight
                                                                             relatedBy:NSLayoutRelationEqual
                                                                                toItem:nil
                                                                             attribute:NSLayoutAttributeNotAnAttribute
                                                                            multiplier:1.0f
                                                                              constant:kAlbumCoverImageViewSize.height]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.playPauseButton
                                                         attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeCenterY
                                                        multiplier:1.0f
                                                          constant:0.0f]];
        
        [self.playPauseButton addConstraint:[NSLayoutConstraint constraintWithItem:self.playPauseButton
                                                                         attribute:NSLayoutAttributeHeight
                                                                         relatedBy:NSLayoutRelationEqual
                                                                            toItem:nil
                                                                         attribute:NSLayoutAttributeNotAnAttribute
                                                                        multiplier:1.0f
                                                                          constant:kPlayPauseButtonSize.height]];
        
        if (animated) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.alpha = 0.0f;
                self.topSpaceConstraint.constant = -_height;
                [self.superview setNeedsUpdateConstraints];
                
                [UIView animateWithDuration:0.25f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^{
                    self.alpha = 1.0f;
                    [self.superview layoutIfNeeded];
                } completion:nil];
            });
        } else {
            self.topSpaceConstraint.constant = -_height;
            [self.superview setNeedsUpdateConstraints];
            [self.superview layoutIfNeeded];
        }
    }
}

- (void)hideAnimated:(BOOL)animated {
    if (animated) {
        self.alpha = 1.0f;
        self.topSpaceConstraint.constant = 0.0f;
        [self.superview setNeedsUpdateConstraints];
        
        [UIView animateWithDuration:0.25f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.alpha = 0.0f;
            [self.superview layoutIfNeeded];
        } completion:^(BOOL finished) {
            [self removeAllViews];
            self.playPauseButton.selected = NO;
        }];
    } else {
        [self removeAllViews];
        self.playPauseButton.selected = NO;
    }
}

- (void)removeAllViews {
    for (UIView *subview in self.subviews) {
        [subview removeFromSuperview];
    }
    
    [self removeFromSuperview];
}

- (void)play {
    self.playPauseButton.selected = YES;
    [self.trackTitleLabel unpauseLabel];
    [self.artistNameLabel unpauseLabel];
}

- (void)pause {
    self.playPauseButton.selected = NO;
    [self.trackTitleLabel pauseLabel];
    [self.artistNameLabel pauseLabel];
}

- (void)stop {
    [self.trackTitleLabel pauseLabel];
    [self.artistNameLabel pauseLabel];
    [self hideAnimated:YES];
}

#pragma mark - UIGestureRecognizer

- (void)handleTapGestureRecognizer {
    if ([self.delegate respondsToSelector:@selector(nowPlayingViewWillDisappear:)]) {
        [self.delegate nowPlayingViewWillDisappear:self];
    }
    
    [self hideAnimated:NO];
}

- (void)handleSwipeUpGestureRecognizer {
    if ([self.delegate respondsToSelector:@selector(nowPlayingViewWillDisappear:)]) {
        [self.delegate nowPlayingViewWillDisappear:self];
    }
    
    [self hideAnimated:NO];
}

- (void)handleSwipeDownGestureRecognizer {
    if ([self.delegate respondsToSelector:@selector(nowPlayingViewWillStop:)]) {
        [self.delegate nowPlayingViewWillStop:self];
    }
    
    [self hideAnimated:YES];
}

- (void)handleSwipeLeftGestureRecognizer {
    if ([self.delegate respondsToSelector:@selector(nowPlayingViewWillStop:)]) {
        [self.delegate nowPlayingViewWillStop:self];
    }
    
    [self hideAnimated:YES];
}

- (void)handleSwipeRightGestureRecognizer {
    if ([self.delegate respondsToSelector:@selector(nowPlayingViewWillStop:)]) {
        [self.delegate nowPlayingViewWillStop:self];
    }
    
    [self hideAnimated:YES];
}

#pragma mark - UIView

- (UIView *)separatorView {
    if (_separatorView == nil) {
        _separatorView = [[UIView alloc] init];
        _separatorView.translatesAutoresizingMaskIntoConstraints = NO;
        _separatorView.backgroundColor = [[BaseTheme currentTheme] separatorColor];
    }
    
    return _separatorView;
}

#pragma mark - UIImageView

- (UIImageView *)albumCoverImageView {
    if (_albumCoverImageView == nil) {
        _albumCoverImageView = [[UIImageView alloc] init];
        _albumCoverImageView.translatesAutoresizingMaskIntoConstraints = NO;
        _albumCoverImageView.contentMode = UIViewContentModeScaleAspectFit;
        _albumCoverImageView.layer.cornerRadius = 3.0f;
        _albumCoverImageView.clipsToBounds = YES;
    }
    
    return _albumCoverImageView;
}

#pragma mark - UILabel

- (MarqueeLabel *)trackTitleLabel {
    if (_trackTitleLabel == nil) {
        _trackTitleLabel = [[MarqueeLabel alloc] initWithFrame:CGRectZero
                                                          rate:20.0f
                                                 andFadeLength:10.0f];
        _trackTitleLabel.marqueeType = MLLeftRight;
        _trackTitleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _trackTitleLabel.font = [UIFont systemFontOfSize:17.0f];
        _trackTitleLabel.textColor = [UIColor blackColor];
    }
    
    return _trackTitleLabel;
}

- (MarqueeLabel *)artistNameLabel {
    if (_artistNameLabel == nil) {
        _artistNameLabel = [[MarqueeLabel alloc] initWithFrame:CGRectZero
                                                          rate:20.0f
                                                 andFadeLength:10.0f];
        _artistNameLabel.marqueeType = MLLeftRight;
        _artistNameLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _artistNameLabel.font = [UIFont systemFontOfSize:12.0f];
        _artistNameLabel.textColor = [UIColor darkGrayColor];
    }
    
    return _artistNameLabel;
}

#pragma mark - UIButton

- (UIButton *)playPauseButton {
    if (_playPauseButton == nil) {
        _playPauseButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _playPauseButton.translatesAutoresizingMaskIntoConstraints = NO;
        [_playPauseButton addTarget:self
                             action:@selector(playPauseButtonTapped:)
                   forControlEvents:UIControlEventTouchUpInside];
        [_playPauseButton setImage:[UIImage imageNamed:@"ic_play_green.png"]
                          forState:UIControlStateNormal];
        [_playPauseButton setImage:[UIImage imageNamed:@"ic_pause_green.png"]
                          forState:UIControlStateSelected];
    }
    
    return _playPauseButton;
}

- (void)playPauseButtonTapped:(id)sender {
    if (self.playPauseButton.selected) {
        if ([self.delegate respondsToSelector:@selector(nowPlayingViewWillPause:)]) {
            [self.delegate nowPlayingViewWillPause:self];
        }
        
        self.playPauseButton.selected = NO;
        [self.trackTitleLabel pauseLabel];
        [self.artistNameLabel pauseLabel];
    } else {
        if ([self.delegate respondsToSelector:@selector(nowPlayingViewWillPlay:)]) {
            [self.delegate nowPlayingViewWillPlay:self];
        }
        
        self.playPauseButton.selected = YES;
        [self.trackTitleLabel unpauseLabel];
        [self.artistNameLabel unpauseLabel];
    }
}

@end
