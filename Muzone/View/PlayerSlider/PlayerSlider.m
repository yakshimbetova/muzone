//
//  PlayerSlider.m
//  Muzone
//
//  Created by Sukhrob Khakimov on 5/25/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import "BaseTheme.h"
#import "PlayerSlider.h"

@implementation PlayerSlider

- (instancetype)init {
    self = [super init];
    
    if (self) {
        UIImage *thumbImage = [[UIImage imageNamed:@"ic_slider.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [self setThumbImage:thumbImage
                   forState:UIControlStateNormal];
        [self setThumbImage:thumbImage
                   forState:UIControlStateHighlighted];
        self.tintColor = [[BaseTheme currentTheme] controlTintColor];
    }
    
    return self;
}

- (CGRect)thumbRectForBounds:(CGRect)bounds trackRect:(CGRect)rect value:(float)value {
    rect.origin.x = rect.origin.x - 10.0f;
    rect.size.width = rect.size.width + 20.0f;

    return CGRectInset([super thumbRectForBounds:bounds trackRect:rect value:value], 10.0f, 10.0f);
}

@end
