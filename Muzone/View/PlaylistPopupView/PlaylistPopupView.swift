//
//  PlaylistPopupView.swift
//  Muzone
//
//  Created by Sukhrob Khakimov on 10/24/17.
//  Copyright © 2017 Muzone LLC. All rights reserved.
//

import UIKit

protocol PlaylistPopupViewDelegate: class {
    func playlistPopupViewDidCancel(_ playlistPopupView: PlaylistPopupView)
    func playlistPopupViewDidFinish(_ playlistPopupView: PlaylistPopupView)
}

extension PlaylistPopupViewDelegate {
    func playlistPopupViewDidCancel(_ playlistPopupView: PlaylistPopupView) {}
    func playlistPopupViewDidFinish(_ playlistPopupView: PlaylistPopupView) {}
}

class PlaylistPopupView: UIView {
    fileprivate let horizontalSpace = CGFloat(30.0)
    fileprivate let verticalSpace = CGFloat(20.0)
    fileprivate let containerViewSize = CGSize(width: 280.0, height: 232.0)
    fileprivate let containerViewBottomSpace = CGFloat(20.0)
    fileprivate let playlistTitleLimit = 50
    fileprivate let playlistTitleTextFieldHeight = CGFloat(36.0)
    fileprivate let separatorViewHeight = CGFloat(1.0)
    fileprivate let playlistVisibilityButtonSize = CGSize(width: 25.0, height: 25.0)
    fileprivate let actionButtonHeight = CGFloat(44.0)

    fileprivate(set) var playlist: Playlist!

    var animationDuration: TimeInterval = 0.2
    var animationDelay: TimeInterval = 0.2
    var isNew: Bool = false {
        didSet {
            if isNew {
                titleLabel.text = "create_new_playlist".localized
                doneButton.setTitle(
                    "create".localized.uppercased(with: Locale.current),
                    for: .normal
                )
            } else {
                titleLabel.text = "edit_playlist".localized
                doneButton.setTitle(
                    "save".localized.uppercased(with: Locale.current),
                    for: .normal
                )
            }
        }
    }

    weak var delegate: PlaylistPopupViewDelegate?

    fileprivate var containerViewCenterYConstraint: NSLayoutConstraint!

    lazy fileprivate var tapGestureRecognizer: UITapGestureRecognizer = {
        var tapGestureRecognizer = UITapGestureRecognizer()
        tapGestureRecognizer.delegate = self
        tapGestureRecognizer.addTarget(
            self, action:
            #selector(handle(tapGestureRecognizer:))
        )

        return tapGestureRecognizer
    }()

    lazy fileprivate var containerView: UIView = {
        var containerView = UIView()
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.backgroundColor = .white
        containerView.layer.cornerRadius = 3.0
        containerView.layer.shadowColor = UIColor.black.cgColor
        containerView.layer.shadowOpacity = 0.8
        containerView.layer.shadowRadius = 20.0
        containerView.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)

        return containerView
    }()

    lazy fileprivate var titleLabel: UILabel = {
        var titleLabel = UILabel()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.font = UIFont(name: "YanoneKaffeesatz-Regular", size: 24.0) // FIXME: load from theme
        titleLabel.textColor = .rgb(value: 0.0, alpha: 0.87)
        titleLabel.text = "edit_playlist".localized

        return titleLabel
    }()

    lazy fileprivate var playlistTitleLabel: UILabel = {
        var playlistTitleLabel = UILabel()
        playlistTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        playlistTitleLabel.font = UIFont(name: "YanoneKaffeesatz-Regular", size: 13.0) // FIXME: load from theme
        playlistTitleLabel.textColor = .rgb(value: 0.0, alpha: 0.38)
        playlistTitleLabel.text = "give_playlist_name".localized

        return playlistTitleLabel
    }()

    lazy fileprivate var playlistTitleTextField: UITextField = {
        var playlistTitleTextField = UITextField()
        playlistTitleTextField.translatesAutoresizingMaskIntoConstraints = false
        playlistTitleTextField.font = UIFont(name: "YanoneKaffeesatz-Regular", size: 16.0) // FIXME: load from theme
        playlistTitleTextField.textColor = .rgb(value: 0.0, alpha: 0.87)
        playlistTitleTextField.clearButtonMode = .whileEditing
        playlistTitleTextField.delegate = self

        return playlistTitleTextField
    }()

    lazy fileprivate var separatorView: UIView = {
        var separatorView = UIView()
        separatorView.translatesAutoresizingMaskIntoConstraints = false
        separatorView.backgroundColor = .rgb(red: 192.0, green: 202.0, blue: 51.0)

        return separatorView
    }()

    lazy fileprivate var playlistTitleLimitLabel: UILabel = {
        var playlistTitleLimitLabel = UILabel()
        playlistTitleLimitLabel.translatesAutoresizingMaskIntoConstraints = false
        playlistTitleLimitLabel.font = UIFont(name: "YanoneKaffeesatz-Regular", size: 13.0) // FIXME: load from theme
        playlistTitleLimitLabel.textColor = .rgb(value: 0.0, alpha: 0.38)

        return playlistTitleLimitLabel
    }()

    lazy fileprivate var playlistVisibilityButton: UIButton = {
        var playlistVisibilityButton = UIButton(type: .custom)
        playlistVisibilityButton.translatesAutoresizingMaskIntoConstraints = false
        playlistVisibilityButton.setImage(
            UIImage(named: "ic_checkbox_blank.png")?.withRenderingMode(.alwaysTemplate),
            for: .normal
        )
        playlistVisibilityButton.setImage(
            UIImage(named: "ic_checkbox_blank.png")?.withRenderingMode(.alwaysTemplate),
            for: .highlighted
        )
        playlistVisibilityButton.setImage(
            UIImage(named: "ic_checkbox.png")?.withRenderingMode(.alwaysTemplate),
            for: .selected
        )
        playlistVisibilityButton.imageView?.tintColor = .rgb(red: 192.0, green: 202.0, blue: 51.0)
        playlistVisibilityButton.addTarget(
            self,
            action: #selector(touchUpInside(playlistVisibilityButton:)),
            for: .touchUpInside
        )

        return playlistVisibilityButton
    }()

    lazy fileprivate var playlistVisibilityTitleLabel: UILabel = {
        var playlistVisibilityTitleLabel = UILabel()
        playlistVisibilityTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        playlistVisibilityTitleLabel.font = UIFont(name: "YanoneKaffeesatz-Regular", size: 13.0) // FIXME: load from theme
        playlistVisibilityTitleLabel.textColor = .rgb(value: 0.0, alpha: 0.54)
        playlistVisibilityTitleLabel.text = "make_it_public".localized

        return playlistVisibilityTitleLabel
    }()

    lazy fileprivate var cancelButton: UIButton = {
        var cancelButton = UIButton(type: .custom)
        cancelButton.translatesAutoresizingMaskIntoConstraints = false
        cancelButton.titleLabel?.font = UIFont(name: "YanoneKaffeesatz-Regular", size: 16.0) // FIXME: load from theme
        cancelButton.setTitleColor(.rgb(value: 0.0, alpha: 0.87), for: .normal)
        cancelButton.setTitleColor(.rgb(value: 0.0, alpha: 0.5), for: .highlighted)
        cancelButton.setTitle(
            "cancel".localized.uppercased(with: Locale.current),
            for: .normal
        )
        cancelButton.addTarget(
            self,
            action: #selector(touchUpInside(cancelButton:)),
            for: .touchUpInside
        )

        return cancelButton
    }()

    lazy fileprivate var doneButton: UIButton = {
        var doneButton = UIButton(type: .custom)
        doneButton.translatesAutoresizingMaskIntoConstraints = false
        doneButton.titleLabel?.font = UIFont(name: "YanoneKaffeesatz-Regular", size: 16.0) // FIXME: load from theme
        doneButton.setTitleColor(.rgb(red: 63.0, green: 81.0, blue: 181.0), for: .normal)
        doneButton.setTitleColor(.rgb(red: 63.0, green: 81.0, blue: 181.0, alpha: 0.5), for: .highlighted)
        doneButton.setTitleColor(.rgb(value: 158.0, alpha: 1.0), for: .disabled)
        doneButton.setTitle(
            "save".localized.uppercased(with: Locale.current),
            for: .normal
        )
        doneButton.addTarget(
            self,
            action: #selector(touchUpInside(doneButton:)),
            for: .touchUpInside
        )

        return doneButton
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)

        translatesAutoresizingMaskIntoConstraints = false

        addGestureRecognizer(tapGestureRecognizer)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: -

    fileprivate func setupViews(superView: UIView) {
        var cancelButtonWidth = CGFloat(0.0)
        var doneButtonWidth = CGFloat(0.0)
        let constrainedSize = CGSize(
            width: bounds.size.width,
            height: .greatestFiniteMagnitude
        )

        if let text = cancelButton.titleLabel?.text, let font = cancelButton.titleLabel?.font {
            let cancelButtonRect = text.boundingRect(
                with: constrainedSize,
                options: .usesLineFragmentOrigin,
                attributes: [NSFontAttributeName: font],
                context: nil
            )
            cancelButtonWidth = ceil(cancelButtonRect.size.width)
        }

        if let text = doneButton.titleLabel?.text, let font = doneButton.titleLabel?.font {
            let doneButtonRect = text.boundingRect(
                with: constrainedSize,
                options: .usesLineFragmentOrigin,
                attributes: [NSFontAttributeName: font],
                context: nil
            )
            doneButtonWidth = ceil(doneButtonRect.size.width)
        }

        let bindings: [String: AnyObject] = [
            "view": self,
            "containerView": containerView,
            "titleLabel": titleLabel,
            "playlistTitleLabel": playlistTitleLabel,
            "playlistTitleTextField": playlistTitleTextField,
            "separatorView": separatorView,
            "playlistTitleLimitLabel": playlistTitleLimitLabel,
            "playlistVisibilityButton": playlistVisibilityButton,
            "playlistVisibilityTitleLabel": playlistVisibilityTitleLabel,
            "cancelButton": cancelButton,
            "doneButton": doneButton
        ]

        superView.addSubview(self)
        addSubview(containerView)
        containerView.addSubview(titleLabel)
        containerView.addSubview(playlistTitleLabel)
        containerView.addSubview(playlistTitleTextField)
        containerView.addSubview(separatorView)
        containerView.addSubview(playlistTitleLimitLabel)
        containerView.addSubview(playlistVisibilityButton)
        containerView.addSubview(playlistVisibilityTitleLabel)
        containerView.addSubview(cancelButton)
        containerView.addSubview(doneButton)

        superView.addConstraints(
            NSLayoutConstraint.constraints(
                withVisualFormat: "H:|[view]|",
                options: NSLayoutFormatOptions(rawValue: 0),
                metrics: nil,
                views: bindings
            )
        )

        superView.addConstraints(
            NSLayoutConstraint.constraints(
                withVisualFormat: "V:|[view]|",
                options: NSLayoutFormatOptions(rawValue: 0),
                metrics: nil,
                views: bindings
            )
        )

        containerView.addConstraint(
            NSLayoutConstraint(
                item: containerView,
                attribute: .width,
                relatedBy: .equal,
                toItem: nil,
                attribute: .notAnAttribute,
                multiplier: 1.0,
                constant: containerViewSize.width
            )
        )

        containerView.addConstraint(
            NSLayoutConstraint(
                item: containerView,
                attribute: .height,
                relatedBy: .equal,
                toItem: nil,
                attribute: .notAnAttribute,
                multiplier: 1.0,
                constant: containerViewSize.height
            )
        )

        addConstraint(
            NSLayoutConstraint(
                item: containerView,
                attribute: .centerX,
                relatedBy: .equal,
                toItem: self,
                attribute: .centerX,
                multiplier: 1.0,
                constant: 0.0
            )
        )

        containerViewCenterYConstraint = NSLayoutConstraint(
            item: containerView,
            attribute: .centerY,
            relatedBy: .equal,
            toItem: self,
            attribute: .centerY,
            multiplier: 1.0,
            constant: 0.0
        )
        addConstraint(containerViewCenterYConstraint)

        containerView.addConstraints(
            NSLayoutConstraint.constraints(
                withVisualFormat: "H:|-space-[titleLabel]-space-|",
                options: NSLayoutFormatOptions(rawValue: 0),
                metrics: ["space": 25.0],
                views: bindings
            )
        )

        containerView.addConstraints(
            NSLayoutConstraint.constraints(
                withVisualFormat: "H:|-space-[playlistTitleLabel]-space-|",
                options: NSLayoutFormatOptions(rawValue: 0),
                metrics: ["space": horizontalSpace],
                views: bindings
            )
        )

        containerView.addConstraints(
            NSLayoutConstraint.constraints(
                withVisualFormat: "H:|-space-[playlistTitleTextField]-space-|",
                options: NSLayoutFormatOptions(rawValue: 0),
                metrics: ["space": horizontalSpace],
                views: bindings
            )
        )

        containerView.addConstraints(
            NSLayoutConstraint.constraints(
                withVisualFormat: "H:|-space-[separatorView]-space-|",
                options: NSLayoutFormatOptions(rawValue: 0),
                metrics: ["space": horizontalSpace],
                views: bindings
            )
        )

        containerView.addConstraints(
            NSLayoutConstraint.constraints(
                withVisualFormat: "H:[playlistTitleLimitLabel]-space-|",
                options: NSLayoutFormatOptions(rawValue: 0),
                metrics: ["space": horizontalSpace],
                views: bindings
            )
        )

        containerView.addConstraints(
            NSLayoutConstraint.constraints(
                withVisualFormat: "H:|-space1-[playlistVisibilityButton(width)]-space2-[playlistVisibilityTitleLabel]-space3-|",
                options: NSLayoutFormatOptions(rawValue: 0),
                metrics: [
                    "space1": 25.0,
                    "space2": 10.0,
                    "space3": horizontalSpace,
                    "width": playlistVisibilityButtonSize.width
                ],
                views: bindings
            )
        )

        containerView.addConstraints(
            NSLayoutConstraint.constraints(
                withVisualFormat: "H:[cancelButton(width1)][doneButton(width2)]|",
                options: NSLayoutFormatOptions(rawValue: 0),
                metrics: [
                    "space": horizontalSpace,
                    "width1": cancelButtonWidth + 20.0 * 2,
                    "width2": doneButtonWidth + 20.0 * 2
                ],
                views: bindings
            )
        )

        containerView.addConstraints(
            NSLayoutConstraint.constraints(
                withVisualFormat: "V:|-space1-[titleLabel]-space1-[playlistTitleLabel][playlistTitleTextField(height1)][separatorView]-space2-[playlistTitleLimitLabel]-space2-[playlistVisibilityButton(height2)]",
                options: NSLayoutFormatOptions(rawValue: 0),
                metrics: [
                    "space1": verticalSpace,
                    "space2": 5.0,
                    "height1": playlistTitleTextFieldHeight,
                    "height2": playlistVisibilityButtonSize.height
                ],
                views: bindings
            )
        )

        separatorView.addConstraint(
            NSLayoutConstraint(
                item: separatorView,
                attribute: .height,
                relatedBy: .equal,
                toItem: nil,
                attribute: .notAnAttribute,
                multiplier: 1.0,
                constant: separatorViewHeight
            )
        )

        containerView.addConstraint(
            NSLayoutConstraint(
                item: playlistVisibilityTitleLabel,
                attribute: .centerY,
                relatedBy: .equal,
                toItem: playlistVisibilityButton,
                attribute: .centerY,
                multiplier: 1.0,
                constant: 0.0
            )
        )

        containerView.addConstraints(
            NSLayoutConstraint.constraints(
                withVisualFormat: "V:[doneButton(height)]|",
                options: NSLayoutFormatOptions(rawValue: 0),
                metrics: ["height": actionButtonHeight],
                views: bindings
            )
        )

        containerView.addConstraint(
            NSLayoutConstraint(
                item: cancelButton,
                attribute: .height,
                relatedBy: .equal,
                toItem: doneButton,
                attribute: .height,
                multiplier: 1.0,
                constant: 0.0
            )
        )

        containerView.addConstraint(
            NSLayoutConstraint(
                item: cancelButton,
                attribute: .centerY,
                relatedBy: .equal,
                toItem: doneButton,
                attribute: .centerY,
                multiplier: 1.0,
                constant: 0.0
            )
        )
    }

    // MARK: - UIKeyboard

    fileprivate func registerForKeyboardNotifications() {
        let notificationCenter = NotificationCenter.default

        notificationCenter.addObserver(
            self,
            selector: #selector(keyboardWillShow(_:)),
            name: .UIKeyboardWillShow,
            object: nil
        )
        notificationCenter.addObserver(
            self,
            selector: #selector(keyboardWillHide(_:)),
            name: .UIKeyboardWillHide,
            object: nil
        )
    }

    fileprivate func unregisterFromKeyboardNotifications() {
        let notificationCenter = NotificationCenter.default

        notificationCenter.removeObserver(
            self,
            name: .UIKeyboardWillShow,
            object: nil
        )
        notificationCenter.removeObserver(
            self,
            name: .UIKeyboardWillHide,
            object: nil
        )
    }

    fileprivate func dismissKeyboard() {
        endEditing(true)
    }

    fileprivate func animate(userInfo: [AnyHashable : Any]) {
        if let duration = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue,
           let option = (userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber)?.uintValue {
            UIView.animate(withDuration: duration, delay: 0, options: UIViewAnimationOptions(rawValue: option), animations: {
                self.layoutIfNeeded()
            }, completion: nil)
        }
    }

    func keyboardWillShow(_ notification: NSNotification) {
        if let userInfo = notification.userInfo,
           let keyboardSize = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let difference = keyboardSize.height + (containerView.bounds.size.height - bounds.size.height) / 2

            if difference > 0 {
                containerViewCenterYConstraint.constant = -(difference + containerViewBottomSpace)
                setNeedsUpdateConstraints()
                animate(userInfo: userInfo)
            }
        }
    }

    func keyboardWillHide(_ notification: NSNotification) {
        if let userInfo = notification.userInfo {
            containerViewCenterYConstraint.constant = 0.0
            setNeedsUpdateConstraints()
            animate(userInfo: userInfo)
        }
    }

    // MARK: - UITapGestureRecognizer

    func handle(tapGestureRecognizer: UITapGestureRecognizer) {
        hide { (finished) in
            self.delegate?.playlistPopupViewDidCancel(self)
        }
    }

    // MARK: -

    func show(playlist: Playlist, in superView: UIView, completionHandler: ((Bool) -> ())? = nil) {
        self.playlist = playlist
        setupViews(superView: superView)
        playlistTitleLimitLabel.text = String(playlist.title.characters.count) + "/" + String(playlistTitleLimit)
        playlistVisibilityButton.isSelected = playlist.isPublic
        doneButton.isEnabled = isFormValid()

        containerView.transform = CGAffineTransform(
            scaleX: 1 / containerViewSize.width,
            y: 1 / containerViewSize.height
        )

        UIView.animate(withDuration: animationDuration, delay: 0.0, options: .curveEaseInOut, animations: {
            self.containerView.transform = CGAffineTransform.identity
        }) { (finished) in
            self.registerForKeyboardNotifications()
            self.playlistTitleTextField.becomeFirstResponder()

            if let completionHandler = completionHandler {
                completionHandler(true)
            }
        }
    }

    func hide(completionHandler: ((Bool) -> ())? = nil) {
        hide(delay: 0.0, completionHandler: completionHandler)
    }

    func hide(delay: TimeInterval, completionHandler: ((Bool) -> ())? = nil) {
        containerView.transform = CGAffineTransform.identity

        UIView.animate(withDuration: animationDuration, delay: 0.0, options: .curveEaseInOut, animations: {
            self.containerView.transform = CGAffineTransform(
                scaleX: 1 / self.containerViewSize.width,
                y: 1 / self.containerViewSize.height
            )
        }) { (finished) in
            self.dismissKeyboard()
            self.unregisterFromKeyboardNotifications()
            self.removeFromSuperview()

            if let completionHandler = completionHandler {
                completionHandler(true)
            }
        }
    }

    fileprivate func isFormValid() -> Bool {
        return !playlist.title.isEmpty
    }

    // MARK: - UIButton

    func touchUpInside(playlistVisibilityButton: UIButton) {
        playlist.isPublic = !playlist.isPublic
        playlistVisibilityButton.isSelected = playlist.isPublic
    }

    func touchUpInside(cancelButton: UIButton) {
        hide(delay: animationDelay) { (finished) in
            self.delegate?.playlistPopupViewDidCancel(self)
        }
    }

    func touchUpInside(doneButton: UIButton) {
        hide(delay: animationDelay) { (finished) in
            self.delegate?.playlistPopupViewDidFinish(self)
        }
    }
}

// MARK: - UIGestureRecognizerDelegate

extension PlaylistPopupView: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if let view = touch.view, view.isDescendant(of: containerView) {
            return false
        }

        return true
    }
}

// MARK: - UITextFieldDelegate

extension PlaylistPopupView: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text {
            let text = (text as NSString).replacingCharacters(
                in: range,
                with: string
            )

            if text.characters.count <= playlistTitleLimit {
                playlist.title = text
                playlistTitleLimitLabel.text = String(playlist.title.characters.count) + "/" + String(playlistTitleLimit)
                doneButton.isEnabled = isFormValid()

                return true
            }
        }

        return false
    }

    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        playlist.title = ""
        playlistTitleLimitLabel.text = String(playlist.title.characters.count) + "/" + String(playlistTitleLimit)
        doneButton.isEnabled = isFormValid()

        return true
    }
}
