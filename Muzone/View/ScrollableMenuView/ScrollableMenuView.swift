//
//  TabBarView.swift
//  Muzone
//
//  Created by Sukhrob Khakimov on 10/28/17.
//  Copyright © 2017 Muzone LLC. All rights reserved.
//

import UIKit

protocol ScrollableMenuViewDataSource: class {
    func numberOfItems(in scrollableMenuView: ScrollableMenuView) -> Int
    func scrollableMenuView(_ scrollableMenuView: ScrollableMenuView, viewForItemAt index: Int, isSelected: Bool) -> UIView
}

protocol ScrollableMenuViewDelegate: class {
    func scrollableMenuView(_ scrollableMenuView: ScrollableMenuView, sizeForItemAt index: Int) -> CGSize
    func scrollableMenuView(_ scrollableMenuView: ScrollableMenuView, willSelectItemAt index: Int)
    func scrollableMenuView(_ scrollableMenuView: ScrollableMenuView, didSelectItemAt index: Int)
}

class ScrollableMenuView: UIView {
    fileprivate let cellReuseIdentifier: String

    fileprivate(set) var selectedIndex: Int
    fileprivate(set) var numberOfItems: Int
    fileprivate var selectionIndicatorViewLeadingSpaceConstraint: NSLayoutConstraint!
    fileprivate var selectionIndicatorViewWidthConstraint: NSLayoutConstraint!

    var selectionIndicatorViewBackgroundColor: UIColor? {
        didSet {
            selectionIndicatorView.backgroundColor = selectionIndicatorViewBackgroundColor
        }
    }

    weak var dataSource: ScrollableMenuViewDataSource?
    weak var delegate: ScrollableMenuViewDelegate?

    lazy fileprivate var itemSizes: [CGSize] = []

    lazy fileprivate var collectionViewFlowLayout: UICollectionViewFlowLayout = {
        var collectionViewFlowLayout = UICollectionViewFlowLayout()
        collectionViewFlowLayout.sectionInset = .zero
        collectionViewFlowLayout.minimumLineSpacing = 0.0
        collectionViewFlowLayout.minimumInteritemSpacing = 0.0
        collectionViewFlowLayout.scrollDirection = .horizontal

        return collectionViewFlowLayout
    }()

    lazy fileprivate var collectionView: UICollectionView = {
        var collectionView = UICollectionView(
            frame: .zero,
            collectionViewLayout: self.collectionViewFlowLayout
        )
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .clear
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.bounces = false
        collectionView.isScrollEnabled = false
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(
            UICollectionViewCell.self,
            forCellWithReuseIdentifier: String(describing: UICollectionViewCell.self)
        )

        return collectionView
    }()

    lazy fileprivate var selectionIndicatorView: UIView = {
        var selectionIndicatorView = UIView()
        selectionIndicatorView.translatesAutoresizingMaskIntoConstraints = false
        selectionIndicatorView.backgroundColor = .black

        return selectionIndicatorView
    }()

    init() {
        cellReuseIdentifier = String(describing: UICollectionViewCell.self)
        selectedIndex = 0
        numberOfItems = 0

        super.init(frame: .zero)

        setupViews()
    }

    override init(frame: CGRect) {
        fatalError("init(frame:) has not been implemented")
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    fileprivate func setupViews() {
        let bindings: [String: AnyObject] = [
            "collectionView": collectionView,
            "selectionIndicatorView": selectionIndicatorView
        ]

        addSubview(collectionView)
        addSubview(selectionIndicatorView)

        addConstraints(
            NSLayoutConstraint.constraints(
                withVisualFormat: "H:|[collectionView]|",
                options: NSLayoutFormatOptions(rawValue: 0),
                metrics: nil,
                views: bindings
            )
        )

        addConstraints(
            NSLayoutConstraint.constraints(
                withVisualFormat: "V:|[collectionView]|",
                options: NSLayoutFormatOptions(rawValue: 0),
                metrics: nil,
                views: bindings
            )
        )

        addConstraints(
            NSLayoutConstraint.constraints(
                withVisualFormat: "V:[selectionIndicatorView(height)]|",
                options: NSLayoutFormatOptions(rawValue: 0),
                metrics: ["height": 3.0],
                views: bindings
            )
        )

        selectionIndicatorViewLeadingSpaceConstraint = NSLayoutConstraint(
            item: selectionIndicatorView,
            attribute: .leading,
            relatedBy: .equal,
            toItem: self,
            attribute: .leading,
            multiplier: 1.0,
            constant: 0.0
        )
        addConstraint(selectionIndicatorViewLeadingSpaceConstraint)

        selectionIndicatorViewWidthConstraint = NSLayoutConstraint(
            item: selectionIndicatorView,
            attribute: .width,
            relatedBy: .equal,
            toItem: nil,
            attribute: .notAnAttribute,
            multiplier: 1.0,
            constant: 0.0
        )
        addConstraint(selectionIndicatorViewWidthConstraint)
    }

    func selectItem(at index: Int, animated: Bool, completion: ((_ finished: Bool) -> Void)? = nil) {
        if index != selectedIndex && index >= 0 && index <= numberOfItems - 1 {
            let previousIndex = selectedIndex
            let previousIndexPath = IndexPath(
                item: previousIndex,
                section: 0
            )

            selectedIndex = index
            let selectedIndexPath = IndexPath(
                item: selectedIndex,
                section: 0
            )

            collectionView.reloadItems(
                at: [previousIndexPath, selectedIndexPath]
            )

            var visibleIndexPaths = collectionView.indexPathsForVisibleItems
            visibleIndexPaths = visibleIndexPaths.sorted()

            let width = itemSizes[selectedIndex].width
            var space = CGFloat(0.0)

            if let firstVisibleIndexPath = visibleIndexPaths.first,
                let lastVisibleIndexPath = visibleIndexPaths.last {
                var indexPath: IndexPath
                var position: UICollectionViewScrollPosition

                if firstVisibleIndexPath.item == selectedIndex ||
                    firstVisibleIndexPath.item + 1 == selectedIndex ||
                    lastVisibleIndexPath.item == selectedIndex ||
                    lastVisibleIndexPath.item - 1 == selectedIndex {
                    if firstVisibleIndexPath.item == selectedIndex {
                        position = .left

                        if selectedIndex > 0 {
                            space = itemSizes[selectedIndex - 1].width
                            indexPath = IndexPath(
                                item: selectedIndex - 1,
                                section: 0
                            )
                        } else {
                            indexPath = IndexPath(
                                item: selectedIndex,
                                section: 0
                            )
                        }
                    } else if firstVisibleIndexPath.item + 1 == selectedIndex {
                        space = itemSizes[selectedIndex - 1].width

                        if previousIndex < selectedIndex {
                            indexPath = IndexPath(
                                item: selectedIndex + 1,
                                section: 0
                            )
                            position = .right
                        } else {
                            indexPath = IndexPath(
                                item: selectedIndex - 1,
                                section: 0
                            )
                            position = .left
                        }
                    } else if lastVisibleIndexPath.item == selectedIndex {
                        space = bounds.size.width - itemSizes[selectedIndex].width

                        if selectedIndex == numberOfItems - 1 {
                            indexPath = IndexPath(
                                item: selectedIndex,
                                section: 0
                            )
                        } else {
                            space -= itemSizes[selectedIndex + 1].width
                            indexPath = IndexPath(
                                item: selectedIndex + 1,
                                section: 0
                            )
                        }

                        position = .right
                    } else {
                        space = bounds.size.width - itemSizes[selectedIndex].width - itemSizes[selectedIndex + 1].width

                        if previousIndex < selectedIndex {
                            indexPath = IndexPath(
                                item: selectedIndex + 1,
                                section: 0
                            )
                            position = .right
                        } else {
                            indexPath = IndexPath(
                                item: selectedIndex - 1,
                                section: 0
                            )
                            position = .left
                        }
                    }

                    // FIXME: fix
                    //                    collectionView.scrollToItem(
                    //                        at: indexPath,
                    //                        at: position,
                    //                        animated: animated
                    //                    )
                } else {
                    if previousIndex < selectedIndex {
                        for index in previousIndex..<selectedIndex {
                            space += itemSizes[index].width
                        }

                        space += selectionIndicatorViewLeadingSpaceConstraint.constant
                    } else {
                        for index in selectedIndex..<previousIndex {
                            space += itemSizes[index].width
                        }

                        space = selectionIndicatorViewLeadingSpaceConstraint.constant - space
                    }
                }

                selectionIndicatorViewLeadingSpaceConstraint.constant = space
                selectionIndicatorViewWidthConstraint.constant = width
                setNeedsUpdateConstraints()

                if animated {
                    UIView.animate(withDuration: 0.25, animations: {
                        self.layoutIfNeeded()
                    }, completion: completion)
                } else {
                    layoutIfNeeded()

                    if let completion = completion {
                        completion(true)
                    }
                }
            }
        }
    }
}

extension ScrollableMenuView: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let numberOfItems = dataSource?.numberOfItems(in: self) {
            self.numberOfItems = numberOfItems
        }

        return numberOfItems
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: cellReuseIdentifier,
            for: indexPath
        )
        cell.backgroundColor = .clear

        for subview in cell.contentView.subviews {
            subview.removeFromSuperview()
        }

        let isSelected = indexPath.item == selectedIndex

        if let view = dataSource?.scrollableMenuView(
            self,
            viewForItemAt: indexPath.item,
            isSelected: isSelected
            ) {
            view.translatesAutoresizingMaskIntoConstraints = false

            let bindings = ["view": view]

            cell.contentView.addSubview(view)

            cell.contentView.addConstraints(
                NSLayoutConstraint.constraints(
                    withVisualFormat: "H:|[view]|",
                    options: NSLayoutFormatOptions(rawValue: 0),
                    metrics: nil,
                    views: bindings
                )
            )

            cell.contentView.addConstraints(
                NSLayoutConstraint.constraints(
                    withVisualFormat: "V:|[view]|",
                    options: NSLayoutFormatOptions(rawValue: 0),
                    metrics: nil,
                    views: bindings
                )
            )
        }

        return cell
    }
}

extension ScrollableMenuView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if let size = delegate?.scrollableMenuView(
            self,
            sizeForItemAt: indexPath.item
            ) {
            if indexPath.item < itemSizes.count {
                itemSizes[indexPath.item] = size
            } else {
                itemSizes.append(size)
            }

            if indexPath.item == selectedIndex {
                selectionIndicatorViewWidthConstraint.constant = size.width
                selectionIndicatorView.setNeedsUpdateConstraints()
                selectionIndicatorView.layoutIfNeeded()
            }

            return size
        }

        let size = CGSize.zero

        if indexPath.item < itemSizes.count {
            itemSizes[indexPath.item] = size
        } else {
            itemSizes.append(size)
        }
        
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let index = indexPath.item
        
        if index != selectedIndex {
            delegate?.scrollableMenuView(self, willSelectItemAt: index)
            selectItem(at: index, animated: true, completion: { finished in
                self.delegate?.scrollableMenuView(self, didSelectItemAt: index)
            })
        }
    }
}
