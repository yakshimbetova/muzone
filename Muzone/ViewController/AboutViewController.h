//
//  AboutViewController.h
//  Muzone
//
//  Created by Sukhrob Khakimov on 5/27/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import "BaseViewController.h"

@interface AboutViewController : BaseViewController <UIWebViewDelegate>

@end
