//
//  AlbumViewController.h
//  Muzone
//
//  Created by Sukhrob Khakimov on 6/12/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import "BaseViewController.h"

@class Album;

@interface AlbumViewController : BaseViewController <UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate>

- (nonnull instancetype)initWithAlbum:(nonnull Album *)album;

@end
