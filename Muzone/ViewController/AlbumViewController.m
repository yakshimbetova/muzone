//
//  AlbumViewController.m
//  Muzone
//
//  Created by Sukhrob Khakimov on 6/12/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <SDWebImage/UIImageView+WebCache.h>

#import "AppDelegate.h"
#import "Constants.h"
#import "ApiClient.h"
#import "ErrorHandler.h"
#import "TrackRepository.h"
#import "TrackManager.h"
#import "FavoriteManager.h"

#import "Album.h"
#import "Artist.h"
#import "Track.h"

#import "BaseNavigationController.h"
#import "AlbumViewController.h"

#import "ParallaxHeaderView.h"
#import "SZKMessageView.h"
#import "SZKProgressView.h"

#import "AlbumTableViewCell.h"

@interface AlbumViewController () <SZKMessageViewDelegate>

@property (nonnull, nonatomic, strong) NSManagedObjectContext *mainManagedObjectContext;
@property (nonnull, nonatomic, strong) NSManagedObjectContext *temporaryManagedObjectContext;
@property (nonnull, nonatomic, strong) NSManagedObjectContext *workerManagedObjectContext;
@property (nonnull, nonatomic, strong) TrackRepository *trackRepository;
@property (nonnull, nonatomic, strong) TrackManager *trackManager;
@property (nonnull, nonatomic, strong) NSURLSessionDataTask *loadingTask;

@property (nonatomic, assign) BOOL viewAlreadyAppeared;
@property (nonnull, nonatomic, copy) NSString *cellReuseIdentifier;
@property (nonnull, nonatomic, strong) Album *album;
@property (nonnull, nonatomic, strong) NSMutableArray *tracks;

@property (nonnull, nonatomic, strong) UITableView *tableView;
@property (nonnull, nonatomic, strong) SZKMessageView *messageView;
@property (nonnull, nonatomic, strong) SZKProgressView *progressView;

@end

@implementation AlbumViewController

- (instancetype)initWithAlbum:(Album *)album {
    self = [super init];
    
    if (self) {
        self.album = album;
        self.cellReuseIdentifier = NSStringFromClass([AlbumTableViewCell class]);
    }
    
    return self;
}

- (void)loadView {
    [super loadView];
    
    [self.view addSubview:self.tableView];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[tableView]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:@{@"tableView": self.tableView}]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[tableView]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:@{@"tableView": self.tableView}]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = self.album.title;
    
    ParallaxHeaderView *headerView = [ParallaxHeaderView parallaxHeaderViewWithImage:[UIImage imageNamed:@"img_placeholder.png"]
                                                                             forSize:CGSizeMake(self.view.bounds.size.width, 200.0f)];
    self.tableView.tableHeaderView = headerView;
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:[NSURL URLWithString:self.album.imageUrl] options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
        if (image) {
             headerView.headerImage = image;
            [(ParallaxHeaderView *)self.tableView.tableHeaderView refreshBlurViewForNewImage];
        }
    }];
    
    [self loadTracks];
    
    [Answers logContentViewWithName:self.album.title
                        contentType:@"Album"
                          contentId:[NSString stringWithFormat:@"%@", self.album.uid]
                   customAttributes:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    ((BaseNavigationController *)self.navigationController).statusBarStyle = UIStatusBarStyleLightContent;
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"bg_nav.png"]
                                                  forBarMetrics:UIBarMetricsDefault];
    
    // NSNotificationCenter
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    
    [notificationCenter addObserver:self
                           selector:@selector(playerViewControllerWillPlay:)
                               name:PlayerViewControllerWillPlay
                             object:nil];
    
    [notificationCenter addObserver:self
                           selector:@selector(playerViewControllerWillPause:)
                               name:PlayerViewControllerWillPause
                             object:nil];
    
    [notificationCenter addObserver:self
                           selector:@selector(playerViewControllerWillStop:)
                               name:PlayerViewControllerWillStop
                             object:nil];
    
    [notificationCenter addObserver:self
                           selector:@selector(playerViewControllerWillDispose:)
                               name:PlayerViewControllerWillDispose
                             object:nil];
    
    [notificationCenter addObserver:self
                           selector:@selector(playerViewControllerDidChangeCurrentTrack:)
                               name:PlayerViewControllerDidChangeCurrentTrack
                             object:nil];
    
    [notificationCenter addObserver:self
                           selector:@selector(playerViewControllerShouldHandleError:)
                               name:PlayerViewControllerShouldHandleError
                             object:nil];
    
    [notificationCenter addObserver:self
                           selector:@selector(downloadManagerProcessWillStart:)
                               name:DownloadManagerProcessWillStart
                             object:nil];
    
    [notificationCenter addObserver:self
                           selector:@selector(downloadManagerProcessInProgress:)
                               name:DownloadManagerProcessInProgress
                             object:nil];
    
    [notificationCenter addObserver:self
                           selector:@selector(downloadManagerProcessDidEnd:)
                               name:DownloadManagerProcessDidEnd
                             object:nil];
    
    [notificationCenter addObserver:self
                           selector:@selector(trackManagerDidDeleteTrack:)
                               name:TrackManagerDidDeleteTrack
                             object:nil];
    
    [self.tableView reloadData];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.tableView.contentInset = [self tableViewContentInset];
    
    if (!self.viewAlreadyAppeared) {
        self.viewAlreadyAppeared = YES;
        [(ParallaxHeaderView *)self.tableView.tableHeaderView refreshBlurViewForNewImage];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.loadingTask cancel];
    
    // NSNotificationCenter
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    
    [notificationCenter removeObserver:self
                                  name:PlayerViewControllerWillPlay
                                object:nil];
    
    [notificationCenter removeObserver:self
                                  name:PlayerViewControllerWillPause
                                object:nil];
    
    [notificationCenter removeObserver:self
                                  name:PlayerViewControllerWillStop
                                object:nil];
    
    [notificationCenter removeObserver:self
                                  name:PlayerViewControllerWillDispose
                                object:nil];
    
    [notificationCenter removeObserver:self
                                  name:PlayerViewControllerDidChangeCurrentTrack
                                object:nil];
    
    [notificationCenter removeObserver:self
                                  name:PlayerViewControllerShouldHandleError
                                object:nil];
    
    [notificationCenter removeObserver:self
                                  name:DownloadManagerProcessWillStart
                                object:nil];
    
    [notificationCenter removeObserver:self
                                  name:DownloadManagerProcessInProgress
                                object:nil];
    
    [notificationCenter removeObserver:self
                                  name:DownloadManagerProcessDidEnd
                                object:nil];
    
    [notificationCenter removeObserver:self
                                  name:TrackManagerDidDeleteTrack
                                object:nil];
    
    [super viewWillDisappear:animated];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -

- (nullable Track *)findTrackWithFileUrl:(NSURL *)fileUrl {
    for (Track *track in self.tracks) {
        if ([[track.fileUrl stringByRemovingPercentEncoding] isEqualToString:[fileUrl.absoluteString stringByRemovingPercentEncoding]]) {
            return track;
        }
    }
    return nil;
}

- (nullable Track *)findTrackWithUid:(NSNumber *)uid {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uid = %@", uid];
    NSArray *result = [self.tracks filteredArrayUsingPredicate:predicate];
    
    if (result.count > 0) {
        return result[0];
    }
    
    return nil;
}

- (void)setState:(TrackState)state progress:(double)progress forFileUrl:(NSURL *)fileUrl {
    Track *track = [self findTrackWithFileUrl:fileUrl];
    
    if (track) {
        NSInteger index = [self.tracks indexOfObject:track];
        
        if (index != NSNotFound) {
            track.state = @(state);
            track.progress = @(progress);
            
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index
                                                        inSection:0];
            AlbumTableViewCell *cell = (AlbumTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
            
            WEAKIFY_SELF;
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                STRONGIFY_SELF;
                [self setTrack:track
                       forCell:cell];
            });
        }
    }
}

#pragma mark - NSMutableArray

- (NSMutableArray *)tracks {
    if (_tracks == nil) {
        _tracks = [NSMutableArray new];
    }
    
    return _tracks;
}

#pragma mark - CoreData

- (NSManagedObjectContext *)mainManagedObjectContext {
    if (_mainManagedObjectContext == nil) {
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        _mainManagedObjectContext = appDelegate.mainManagedObjectContext;
    }
    
    return _mainManagedObjectContext;
}

- (NSManagedObjectContext *)temporaryManagedObjectContext {
    if (_temporaryManagedObjectContext == nil) {
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        _temporaryManagedObjectContext = appDelegate.temporaryManagedObjectContext;
    }
    
    return _temporaryManagedObjectContext;
}

- (NSManagedObjectContext *)workerManagedObjectContext {
    if (_workerManagedObjectContext == nil) {
        _workerManagedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        _workerManagedObjectContext.parentContext = self.mainManagedObjectContext;
    }
    
    return _workerManagedObjectContext;
}

- (TrackRepository *)trackRepository {
    if (_trackRepository == nil) {
        _trackRepository = [[TrackRepository alloc] initWithManagedObjectContext:self.temporaryManagedObjectContext];
    }
    
    return _trackRepository;
}

- (TrackManager *)trackManager {
    if (_trackManager == nil) {
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        _trackManager = appDelegate.trackManager;
    }
    
    return _trackManager;
}

- (void)deleteTrackAtIndexPath:(NSIndexPath *)indexPath {
    Track *track = self.tracks[indexPath.row];
    [self.trackManager deleteTrack:track];
}

#pragma mark - NSNotificationCenter

- (void)playerViewControllerWillPlay:(NSNotification *)notification {
    self.tableView.contentInset = [self tableViewContentInset];
    [self.tableView reloadData];
}

- (void)playerViewControllerWillPause:(NSNotification *)notification {
    [self.tableView reloadData];
}

- (void)playerViewControllerWillStop:(NSNotification *)notification {
    self.tableView.contentInset = [self tableViewContentInset];
    [self.tableView reloadData];
}

- (void)playerViewControllerWillDispose:(NSNotification *)notification {
    [self.tableView reloadData];
}

- (void)playerViewControllerDidChangeCurrentTrack:(NSNotification *)notification {
    [self.tableView reloadData];
}

- (void)playerViewControllerShouldHandleError:(NSNotification *)notification {
    [self.tableView reloadData];
    
    NSError *error = notification.userInfo[PlayerViewControllerErrorKey];
    [ErrorHandler handleError:error];
}

- (void)downloadManagerProcessWillStart:(NSNotification *)notification {
    NSURL *fileUrl = notification.userInfo[DownloadManagerFileUrlKey];
    Track *track = [self findTrackWithFileUrl:fileUrl];
    
    if (track) {
        track = [track copyWithContext:self.temporaryManagedObjectContext];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:TrackQueueDidEnqueue
                                                            object:self
                                                          userInfo:@{TrackQueueKey: track}];
        
        [self setState:TrackStateInProgress
              progress:0
            forFileUrl:fileUrl];
    }
    
    [self setRightBarButtonItem:NO];
}

- (void)downloadManagerProcessInProgress:(NSNotification *)notification {
    NSURL *fileUrl = notification.userInfo[DownloadManagerFileUrlKey];
    double progress = [notification.userInfo[DownloadManagerProgressKey] doubleValue];
    
    [self setState:TrackStateDownloading
          progress:progress
        forFileUrl:fileUrl];
}

- (void)downloadManagerProcessDidEnd:(NSNotification *)notification {
    NSURL *fileUrl = notification.userInfo[DownloadManagerFileUrlKey];
    NSString *localFileName = notification.userInfo[DownloadManagerLocalFileNameKey];
    NSError *error = notification.userInfo[DownloadManagerErrorKey];
    TrackState state;
    
    if (error) {
        state = TrackStateNone;
        [ErrorHandler handleError:error];
    } else {
        state = TrackStateDownloaded;
        
        Track *track = [self findTrackWithFileUrl:fileUrl];
        
        if (track) {
            track.localFileName = localFileName;
        }
    }
    
    [self setState:state
          progress:0
        forFileUrl:fileUrl];
    [self setRightBarButtonItem:NO];
}

- (void)trackManagerDidDeleteTrack:(NSNotification *)notification {
    NSNumber *uid = notification.userInfo[TrackManagerTrackUidKey];
    NSError *error = notification.userInfo[TrackManagerErrorKey];
    
    if (error) {
        [ErrorHandler handleError:error];
    } else {
        Track *track = [self findTrackWithUid:uid];
        
        if (track) {
            track.localFileName = nil;
            track.state = @(TrackStateNone);
            
            [self setState:TrackStateNone
                  progress:0
                forFileUrl:[NSURL URLWithString:track.fileUrl]];
            
            AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
            
            if (appDelegate.playerViewController.isRunning && [appDelegate.playerViewController.currentTrack.uid isEqual:track.uid]) {
                [appDelegate.playerViewController stop];
            }
        }
    }
    
    [self setRightBarButtonItem:NO];
}

#pragma mark - UINavigationBar

- (void)rightBarButtonItemTapped:(id)sender {
    for (Track *track in self.tracks) {
        UIBarButtonItem *right = [self.navigationItem.rightBarButtonItems objectAtIndex:0];
        if (right.tag == 1) {
            [self downloadTrack:track];
        } else {
            [self cancelTrack:track];
        }
    }
}

- (void)setRightBarButtonItem:(BOOL)progress {
    NSInteger stateInProgressCount = 0;
    NSInteger stateDownloadingCount = 0;
    NSInteger stateDownloadedCount = 0;
    self.navigationItem.rightBarButtonItems = nil;
    
    for (Track *track in self.tracks) {
        if ([track.state integerValue] == TrackStateInProgress) {
            stateInProgressCount++;
        } else if ([track.state integerValue] == TrackStateDownloading) {
            stateDownloadingCount++;
        } else if ([track.state integerValue] == TrackStateDownloaded) {
            stateDownloadedCount++;
        }
    }
    
    if (self.tracks.count == stateDownloadedCount) {
        self.navigationItem.rightBarButtonItem = [self rightFavoriteButton:progress];
    } else if (self.tracks.count == (stateInProgressCount + stateDownloadingCount + stateDownloadedCount)) {
        UIBarButtonItem *right = [self rightDownloadedButton:@"ic_stop.png"];
        right.tag = 2;
        self.navigationItem.rightBarButtonItems = @[right, [self rightFavoriteButton:progress]];
    } else {
        UIBarButtonItem *right = [self rightDownloadedButton:@"ic_download_white.png"];
        right.tag = 1;
        self.navigationItem.rightBarButtonItems = @[right, [self rightFavoriteButton:progress]];
    }
}

-(UIBarButtonItem*)rightDownloadedButton:(NSString*)imageName
{
   UIBarButtonItem *button =  [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:imageName]
                                     style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(rightBarButtonItemTapped:)];
    return button;
}

-(UIBarButtonItem*)rightFavoriteButton:(BOOL)progress
{
    if (progress) {
        UIActivityIndicatorView * activityView = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
        [activityView startAnimating];
        UIBarButtonItem *loadingView = [[UIBarButtonItem alloc] initWithCustomView:activityView];
        return loadingView;

    }
    if ([[FavoriteManager sharedManager] isFavoriteAlbum:self.album]) {
        
        UIButton *but = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
        [but setImage:[UIImage imageNamed:@"ic_favorite_white"] forState:UIControlStateNormal];
        [but setImage:[UIImage imageNamed:@"ic_favorite_white"] forState:UIControlStateHighlighted];
        [but addTarget:self action:@selector(removeFromFavorite:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithCustomView:but];
        return button;

    }
    else
    {
    
        UIButton *but = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
        [but setImage:[UIImage imageNamed:@"ic_favorite_border_white"] forState:UIControlStateNormal];
        [but setImage:[UIImage imageNamed:@"ic_favorite_border_white"] forState:UIControlStateHighlighted];
        [but addTarget:self action:@selector(addToFavorite:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithCustomView:but];
        return button;
    }
}

#pragma mark - Favorite

-(void)addToFavorite:(id)sender
{
    if ([User currentUser].accessToken == nil)
    {
        [[User currentUser] showLoginForm];
    }
    else
    {
        [self showLoadingIndicator];
        [self setRightBarButtonItem:YES];
        WEAKIFY_SELF;
        FavoriteManager *fManager = [FavoriteManager sharedManager];
        [fManager addToFavoriteAlbum:self.album complete:^(BOOL success) {
            STRONGIFY_SELF;
            [self setRightBarButtonItem:NO];
            [self hideLoadingIndicator];
        }];
    }
}

-(void)removeFromFavorite:(id)sender
{
    if ([User currentUser].accessToken == nil)
    {
        [[User currentUser] showLoginForm];
    }
    else
    {
        [self showLoadingIndicator];
        [self setRightBarButtonItem:YES];
        WEAKIFY_SELF;
        FavoriteManager *fManager = [FavoriteManager sharedManager];
        [fManager deleteFromFavoriteAlbum:self.album complete:^(BOOL success) {
            STRONGIFY_SELF;
            [self setRightBarButtonItem:NO];
            [self hideLoadingIndicator];

        }];
    }
}
#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == self.tableView) {
        [(ParallaxHeaderView *)self.tableView.tableHeaderView layoutHeaderViewForScrollViewOffset:scrollView.contentOffset];
    }
}

#pragma mark - UITableView

- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero
                                                  style:UITableViewStylePlain];
        _tableView.translatesAutoresizingMaskIntoConstraints = NO;
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.hidden = YES;
        [_tableView registerClass:[AlbumTableViewCell class]
           forCellReuseIdentifier:self.cellReuseIdentifier];
    }
    
    return _tableView;
}

- (UIEdgeInsets)tableViewContentInset {
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    return [appDelegate contentInsetForScrollView:self.tableView];
}

- (void)deselectSelectedRow {
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    
    if (indexPath) {
        [self.tableView deselectRowAtIndexPath:indexPath
                                      animated:YES];
    }
}

- (void)setTrack:(nonnull Track *)track forCell:(nonnull AlbumTableViewCell *)cell {
    cell.circularProgressView.progress = [track.progress floatValue];
    
    if ([track.state integerValue] == TrackStateInProgress) {
        cell.actionButton.hidden = YES;
        cell.circularActivityIndicatorView.hidden = NO;
        [cell.circularActivityIndicatorView startAnimating];
        cell.circularProgressView.hidden = YES;
    } else if ([track.state integerValue] == TrackStateDownloading) {
        cell.actionButton.hidden = YES;
        cell.circularActivityIndicatorView.hidden = YES;
        [cell.circularActivityIndicatorView stopAnimating];
        cell.circularProgressView.hidden = NO;
    } else if ([track.state integerValue] == TrackStateDownloaded) {
        cell.actionButton.hidden = NO;
        [cell.actionButton setImage:[UIImage imageNamed:@"ic_delete.png"]
                           forState:UIControlStateNormal];
        cell.circularActivityIndicatorView.hidden = YES;
        [cell.circularActivityIndicatorView stopAnimating];
        cell.circularProgressView.hidden = YES;
    } else {
        cell.actionButton.hidden = NO;
        [cell.actionButton setImage:[UIImage imageNamed:@"ic_download.png"]
                           forState:UIControlStateNormal];
        cell.circularActivityIndicatorView.hidden = YES;
        [cell.circularActivityIndicatorView stopAnimating];
        cell.circularProgressView.hidden = YES;
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.tracks.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Track *track = self.tracks[indexPath.row];
    
    AlbumTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:self.cellReuseIdentifier
                                                               forIndexPath:indexPath];
    cell.titleLabel.text = track.title;
    cell.artistNameLabel.text = track.artist.name;
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    PlayerViewController *playerViewController = appDelegate.playerViewController;
    
    if (playerViewController.isRunning && [track.uid isEqual:playerViewController.currentTrack.uid]) {
        cell.iconImageView.image = [UIImage imageNamed:@"ic_pause_green.png"];
    } else {
        cell.iconImageView.image = [UIImage imageNamed:@"ic_play_green.png"];
    }
    
    [cell.actionButton addTarget:self
                          action:@selector(actionButtonTapped:)
                forControlEvents:UIControlEventTouchUpInside];
    [cell.actionButton setImage:[UIImage imageNamed:@"ic_download.png"]
                       forState:UIControlStateNormal];
    
    cell.tapGestureRecognizer.delegate = self;
    [cell.tapGestureRecognizer addTarget:self
                                  action:@selector(handleTapGestureRecognizer:)];
    
    [cell.circularProgressView.stopButton addTarget:self
                                             action:@selector(stopButtonTapped:)
                                   forControlEvents:UIControlEventTouchUpInside];
    
    [self setTrack:track
           forCell:cell];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Track *track = self.tracks[indexPath.row];
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    if (appDelegate.playerViewController) {
        if ([track.album.uid isEqual:appDelegate.playerViewController.currentTrack.album.uid]) {
            if (appDelegate.playerViewController.isRunning) {
                if ([track.uid isEqual:appDelegate.playerViewController.currentTrack.uid]) {
                    [appDelegate.playerViewController pause];
                } else {
                    [appDelegate.playerViewController pause];
                    appDelegate.playerViewController.currentTrack = track;
                    [appDelegate.playerViewController play];
                }
            } else {
                if ([track.uid isEqual:appDelegate.playerViewController.currentTrack.uid]) {
                    [appDelegate.playerViewController play];
                } else {
                    appDelegate.playerViewController.currentTrack = track;
                    [appDelegate.playerViewController play];
                }
            }
        } else {
            [appDelegate.playerViewController pause];
            appDelegate.playerViewController = [[PlayerViewController alloc] initWithTracks:self.tracks
                                                                               currentTrack:track];
            [appDelegate.playerViewController play];
        }
    } else {
        appDelegate.playerViewController = [[PlayerViewController alloc] initWithTracks:self.tracks
                                                                           currentTrack:track];
        [appDelegate.playerViewController play];
    }
    
    [self.tableView reloadData];
}

#pragma mark - UIAlertController

- (void)showAlertControllerForIndexPath:(nonnull NSIndexPath *)indexPath {
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"cancel", nil)
                                                           style:UIAlertActionStyleCancel
                                                         handler:nil];
    UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"delete", nil)
                                                           style:UIAlertActionStyleDestructive
                                                         handler:^(UIAlertAction *action) {
                                                             [self deleteTrackAtIndexPath:indexPath];
                                                         }];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"track", nil)
                                                                             message:NSLocalizedString(@"track.delete_message", nil)
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:cancelAction];
    [alertController addAction:deleteAction];
    [self presentViewController:alertController
                       animated:YES
                     completion:nil];
}

#pragma mark - UIButton

- (void)actionButtonTapped:(UIButton *)button {
    CGPoint position = [button convertPoint:CGPointZero
                                     toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:position];
    Track *track = self.tracks[indexPath.row];
    
    if (track.state.integerValue == TrackStateDownloaded) {
        [self showAlertControllerForIndexPath:indexPath];
    } else {
        [self downloadTrack:track];
    }
}

- (void)stopButtonTapped:(UIButton *)button {
    CGPoint position = [button convertPoint:CGPointZero
                                     toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:position];
    Track *track = self.tracks[indexPath.row];
    
    [self cancelTrack:track];
}

#pragma mark - UITapGestureRecognizer

- (void)handleTapGestureRecognizer:(UITapGestureRecognizer *)tapGestureRecognizer {
    CGPoint position = [tapGestureRecognizer.view convertPoint:CGPointZero
                                                        toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:position];
    Track *track = self.tracks[indexPath.row];
    
    [self cancelTrack:track];
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    CGPoint position = [gestureRecognizer.view convertPoint:CGPointZero
                                                        toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:position];
    Track *track = self.tracks[indexPath.row];
    
    if (track.state.integerValue == TrackStateDownloaded) {
        return NO;
    }
    
    return YES;
}

#pragma mark - SZKMessageView

- (SZKMessageView *)messageView {
    if (_messageView == nil) {
        _messageView = [[SZKMessageView alloc] init];
        _messageView.delegate = self;
    }
    
    return _messageView;
}

#pragma mark - SZKMessageViewDelegate

- (void)messageView:(SZKMessageView *)messageView didTapActionButton:(UIButton *)actionButton {
    [self loadTracks];
}

#pragma mark - UIActivityIndicatorView

- (SZKProgressView *)progressView {
    if (_progressView == nil) {
        _progressView = [[SZKProgressView alloc] init];
    }
    
    return _progressView;
}

#pragma mark - Loading indicator

- (void)showLoadingIndicator {
    [self.messageView hide];
    [self.progressView showInView:self.view];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)hideLoadingIndicator {
    [self.progressView hide];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

#pragma mark - ApiClient

- (void)loadTracks {
    [self showLoadingIndicator];
    ApiClient *client = [ApiClient sharedClient];
    self.loadingTask = [client albumWithParameters:@{@"id": self.album.uid} success:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        [self.tracks removeAllObjects];
        
        NSInteger totalCount = self.tracks.count;
        NSArray *data = responseObject[@"trackList"];
        NSInteger numberOfResults = data.count;
        
        if (numberOfResults > 0) {
            [self.workerManagedObjectContext performBlock:^{
                for (NSInteger index = 0; index < numberOfResults; index++) {
                    NSDictionary *trackData = data[index];
                    
                    Track *savedTrack = [self.trackRepository fetchOneByUid:trackData[@"id"]];
                    Track *track = [[Track alloc] initWithDictionary:trackData
                                                             context:self.workerManagedObjectContext];
                    
                    if (savedTrack) {
                        if (savedTrack.objectID.isTemporaryID) {
                            track.state = @(TrackStateInProgress);
                        } else {
                            track.localFileName = savedTrack.localFileName;
                            track.state = @(TrackStateDownloaded);
                        }
                    } else {
                        track.state = @(TrackStateNone);
                    }
                    
                    [self.tracks addObject:track];
                }
                
                [self.mainManagedObjectContext performBlock:^{
                    [self hideLoadingIndicator];
                    [self setRightBarButtonItem:NO];
                    
                    self.tableView.hidden = NO;
                    [self.tableView reloadData];
                }];
            }];
        } else if (totalCount == 0) {
            [self hideLoadingIndicator];
            self.tableView.hidden = YES;
            [self.tableView reloadData];
            [self.messageView showInView:self.view
                               iconImage:nil
                                   title:nil
                                 message:NSLocalizedString(@"no_data", nil)
                       actionButtonTitle:NSLocalizedString(@"reload", nil)];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error) {
        [self hideLoadingIndicator];
        
        if (error.code != NSURLErrorCancelled) {
            if (self.tracks.count > 0) {
                [ErrorHandler handleError:error];
            } else {
                [self.messageView showInView:self.view
                                   iconImage:nil
                                       title:nil
                                     message:[ErrorHandler messageFromError:error]
                           actionButtonTitle:NSLocalizedString(@"reload", nil)];
            }
        } else {
            [self.messageView showInView:self.view
                               iconImage:nil
                                   title:nil
                                 message:nil
                       actionButtonTitle:NSLocalizedString(@"reload", nil)];
        }
    }];
}

#pragma mark - DownloadManager

- (void)downloadTrack:(Track *)track {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = paths.lastObject;
    NSString *targetPath = [documentsDirectory stringByAppendingPathComponent:TRACKS_DIRECTORY];
    
    NSError *error;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if (![fileManager fileExistsAtPath:targetPath]) {
        [fileManager createDirectoryAtPath:targetPath
               withIntermediateDirectories:NO
                                attributes:nil
                                     error:&error];
    }
    
    if (error) {
        [ErrorHandler handleError:error];
    } else {
        if ([track.state integerValue] != TrackStateDownloaded) {
            AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
            DownloadManager *downloadManager = appDelegate.downloadManager;
            [downloadManager downloadFileWithUrl:[NSURL URLWithString:track.fileUrl]
                                    toTargetPath:[NSURL fileURLWithPath:targetPath]];
        }
    }
}

- (void)cancelTrack:(Track *)track {
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    DownloadManager *downloadManager = appDelegate.downloadManager;
    [downloadManager cancelFileWithUrl:[NSURL URLWithString:track.fileUrl]];
}

@end
