//
//  AlbumsViewController.h
//  Muzone
//
//  Created by Sukhrob Khakimov on 5/27/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import "BaseViewController.h"

@class Artist;
@class Genre;

@interface AlbumsViewController : BaseViewController <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

- (nonnull instancetype)initWithArtist:(nonnull Artist *)artist;
- (nonnull instancetype)initWithGenre:(nonnull Genre *)genre;

@end
