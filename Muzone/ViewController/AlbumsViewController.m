//
//  AlbumsViewController.m
//  Muzone
//
//  Created by Sukhrob Khakimov on 5/27/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <SDWebImage/UIImageView+WebCache.h>

#import "AppDelegate.h"
#import "BaseTheme.h"
#import "ApiClient.h"
#import "ErrorHandler.h"

#import "Album.h"
#import "Artist.h"
#import "Genre.h"
#import "Constants.h"

#import "BaseNavigationController.h"
#import "AlbumsViewController.h"
#import "AlbumViewController.h"

#import "SZKProgressView.h"
#import "SZKMessageView.h"

#import "AlbumsCollectionViewCell.h"
#import "FavoriteManager.h"
#import "User.h"

static CGFloat const padding = 8.0f;
static NSInteger const numberOfColumns = 2;

@interface AlbumsViewController () <SZKMessageViewDelegate>

@property (nonnull, nonatomic, strong) NSManagedObjectContext *mainManagedObjectContext;
@property (nonnull, nonatomic, strong) NSManagedObjectContext *workerManagedObjectContext;

@property (nonatomic, assign) BOOL allowLoading;
@property (nonatomic, assign) BOOL reloading;
@property (nonatomic, assign) NSInteger page;

@property (nonnull, nonatomic, copy) NSString *cellReuseIdentifier;
@property (nonnull, nonatomic, strong) NSMutableArray *albums;
@property (nonnull, nonatomic, strong) Artist *artist;
@property (nonnull, nonatomic, strong) Genre *genre;

@property (nonnull, nonatomic, strong) UICollectionView *collectionView;
@property (nonnull, nonatomic, strong) UICollectionViewFlowLayout *collectionViewFlowLayout;

@property (nonnull, nonatomic, strong) UIRefreshControl *refreshControl;
@property (nonnull, nonatomic, strong) SZKProgressView *progressView;
@property (nonnull, nonatomic, strong) SZKMessageView *messageView;

@end

@implementation AlbumsViewController

- (instancetype)init {
    self = [super init];
    
    if (self) {
        [self commonInit];
    }
    
    return self;
}

- (instancetype)initWithArtist:(Artist *)artist {
    self = [super init];
    
    if (self) {
        [self commonInit];
        self.artist = artist;
    }
    
    return self;
}

- (instancetype)initWithGenre:(Genre *)genre {
    self = [super init];
    
    if (self) {
        [self commonInit];
        self.genre = genre;
    }
    
    return self;
}

- (void)commonInit {
    self.allowLoading = YES;
    self.reloading = NO;
    self.page = 1;
    self.cellReuseIdentifier = NSStringFromClass([AlbumsCollectionViewCell class]);
}

- (void)loadView {
    [super loadView];
    
    if (self.artist) {
        self.navigationItem.title = self.artist.name;
        self.navigationItem.rightBarButtonItem = [self rightFavoriteButton:NO];
    } else if (self.genre) {
        self.navigationItem.title = self.genre.title;
    } else {
        self.navigationItem.title = NSLocalizedString(@"albums", nil);
    }
    
    [self.view addSubview:self.collectionView];
    [self.collectionView insertSubview:self.refreshControl
                               atIndex:0];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[collectionView]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:@{@"collectionView": self.collectionView}]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[collectionView]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:@{@"collectionView": self.collectionView}]];
}

-(UIBarButtonItem*)rightFavoriteButton:(BOOL)progress
{
    if (progress) {
        UIActivityIndicatorView * activityView = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
        [activityView sizeToFit];
        [activityView setAutoresizingMask:(UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin)];
        [activityView startAnimating];
        UIBarButtonItem *loadingView = [[UIBarButtonItem alloc] initWithCustomView:activityView];
        return loadingView;
    }
    if ([[FavoriteManager sharedManager] isFavoriteArtist:self.artist]) {
        UIBarButtonItem *button =  [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_favorite_white"]
                                                                    style:UIBarButtonItemStylePlain
                                                                   target:self
                                                                   action:@selector(removeFromFavorite:)];
        return button;
        
    }
    else
    {
        
        UIBarButtonItem *button =  [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_favorite_border_white"]
                                                                    style:UIBarButtonItemStylePlain
                                                                   target:self
                                                                   action:@selector(addToFavorite:)];
        return button;
    }
}



#pragma mark - Favorite

-(void)addToFavorite:(id)sender
{
    if ([User currentUser].accessToken == nil)
    {
        [[User currentUser] showLoginForm];
    }
    else
    {
        [self showLoadingIndicator];
        self.navigationItem.rightBarButtonItem = [self rightFavoriteButton:YES];
        WEAKIFY_SELF;
        FavoriteManager *fManager = [FavoriteManager sharedManager];
        [fManager addToFavoriteArtist:self.artist complete:^(BOOL success) {
            STRONGIFY_SELF;
            [self setRightBarButtonItem];
            [self hideLoadingIndicator];
        }];
    }
}

-(void)removeFromFavorite:(id)sender
{
    if ([User currentUser].accessToken == nil)
    {
        [[User currentUser] showLoginForm];
    }
    else
    {
        [self showLoadingIndicator];
        self.navigationItem.rightBarButtonItem = [self rightFavoriteButton:YES];
        WEAKIFY_SELF;
        FavoriteManager *fManager = [FavoriteManager sharedManager];
        [fManager deleteFromFavoriteArtist:self.artist complete:^(BOOL success) {
            STRONGIFY_SELF;
            [self setRightBarButtonItem];
            [self hideLoadingIndicator];
            
        }];
    }
}

- (void)setRightBarButtonItem {
    self.navigationItem.rightBarButtonItem = [self rightFavoriteButton:NO];
}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.artist) {
        [Answers logContentViewWithName:self.artist.name
                            contentType:@"Albums"
                              contentId:[NSString stringWithFormat:@"%@", self.artist.uid]
                       customAttributes:@{@"type": @"Artist"}];
    } else if (self.genre) {
        [Answers logContentViewWithName:self.genre.title
                            contentType:@"Albums"
                              contentId:[NSString stringWithFormat:@"%@", self.genre.uid]
                       customAttributes:@{@"type": @"Genre"}];
    } else {
        [Answers logContentViewWithName:@"All"
                            contentType:@"Albums"
                              contentId:nil
                       customAttributes:nil];
    }
    
    [self loadAlbumsWithPage:self.page];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.collectionView.contentInset = [self collectionViewContentInset];
    
    // NSNotificationCenter
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    
    [notificationCenter addObserver:self
                           selector:@selector(playerViewControllerWillPlay:)
                               name:PlayerViewControllerWillPlay
                             object:nil];
    
    [notificationCenter addObserver:self
                           selector:@selector(playerViewControllerWillStop:)
                               name:PlayerViewControllerWillStop
                             object:nil];
    
    ((BaseNavigationController *)self.navigationController).statusBarStyle = UIStatusBarStyleLightContent;
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"bg_nav.png"]
                                                  forBarMetrics:UIBarMetricsDefault];
}

- (void)viewWillDisappear:(BOOL)animated {
    // NSNotificationCenter
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    
    [notificationCenter removeObserver:self
                                  name:PlayerViewControllerWillPlay
                                object:nil];
    
    [notificationCenter removeObserver:self
                                  name:PlayerViewControllerWillStop
                                object:nil];
    
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - NSMutableArray

- (NSMutableArray *)albums {
    if (_albums == nil) {
        _albums = [NSMutableArray new];
    }
    
    return _albums;
}

#pragma mark - NSNotificationCenter

- (void)playerViewControllerWillPlay:(NSNotification *)notification {
    self.collectionView.contentInset = [self collectionViewContentInset];
}

- (void)playerViewControllerWillStop:(NSNotification *)notification {
    self.collectionView.contentInset = [self collectionViewContentInset];
}

#pragma mark - CoreData

- (NSManagedObjectContext *)mainManagedObjectContext {
    if (_mainManagedObjectContext == nil) {
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        _mainManagedObjectContext = appDelegate.mainManagedObjectContext;
    }
    
    return _mainManagedObjectContext;
}

- (NSManagedObjectContext *)workerManagedObjectContext {
    if (_workerManagedObjectContext == nil) {
        _workerManagedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        _workerManagedObjectContext.parentContext = self.mainManagedObjectContext;
    }
    
    return _workerManagedObjectContext;
}

#pragma mark - UICollectionView

- (UICollectionView *)collectionView {
    if (_collectionView == nil) {
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero
                                             collectionViewLayout:self.collectionViewFlowLayout];
        _collectionView.translatesAutoresizingMaskIntoConstraints = NO;
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.alwaysBounceVertical = YES;
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        [_collectionView registerClass:[AlbumsCollectionViewCell class]
            forCellWithReuseIdentifier:self.cellReuseIdentifier];
    }
    
    return _collectionView;
}

- (UIEdgeInsets)collectionViewContentInset {
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    return [appDelegate contentInsetForScrollView:self.collectionView];
}

#pragma mark - UICollectionViewFlowLayout

- (UICollectionViewFlowLayout *)collectionViewFlowLayout {
    if (_collectionViewFlowLayout == nil) {
        _collectionViewFlowLayout = [[UICollectionViewFlowLayout alloc] init];
        _collectionViewFlowLayout.minimumLineSpacing = padding;
        _collectionViewFlowLayout.minimumInteritemSpacing = 0.0f;
        _collectionViewFlowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    }
    
    return _collectionViewFlowLayout;
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.albums.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    Album *album = self.albums[indexPath.item];
    
    AlbumsCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:self.cellReuseIdentifier
                                                                                  forIndexPath:indexPath];
    __weak AlbumsCollectionViewCell *weakCell = cell;
    
    [cell.activityIndicatorView startAnimating];
    [cell.coverImageView sd_setImageWithURL:[NSURL URLWithString:album.imageUrl] placeholderImage:[UIImage imageNamed:@"img_placeholder.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [weakCell.activityIndicatorView stopAnimating];
        
        if (image) {
            if (cacheType == SDImageCacheTypeNone) {
                weakCell.coverImageView.alpha = 0.0f;
                [UIView animateWithDuration:0.25f animations:^{
                    weakCell.coverImageView.alpha = 1.0f;
                } completion:^(BOOL finished) {
                    weakCell.coverImageView.image = image;
                }];
            } else {
                weakCell.coverImageView.image = image;
            }
        }
    }];
    cell.titleLabel.text = album.title;
    cell.yearLabel.text = [NSString stringWithFormat:@"%@", album.year];
    cell.artistNameLabel.text = [NSString stringWithFormat:NSLocalizedString(@"by", nil), album.artist.name];
    
    return cell;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(padding, padding, padding, padding);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat width = (self.view.bounds.size.width - (numberOfColumns + 1) * padding) / numberOfColumns;
    CGFloat height = width + 45.0f + 2.0f; // Plus 2.0f to show the bottom shadow
    
    return CGSizeMake(width, height);
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.allowLoading && indexPath.item == self.albums.count - 1) {
        [self loadAlbumsWithPage:self.page + 1];
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    Album *album = self.albums[indexPath.item];
    AlbumViewController *viewController = [[AlbumViewController alloc] initWithAlbum:album];
    [self.navigationController pushViewController:viewController
                                         animated:YES];
}

#pragma mark - UIRefreshControl

- (UIRefreshControl *)refreshControl {
    if (_refreshControl == nil) {
        _refreshControl = [[UIRefreshControl alloc] init];
        _refreshControl.backgroundColor = [UIColor whiteColor];
        [_refreshControl addTarget:self
                            action:@selector(refresh)
                  forControlEvents:UIControlEventValueChanged];
    }
    
    return _refreshControl;
}

- (void)refresh {
    if (self.allowLoading) {
        [self reloadAlbums];
    } else {
        [self.refreshControl endRefreshing];
    }
}

#pragma mark - SZKProgressView

- (SZKProgressView *)progressView {
    if (_progressView == nil) {
        _progressView = [[SZKProgressView alloc] init];
    }
    
    return _progressView;
}

#pragma mark - SZKMessageView

- (SZKMessageView *)messageView {
    if (_messageView == nil) {
        _messageView = [[SZKMessageView alloc] init];
        _messageView.delegate = self;
    }
    
    return _messageView;
}

#pragma mark - SZKMessageViewDelegate

- (void)messageView:(SZKMessageView *)messageView didTapActionButton:(UIButton *)actionButton {
    [self reloadAlbums];
}

#pragma mark - Loading indicator

- (void)showLoadingIndicator {
    if (self.albums.count == 0) {
        [self.messageView hide];
        [self.progressView showInView:self.view];
    }
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    self.allowLoading = NO;
}

- (void)hideLoadingIndicator {
    [self.refreshControl endRefreshing];
    [self.progressView hide];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    self.allowLoading = YES;
}

#pragma mark - ApiClient

- (void)loadAlbumsWithPage:(NSInteger)page {
    NSString *type;
    NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithDictionary:@{@"page": @(page)}];
    
    if (self.artist) {
        type = @"byartist";
        [parameters setObject:self.artist.uid
                       forKey:@"id"];
    } else if (self.genre) {
        type = @"bygenre";
        [parameters setObject:self.genre.uid
                       forKey:@"id"];
    } else {
        type = @"all";
    }
    
    [self showLoadingIndicator];
    ApiClient *client = [ApiClient sharedClient];
    [client albumsWithParameters:parameters type:type success:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        [self hideLoadingIndicator];
        
        if (self.reloading) {
            self.reloading = NO;
            self.collectionView.contentOffset = CGPointMake(0.0f, 0.0f - self.collectionView.contentInset.top);
            [self.albums removeAllObjects];
        }
        
        NSInteger totalCount = self.albums.count;
        NSArray *data = responseObject;
        NSInteger numberOfResults = data.count;
        
        if (numberOfResults > 0) {
            NSMutableArray *indexPaths = [NSMutableArray new];
            
            for (NSInteger index = 0; index < numberOfResults; index++) {
                Album *album = [[Album alloc] initWithDictionary:data[index]
                                                         context:self.workerManagedObjectContext];
                [self.albums addObject:album];
                
                NSIndexPath *indexPath = [NSIndexPath indexPathForItem:totalCount + index
                                                             inSection:0];
                [indexPaths addObject:indexPath];
            }
            
            self.page = page;
            
            if (totalCount == 0) {
                [self.collectionView reloadData];
            } else {
                [self.collectionView insertItemsAtIndexPaths:indexPaths];
            }
        } else if (totalCount == 0) {
            [self.collectionView reloadData];
            [self.messageView showInView:self.view
                               iconImage:nil
                                   title:nil
                                 message:NSLocalizedString(@"no_data", nil)
                       actionButtonTitle:NSLocalizedString(@"reload", nil)];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error) {
        [self hideLoadingIndicator];
        
        if (error.code != NSURLErrorCancelled) {
            if (self.albums.count > 0) {
                [ErrorHandler handleError:error];
            } else {
                [self.messageView showInView:self.view
                                   iconImage:nil
                                       title:nil
                                     message:[ErrorHandler messageFromError:error]
                           actionButtonTitle:NSLocalizedString(@"reload", nil)];
            }
        } else {
            [self.messageView showInView:self.view
                               iconImage:nil
                                   title:nil
                                 message:nil
                       actionButtonTitle:NSLocalizedString(@"reload", nil)];
        }
    }];
}

- (void)reloadAlbums {
    self.reloading = YES;
    [self loadAlbumsWithPage:1];
}

@end
