//
//  AllAlbumsViewController.h
//  Muzone
//
//  Created by Macbook on 4/8/17.
//  Copyright © 2017 Muzone LLC. All rights reserved.
//

#import "BaseViewController.h"

@class Artist;
@class Genre;


@interface AllAlbumsViewController : BaseViewController <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

- (instancetype)initWithType:(LoadingListType)type;
@end
