//
//  AllAlbumsViewController.m
//  Muzone
//
//  Created by Macbook on 4/8/17.
//  Copyright © 2017 Muzone LLC. All rights reserved.
//

#import "AllAlbumsViewController.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <SDWebImage/UIImageView+WebCache.h>

#import "AppDelegate.h"
#import "BaseTheme.h"
#import "ApiClient.h"
#import "ErrorHandler.h"
#import "Constants.h"
#import "FavoriteManager.h"

#import "Album.h"
#import "Artist.h"
#import "Genre.h"
#import "Track.h"

#import "BaseNavigationController.h"
#import "AlbumViewController.h"

#import "SZKProgressView.h"
#import "SZKMessageView.h"

#import "AlbumsCollectionViewCell.h"

#import "TrackRepository.h"


static CGFloat const padding = 8.0f;
static NSInteger const numberOfColumns = 2;


@interface AllAlbumsViewController () <SZKMessageViewDelegate>
@property (nonnull, nonatomic, strong) NSManagedObjectContext *mainManagedObjectContext;
@property (nonnull, nonatomic, strong) NSManagedObjectContext *workerManagedObjectContext;
@property (nonnull, nonatomic, strong) TrackRepository *trackRepository;
@property (nonnull, nonatomic, strong) TrackManager *trackManager;


@property (nonatomic, assign) BOOL allowLoading;
@property (nonatomic, assign) BOOL reloading;

@property (nonnull, nonatomic, copy) NSString *cellReuseIdentifier;
@property (nonnull, nonatomic, strong) NSMutableArray *albums;
@property (nonnull, nonatomic, strong) UICollectionView *collectionView;
@property (nonnull, nonatomic, strong) UICollectionViewFlowLayout *collectionViewFlowLayout;

@property (nonnull, nonatomic, strong) UIRefreshControl *refreshControl;
@property (nonnull, nonatomic, strong) SZKProgressView *progressView;
@property (nonnull, nonatomic, strong) SZKMessageView *messageView;

@property (nonatomic) LoadingListType type;

@end

@implementation AllAlbumsViewController

- (instancetype)init {
    self = [super init];
    
    if (self) {
        [self commonInit];
    }
    
    return self;
}

- (instancetype)initWithType:(LoadingListType)type {
    self = [super init];
    
    if (self) {
        [self commonInit];
        self.type = type;
    }
    
    return self;
}

- (void)commonInit {
    self.allowLoading = YES;
    self.reloading = NO;
    self.cellReuseIdentifier = NSStringFromClass([AlbumsCollectionViewCell class]);
}

- (void)loadView {
    [super loadView];
    
    self.navigationItem.title = NSLocalizedString(@"albums", nil);
    
    
    [self.view addSubview:self.collectionView];
    [self.collectionView insertSubview:self.refreshControl
                               atIndex:0];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[collectionView]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:@{@"collectionView": self.collectionView}]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[collectionView]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:@{@"collectionView": self.collectionView}]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [Answers logContentViewWithName:@"Albums"
                        contentType:@"Menu"
                          contentId:nil
                   customAttributes:@{@"type": (self.type == LoadingListTypeSaved) ? @"Saved" : @"Favorites"}];
    [self loadAlbums];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.collectionView.contentInset = [self collectionViewContentInset];
    
    // NSNotificationCenter
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    
    [notificationCenter addObserver:self
                           selector:@selector(playerViewControllerWillPlay:)
                               name:PlayerViewControllerWillPlay
                             object:nil];
    
    [notificationCenter addObserver:self
                           selector:@selector(playerViewControllerWillStop:)
                               name:PlayerViewControllerWillStop
                             object:nil];

    [notificationCenter addObserver:self
                           selector:@selector(reloadAlbums:)
                               name:FavoriteManagerUpdateAlbumsNotification
                             object:nil];

    
    ((BaseNavigationController *)self.navigationController).statusBarStyle = UIStatusBarStyleLightContent;
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"bg_nav.png"]
                                                  forBarMetrics:UIBarMetricsDefault];
}

- (void)viewWillDisappear:(BOOL)animated {
    // NSNotificationCenter
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    
    [notificationCenter removeObserver:self
                                  name:PlayerViewControllerWillPlay
                                object:nil];
    
    [notificationCenter removeObserver:self
                                  name:PlayerViewControllerWillStop
                                object:nil];
    
    [super viewWillDisappear:animated];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - NSMutableArray

- (NSMutableArray *)albums {
    if (_albums == nil) {
        _albums = [NSMutableArray new];
    }
    
    return _albums;
}

#pragma mark - NSNotificationCenter

- (void)playerViewControllerWillPlay:(NSNotification *)notification {
    self.collectionView.contentInset = [self collectionViewContentInset];
}

- (void)playerViewControllerWillStop:(NSNotification *)notification {
    self.collectionView.contentInset = [self collectionViewContentInset];
}

- (void)reloadAlbums:(NSNotification *)notification {
    [self loadAlbums];
}


#pragma mark - CoreData

- (NSManagedObjectContext *)mainManagedObjectContext {
    if (_mainManagedObjectContext == nil) {
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        _mainManagedObjectContext = appDelegate.mainManagedObjectContext;
    }
    
    return _mainManagedObjectContext;
}

- (NSManagedObjectContext *)workerManagedObjectContext {
    if (_workerManagedObjectContext == nil) {
        _workerManagedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        _workerManagedObjectContext.parentContext = self.mainManagedObjectContext;
    }
    
    return _workerManagedObjectContext;
}

- (TrackRepository *)trackRepository {
    if (_trackRepository == nil) {
        _trackRepository = [[TrackRepository alloc] initWithManagedObjectContext:self.workerManagedObjectContext];
    }
    
    return _trackRepository;
}


- (TrackManager *)trackManager {
    if (_trackManager == nil) {
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        _trackManager = appDelegate.trackManager;
    }
    
    return _trackManager;
}


#pragma mark - UICollectionView

- (UICollectionView *)collectionView {
    if (_collectionView == nil) {
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero
                                             collectionViewLayout:self.collectionViewFlowLayout];
        _collectionView.translatesAutoresizingMaskIntoConstraints = NO;
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.alwaysBounceVertical = YES;
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        [_collectionView registerClass:[AlbumsCollectionViewCell class]
            forCellWithReuseIdentifier:self.cellReuseIdentifier];
    }
    
    return _collectionView;
}

- (UIEdgeInsets)collectionViewContentInset {
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    return [appDelegate contentInsetForScrollView:self.collectionView];
}

#pragma mark - UICollectionViewFlowLayout

- (UICollectionViewFlowLayout *)collectionViewFlowLayout {
    if (_collectionViewFlowLayout == nil) {
        _collectionViewFlowLayout = [[UICollectionViewFlowLayout alloc] init];
        _collectionViewFlowLayout.minimumLineSpacing = padding;
        _collectionViewFlowLayout.minimumInteritemSpacing = 0.0f;
        _collectionViewFlowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    }
    
    return _collectionViewFlowLayout;
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.albums.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    Album *album = self.albums[indexPath.item];
    
    AlbumsCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:self.cellReuseIdentifier
                                                                               forIndexPath:indexPath];
    __weak AlbumsCollectionViewCell *weakCell = cell;
    
    [cell.activityIndicatorView startAnimating];
    [cell.coverImageView sd_setImageWithURL:[NSURL URLWithString:album.imageUrl] placeholderImage:[UIImage imageNamed:@"img_placeholder.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [weakCell.activityIndicatorView stopAnimating];
        
        if (image) {
            if (cacheType == SDImageCacheTypeNone) {
                weakCell.coverImageView.alpha = 0.0f;
                [UIView animateWithDuration:0.25f animations:^{
                    weakCell.coverImageView.alpha = 1.0f;
                } completion:^(BOOL finished) {
                    weakCell.coverImageView.image = image;
                }];
            } else {
                weakCell.coverImageView.image = image;
            }
        }
    }];
    cell.titleLabel.text = album.title;
    cell.yearLabel.text = [NSString stringWithFormat:@"%@", album.year];
    cell.artistNameLabel.text = [NSString stringWithFormat:NSLocalizedString(@"by", nil), album.artist.name];
    
    return cell;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(padding, padding, padding, padding);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat width = (self.view.bounds.size.width - (numberOfColumns + 1) * padding) / numberOfColumns;
    CGFloat height = width + 45.0f + 2.0f; // Plus 2.0f to show the bottom shadow
    
    return CGSizeMake(width, height);
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    Album *album = self.albums[indexPath.item];
    AlbumViewController *viewController = [[AlbumViewController alloc] initWithAlbum:album];
    [self.navigationController pushViewController:viewController
                                         animated:YES];
}

#pragma mark - UIRefreshControl

- (UIRefreshControl *)refreshControl {
    if (_refreshControl == nil) {
        _refreshControl = [[UIRefreshControl alloc] init];
        _refreshControl.backgroundColor = [UIColor whiteColor];
        [_refreshControl addTarget:self
                            action:@selector(refresh)
                  forControlEvents:UIControlEventValueChanged];
    }
    
    return _refreshControl;
}

- (void)refresh {
    if (self.allowLoading) {
        [self reloadAlbums];
    } else {
        [self.refreshControl endRefreshing];
    }
}

#pragma mark - SZKProgressView

- (SZKProgressView *)progressView {
    if (_progressView == nil) {
        _progressView = [[SZKProgressView alloc] init];
    }
    
    return _progressView;
}

#pragma mark - SZKMessageView

- (SZKMessageView *)messageView {
    if (_messageView == nil) {
        _messageView = [[SZKMessageView alloc] init];
        _messageView.delegate = self;
    }
    
    return _messageView;
}

#pragma mark - SZKMessageViewDelegate

- (void)messageView:(SZKMessageView *)messageView didTapActionButton:(UIButton *)actionButton {
    [self reloadAlbums];
}

#pragma mark - Loading indicator

- (void)showLoadingIndicator {
    if (self.albums.count == 0) {
        [self.messageView hide];
        [self.progressView showInView:self.view];
    }
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    self.allowLoading = NO;
}

- (void)hideLoadingIndicator {
    [self.refreshControl endRefreshing];
    [self.progressView hide];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    self.allowLoading = YES;
}

#pragma mark - ApiClient

- (void)loadAlbums {
    
    [self showLoadingIndicator];
    if (self.type == LoadingListTypeSaved) {
        [self.albums removeAllObjects];
        NSArray *result = [NSMutableArray arrayWithArray:[self.trackRepository fetchAllTracks]];
        self.albums = [[FavoriteManager sharedManager] getUniqueAlbumsFromTracks:[NSMutableArray arrayWithArray:result]];
        if (self.albums.count > 0) {
            [self.collectionView reloadData];
            [self hideLoadingIndicator];
        }
        else
        {
            [self.collectionView reloadData];
            [self hideLoadingIndicator];
            [self.messageView showInView:self.view
                               iconImage:nil
                                   title:nil
                                 message:NSLocalizedString(@"no_data", nil)
                       actionButtonTitle:NSLocalizedString(@"reload", nil)];
            
        }

        
    }
    else if (self.type == LoadingListTypeFavorites)
    {
        [self.albums removeAllObjects];
        WEAKIFY_SELF;
        FavoriteManager *fManager = [FavoriteManager sharedManager];
        [fManager getFavoriteAlbums:^(BOOL success, NSArray * _Nullable result) {
            if (success) {
                STRONGIFY_SELF;
                [self hideLoadingIndicator];
                
                self.albums = [NSMutableArray arrayWithArray:result];
                if (self.albums.count > 0) {
                    [self.collectionView reloadData];
                    [self hideLoadingIndicator];
                }
                else
                {
                    [self.collectionView reloadData];
                    [self hideLoadingIndicator];
                    [self.messageView showInView:self.view
                                       iconImage:nil
                                           title:nil
                                         message:NSLocalizedString(@"no_data", nil)
                               actionButtonTitle:NSLocalizedString(@"reload", nil)];
                    
                }

            }
            else
            {
                [self.collectionView reloadData];
                [self hideLoadingIndicator];
                [self.messageView showInView:self.view
                                   iconImage:nil
                                       title:nil
                                     message:NSLocalizedString(@"no_data", nil)
                           actionButtonTitle:NSLocalizedString(@"reload", nil)];
            
            }
        }];
    }
}



- (void)reloadAlbums {
    self.reloading = YES;
    [self loadAlbums];
}


@end
