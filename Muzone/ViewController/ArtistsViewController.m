//
//  ArtistsViewController.m
//  Muzone
//
//  Created by Macbook on 3/26/17.
//  Copyright © 2017 Muzone LLC. All rights reserved.
//

#import "ArtistsViewController.h"
#import "AppDelegate.h"
#import "UIImageView+WebCache.h"
#import "User.h"
#import "ApiClient.h"
#import "TrackRepository.h"
#import "Track+CoreDataProperties.h"
#import "Track.h"
#import "SZKProgressView.h"
#import "SZKMessageView.h"
#import "Constants.h"
#import "FavoriteManager.h"
#import "ArtistsTableViewCell.h"
#import "Artist.h"
#import "BaseTheme.h"
#import "AlbumsViewController.h"

@interface ArtistsViewController () <UITableViewDataSource, UITableViewDelegate, SZKMessageViewDelegate>

@property (nonatomic, retain) NSMutableArray *artists;

@property (nonnull, nonatomic, strong) NSManagedObjectContext *mainManagedObjectContext;
@property (nonnull, nonatomic, strong) NSManagedObjectContext *workerManagedObjectContext;
@property (nonnull, nonatomic, strong) TrackRepository *trackRepository;
@property (nonnull, nonatomic, strong) TrackManager *trackManager;

@property (nonnull, nonatomic, strong) SZKProgressView *progressView;
@property (nonnull, nonatomic, strong) SZKMessageView *messageView;

@property (nonnull, nonatomic, strong) UITableView *tableView;
@property (nonnull, nonatomic, strong) NSMutableArray *sectionTitles;
@property (nonnull, nonatomic, strong) NSMutableArray *numberOfRowsInSections;
@property (nonnull, nonatomic, strong) NSMutableArray *lastIndexesInSections;


@property (nonatomic) LoadingListType type;

@end

@implementation ArtistsViewController

- (instancetype)initWithType:(LoadingListType)type {
    self = [super init];
    
    if (self) {
        self.type = type;
    }
    
    return self;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    [Answers logContentViewWithName:@"Artists"
                        contentType:@"Menu"
                          contentId:nil
                   customAttributes:@{@"type": (self.type == LoadingListTypeSaved) ? @"Saved" : @"Favorites"}];

    [self loadArtists:self.type];
}

-(void)loadView
{
    [super loadView];
    
    self.navigationItem.title = NSLocalizedString(@"artists", nil);;
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:self.tableView];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[tableView]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:@{@"tableView": self.tableView}]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[tableView]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:@{@"tableView": self.tableView}]];

}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // NSNotificationCenter
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    

    [notificationCenter addObserver:self
                           selector:@selector(reloadArtists:)
                               name:FavoriteManagerUpdateArtistsNotification
                             object:nil];
    
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


-(void)reloadArtists:(NSNotification *)notification
{
    [self loadArtists:self.type];
}

- (void)loadArtists:(LoadingListType)type {
    
    [self showLoadingIndicator];
    FavoriteManager *fManager = [FavoriteManager sharedManager];
    if (type == LoadingListTypeSaved)
    {
        [self hideLoadingIndicator];
        NSMutableArray *initailResult = [NSMutableArray arrayWithArray:[self.trackRepository fetchAllTracks]];
        NSMutableArray *result = [fManager getUniqueArtistsFromTracks:initailResult];
        if (result.count > 0) {
            [self reloadTable:result];
            self.tableView.hidden = NO;
            [self hideLoadingIndicator];

        }
        else
        {
            [self.tableView reloadData];
            self.tableView.hidden = YES;
            [self hideLoadingIndicator];
            [self.messageView showInView:self.view
                               iconImage:nil
                                   title:nil
                                 message:NSLocalizedString(@"no_data", nil)
                       actionButtonTitle:NSLocalizedString(@"reload", nil)];
        }
    }
    else if (type == LoadingListTypeFavorites)
    {
        [fManager getFavoriteArtists:^(BOOL success, NSArray * _Nullable result) {
            if (success)
            {
                if (result.count > 0) {
                    [self reloadTable:result];
                    self.tableView.hidden = NO;
                    [self hideLoadingIndicator];
                }
                else
                {
                    [self.tableView reloadData];
                    self.tableView.hidden = YES;
                    [self hideLoadingIndicator];
                    [self.messageView showInView:self.view
                                       iconImage:nil
                                           title:nil
                                         message:NSLocalizedString(@"no_data", nil)
                               actionButtonTitle:NSLocalizedString(@"reload", nil)];
                    

                }
            }
            else
            {
                [self.tableView reloadData];
                self.tableView.hidden = YES;
                [self hideLoadingIndicator];
                [self.messageView showInView:self.view
                                   iconImage:nil
                                       title:nil
                                     message:NSLocalizedString(@"no_data", nil)
                           actionButtonTitle:NSLocalizedString(@"reload", nil)];
            }
        }];
    }
}

-(void)reloadTable:(NSArray*)artists
{
    [self.sectionTitles removeAllObjects];
    [self.numberOfRowsInSections removeAllObjects];
    [self.lastIndexesInSections removeAllObjects];
    [self.artists removeAllObjects];
    
    
    NSArray *data = artists;
    NSInteger numberOfResults = data.count;
    
    if (numberOfResults > 0) {
        NSInteger numberOfRowsInSection = 0;
        
        for (NSInteger index = 0; index < numberOfResults; index++) {
            Artist *artist = data[index];
            [self.artists addObject:artist];
            
            NSString *sectionTitle = [artist.name substringToIndex:1];
            
            if (![self.sectionTitles.lastObject isEqual:sectionTitle]) {
                numberOfRowsInSection = 0;
                
                [self.sectionTitles addObject:sectionTitle];
                [self.numberOfRowsInSections addObject:@(numberOfRowsInSection + 1)];
                
                NSInteger lastIndexInSection = [self.lastIndexesInSections.lastObject integerValue];
                [self.lastIndexesInSections addObject:@(lastIndexInSection + numberOfRowsInSection + 1)];
            } else {
                NSInteger numberOfRowsInLastSection = [self.numberOfRowsInSections.lastObject integerValue];
                [self.numberOfRowsInSections replaceObjectAtIndex:self.sectionTitles.count - 1
                                                       withObject:@(numberOfRowsInLastSection + numberOfRowsInSection + 1)];
                
                NSInteger lastIndexInSection = [self.lastIndexesInSections.lastObject integerValue];
                [self.lastIndexesInSections replaceObjectAtIndex:self.lastIndexesInSections.count - 1
                                                      withObject:@(lastIndexInSection + numberOfRowsInSection + 1)];
            }
        }
        
    }
    [self.tableView reloadData];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView

- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero
                                                  style:UITableViewStylePlain];
        _tableView.translatesAutoresizingMaskIntoConstraints = NO;
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        [_tableView registerClass:[ArtistsTableViewCell class]
           forCellReuseIdentifier:NSStringFromClass([ArtistsTableViewCell class])];
        _tableView.sectionIndexColor = [[BaseTheme currentTheme] tintColor];
        _tableView.sectionIndexBackgroundColor = [UIColor clearColor];
        _tableView.dataSource = self;
        _tableView.delegate = self;

    }
    
    return _tableView;
}

- (NSMutableArray *)sectionTitles {
    if (_sectionTitles == nil) {
        _sectionTitles = [NSMutableArray new];
    }
    
    return _sectionTitles;
}

- (NSMutableArray *)numberOfRowsInSections {
    if (_numberOfRowsInSections == nil) {
        _numberOfRowsInSections = [NSMutableArray new];
    }
    
    return _numberOfRowsInSections;
}

- (NSMutableArray *)lastIndexesInSections {
    if (_lastIndexesInSections == nil) {
        _lastIndexesInSections = [NSMutableArray new];
    }
    
    return _lastIndexesInSections;
}

- (NSMutableArray *)artists {
    if (_artists == nil) {
        _artists = [NSMutableArray new];
    }
    
    return _artists;
}




- (NSInteger)indexFromIndexPath:(NSIndexPath *)indexPath {
    return [self.lastIndexesInSections[indexPath.section] integerValue] - [self.numberOfRowsInSections[indexPath.section] integerValue] + indexPath.row;
}

#pragma mark - UITableViewDataSource

- (NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return self.sectionTitles;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.sectionTitles.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.numberOfRowsInSections[section] integerValue];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger index = [self indexFromIndexPath:indexPath];
    Artist *artist = self.artists[index];
    
    ArtistsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ArtistsTableViewCell class])
                                                                 forIndexPath:indexPath];
    cell.titleLabel.text = artist.name;
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [[BaseTheme currentTheme] sectionBackgroundColor];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    titleLabel.font = [UIFont boldSystemFontOfSize:17.0f];
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.text = self.sectionTitles[section];
    
    [headerView addSubview:titleLabel];
    
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-space-[titleLabel]-space-|"
                                                                       options:0
                                                                       metrics:@{@"space": @(15.0f)}
                                                                         views:@{@"titleLabel": titleLabel}]];
    
    [headerView addConstraint:[NSLayoutConstraint constraintWithItem:titleLabel
                                                           attribute:NSLayoutAttributeCenterY
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:headerView
                                                           attribute:NSLayoutAttributeCenterY
                                                          multiplier:1.0f
                                                            constant:0.0f]];
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 32.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger index = [self indexFromIndexPath:indexPath];
    Artist *artist = self.artists[index];
    
    AlbumsViewController *viewController = [[AlbumsViewController alloc] initWithArtist:artist];
    [self.navigationController pushViewController:viewController
                                         animated:YES];
}


#pragma mark - Loading indicator

- (void)showLoadingIndicator {
    if (self.artists.count == 0) {
        [self.messageView hide];
        [self.progressView showInView:self.view];
    }
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)hideLoadingIndicator {
    [self.progressView hide];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}


#pragma mark - CoreData

- (NSManagedObjectContext *)mainManagedObjectContext {
    if (_mainManagedObjectContext == nil) {
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        _mainManagedObjectContext = appDelegate.mainManagedObjectContext;
    }
    
    return _mainManagedObjectContext;
}

- (NSManagedObjectContext *)workerManagedObjectContext {
    if (_workerManagedObjectContext == nil) {
        _workerManagedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        _workerManagedObjectContext.parentContext = self.mainManagedObjectContext;
    }
    
    return _workerManagedObjectContext;
}

- (TrackRepository *)trackRepository {
    if (_trackRepository == nil) {
        _trackRepository = [[TrackRepository alloc] initWithManagedObjectContext:self.workerManagedObjectContext];
    }
    
    return _trackRepository;
}

- (TrackManager *)trackManager {
    if (_trackManager == nil) {
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        _trackManager = appDelegate.trackManager;
    }
    
    return _trackManager;
}


#pragma mark - SZKProgressView

- (SZKProgressView *)progressView {
    if (_progressView == nil) {
        _progressView = [[SZKProgressView alloc] init];
    }
    
    return _progressView;
}

#pragma mark - SZKMessageView

- (SZKMessageView *)messageView {
    if (_messageView == nil) {
        _messageView = [[SZKMessageView alloc] init];
        _messageView.delegate = self;
    }
    
    return _messageView;
}

#pragma mark - SZKMessageViewDelegate

- (void)messageView:(SZKMessageView *)messageView didTapActionButton:(UIButton *)actionButton {
    [self loadArtists:self.type];
}




@end
