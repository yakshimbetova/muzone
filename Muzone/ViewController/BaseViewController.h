//
//  BaseViewController.h
//  Muzone
//
//  Created by Sukhrob Khakimov on 5/22/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"
typedef NS_ENUM (NSInteger, LoadingListType) {
    LoadingListTypeSaved,
    LoadingListTypeFavorites,
};

@interface BaseViewController : UIViewController

@end
