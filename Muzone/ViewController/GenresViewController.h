//
//  GenresViewController.h
//  Muzone
//
//  Created by Macbook on 3/26/17.
//  Copyright © 2017 Muzone LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
@interface GenresViewController : BaseViewController
- (instancetype)initWithType:(LoadingListType)type;
@end
