//
//  GenresViewController.m
//  Muzone
//
//  Created by Macbook on 3/26/17.
//  Copyright © 2017 Muzone LLC. All rights reserved.
//

#import "GenresViewController.h"
#import "GenresCollectionViewCell.h"
#import "Genre.h"
#import "Genre+CoreDataProperties.h"
#import "AppDelegate.h"
#import "UIImageView+WebCache.h"
#import "AlbumsViewController.h"
#import "User.h"
#import "ApiClient.h"
#import "TrackRepository.h"
#import "Track+CoreDataProperties.h"
#import "Track.h"
#import "SZKProgressView.h"
#import "SZKMessageView.h"
#import "Constants.h"
#import "FavoriteManager.h"
#import "ArtistsTableViewCell.h"
#import "BaseTheme.h"

@interface GenresViewController () <SZKMessageViewDelegate, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, retain) NSMutableArray *genres;

@property (nonnull, nonatomic, strong) NSManagedObjectContext *mainManagedObjectContext;
@property (nonnull, nonatomic, strong) NSManagedObjectContext *workerManagedObjectContext;
@property (nonnull, nonatomic, strong) TrackRepository *trackRepository;
@property (nonnull, nonatomic, strong) TrackManager *trackManager;

@property (nonnull, nonatomic, strong) SZKProgressView *progressView;
@property (nonnull, nonatomic, strong) SZKMessageView *messageView;

@property (nonnull, nonatomic, strong) UITableView *tableView;
@property (nonnull, nonatomic, strong) NSMutableArray *sectionTitles;
@property (nonnull, nonatomic, strong) NSMutableArray *numberOfRowsInSections;
@property (nonnull, nonatomic, strong) NSMutableArray *lastIndexesInSections;

@property (nonatomic) LoadingListType type;
@end

@implementation GenresViewController

- (instancetype)initWithType:(LoadingListType)type {
    self = [super init];
    
    if (self) {
        self.type = type;
    }
    
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [Answers logContentViewWithName:@"Genres"
                        contentType:@"Menu"
                          contentId:nil
                   customAttributes:@{@"type": (self.type == LoadingListTypeSaved) ? @"Saved" : @"Favorites"}];
    
    [self reloadGenresWithType:self.type];
    
}

-(void)loadView
{
    [super loadView];
    
    self.navigationItem.title = NSLocalizedString(@"genres", nil);
    
    [self.view addSubview:self.tableView];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[tableView]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:@{@"tableView": self.tableView}]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[tableView]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:@{@"tableView": self.tableView}]];

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // NSNotificationCenter
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    
    
    [notificationCenter addObserver:self
                           selector:@selector(reloadTracks:)
                               name:FavoriteManagerUpdateTracksNotification
                             object:nil];
    
    ;
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark - NSNotificationCenter


- (void)reloadTracks:(NSNotification *)notification {
    [self reloadGenresWithType:self.type];
}



- (void)reloadGenresWithType:(LoadingListType)type  {
    [self showLoadingIndicator];
    if (type == LoadingListTypeSaved) {
            NSMutableArray *result = [NSMutableArray arrayWithArray:[self.trackRepository fetchAllTracks]];
            NSInteger totalResult = result.count;
            [self.genres removeAllObjects];
            [self hideLoadingIndicator];
            if (totalResult > 0) {
                [self reloadTable:[[FavoriteManager sharedManager] getUniqueGenresFromTracks:result]];
                self.tableView.hidden = NO;
            }        
            else
            {
                [self.tableView reloadData];
                self.tableView.hidden = YES;
                [self.messageView showInView:self.view
                                   iconImage:nil
                                       title:nil
                                     message:NSLocalizedString(@"no_data", nil)
                           actionButtonTitle:NSLocalizedString(@"reload", nil)];

            }
    }
    else
    {
        WEAKIFY_SELF;
        FavoriteManager *fManager = [FavoriteManager sharedManager];
        [fManager getFavoriteGenres:^(BOOL success, NSArray * _Nullable result) {
            STRONGIFY_SELF;
            [self hideLoadingIndicator];
            if (success)
            {
                if (result.count > 0) {
                    [self reloadTable:result];
                    self.tableView.hidden = NO;
                }
                else
                {
                    [self.tableView reloadData];
                    self.tableView.hidden = YES;
                    [self.messageView showInView:self.view
                                       iconImage:nil
                                           title:nil
                                         message:NSLocalizedString(@"no_data", nil)
                               actionButtonTitle:NSLocalizedString(@"reload", nil)];
                    
                }

            }
            else
            {
                [self.tableView reloadData];
                [self hideLoadingIndicator];
                [self.messageView showInView:self.view
                                   iconImage:nil
                                       title:nil
                                     message:NSLocalizedString(@"no_data", nil)
                           actionButtonTitle:NSLocalizedString(@"reload", nil)];
            }

        }];
    }
}


-(void)reloadTable:(NSArray*)artists
{
    [self.sectionTitles removeAllObjects];
    [self.numberOfRowsInSections removeAllObjects];
    [self.lastIndexesInSections removeAllObjects];
    [self.genres removeAllObjects];
    
    
    NSArray *data = artists;
    NSInteger numberOfResults = data.count;
    
    if (numberOfResults > 0) {
        NSInteger numberOfRowsInSection = 0;
        
        for (NSInteger index = 0; index < numberOfResults; index++) {
            Genre *genre = data[index];
            [self.genres addObject:genre];
            
            NSString *sectionTitle = [genre.title substringToIndex:1];
            
            if (![self.sectionTitles.lastObject isEqual:sectionTitle]) {
                numberOfRowsInSection = 0;
                
                [self.sectionTitles addObject:sectionTitle];
                [self.numberOfRowsInSections addObject:@(numberOfRowsInSection + 1)];
                
                NSInteger lastIndexInSection = [self.lastIndexesInSections.lastObject integerValue];
                [self.lastIndexesInSections addObject:@(lastIndexInSection + numberOfRowsInSection + 1)];
            } else {
                NSInteger numberOfRowsInLastSection = [self.numberOfRowsInSections.lastObject integerValue];
                [self.numberOfRowsInSections replaceObjectAtIndex:self.sectionTitles.count - 1
                                                       withObject:@(numberOfRowsInLastSection + numberOfRowsInSection + 1)];
                
                NSInteger lastIndexInSection = [self.lastIndexesInSections.lastObject integerValue];
                [self.lastIndexesInSections replaceObjectAtIndex:self.lastIndexesInSections.count - 1
                                                      withObject:@(lastIndexInSection + numberOfRowsInSection + 1)];
            }
        }
        
    }
    [self.tableView reloadData];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - NSMutableArray

- (NSMutableArray *)genres {
    if (_genres == nil) {
        _genres = [NSMutableArray new];
    }
    
    return _genres;
}


#pragma mark - UITableView

- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero
                                                  style:UITableViewStylePlain];
        _tableView.translatesAutoresizingMaskIntoConstraints = NO;
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        [_tableView registerClass:[ArtistsTableViewCell class]
           forCellReuseIdentifier:NSStringFromClass([ArtistsTableViewCell class])];
        _tableView.sectionIndexColor = [[BaseTheme currentTheme] tintColor];
        _tableView.sectionIndexBackgroundColor = [UIColor clearColor];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        
    }
    
    return _tableView;
}

- (NSMutableArray *)sectionTitles {
    if (_sectionTitles == nil) {
        _sectionTitles = [NSMutableArray new];
    }
    
    return _sectionTitles;
}

- (NSMutableArray *)numberOfRowsInSections {
    if (_numberOfRowsInSections == nil) {
        _numberOfRowsInSections = [NSMutableArray new];
    }
    
    return _numberOfRowsInSections;
}

- (NSMutableArray *)lastIndexesInSections {
    if (_lastIndexesInSections == nil) {
        _lastIndexesInSections = [NSMutableArray new];
    }
    
    return _lastIndexesInSections;
}



- (NSInteger)indexFromIndexPath:(NSIndexPath *)indexPath {
    return [self.lastIndexesInSections[indexPath.section] integerValue] - [self.numberOfRowsInSections[indexPath.section] integerValue] + indexPath.row;
}

#pragma mark - UITableViewDataSource

- (NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return self.sectionTitles;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.sectionTitles.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.numberOfRowsInSections[section] integerValue];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger index = [self indexFromIndexPath:indexPath];
    Genre *genre = self.genres[index];
    
    ArtistsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ArtistsTableViewCell class])
                                                                 forIndexPath:indexPath];
    cell.titleLabel.text = genre.title;
    return cell;
}

#pragma mark - UITableViewDelegate

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [[BaseTheme currentTheme] sectionBackgroundColor];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    titleLabel.font = [UIFont boldSystemFontOfSize:17.0f];
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.text = self.sectionTitles[section];
    
    [headerView addSubview:titleLabel];
    
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-space-[titleLabel]-space-|"
                                                                       options:0
                                                                       metrics:@{@"space": @(15.0f)}
                                                                         views:@{@"titleLabel": titleLabel}]];
    
    [headerView addConstraint:[NSLayoutConstraint constraintWithItem:titleLabel
                                                           attribute:NSLayoutAttributeCenterY
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:headerView
                                                           attribute:NSLayoutAttributeCenterY
                                                          multiplier:1.0f
                                                            constant:0.0f]];
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 32.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger index = [self indexFromIndexPath:indexPath];
    Genre *genre = self.genres[index];
    AlbumsViewController *viewController = [[AlbumsViewController alloc] initWithGenre:genre];
    [self.navigationController pushViewController:viewController
                                         animated:YES];
}



#pragma mark - Loading indicator

- (void)showLoadingIndicator {
    if (self.genres.count == 0) {
        [self.messageView hide];
        [self.progressView showInView:self.view];
    }
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)hideLoadingIndicator {
    [self.progressView hide];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

#pragma mark - CoreData
         
 - (NSManagedObjectContext *)mainManagedObjectContext {
     if (_mainManagedObjectContext == nil) {
         AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
         _mainManagedObjectContext = appDelegate.mainManagedObjectContext;
     }
     
     return _mainManagedObjectContext;
 }
 
 - (NSManagedObjectContext *)workerManagedObjectContext {
     if (_workerManagedObjectContext == nil) {
         _workerManagedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
         _workerManagedObjectContext.parentContext = self.mainManagedObjectContext;
     }
     
     return _workerManagedObjectContext;
 }
 
 - (TrackRepository *)trackRepository {
     if (_trackRepository == nil) {
         _trackRepository = [[TrackRepository alloc] initWithManagedObjectContext:self.workerManagedObjectContext];
     }
     
     return _trackRepository;
 }
 
 - (TrackManager *)trackManager {
     if (_trackManager == nil) {
         AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
         _trackManager = appDelegate.trackManager;
     }
     
     return _trackManager;
 }


#pragma mark - SZKProgressView

- (SZKProgressView *)progressView {
    if (_progressView == nil) {
        _progressView = [[SZKProgressView alloc] init];
    }
    
    return _progressView;
}

#pragma mark - SZKMessageView

- (SZKMessageView *)messageView {
    if (_messageView == nil) {
        _messageView = [[SZKMessageView alloc] init];
        _messageView.delegate = self;
    }
    
    return _messageView;
}

#pragma mark - SZKMessageViewDelegate

- (void)messageView:(SZKMessageView *)messageView didTapActionButton:(UIButton *)actionButton {
    [self reloadGenresWithType:self.type];
}



@end
