//
//  HomeViewController.h
//  Muzone
//
//  Created by Sukhrob Khakimov on 5/22/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import "BaseViewController.h"

@interface HomeViewController : BaseViewController <UIToolbarDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITableViewDataSource, UITableViewDelegate>

@end
