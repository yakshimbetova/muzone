//
//  HomeViewController.m
//  Muzone
//
//  Created by Sukhrob Khakimov on 5/22/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <SDWebImage/UIImageView+WebCache.h>

#import "AppDelegate.h"
#import "Constants.h"
#import "BaseTheme.h"
#import "ApiClient.h"
#import "ErrorHandler.h"

#import "Album.h"
#import "Artist.h"
#import "Genre.h"

#import "BaseNavigationController.h"
#import "HomeViewController.h"
#import "SearchResultsViewController.h"
#import "AlbumsViewController.h"
#import "AlbumViewController.h"

#import "SZKPaginatorView.h"
#import "SZKMessageView.h"

#import "PageCollectionViewCollectionReusableView.h"
#import "PageCollectionViewCollectionViewCell.h"
#import "PageTableViewCollectionViewCell.h"
#import "AlbumsCollectionViewCollectionViewCell.h"
#import "AlbumsCollectionViewCell.h"
#import "GenresCollectionViewCell.h"
#import "ArtistsTableViewCell.h"

#import "Muzone-Swift.h"

static CGFloat const toolbarHeight = 50.0f;
static CGFloat const padding = 8.0f;
static NSInteger const numberOfColumns = 2;

@interface HomeViewController () <SZKPaginatorViewDataSource, SZKPaginatorViewDelegate, SZKMessageViewDelegate>

@property (nonnull, nonatomic, strong) NSManagedObjectContext *mainManagedObjectContext;
@property (nonnull, nonatomic, strong) NSManagedObjectContext *workerManagedObjectContext;

@property (nonatomic, assign) BOOL allowSelection;

@property (nonnull, nonatomic, strong) NSArray *pages;

@property (nonnull, nonatomic, strong) NSMutableArray *reloadingStates;
@property (nonnull, nonatomic, strong) NSMutableArray *loadingStates;
@property (nonnull, nonatomic, strong) NSMutableArray *errorMessages;
@property (nonnull, nonatomic, strong) NSMutableArray *sections;
@property (nonnull, nonatomic, strong) NSMutableArray *allAlbums;
@property (nonnull, nonatomic, strong) NSMutableArray *featuredAlbums;
@property (nonnull, nonatomic, strong) NSMutableArray *latestAlbums;
@property (nonnull, nonatomic, strong) NSMutableArray *allGenres;
@property (nonnull, nonatomic, strong) NSMutableArray *featuredGenres;
@property (nonnull, nonatomic, strong) NSMutableArray *artists;
@property (nonnull, nonatomic, strong) NSMutableArray *playlists;
@property (nonnull, nonatomic, strong) NSMutableArray *currentPages;
@property (nonnull, nonatomic, strong) NSMutableArray *scrollViewContentOffsets;
@property (nonnull, nonatomic, strong) NSMutableArray *sectionTitles;
@property (nonnull, nonatomic, strong) NSMutableArray *numberOfRowsInSections;
@property (nonnull, nonatomic, strong) NSMutableArray *lastIndexesInSections;

@property (nonnull, nonatomic, strong) UIToolbar *toolbar;
@property (nonnull, nonatomic, strong) SZKPaginatorView *paginatorView;

@property (nonnull, nonatomic, strong) UICollectionView *collectionView;
@property (nonnull, nonatomic, strong) UICollectionViewFlowLayout *collectionViewFlowLayout;

@end

@implementation HomeViewController

- (instancetype)init {
    self = [super init];
    
    if (self) {
        self.allowSelection = YES;
    }
    
    return self;
}

- (void)loadView {
    [super loadView];
    
    self.navigationItem.title = APP_NAME;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_search.png"]
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(rightBarButtonItemTapped:)];
    
    [self.view addSubview:self.toolbar];
    [self.view addSubview:self.collectionView];
    [self.toolbar addSubview:self.paginatorView];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[toolbar]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:@{@"toolbar": self.toolbar}]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[collectionView]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:@{@"collectionView": self.collectionView}]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[toolbar(height)][collectionView]|"
                                                                      options:0
                                                                      metrics:@{@"height": @(toolbarHeight)}
                                                                        views:@{@"toolbar": self.toolbar,
                                                                                @"collectionView": self.collectionView}]];
    
    [self.toolbar addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[paginatorView]|"
                                                                         options:0
                                                                         metrics:nil
                                                                           views:@{@"paginatorView": self.paginatorView}]];
    
    [self.toolbar addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[paginatorView]|"
                                                                         options:0
                                                                         metrics:nil
                                                                           views:@{@"paginatorView": self.paginatorView}]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self reloadAlbumsWithType:@"featured"
                  selectedPage:0];
    [self reloadAlbumsWithType:@"featured"
                  selectedPage:1];
    [self reloadAlbumsWithType:@"latest"
                  selectedPage:2];
    
    [self reloadGenresWithType:@"featured"
                  selectedPage:3];
    
    [self reloadArtistsWithSelectedPage:4];
    [self reloadPlaylistsWithSelectedPage:5];
    
    [Answers logContentViewWithName:@"Home"
                        contentType:@"Menu"
                          contentId:nil
                   customAttributes:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // NSNotificationCenter
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    
    [notificationCenter addObserver:self
                           selector:@selector(playerViewControllerWillPlay:)
                               name:PlayerViewControllerWillPlay
                             object:nil];
    
    [notificationCenter addObserver:self
                           selector:@selector(playerViewControllerWillStop:)
                               name:PlayerViewControllerWillStop
                             object:nil];
    
    ((BaseNavigationController *)self.navigationController).statusBarStyle = UIStatusBarStyleLightContent;
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"bg_nav.png"]
                                                  forBarMetrics:UIBarMetricsDefault];
    
    [self updateScrollViewsContentInset];
    [self deselectSelectedRow];
}

- (void)viewWillDisappear:(BOOL)animated {
    // NSNotificationCenter
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    
    [notificationCenter removeObserver:self
                                  name:PlayerViewControllerWillPlay
                                object:nil];
    
    [notificationCenter removeObserver:self
                                  name:PlayerViewControllerWillStop
                                object:nil];
    
    [self hideLoadingIndicator];
    
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - NSArray

- (NSArray *)pages {
    if (_pages == nil) {
        _pages = @[[NSLocalizedString(@"home", nil) uppercaseStringWithLocale:[NSLocale currentLocale]],
                   [NSLocalizedString(@"featured", nil) uppercaseStringWithLocale:[NSLocale currentLocale]],
                   [NSLocalizedString(@"latest", nil) uppercaseStringWithLocale:[NSLocale currentLocale]],
                   [NSLocalizedString(@"genres", nil) uppercaseStringWithLocale:[NSLocale currentLocale]],
                   [NSLocalizedString(@"artists", nil) uppercaseStringWithLocale:[NSLocale currentLocale]],
                   [NSLocalizedString(@"playlists", nil) uppercaseStringWithLocale:[NSLocale currentLocale]]];
    }
    
    return _pages;
}

#pragma mark - NSMutableArray

- (NSMutableArray *)reloadingStates {
    if (_reloadingStates == nil) {
        _reloadingStates = [NSMutableArray new];
        
        for (NSInteger index = 0; index < self.pages.count; index++) {
            _reloadingStates[index] = @YES;
        }
    }
    
    return _reloadingStates;
}

- (NSMutableArray *)loadingStates {
    if (_loadingStates == nil) {
        _loadingStates = [NSMutableArray new];
        
        for (NSInteger index = 0; index < self.pages.count; index++) {
            _loadingStates[index] = @NO;
        }
    }
    
    return _loadingStates;
}

- (NSMutableArray *)errorMessages {
    if (_errorMessages == nil) {
        _errorMessages = [NSMutableArray new];
        
        for (NSInteger index = 0; index < self.pages.count; index++) {
            _errorMessages[index] = @"";
        }
    }
    
    return _errorMessages;
}

- (NSMutableArray *)sections {
    if (_sections == nil) {
        _sections = [NSMutableArray new];
    }
    
    return _sections;
}

- (NSMutableArray *)allAlbums {
    if (_allAlbums == nil) {
        _allAlbums = [NSMutableArray new];
    }
    
    return _allAlbums;
}

- (NSMutableArray *)featuredAlbums {
    if (_featuredAlbums == nil) {
        _featuredAlbums = [NSMutableArray new];
    }
    
    return _featuredAlbums;
}

- (NSMutableArray *)latestAlbums {
    if (_latestAlbums == nil) {
        _latestAlbums = [NSMutableArray new];
    }
    
    return _latestAlbums;
}

- (NSMutableArray *)allGenres {
    if (_allGenres == nil) {
        _allGenres = [NSMutableArray new];
    }
    
    return _allGenres;
}

- (NSMutableArray *)featuredGenres {
    if (_featuredGenres == nil) {
        _featuredGenres = [NSMutableArray new];
    }
    
    return _featuredGenres;
}

- (NSMutableArray *)artists {
    if (_artists == nil) {
        _artists = [NSMutableArray new];
    }
    
    return _artists;
}

- (NSMutableArray *)playlists {
    if (_playlists == nil) {
        _playlists = [NSMutableArray new];
    }
    
    return _playlists;
}

- (NSMutableArray *)currentPages {
    if (_currentPages == nil) {
        _currentPages = [NSMutableArray new];
        
        for (NSInteger index = 0; index <= self.pages.count; index++) {
            if (index == 1 || index == 3) {
                _currentPages[index] = @(2);
            } else {
                _currentPages[index] = @(1);
            }
        }
    }
    
    return _currentPages;
}

- (NSMutableArray *)scrollViewContentOffsets {
    if (_scrollViewContentOffsets == nil) {
        _scrollViewContentOffsets = [NSMutableArray new];
        
        for (NSInteger index = 0; index <= self.pages.count; index++) {
            _scrollViewContentOffsets[index] = NSStringFromCGPoint(CGPointZero);
        }
    }
    
    return _scrollViewContentOffsets;
}

- (NSMutableArray *)sectionTitles {
    if (_sectionTitles == nil) {
        _sectionTitles = [NSMutableArray new];
    }
    
    return _sectionTitles;
}

- (NSMutableArray *)numberOfRowsInSections {
    if (_numberOfRowsInSections == nil) {
        _numberOfRowsInSections = [NSMutableArray new];
    }
    
    return _numberOfRowsInSections;
}

- (NSMutableArray *)lastIndexesInSections {
    if (_lastIndexesInSections == nil) {
        _lastIndexesInSections = [NSMutableArray new];
    }
    
    return _lastIndexesInSections;
}

#pragma mark - NSNotificationCenter

- (void)playerViewControllerWillPlay:(NSNotification *)notification {
    [self updateScrollViewsContentInset];
}

- (void)playerViewControllerWillStop:(NSNotification *)notification {
    [self updateScrollViewsContentInset];
}

#pragma mark - CoreData

- (NSManagedObjectContext *)mainManagedObjectContext {
    if (_mainManagedObjectContext == nil) {
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        _mainManagedObjectContext = appDelegate.mainManagedObjectContext;
    }
    
    return _mainManagedObjectContext;
}

- (NSManagedObjectContext *)workerManagedObjectContext {
    if (_workerManagedObjectContext == nil) {
        _workerManagedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        _workerManagedObjectContext.parentContext = self.mainManagedObjectContext;
    }
    
    return _workerManagedObjectContext;
}

#pragma mark - UINavigationBar

- (void)rightBarButtonItemTapped:(id)sender {
    SearchResultsViewController *viewController = [[SearchResultsViewController alloc] init];
    BaseNavigationController *navigationController = [[BaseNavigationController alloc] initWithRootViewController:viewController];
    navigationController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    navigationController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:navigationController
                       animated:YES
                     completion:nil];
}

#pragma mark - UIToolbar

- (UIToolbar *)toolbar {
    if (_toolbar == nil) {
        _toolbar = [[UIToolbar alloc] init];
        _toolbar.translatesAutoresizingMaskIntoConstraints = NO;
        _toolbar.backgroundColor = [[BaseTheme currentTheme] tintColor];
        _toolbar.delegate = self;
    }
    
    return _toolbar;
}

#pragma mark - UIToolbarDelegate

- (UIBarPosition)positionForBar:(id<UIBarPositioning>)bar {
    return UIBarPositionTopAttached;
}

#pragma mark - SZKPaginatorView

- (SZKPaginatorView *)paginatorView {
    if (_paginatorView == nil) {
        _paginatorView = [[SZKPaginatorView alloc] init];
        _paginatorView.translatesAutoresizingMaskIntoConstraints = NO;
        _paginatorView.backgroundColor = [[BaseTheme currentTheme] tintColor];
        _paginatorView.dataSource = self;
        _paginatorView.delegate = self;
    }
    
    return _paginatorView;
}

#pragma mark - SZKPaginatorViewDataSource

- (NSInteger)numberOfPagesInPaginatorView:(SZKPaginatorView *)paginatorView {
    return self.pages.count;
}

- (UIView *)paginatorView:(SZKPaginatorView *)paginatorView viewForPageAtIndex:(NSInteger)index selected:(BOOL)selected {
    UIView *view = [[UIView alloc] init];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    titleLabel.font = [UIFont boldSystemFontOfSize:11.0f];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = self.pages[index];
    
    if (selected) {
        titleLabel.textColor = [UIColor whiteColor];
    } else {
        titleLabel.textColor = [UIColor colorWithRed:255.0f / 255
                                               green:255.0f / 255
                                                blue:255.0f / 255
                                               alpha:0.5f];
    }
    
    [view addSubview:titleLabel];
    
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[titleLabel]|"
                                                                 options:0
                                                                 metrics:nil
                                                                   views:@{@"titleLabel": titleLabel}]];
    
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[titleLabel]|"
                                                                 options:0
                                                                 metrics:nil
                                                                   views:@{@"titleLabel": titleLabel}]];
    
    return view;
}

#pragma mark - SZKPaginatorViewDelegate

- (CGSize)paginatorView:(SZKPaginatorView *)paginatorView sizeForPageAtIndex:(NSInteger)index {
    NSString *title = self.pages[index];
    CGRect rect = [title boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, toolbarHeight)
                                      options:NSStringDrawingUsesLineFragmentOrigin
                                   attributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:11.0f]}
                                      context:nil];
    
    return CGSizeMake(rect.size.width + 12.0f * 2, toolbarHeight);
}

- (void)paginatorView:(SZKPaginatorView *)paginatorView willSelectPageAtIndex:(NSInteger)index {
    self.allowSelection = NO;
    [self.collectionView selectItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0]
                                      animated:YES
                                scrollPosition:UICollectionViewScrollPositionCenteredHorizontally];
}

- (void)paginatorView:(SZKPaginatorView *)paginatorView didSelectPageAtIndex:(NSInteger)index {
    self.allowSelection = YES;
}

#pragma mark - SZKMessageViewDelegate

- (void)messageView:(SZKMessageView *)messageView didTapActionButton:(UIButton *)actionButton {
    [self refresh];
}

#pragma mark - UICollectionView

- (UICollectionView *)collectionView {
    if (_collectionView == nil) {
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero
                                             collectionViewLayout:self.collectionViewFlowLayout];
        _collectionView.translatesAutoresizingMaskIntoConstraints = NO;
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.pagingEnabled = YES;
        _collectionView.showsHorizontalScrollIndicator = NO;
        _collectionView.bounces = NO;
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        [_collectionView registerClass:[PageCollectionViewCollectionViewCell class]
            forCellWithReuseIdentifier:NSStringFromClass([PageCollectionViewCollectionViewCell class])];
        [_collectionView registerClass:[PageTableViewCollectionViewCell class]
            forCellWithReuseIdentifier:NSStringFromClass([PageTableViewCollectionViewCell class])];
    }
    
    return _collectionView;
}

- (UIEdgeInsets)contentInsetForScrollView:(UIScrollView *)scrollView {
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    return [appDelegate contentInsetForScrollView:scrollView];
}

- (void)updateScrollViewsContentInset {
    for (NSInteger index = 0; index < 4; index++) {
        PageCollectionViewCollectionViewCell *cell = (PageCollectionViewCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0]];
        if ([cell collectionView] != nil) {
            cell.collectionView.contentInset = [self contentInsetForScrollView:cell.collectionView];
        }
        
    }
    
    for (NSInteger index = 4; index < 5; index++) {
        PageTableViewCollectionViewCell *cell = (PageTableViewCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0]];
        if ([cell respondsToSelector:@selector(tableView)]) {
            if ([cell tableView] != nil) {
                cell.tableView.contentInset = [self contentInsetForScrollView:cell.tableView];
            }
    }
    }
}

#pragma mark - UICollectionViewFlowLayout

- (UICollectionViewFlowLayout *)collectionViewFlowLayout {
    if (_collectionViewFlowLayout == nil) {
        _collectionViewFlowLayout = [[UICollectionViewFlowLayout alloc] init];
        _collectionViewFlowLayout.sectionInset = UIEdgeInsetsZero;
        _collectionViewFlowLayout.minimumLineSpacing = 0.0f;
        _collectionViewFlowLayout.minimumInteritemSpacing = 0.0f;
        _collectionViewFlowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    }
    
    return _collectionViewFlowLayout;
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    if (collectionView == self.collectionView) {
        return 1;
    } else if (collectionView.tag == 0) {
        NSInteger numberOfSections = self.sections.count;
        
        if (numberOfSections != 2) {
            return 0;
        }
        
        return numberOfSections;
    }
    
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (collectionView == self.collectionView) {
        return self.pages.count;
    } else if (collectionView.tag == 0) {
        if (section == 0) {
            return 1;
        }
        
        return self.featuredGenres.count;
    } else if (collectionView.tag == 1) {
        return self.featuredAlbums.count;
    } else if (collectionView.tag == 2) {
        return self.latestAlbums.count;
    } else if (collectionView.tag == 3) {
        return self.allGenres.count;
    } else if (collectionView.tag == self.pages.count) {
        return self.allAlbums.count;
    }
    
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == self.collectionView) {
        if (indexPath.item == 4 || indexPath.item == 5) {
            PageTableViewCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([PageTableViewCollectionViewCell class])
                                                                                                 forIndexPath:indexPath];
            cell.tableView.contentInset = [self contentInsetForScrollView:cell.tableView];
            cell.messageView.delegate = self;
            [cell.messageView hide];
            [cell.refreshControl addTarget:self
                                    action:@selector(refresh)
                          forControlEvents:UIControlEventValueChanged];
            [cell.refreshControl endRefreshing];
            
            if ([self.reloadingStates[indexPath.item] boolValue]) {
                if (self.artists.count > 0) {
                    [cell.refreshControl beginRefreshing];
                } else {
                    [cell.progressView showInView:cell.contentView];
                }
            } else {
                [cell.refreshControl endRefreshing];
                [cell.progressView hide];
            
                NSString *errorMessage = self.errorMessages[indexPath.item];
                
                if (errorMessage.length > 0) {
                    if (self.artists.count == 0) {
                        [cell.refreshControl removeFromSuperview];
                        [cell.messageView showInView:cell.contentView
                                           iconImage:nil
                                               title:nil
                                             message:errorMessage
                                   actionButtonTitle:NSLocalizedString(@"reload", nil)];
                    } else {
                        [cell.tableView insertSubview:cell.refreshControl
                                              atIndex:0];
                    }
                } else {
                    if (self.artists.count == 0) {
                        [cell.refreshControl removeFromSuperview];
                        [cell.messageView showInView:cell.contentView
                                           iconImage:nil
                                               title:nil
                                             message:NSLocalizedString(@"no_data", nil)
                                   actionButtonTitle:NSLocalizedString(@"reload", nil)];
                    } else {
                        [cell.tableView insertSubview:cell.refreshControl
                                              atIndex:0];
                    }
                }
            }
            
            cell.tableView.sectionIndexColor = [[BaseTheme currentTheme] tintColor];
            cell.tableView.sectionIndexBackgroundColor = [UIColor clearColor];
            cell.tableView.tag = indexPath.item;
            cell.tableView.dataSource = self;
            cell.tableView.delegate = self;
            [cell.tableView reloadData];
            cell.tableView.contentOffset = CGPointFromString(self.scrollViewContentOffsets[indexPath.item]);
            
            if (indexPath.item == 5) {
                cell.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
            } else {
                cell.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
            }
            
            return cell;
        }
        
        PageCollectionViewCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([PageCollectionViewCollectionViewCell class])
                                                                                                  forIndexPath:indexPath];
        cell.collectionView.contentInset = [self contentInsetForScrollView:cell.collectionView];
        cell.messageView.delegate = self;
        [cell.messageView hide];
        [cell.refreshControl addTarget:self
                                action:@selector(refresh)
                      forControlEvents:UIControlEventValueChanged];
        [cell.refreshControl endRefreshing];
        
        NSString *errorMessage = self.errorMessages[indexPath.item];
        
        if (indexPath.item == 0) {
            if ([self.reloadingStates[indexPath.item] boolValue]) {
                if (self.sections.count == 2) {
                    [cell.refreshControl beginRefreshing];
                } else {
                    [cell.progressView showInView:cell.contentView];
                }
            } else {
                [cell.refreshControl endRefreshing];
                [cell.progressView hide];
                
                if (errorMessage.length > 0) {
                    if (self.sections.count == 0) {
                        [cell.refreshControl removeFromSuperview];
                        [cell.messageView showInView:cell.contentView
                                           iconImage:nil
                                               title:nil
                                             message:errorMessage
                                   actionButtonTitle:NSLocalizedString(@"reload", nil)];
                    } else {
                        [cell.collectionView insertSubview:cell.refreshControl
                                                   atIndex:0];
                    }
                } else {
                    if (self.sections.count == 0) {
                        [cell.refreshControl removeFromSuperview];
                        [cell.messageView showInView:cell.contentView
                                           iconImage:nil
                                               title:nil
                                             message:NSLocalizedString(@"no_data", nil)
                                   actionButtonTitle:NSLocalizedString(@"reload", nil)];
                    } else {
                        [cell.collectionView insertSubview:cell.refreshControl
                                                   atIndex:0];
                    }
                }
            }
        } else if (indexPath.item == 1) {
            if ([self.reloadingStates[indexPath.item] boolValue]) {
                if (self.featuredAlbums.count > 0) {
                    [cell.refreshControl beginRefreshing];
                } else {
                    [cell.progressView showInView:cell.contentView];
                }
            } else {
                [cell.refreshControl endRefreshing];
                [cell.progressView hide];
                
                if (errorMessage.length > 0) {
                    if (self.featuredAlbums.count == 0) {
                        [cell.refreshControl removeFromSuperview];
                        [cell.messageView showInView:cell.contentView
                                           iconImage:nil
                                               title:nil
                                             message:errorMessage
                                   actionButtonTitle:NSLocalizedString(@"reload", nil)];
                    } else {
                        [cell.collectionView insertSubview:cell.refreshControl
                                                   atIndex:0];
                    }
                } else {
                    if (self.featuredAlbums.count == 0) {
                        [cell.refreshControl removeFromSuperview];
                        [cell.messageView showInView:cell.contentView
                                           iconImage:nil
                                               title:nil
                                             message:NSLocalizedString(@"no_data", nil)
                                   actionButtonTitle:NSLocalizedString(@"reload", nil)];
                    } else {
                        [cell.collectionView insertSubview:cell.refreshControl
                                                   atIndex:0];
                    }
                }
            }
        } else if (indexPath.item == 2) {
            if ([self.reloadingStates[indexPath.item] boolValue]) {
                if (self.latestAlbums.count > 0) {
                    [cell.refreshControl beginRefreshing];
                } else {
                    [cell.progressView showInView:cell.contentView];
                }
            } else {
                [cell.refreshControl endRefreshing];
                [cell.progressView hide];
                
                if (errorMessage.length > 0) {
                    if (self.latestAlbums.count == 0) {
                        [cell.refreshControl removeFromSuperview];
                        [cell.messageView showInView:cell.contentView
                                           iconImage:nil
                                               title:nil
                                             message:errorMessage
                                   actionButtonTitle:NSLocalizedString(@"reload", nil)];
                    } else {
                        [cell.collectionView insertSubview:cell.refreshControl
                                                   atIndex:0];
                    }
                } else {
                    if (self.latestAlbums.count == 0) {
                        [cell.refreshControl removeFromSuperview];
                        [cell.messageView showInView:cell.contentView
                                           iconImage:nil
                                               title:nil
                                             message:NSLocalizedString(@"no_data", nil)
                                   actionButtonTitle:NSLocalizedString(@"reload", nil)];
                    } else {
                        [cell.collectionView insertSubview:cell.refreshControl
                                                   atIndex:0];
                    }
                }
            }
        } else if (indexPath.item == 3) {
            if ([self.reloadingStates[indexPath.item] boolValue]) {
                if (self.allGenres.count > 0) {
                    [cell.refreshControl beginRefreshing];
                } else {
                    [cell.progressView showInView:cell.contentView];
                }
            } else {
                [cell.refreshControl endRefreshing];
                [cell.progressView hide];
                
                if (errorMessage.length > 0) {
                    if (self.allGenres.count == 0) {
                        [cell.refreshControl removeFromSuperview];
                        [cell.messageView showInView:cell.contentView
                                           iconImage:nil
                                               title:nil
                                             message:errorMessage
                                   actionButtonTitle:NSLocalizedString(@"reload", nil)];
                    } else {
                        [cell.collectionView insertSubview:cell.refreshControl
                                                   atIndex:0];
                    }
                } else {
                    if (self.allGenres.count == 0) {
                        [cell.refreshControl removeFromSuperview];
                        [cell.messageView showInView:cell.contentView
                                           iconImage:nil
                                               title:nil
                                             message:NSLocalizedString(@"no_data", nil)
                                   actionButtonTitle:NSLocalizedString(@"reload", nil)];
                    } else {
                        [cell.collectionView insertSubview:cell.refreshControl
                                                   atIndex:0];
                    }
                }
            }
        }
        
        cell.collectionView.tag = indexPath.item;
        cell.collectionView.dataSource = self;
        cell.collectionView.delegate = self;
        [cell.collectionView reloadData];
        cell.collectionView.contentOffset = CGPointFromString(self.scrollViewContentOffsets[indexPath.item]);
        
        return cell;
    } else if (collectionView.tag == 0) {
        if (indexPath.section == 0) {
            AlbumsCollectionViewCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([AlbumsCollectionViewCollectionViewCell class])
                                                                                                        forIndexPath:indexPath];
            cell.collectionView.tag = self.pages.count;
            cell.collectionView.dataSource = self;
            cell.collectionView.delegate = self;
            [cell.collectionView reloadData];
            cell.collectionView.contentOffset = CGPointFromString(self.scrollViewContentOffsets[cell.collectionView.tag]);
            
            return cell;
        }
        
        return [self genres:self.featuredGenres
             collectionView:collectionView
     cellForItemAtIndexPath:indexPath];
    } else if (collectionView.tag == 1) {
        return [self albums:self.featuredAlbums
             collectionView:collectionView
     cellForItemAtIndexPath:indexPath];
    } else if (collectionView.tag == 2) {
        return [self albums:self.latestAlbums
             collectionView:collectionView
     cellForItemAtIndexPath:indexPath];
    } else if (collectionView.tag == 3) {
        return [self genres:self.allGenres
             collectionView:collectionView
     cellForItemAtIndexPath:indexPath];
    } else if (collectionView.tag == self.pages.count) {
        return [self albums:self.allAlbums
             collectionView:collectionView
     cellForItemAtIndexPath:indexPath];
    }
    
    return nil;
}

- (UICollectionViewCell *)albums:(NSArray *)albums collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    Album *album = albums[indexPath.item];
    
    AlbumsCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([AlbumsCollectionViewCell class])
                                                                               forIndexPath:indexPath];
    __weak AlbumsCollectionViewCell *weakCell = cell;
    
    [cell.activityIndicatorView startAnimating];
    [cell.coverImageView sd_setImageWithURL:[NSURL URLWithString:album.imageUrl] placeholderImage:[UIImage imageNamed:@"img_placeholder.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [weakCell.activityIndicatorView stopAnimating];
        
        if (image) {
            if (cacheType == SDImageCacheTypeNone) {
                weakCell.coverImageView.alpha = 0.0f;
                [UIView animateWithDuration:0.25f animations:^{
                    weakCell.coverImageView.alpha = 1.0f;
                } completion:^(BOOL finished) {
                    weakCell.coverImageView.image = image;
                }];
            } else {
                weakCell.coverImageView.image = image;
            }
        }
    }];
    cell.titleLabel.text = album.title;
    cell.yearLabel.text = [NSString stringWithFormat:@"%@", album.year];
    cell.artistNameLabel.text = [NSString stringWithFormat:NSLocalizedString(@"by", nil), album.artist.name];
    
    return cell;
}

- (UICollectionViewCell *)genres:(NSArray *)genres collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    Genre *genre = genres[indexPath.item];
    
    GenresCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([GenresCollectionViewCell class])
                                                                                  forIndexPath:indexPath];
    __weak GenresCollectionViewCell *weakCell = cell;
    
    [cell.activityIndicatorView startAnimating];
    [cell.coverImageView sd_setImageWithURL:[NSURL URLWithString:genre.imageUrl] placeholderImage:[UIImage imageNamed:@"img_placeholder.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [weakCell.activityIndicatorView stopAnimating];
        
        if (image) {
            if (cacheType == SDImageCacheTypeNone) {
                weakCell.coverImageView.alpha = 0.0f;
                [UIView animateWithDuration:0.25f animations:^{
                    weakCell.coverImageView.alpha = 1.0f;
                } completion:^(BOOL finished) {
                    weakCell.coverImageView.image = image;
                }];
            } else {
                weakCell.coverImageView.image = image;
            }
        }
    }];
    
    return cell;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    if (collectionView == self.collectionView) {
        return UIEdgeInsetsZero;
    } else if (collectionView.tag == 0 && section == 0) {
        return UIEdgeInsetsZero;
    } else if (collectionView.tag == self.pages.count) {
        return UIEdgeInsetsMake(0.0f, padding, 0.0f, padding);
    }
    
    return UIEdgeInsetsMake(padding, padding, padding, padding);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == self.collectionView) {
        return self.collectionView.bounds.size;
    } else if (collectionView.tag == 0) {
        if (indexPath.section == 0) {
            CGFloat width = (self.view.bounds.size.width - (numberOfColumns + 1) * padding) / 2.5f;
            CGFloat height = width + 45.0f + 2.0f; // Plus 2.0f to show the bottom shadow
            
            return CGSizeMake(self.view.bounds.size.width, height);
        }
        
        CGFloat width = (self.view.bounds.size.width - (numberOfColumns + 1) * padding) / 2;
        
        return CGSizeMake(width, width);
    } else if (collectionView.tag == 3) {
        CGFloat width = (self.view.bounds.size.width - (numberOfColumns + 1) * padding) / 2;
        
        return CGSizeMake(width, width);
    } else if (collectionView.tag == self.pages.count) {
        CGFloat width = (self.view.bounds.size.width - (numberOfColumns + 1) * padding) / 2.5f;
        CGFloat height = width + 45.0f;
        
        return CGSizeMake(width, height);
    }
    
    CGFloat width = (self.view.bounds.size.width - (numberOfColumns + 1) * padding) / 2;
    CGFloat height = width + 45.0f;
    
    return CGSizeMake(width, height);
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    PageCollectionViewCollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                                                                                                 withReuseIdentifier:NSStringFromClass([PageCollectionViewCollectionReusableView class])
                                                                                                        forIndexPath:indexPath];
    
    if (collectionView.tag == 0) {
        headerView.titleLabel.text = self.sections[indexPath.section];
    }
    
    return headerView;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    if (collectionView == self.collectionView) {
        return CGSizeZero;
    } else if (collectionView.tag == 0) {
        return CGSizeMake(self.view.bounds.size.width, 44.0f);
    }
    
    return CGSizeZero;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (collectionView != self.collectionView) {
        NSInteger selectedPage = collectionView.tag;
        NSInteger currentPage = [self.currentPages[selectedPage] integerValue];
        
        if (selectedPage == 1) {
            if (![self.loadingStates[selectedPage] boolValue] &&
                indexPath.item == self.featuredAlbums.count - 1) {
                [self loadAlbumsWithType:@"featured"
                            selectedPage:selectedPage
                             currentPage:currentPage + 1];
            }
        } else if (selectedPage == 2) {
            if (![self.loadingStates[selectedPage] boolValue] &&
                indexPath.item == self.latestAlbums.count - 1) {
                [self loadAlbumsWithType:@"latest"
                            selectedPage:selectedPage
                             currentPage:currentPage + 1];
            }
        } else if (selectedPage == 3) {
            if (![self.loadingStates[selectedPage] boolValue] &&
                indexPath.item == self.allGenres.count - 1) {
                [self loadGenresWithType:@"featured"
                            selectedPage:selectedPage
                             currentPage:currentPage + 1];
            }
        }
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger selectedPage = collectionView.tag;
    
    if (selectedPage == 0) {
        Genre *genre = self.featuredGenres[indexPath.item];
        AlbumsViewController *viewController = [[AlbumsViewController alloc] initWithGenre:genre];
        [self.navigationController pushViewController:viewController
                                             animated:YES];
    } else if (selectedPage == 1) {
        Album *album = self.featuredAlbums[indexPath.item];
        AlbumViewController *viewController = [[AlbumViewController alloc] initWithAlbum:album];
        [self.navigationController pushViewController:viewController
                                             animated:YES];
    } else if (selectedPage == 2) {
        Album *album = self.latestAlbums[indexPath.item];
        AlbumViewController *viewController = [[AlbumViewController alloc] initWithAlbum:album];
        [self.navigationController pushViewController:viewController
                                             animated:YES];
    } else if (selectedPage == 3) {
        Genre *genre = self.allGenres[indexPath.item];
        AlbumsViewController *viewController = [[AlbumsViewController alloc] initWithGenre:genre];
        [self.navigationController pushViewController:viewController
                                             animated:YES];
    } else if (selectedPage == self.pages.count) {
        Album *album = self.allAlbums[indexPath.item];
        AlbumViewController *viewController = [[AlbumViewController alloc] initWithAlbum:album];
        [self.navigationController pushViewController:viewController
                                             animated:YES];
    }
}

#pragma mark - UITableView

- (void)deselectSelectedRow {
    PageTableViewCollectionViewCell *cell = (PageTableViewCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:4 inSection:0]];
    NSIndexPath *indexPath = [cell.tableView indexPathForSelectedRow];
    
    if (indexPath) {
        [cell.tableView deselectRowAtIndexPath:indexPath
                                      animated:YES];
    }
}

- (NSInteger)indexFromIndexPath:(NSIndexPath *)indexPath {
    return [self.lastIndexesInSections[indexPath.section] integerValue] - [self.numberOfRowsInSections[indexPath.section] integerValue] + indexPath.row;
}

#pragma mark - UITableViewDataSource

- (NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    if (tableView.tag == 4) {
        return self.sectionTitles;
    }

    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (tableView.tag == 4) {
        return self.sectionTitles.count;
    }
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView.tag == 4) {
        return [self.numberOfRowsInSections[section] integerValue];
    }
    
    return self.playlists.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView.tag == 4) {
        NSInteger index = [self indexFromIndexPath:indexPath];
        Artist *artist = self.artists[index];
        
        ArtistsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ArtistsTableViewCell class])
                                                                     forIndexPath:indexPath];
        cell.titleLabel.text = artist.name;
        
        return cell;
    }
    
    Playlist *playlist = self.playlists[indexPath.row];
    
    PlaylistTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PlaylistTableViewCell class])
                                                                  forIndexPath:indexPath];
    [cell updateContentWithPlaylist:playlist];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (tableView.tag == 4) {
        UIView *headerView = [[UIView alloc] init];
        headerView.backgroundColor = [[BaseTheme currentTheme] sectionBackgroundColor];
        
        UILabel *titleLabel = [[UILabel alloc] init];
        titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        titleLabel.font = [UIFont boldSystemFontOfSize:17.0f];
        titleLabel.textColor = [UIColor blackColor];
        titleLabel.text = self.sectionTitles[section];
        
        [headerView addSubview:titleLabel];
        
        [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-space-[titleLabel]-space-|"
                                                                           options:0
                                                                           metrics:@{@"space": @(15.0f)}
                                                                             views:@{@"titleLabel": titleLabel}]];
        
        [headerView addConstraint:[NSLayoutConstraint constraintWithItem:titleLabel
                                                               attribute:NSLayoutAttributeCenterY
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:headerView
                                                               attribute:NSLayoutAttributeCenterY
                                                              multiplier:1.0f
                                                                constant:0.0f]];
        
        return headerView;
    }
    
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (tableView.tag == 4) {
        return 32.0f;
    }
    
    return 0.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView.tag == 4) {
        return 44.0f;
    }
    
    return 75.0f;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger selectedPage = tableView.tag;
    
    if (selectedPage == 4 && ![self.loadingStates[selectedPage] boolValue]) {
        NSInteger index = [self indexFromIndexPath:indexPath];
        
        if (index == self.artists.count - 1) {
            NSInteger currentPage = [self.currentPages[selectedPage] integerValue];
            [self loadArtistsWithSelectedPage:selectedPage
                                  currentPage:currentPage + 1];
        }
    } else if (selectedPage == 5 && ![self.loadingStates[selectedPage] boolValue]) {
        if (indexPath.row == self.playlists.count - 1) {
            NSInteger currentPage = [self.currentPages[selectedPage] integerValue];
            [self loadPlaylistsWithSelectedPage:selectedPage
                                    currentPage:currentPage + 1];
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView.tag == 4) {
        NSInteger index = [self indexFromIndexPath:indexPath];
        Artist *artist = self.artists[index];
        
        AlbumsViewController *viewController = [[AlbumsViewController alloc] initWithArtist:artist];
        [self.navigationController pushViewController:viewController
                                             animated:YES];
    } else {
        // FIXME: implement
    }
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == self.collectionView) {
        if (self.allowSelection) {
            NSInteger page = round(scrollView.contentOffset.x / scrollView.bounds.size.width);
            
            if (page < 0) {
                page = 0;
            } else {
                NSInteger lastPage = self.pages.count - 1;
                
                if (page > lastPage) {
                    page = lastPage;
                }
            }
            
            [self.paginatorView selectPage:page
                                  animated:YES
                                completion:nil];
        }
    } else {
        CGPoint contentOffset = scrollView.contentOffset;
        
        if (contentOffset.y < 0) {
            contentOffset.y = 0.0f;
        }
        
        self.scrollViewContentOffsets[scrollView.tag] = NSStringFromCGPoint(contentOffset);
    }
}

#pragma mark - UIRefreshControl

- (void)refresh {
    NSInteger selectedPage = round(self.collectionView.contentOffset.x / self.collectionView.bounds.size.width);
    [self.collectionView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:selectedPage inSection:0]]];
    
    if (![self.reloadingStates[selectedPage] boolValue]) {
        if (selectedPage == 0) {
            [self reloadAlbumsWithType:@"featured"
                          selectedPage:selectedPage];
        } else if (selectedPage == 1) {
            [self reloadAlbumsWithType:@"featured"
                          selectedPage:selectedPage];
        } else if (selectedPage == 2) {
            [self reloadAlbumsWithType:@"latest"
                          selectedPage:selectedPage];
        } else if (selectedPage == 3) {
            [self reloadGenresWithType:@"featured"
                          selectedPage:selectedPage];
        } else if (selectedPage == 4) {
            [self reloadArtistsWithSelectedPage:selectedPage];
        } else if (selectedPage == 5) {
            [self reloadPlaylistsWithSelectedPage:selectedPage];
        }
    } else {
        PageCollectionViewCollectionViewCell *cell = (PageCollectionViewCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:selectedPage inSection:0]];
        [cell.refreshControl endRefreshing];
    }
}

#pragma mark - Loading indicator

- (void)showLoadingIndicator {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)hideLoadingIndicator {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

#pragma mark - ApiClient

- (void)loadAlbumsWithType:(NSString *)type selectedPage:(NSInteger)selectedPage currentPage:(NSInteger)currentPage {
    PageCollectionViewCollectionViewCell *cell = (PageCollectionViewCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:selectedPage inSection:0]];
    self.loadingStates[selectedPage] = @YES;
    
    [self showLoadingIndicator];
    ApiClient *client = [ApiClient sharedClient];
    [client albumsWithParameters:@{@"page": @(currentPage)} type:type success:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        BOOL reloading = [self.reloadingStates[selectedPage] boolValue];
        
        if (reloading) {
            self.reloadingStates[selectedPage] = @NO;
            cell.collectionView.contentOffset = CGPointMake(0.0f, 0.0f - cell.collectionView.contentInset.top);
            
            if (selectedPage == 0) {
                [self.allAlbums removeAllObjects];
            } else if (selectedPage == 1) {
                [self.featuredAlbums removeAllObjects];
            } else {
                [self.latestAlbums removeAllObjects];
            }
        }
        
        NSArray *data = responseObject;
        NSInteger numberOfResults = data.count;
        
        if (numberOfResults > 0) {
            for (NSInteger index = 0; index < numberOfResults; index++) {
                Album *album = [[Album alloc] initWithDictionary:data[index]
                                                         context:self.workerManagedObjectContext];
                
                if (selectedPage == 0) {
                    [self.allAlbums addObject:album];
                } else if (selectedPage == 1) {
                    [self.featuredAlbums addObject:album];
                } else {
                    [self.latestAlbums addObject:album];
                }
            }
            
            self.currentPages[selectedPage] = @(currentPage);
        }
        
        [self hideLoadingIndicator];
        self.errorMessages[selectedPage] = @"";
        self.reloadingStates[selectedPage] = @NO;
        self.loadingStates[selectedPage] = @NO;
        
        if (selectedPage != 0 && (reloading || numberOfResults > 0)) {
            [self.collectionView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:selectedPage inSection:0]]];
        }
        
        if (selectedPage == 0 && reloading) {
            [self reloadGenresWithType:@"featured"
                          selectedPage:selectedPage];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error) {
        [self hideLoadingIndicator];
        
        if (error.code != NSURLErrorCancelled) {
            self.errorMessages[selectedPage] = [ErrorHandler messageFromError:error];
        }
        
        self.reloadingStates[selectedPage] = @NO;
        self.loadingStates[selectedPage] = @NO;
        
        if ((selectedPage == 0 && self.allAlbums.count > 0) ||
            (selectedPage == 1 && self.featuredAlbums.count > 0) ||
            (selectedPage == 2 && self.latestAlbums.count > 0)) {
            [ErrorHandler handleError:error];
        } else {
            [self.collectionView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:selectedPage inSection:0]]];
        }
    }];
}

- (void)reloadAlbumsWithType:(NSString *)type selectedPage:(NSInteger)selectedPage {
    self.reloadingStates[selectedPage] = @YES;
    
    if (selectedPage == 1) {
        [self loadAlbumsWithType:type
                    selectedPage:selectedPage
                     currentPage:2];
    } else {
        [self loadAlbumsWithType:type
                    selectedPage:selectedPage
                     currentPage:1];
    }
}

- (void)loadGenresWithType:(NSString *)type selectedPage:(NSInteger)selectedPage currentPage:(NSInteger)currentPage {
    PageCollectionViewCollectionViewCell *cell = (PageCollectionViewCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:selectedPage inSection:0]];
    self.loadingStates[selectedPage] = @YES;
    
    [self showLoadingIndicator];
    ApiClient *client = [ApiClient sharedClient];
    [client genresWithParameters:@{@"page": @(currentPage)} type:type success:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        BOOL reloading = [self.reloadingStates[selectedPage] boolValue];
        
        if (reloading) {
            self.reloadingStates[selectedPage] = @NO;
            cell.collectionView.contentOffset = CGPointMake(0.0f, 0.0f - cell.collectionView.contentInset.top);
            
            if (selectedPage == 0) {
                [self.sections removeAllObjects];
                [self.featuredGenres removeAllObjects];
            } else {
                [self.allGenres removeAllObjects];
            }
        }
        
        NSArray *data = responseObject;
        NSInteger numberOfResults = data.count;
        
        if (numberOfResults > 0) {
            for (NSInteger index = 0; index < numberOfResults; index++) {
                Genre *genre = [[Genre alloc] initWithDictionary:data[index]
                                                         context:self.workerManagedObjectContext];
                
                if (selectedPage == 0) {
                    [self.featuredGenres addObject:genre];
                } else {
                    [self.allGenres addObject:genre];
                }
            }
            
            if (selectedPage == 0 && reloading) {
                [self.sections addObject:[[NSString stringWithFormat:NSLocalizedString(@"featured_albums", nil), @"\xC2\xBB"] uppercaseStringWithLocale:[NSLocale currentLocale]]];
                [self.sections addObject:[[NSString stringWithFormat:NSLocalizedString(@"featured_genres", nil), @"\xC2\xBB"] uppercaseStringWithLocale:[NSLocale currentLocale]]];
            }
            
            self.currentPages[selectedPage] = @(currentPage);
        }
        
        [self hideLoadingIndicator];
        self.errorMessages[selectedPage] = @"";
        self.reloadingStates[selectedPage] = @NO;
        self.loadingStates[selectedPage] = @NO;
        
        if (reloading || numberOfResults > 0) {
            [self.collectionView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:selectedPage inSection:0]]];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error) {
        [self hideLoadingIndicator];
        
        if (error.code != NSURLErrorCancelled) {
            self.errorMessages[selectedPage] = [ErrorHandler messageFromError:error];
        }
        
        self.reloadingStates[selectedPage] = @NO;
        self.loadingStates[selectedPage] = @NO;
        
        if ((selectedPage == 0 && self.featuredGenres.count > 0) ||
            (selectedPage == 3 && self.allGenres.count > 0)) {
            [ErrorHandler handleError:error];
        } else {
            [self.collectionView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:selectedPage inSection:0]]];
        }
    }];
}

- (void)reloadGenresWithType:(NSString *)type selectedPage:(NSInteger)selectedPage {
    self.reloadingStates[selectedPage] = @YES;
    
    if (selectedPage == 3) {
        [self loadGenresWithType:type
                    selectedPage:selectedPage
                     currentPage:2];
    } else {
        [self loadGenresWithType:type
                    selectedPage:selectedPage
                     currentPage:1];
    }
}

- (void)loadArtistsWithSelectedPage:(NSInteger)selectedPage currentPage:(NSInteger)currentPage {
    PageTableViewCollectionViewCell *cell = (PageTableViewCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:selectedPage inSection:0]];
    self.loadingStates[selectedPage] = @YES;
    
    [self showLoadingIndicator];
    ApiClient *client = [ApiClient sharedClient];
    [client artistsWithParameters:@{@"page": @(currentPage)} success:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        BOOL reloading = [self.reloadingStates[selectedPage] boolValue];
        
        if (reloading) {
            self.reloadingStates[selectedPage] = @NO;
            cell.tableView.contentOffset = CGPointMake(0.0f, 0.0f - cell.tableView.contentInset.top);
            
            [self.sectionTitles removeAllObjects];
            [self.numberOfRowsInSections removeAllObjects];
            [self.lastIndexesInSections removeAllObjects];
            [self.artists removeAllObjects];
        }
        
        NSArray *data = responseObject;
        NSInteger numberOfResults = data.count;
        
        if (numberOfResults > 0) {
            NSInteger numberOfRowsInSection = 0;
            
            for (NSInteger index = 0; index < numberOfResults; index++) {
                Artist *artist = [[Artist alloc] initWithDictionary:data[index]
                                                            context:self.workerManagedObjectContext];
                [self.artists addObject:artist];
                
                NSString *sectionTitle = [artist.name substringToIndex:1];
                
                if (![self.sectionTitles.lastObject isEqual:sectionTitle]) {
                    numberOfRowsInSection = 0;
                    
                    [self.sectionTitles addObject:sectionTitle];
                    [self.numberOfRowsInSections addObject:@(numberOfRowsInSection + 1)];
                    
                    NSInteger lastIndexInSection = [self.lastIndexesInSections.lastObject integerValue];
                    [self.lastIndexesInSections addObject:@(lastIndexInSection + numberOfRowsInSection + 1)];
                } else {
                    NSInteger numberOfRowsInLastSection = [self.numberOfRowsInSections.lastObject integerValue];
                    [self.numberOfRowsInSections replaceObjectAtIndex:self.sectionTitles.count - 1
                                                           withObject:@(numberOfRowsInLastSection + numberOfRowsInSection + 1)];
                    
                    NSInteger lastIndexInSection = [self.lastIndexesInSections.lastObject integerValue];
                    [self.lastIndexesInSections replaceObjectAtIndex:self.lastIndexesInSections.count - 1
                                                          withObject:@(lastIndexInSection + numberOfRowsInSection + 1)];
                }
            }
            
            self.currentPages[selectedPage] = @(currentPage);
        }
        
        [self hideLoadingIndicator];
        self.errorMessages[selectedPage] = @"";
        self.reloadingStates[selectedPage] = @NO;
        self.loadingStates[selectedPage] = @NO;
        
        if (reloading || numberOfResults > 0) {
            [self.collectionView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:selectedPage inSection:0]]];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error) {
        [self hideLoadingIndicator];
        
        if (error.code != NSURLErrorCancelled) {
            self.errorMessages[selectedPage] = [ErrorHandler messageFromError:error];
        }
        
        self.reloadingStates[selectedPage] = @NO;
        self.loadingStates[selectedPage] = @NO;
        
        if (self.artists.count > 0) {
            [ErrorHandler handleError:error];
        } else {
            [self.collectionView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:selectedPage inSection:0]]];
        }
    }];
}

- (void)reloadArtistsWithSelectedPage:(NSInteger)selectedPage {
    self.reloadingStates[selectedPage] = @YES;
    [self loadArtistsWithSelectedPage:selectedPage
                          currentPage:1];
}

- (void)loadPlaylistsWithSelectedPage:(NSInteger)selectedPage currentPage:(NSInteger)currentPage {
    PageTableViewCollectionViewCell *cell = (PageTableViewCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:selectedPage inSection:0]];
    self.loadingStates[selectedPage] = @YES;
    
    [self showLoadingIndicator];
    ApiClient *client = [ApiClient sharedClient];
    [client publicPlaylistsWithParameters:@{@"page": @(currentPage)} success:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        BOOL reloading = [self.reloadingStates[selectedPage] boolValue];
        
        if (reloading) {
            self.reloadingStates[selectedPage] = @NO;
            cell.tableView.contentOffset = CGPointMake(0.0f, 0.0f - cell.tableView.contentInset.top);
            
            [self.playlists removeAllObjects];
        }
        
        NSArray *data = responseObject;
        NSInteger numberOfResults = data.count;
        
        if (numberOfResults > 0) {
            for (NSInteger index = 0; index < numberOfResults; index++) {
                Playlist *playlist = [[Playlist alloc] initWithDictionary:data[index]
                                                                  context:self.workerManagedObjectContext];
                [self.playlists addObject:playlist];
            }
            
            self.currentPages[selectedPage] = @(currentPage);
        }
        
        [self hideLoadingIndicator];
        self.errorMessages[selectedPage] = @"";
        self.reloadingStates[selectedPage] = @NO;
        self.loadingStates[selectedPage] = @NO;
        
        if (reloading || numberOfResults > 0) {
            [self.collectionView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:selectedPage inSection:0]]];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error) {
        [self hideLoadingIndicator];
        
        if (error.code != NSURLErrorCancelled) {
            self.errorMessages[selectedPage] = [ErrorHandler messageFromError:error];
        }
        
        self.reloadingStates[selectedPage] = @NO;
        self.loadingStates[selectedPage] = @NO;
        
        if (self.playlists.count > 0) {
            [ErrorHandler handleError:error];
        } else {
            [self.collectionView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:selectedPage inSection:0]]];
        }
    }];
}

- (void)reloadPlaylistsWithSelectedPage:(NSInteger)selectedPage {
    self.reloadingStates[selectedPage] = @YES;
    [self loadPlaylistsWithSelectedPage:selectedPage
                            currentPage:1];
}

@end
