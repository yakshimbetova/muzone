//
//  LoginViewController.m
//  Muzone
//
//  Created by Macbook on 3/6/17.
//  Copyright © 2017 Muzone LLC. All rights reserved.
//
#import <AVFoundation/AVFoundation.h>
#import "LoginViewController.h"
#import "DownloadManager.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "ErrorHandler.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "User.h"
#import "ApiClient.h"
#import "MBProgressHUD.h"


@import Firebase;

@interface LoginViewController ()
{
    NSMutableArray *tracksURLs;
}
@property (nonatomic, strong) AVPlayer *avplayer;
@property (nonatomic) NSMutableArray *tracks;
@property (nonatomic) NSURL *currentMovie;
@property (nonatomic) NSInteger selectedMovieIndex;
@property (nonnull, nonatomic, strong) UIButton *singInButton;
@property (nonnull, nonatomic, strong) UIButton *skipButton;
@property (nonatomic, retain) UIView *blurView;
@property (nonatomic, strong) FIRRemoteConfig *remoteConfig;
@property (nonatomic) AVPlayerLayer *avPlayerLayer;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [Answers logContentViewWithName:@"Login"
                        contentType:@"Menu"
                          contentId:nil
                   customAttributes:nil];

    
    self.view.backgroundColor = Rgb2UIColor(52,75,139);
    self.tracks = [NSMutableArray new];
    tracksURLs = [NSMutableArray new];
    
    //Not affecting background music playing
    NSError *sessionError = nil;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:&sessionError];
    [[AVAudioSession sharedInstance] setActive:YES error:&sessionError];
    
    //Set up player
    self.avplayer = [[AVPlayer alloc]init];
    AVPlayerLayer *avPlayerLayer =[AVPlayerLayer playerLayerWithPlayer:self.avplayer];
    [avPlayerLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [avPlayerLayer setFrame:[[UIScreen mainScreen] bounds]];
    [self.view.layer addSublayer:avPlayerLayer];
    self.avPlayerLayer = avPlayerLayer;
    
    //Config player
    [self.avplayer seekToTime:kCMTimeZero];
    [self.avplayer setVolume:0.0f];
    [self.avplayer setActionAtItemEnd:AVPlayerActionAtItemEndNone];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loopVideo:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[self.avplayer currentItem]];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerStartPlaying)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
    
    UIView *blurView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    blurView.backgroundColor = Rgb2UIColor(48, 63, 159);
    blurView.alpha = 0.7;
    [self.view addSubview:blurView];
    self.blurView = blurView;
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"login_icon"]];
    imageView.backgroundColor = [UIColor clearColor];
    imageView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview:imageView];
    [self.view addSubview:self.singInButton];
    [self.view addSubview:self.skipButton];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:imageView
                                                          attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeCenterX
                                                         multiplier:1.0f
                                                           constant:0.0f]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:imageView
                                                          attribute:NSLayoutAttributeCenterY
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeCenterY
                                                         multiplier:1.0f
                                                           constant:-2.5f]];
    
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-30-[signIn]-30-|" options:0 metrics:nil views:@{@"signIn" : self.singInButton}]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-30-[skip]-30-|" options:0 metrics:nil views:@{@"skip" : self.skipButton}]];
    
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[signIn(44)]-5-[skip(44)]-10-|" options:0 metrics:nil views:@{@"signIn" : self.singInButton, @"skip" : self.skipButton}]];
    
    
    
    
    self.remoteConfig = [FIRRemoteConfig remoteConfig];
    FIRRemoteConfigSettings *remoteConfigSettings = [[FIRRemoteConfigSettings alloc] initWithDeveloperModeEnabled:YES];
    self.remoteConfig.configSettings = remoteConfigSettings;
    [self.remoteConfig fetchWithExpirationDuration:0 completionHandler:^(FIRRemoteConfigFetchStatus status, NSError *error) {
        if (status == FIRRemoteConfigFetchStatusSuccess) {
            [self.remoteConfig activateFetched];
            [self startDownload];
        } else {
        }
    }];
    
    NSString *path;
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"defaultVideo"]) {
         path = [[NSBundle mainBundle] pathForResource:@"1girlcardance" ofType:@"mov"];
    }
    else
    {
         path = [[NSUserDefaults standardUserDefaults] objectForKey:@"defaultVideo"];
    }
    NSURL *url = [[NSURL alloc] initFileURLWithPath: path];
    AVAsset *asset = [AVURLAsset URLAssetWithURL:url options:nil];
    AVPlayerItem *anItem = [AVPlayerItem playerItemWithAsset:asset];
    [self.tracks addObject:url];
    self.currentMovie = url;
    self.selectedMovieIndex = 0;
    [self.avplayer replaceCurrentItemWithPlayerItem:anItem];
}



-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    
    [notificationCenter addObserver:self
                           selector:@selector(downloadManagerProcessDidEnd:)
                               name:DownloadManagerProcessDidEnd
                             object:nil];
    [self playerStartPlaying];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.avplayer pause];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)downloadManagerProcessDidEnd:(NSNotification *)notification {
    NSString *localFileName = notification.userInfo[DownloadManagerLocalFileNameKey];
    NSURL *URL = notification.userInfo[DownloadManagerFileUrlKey];
    NSString *movieURL = URL.absoluteString;
    NSError *error = notification.userInfo[DownloadManagerErrorKey];
    
    if (error) {
        [ErrorHandler handleError:error];
    } else {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = paths.lastObject;
        NSString *directoryPath = [documentsDirectory stringByAppendingPathComponent:MOVIES_DIRECTORY];
        NSString *filePath = [directoryPath stringByAppendingPathComponent:localFileName];
        // FIXME: crashes when favorite button is tapped 2-3 times (index beyond bounds for empty array)
        if ([movieURL isEqualToString:tracksURLs[0]] && ![localFileName isEqualToString:@"1girlcardance.mov"]) {
            [[NSUserDefaults standardUserDefaults] setObject:filePath forKey:@"defaultVideo"];
        }
        NSURL *fileUrl= [NSURL fileURLWithPath:filePath];
        [self.tracks addObject:fileUrl];
        if ([self urlOfCurrentlyPlayingInPlayer] == nil) {
            self.currentMovie = fileUrl;
            self.selectedMovieIndex = [self.tracks indexOfObject:self.currentMovie];
            [self playWithUrl:fileUrl];
        }

    }
}

-(NSURL *)urlOfCurrentlyPlayingInPlayer{
    AVAsset *currentPlayerAsset = self.avplayer.currentItem.asset;
    if (![currentPlayerAsset isKindOfClass:AVURLAsset.class]) return nil;
    return [(AVURLAsset *)currentPlayerAsset URL];
}


-(void)startDownload
{
    NSNumber *count = self.remoteConfig[@"VideoCount"].numberValue;
    for (int i = 0; i < count.integerValue; i++) {
        NSString *fileURL = self.remoteConfig[[NSString stringWithFormat:@"VideoURL%d", i+1]].stringValue;
        if (fileURL.length > 0) {
            [tracksURLs addObject:fileURL];
        }
    }
    for (NSString *url in tracksURLs)
        [self downloadMovie:url];
    
}

- (void)downloadMovie:(NSString *)movieURL {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = paths.lastObject;
    NSString *targetPath = [documentsDirectory stringByAppendingPathComponent:MOVIES_DIRECTORY];
    
    NSError *error;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if (![fileManager fileExistsAtPath:targetPath]) {
        [fileManager createDirectoryAtPath:targetPath
               withIntermediateDirectories:NO
                                attributes:nil
                                     error:&error];
    }
    
    if (error) {
        [ErrorHandler handleError:error];
    } else {
        NSString *filename = [movieURL lastPathComponent];
        NSString *filePath = [targetPath stringByAppendingPathComponent:filename];
        if ([filename isEqualToString:@"1girlcardance.mov"]) {
            return;
        }
        if ([fileManager fileExistsAtPath:filePath]) {
            NSMutableDictionary *userInfo = [NSMutableDictionary new];
            userInfo[DownloadManagerLocalFileNameKey] = filename;
            userInfo[DownloadManagerFileUrlKey] = [NSURL URLWithString:movieURL];
            [[NSNotificationCenter defaultCenter] postNotificationName:DownloadManagerProcessDidEnd
                                                                object:self
                                                              userInfo:userInfo];

        }
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        DownloadManager *downloadManager = appDelegate.downloadManager;
        [downloadManager downloadFileWithUrl:[NSURL URLWithString:movieURL]
                                toTargetPath:[NSURL fileURLWithPath:targetPath]];
    }
}

- (void)playerStartPlaying
{
    self.avPlayerLayer.hidden = YES;
    [CATransaction begin];
    [CATransaction setAnimationDuration:0.5];
    [self.avPlayerLayer setHidden:NO];
    [CATransaction commit];
    [self.avplayer play];
}

-(void)playWithUrl:(NSURL*)URL
{
    AVAsset *avAsset = [AVAsset assetWithURL:URL];
    AVPlayerItem *avPlayerItem =[[AVPlayerItem alloc]initWithAsset:avAsset];
    [self.avplayer replaceCurrentItemWithPlayerItem:avPlayerItem];
}

-(void)loopVideo:(NSNotification *)notification
{
    [self next];
    [self playWithUrl:self.currentMovie];
}

- (void)next {
    NSInteger numberOfTracks = _tracks.count;
    
    if (self.selectedMovieIndex < numberOfTracks - 1) {
        self.selectedMovieIndex++;
    } else {
        self.selectedMovieIndex = 0;
    }
    self.currentMovie = _tracks[self.selectedMovieIndex];
}

- (UIButton *)singInButton {
    if (_singInButton == nil) {
        _singInButton = [UIButton new];
        _singInButton.translatesAutoresizingMaskIntoConstraints = NO;
        [_singInButton setContentHuggingPriority:UILayoutPriorityDefaultHigh
                                            forAxis:UILayoutConstraintAxisVertical];
        [_singInButton setContentCompressionResistancePriority:UILayoutPriorityDefaultHigh
                                                          forAxis:UILayoutConstraintAxisVertical];
        [_singInButton addTarget:self
                             action:@selector(signInClicked:)
                   forControlEvents:UIControlEventTouchUpInside];
        _singInButton.backgroundColor = [UIColor colorWithRed:59.0f / 255
                                                        green:89.0f / 255
                                                         blue:152.0f / 255
                                                        alpha:1.0f];
        [_singInButton setTitle:NSLocalizedString(@"Sign in with Facebook", nil) forState:UIControlStateNormal];
        _singInButton.titleLabel.font = [UIFont boldSystemFontOfSize:12.0f];
        _singInButton.layer.cornerRadius = 3.0;
        _singInButton.layer.masksToBounds = YES;
        [_singInButton setImage:[UIImage imageNamed:@"Facebook"] forState:UIControlStateNormal];
        [_singInButton setImage:[UIImage imageNamed:@"Facebook"] forState:UIControlStateHighlighted];
        _singInButton.imageEdgeInsets = UIEdgeInsetsMake(0, -50, 0, 0);

        
    }
    
    return _singInButton;
}

- (UIButton *)skipButton {
    if (_skipButton == nil) {
        _skipButton = [UIButton new];
        _skipButton.translatesAutoresizingMaskIntoConstraints = NO;
        [_skipButton setContentHuggingPriority:UILayoutPriorityDefaultHigh
                                         forAxis:UILayoutConstraintAxisVertical];
        [_skipButton setContentCompressionResistancePriority:UILayoutPriorityDefaultHigh
                                                       forAxis:UILayoutConstraintAxisVertical];
        [_skipButton addTarget:self
                          action:@selector(skipClicked:)
                forControlEvents:UIControlEventTouchUpInside];
        _skipButton.backgroundColor = [UIColor clearColor];
        [_skipButton setTitle:NSLocalizedString(@"Skip", nil) forState:UIControlStateNormal];
        _skipButton.titleLabel.font = [UIFont boldSystemFontOfSize:12.0f];
    }
    return _skipButton;
}


-(void)signInClicked:(id)sender
{
    [self showLoadingIndicator];
    self.singInButton.enabled = NO;
    WEAKIFY_SELF;
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    if ([UIApplication.sharedApplication canOpenURL:[NSURL URLWithString:@"fb://"]])
    {
        login.loginBehavior = FBSDKLoginBehaviorSystemAccount;
    }
    [login
     logInWithReadPermissions: @[@"public_profile",@"email", @"user_birthday", @"user_relationships", @"user_likes", @"user_actions.music"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         STRONGIFY_SELF;
         [self hideLoadingIndicator];
         if (error) {
             [ErrorHandler handleError:error];
             self.singInButton.enabled = YES;
         } else if (result.isCancelled) {
             self.singInButton.enabled = YES;
         } else {             
             FBSDKAccessToken *token = [FBSDKAccessToken currentAccessToken];
             [User currentUser].facebookToken = token.tokenString;
             [self loginWithToken:token.tokenString];
         }
     }];
    
}

-(void)loginWithToken:(NSString*)facebookToken
{
    [self showLoadingIndicator];
    ApiClient *client = [ApiClient sharedClient];
    [client loginWithParameters:@{@"accesstoken":facebookToken} success:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        [self hideLoadingIndicator];
        self.singInButton.enabled = YES;
        [self fillProfileInfo:(NSDictionary*)responseObject];
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        [appDelegate launch];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error) {
        [self hideLoadingIndicator];
        self.singInButton.enabled = YES;
        [ErrorHandler handleError:error];
    }];
}

-(void)createAccountWithToken:(NSString*)facebookToken
{
    [self showLoadingIndicator];
    ApiClient *client = [ApiClient sharedClient];
    [client createAccountWithParameters:@{@"accesstoken":facebookToken} success:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        [self hideLoadingIndicator];
        [User currentUser].accessToken = responseObject[@"access_token"];
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        [appDelegate launch];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error) {
        [self hideLoadingIndicator];
        [ErrorHandler handleError:error];
    }];
}


- (void)showLoadingIndicator {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.bezelView.backgroundColor = [UIColor clearColor];
    hud.center = CGPointMake(self.view.center.x, self.view.center.y + 120);
}

- (void)hideLoadingIndicator {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void)fillProfileInfo:(NSDictionary *)dictionary
{
    User *currentUser = [User currentUser];
    NSDictionary *user = dictionary[@"user"];
    currentUser.accessToken = dictionary[@"access_token"];
    currentUser.email = user[@"email"];
    currentUser.facebookToken = user[@"fbLongToken"];
    currentUser.fbid = user[@"fbid"];
    currentUser.firstName = user[@"firstName"];
    currentUser.idString = user[@"id"];
    currentUser.lastName = user[@"lastName"];
    currentUser.profilePicUrl = user[@"profilePicUrl"];
    currentUser.username = user[@"username"];
}

- (void)skipClicked:(id)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [appDelegate launch];    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
