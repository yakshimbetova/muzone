//
//  LyricsViewController.h
//  Muzone
//
//  Created by Sukhrob Khakimov on 12/8/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Track;

@interface LyricsViewController : UIViewController <UIGestureRecognizerDelegate>

@property (nonnull, nonatomic, strong, readonly) Track *track;

- (nonnull instancetype)initWithTrack:(nonnull Track *)track;

@end
