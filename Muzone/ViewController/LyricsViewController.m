//
//  LyricsViewController.m
//  Muzone
//
//  Created by Sukhrob Khakimov on 12/8/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import "Track.h"

#import "LyricsViewController.h"

static CGFloat const horizontalSpace = 15.0f;
static CGFloat const verticalSpace = 15.0f;

@interface LyricsViewController ()

@property (nonnull, nonatomic, strong) UIVisualEffectView *blurEffectView;
@property (nonnull, nonatomic, strong) UIVisualEffectView *vibrancyEffectView;
@property (nonnull, nonatomic, strong) UIView *containerView;
@property (nonnull, nonatomic, strong) UILabel *titleLabel;
@property (nonnull, nonatomic, strong) UILabel *messageLabel;
@property (nonnull, nonatomic, strong) UITextView *contentTextView;

@end

@implementation LyricsViewController

- (instancetype)initWithTrack:(Track *)track {
    self = [super init];
    
    if (self) {
        self.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        self.modalPresentationStyle = UIModalPresentationOverFullScreen;
        
        _track = track;
    }
    
    return self;
}

- (void)loadView {
    [super loadView];
    
    CGFloat containerViewHeight = self.view.bounds.size.height * 0.6f;
    CGFloat containerViewBottomSpace = -self.containerView.layer.cornerRadius;
    CGFloat titleLabelMaxHeight = self.titleLabel.font.lineHeight * self.titleLabel.numberOfLines;
    CGSize constrainedSize = CGSizeMake(self.view.bounds.size.width - horizontalSpace * 2, CGFLOAT_MAX);
    CGRect titleLabelRect = [_track.title boundingRectWithSize:constrainedSize
                                                       options:NSStringDrawingUsesLineFragmentOrigin
                                                    attributes:@{NSFontAttributeName: self.titleLabel.font}
                                                       context:nil];
    CGFloat titleLabelHeight = ceil(titleLabelRect.size.height);
    
    if (titleLabelHeight > titleLabelMaxHeight) {
        titleLabelHeight = titleLabelMaxHeight;
    }
    
    [self.view addSubview:self.containerView];
    [self.containerView addSubview:self.blurEffectView];
    [self.containerView addSubview:self.titleLabel];
    [self.containerView addSubview:self.contentTextView];
    [self.containerView addSubview:self.messageLabel];
    
    // Uncomment for vibrancy effect
//    [self.vibrancyEffectView.contentView addSubview:self.titleLabel];
//    [self.vibrancyEffectView.contentView addSubview:self.contentTextView];
//    [self.vibrancyEffectView.contentView addSubview:self.messageLabel];
//    [self.blurEffectView.contentView addSubview:self.vibrancyEffectView];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[containerView]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:@{@"containerView": self.containerView}]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[containerView(height)]-space-|"
                                                                      options:0
                                                                      metrics:@{@"space": @(containerViewBottomSpace),
                                                                                @"height": @(containerViewHeight)}
                                                                        views:@{@"containerView": self.containerView}]];
    
    [self.containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-space-[titleLabel]-space-|"
                                                                               options:0
                                                                               metrics:@{@"space": @(horizontalSpace)}
                                                                                 views:@{@"titleLabel": self.titleLabel}]];
    
    [self.containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[contentTextView]|"
                                                                               options:0
                                                                               metrics:nil
                                                                                 views:@{@"contentTextView": self.contentTextView}]];
    
    [self.containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-space-[messageLabel]-space-|"
                                                                               options:0
                                                                               metrics:@{@"space": @(horizontalSpace)}
                                                                                 views:@{@"messageLabel": self.messageLabel}]];
    
    [self.containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-space-[titleLabel]"
                                                                               options:0
                                                                               metrics:@{@"space": @(verticalSpace)}
                                                                                 views:@{@"titleLabel": self.titleLabel}]];
    
    [self.containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-space1-[contentTextView(height)]-space2-|"
                                                                               options:0
                                                                               metrics:@{@"space1": @(titleLabelHeight + verticalSpace * 2),
                                                                                         @"space2": @(verticalSpace),
                                                                                         @"height": @(containerViewHeight - titleLabelHeight - verticalSpace * 3)}
                                                                                 views:@{@"contentTextView": self.contentTextView}]];
    
    [self.containerView addConstraint:[NSLayoutConstraint constraintWithItem:self.messageLabel
                                                                   attribute:NSLayoutAttributeCenterY
                                                                   relatedBy:NSLayoutRelationEqual
                                                                      toItem:self.contentTextView
                                                                   attribute:NSLayoutAttributeCenterY
                                                                  multiplier:1.0f
                                                                    constant:0.0f]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Gestures
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                           action:@selector(handleTapGestureRecognizer:)];
    tapGestureRecognizer.delegate = self;
    [self.view addGestureRecognizer:tapGestureRecognizer];
    
    UISwipeGestureRecognizer *swipeDownGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                                                                     action:@selector(handleSwipeDownGestureRecognizer:)];
    swipeDownGestureRecognizer.direction = UISwipeGestureRecognizerDirectionDown;
    [self.view addGestureRecognizer:swipeDownGestureRecognizer];
    
    if (_track.lyrics.length > 0) {
        self.messageLabel.hidden = YES;
    } else {
        self.messageLabel.hidden = NO;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIGestureRecognizer

- (void)handleTapGestureRecognizer:(UITapGestureRecognizer *)tapGestureRecognizer {
    [self dismissViewControllerAnimated:YES
                             completion:nil];
}

- (void)handleSwipeDownGestureRecognizer:(UISwipeGestureRecognizer *)swipeDownGestureRecognizer {
    [self dismissViewControllerAnimated:YES
                             completion:nil];
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if ([touch.view isEqual:self.view]) {
        return YES;
    }

    return NO;
}

#pragma mark - UIVisualEffectView

- (UIVisualEffectView *)blurEffectView {
    if (_blurEffectView == nil) {
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        _blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        _blurEffectView.frame = self.view.bounds;
    }
    
    return _blurEffectView;
}

- (UIVisualEffectView *)vibrancyEffectView {
    if (_vibrancyEffectView == nil) {
        UIVibrancyEffect *vibrancyEffect = [UIVibrancyEffect effectForBlurEffect:(UIBlurEffect *)self.blurEffectView.effect];
        _vibrancyEffectView = [[UIVisualEffectView alloc] initWithEffect:vibrancyEffect];
        _vibrancyEffectView.frame = self.view.bounds;
    }
    
    return _vibrancyEffectView;
}

#pragma mark - UIView

- (UIView *)containerView {
    if (_containerView == nil) {
        _containerView = [[UIView alloc] init];
        _containerView.translatesAutoresizingMaskIntoConstraints = NO;
        _containerView.layer.cornerRadius = 10.0f;
        _containerView.clipsToBounds = YES;
    }
    
    return _containerView;
}

#pragma mark - UILabel

- (UILabel *)titleLabel {
    if (_titleLabel == nil) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _titleLabel.font = [UIFont boldSystemFontOfSize:20.0f];
        _titleLabel.textColor = [UIColor whiteColor];
        _titleLabel.numberOfLines = 3;
        _titleLabel.text = _track.title;
    }
    
    return _titleLabel;
}

- (UILabel *)messageLabel {
    if (_messageLabel == nil) {
        _messageLabel = [[UILabel alloc] init];
        _messageLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _messageLabel.font = [UIFont boldSystemFontOfSize:18.0f];
        _messageLabel.textColor = [UIColor whiteColor];
        _messageLabel.textAlignment = NSTextAlignmentCenter;
        _messageLabel.text = NSLocalizedString(@"no_lyrics", nil);
    }
    
    return _messageLabel;
}

#pragma mark - UITextView

- (UITextView *)contentTextView {
    if (_contentTextView == nil) {
        _contentTextView = [[UITextView alloc] init];
        _contentTextView.translatesAutoresizingMaskIntoConstraints = NO;
        _contentTextView.backgroundColor = [UIColor clearColor];
        _contentTextView.font = [UIFont boldSystemFontOfSize:14.0f];
        _contentTextView.textColor = [UIColor whiteColor];
        _contentTextView.textContainer.lineFragmentPadding = 0.0f;
        _contentTextView.textContainerInset = UIEdgeInsetsMake(0.0f, horizontalSpace, 0.0f, horizontalSpace);
        _contentTextView.editable = NO;
        _contentTextView.selectable = YES;
        _contentTextView.text = [_track.lyrics stringByReplacingOccurrencesOfString:@"\\n"
                                                                         withString:@"\n"];
    }
    
    return _contentTextView;
}

@end
