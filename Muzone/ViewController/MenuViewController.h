//
//  MenuViewController.h
//  Muzone
//
//  Created by Sukhrob Khakimov on 5/22/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import "BaseViewController.h"

@protocol MenuViewControllerDelegate;

@interface MenuViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nullable, nonatomic, weak) id<MenuViewControllerDelegate> delegate;
@property (nonnull, nonatomic, strong, readonly) UIViewController *selectedViewController;

@end

@protocol MenuViewControllerDelegate <NSObject>

@optional
- (void)menuViewControllerDidSelectViewController:(nonnull MenuViewController *)menuViewController;

@end
