//
//  MenuViewController.m
//  Muzone
//
//  Created by Sukhrob Khakimov on 5/22/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import "AppDelegate.h"
#import "Constants.h"
#import "BaseTheme.h"
#import "FavoriteManager.h"

#import "BaseNavigationController.h"
#import "MenuViewController.h"
#import "HomeViewController.h"
#import "AlbumsViewController.h"
#import "TracksViewController.h"
#import "SettingsViewController.h"
#import "HelpViewController.h"
#import "TermsViewController.h"
#import "AboutViewController.h"

#import "MenuTableViewCell.h"

#import "User.h"

#import "STCollapseTableView.h"

#import "GenresViewController.h"
#import "ArtistsViewController.h"
#import "AllAlbumsViewController.h"

#import "Muzone-Swift.h"

#define kAppVersionLabelHeight 24.0f

@interface MenuViewController ()

@property (nonnull, nonatomic, strong) NSArray *menuItems;
@property (nonnull, nonatomic, strong) NSIndexPath *selectedIndexPath;
@property (nonnull, nonatomic, copy) NSString *cellReuseIdentifier;

@property (nonnull, nonatomic, strong) UIView *topView;
@property (nonnull, nonatomic, strong) STCollapseTableView *tableView;
@property (nonnull, nonatomic, strong) UILabel *appVersionLabel;

@end

@implementation MenuViewController

- (instancetype)init {
    self = [super init];
    
    if (self) {
        self.cellReuseIdentifier = NSStringFromClass([MenuTableViewCell class]);
        self.selectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        _selectedViewController = [self viewControllerAtIndexPath:self.selectedIndexPath];
    }
    
    return self;
}

- (void)loadView {
    [super loadView];
    
    CGFloat topViewHeight = [UIApplication sharedApplication].statusBarFrame.size.height;
    
    [self.view addSubview:self.topView];
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.appVersionLabel];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[topView]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:@{@"topView": self.topView}]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[tableView]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:@{@"tableView": self.tableView}]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[appVersionLabel]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:@{@"appVersionLabel": self.appVersionLabel}]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[topView(height)][tableView]|"
                                                                      options:0
                                                                      metrics:@{@"height": @(topViewHeight)}
                                                                        views:@{@"topView": self.topView,
                                                                                @"tableView": self.tableView}]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[appVersionLabel(height)]|"
                                                                      options:0
                                                                      metrics:@{@"height": @(kAppVersionLabelHeight)}
                                                                        views:@{@"appVersionLabel": self.appVersionLabel}]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // NSNotificationCenter
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    
    
    [notificationCenter addObserver:self
                           selector:@selector(reloadTable)
                               name:UserSignInNotification
                             object:nil];

}

-(void) reloadTable
{
    [self.tableView reloadData];
}

-(void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -

- (UIViewController *)viewControllerAtIndexPath:(NSIndexPath *)indexPath {
    UIViewController *viewController;
    
    // Original
//    if (indexPath.section == 0) {
//        viewController = [[HomeViewController alloc] init];
//    } else if (indexPath.section == 1) {
//        if (indexPath.row == 0) {
//            viewController = [[AlbumsViewController alloc] init];
//        } else if (indexPath.row == 1) {
//            viewController = [[TracksViewController alloc] init];
//        } else {
//            viewController = [[PlaylistsViewController alloc] init];
//        }
//    } else if (indexPath.section == 2) {
//        if (self.selectedIndexPath.row == 0) {
//            viewController = [[SettingsViewController alloc] init];
//        } else {
//            viewController = [[HelpViewController alloc] init];
//        }
//    } else {
//        if (indexPath.row == 0) {
//            viewController = [[TermsViewController alloc] init];
//        } else {
//            viewController = [[AboutViewController alloc] init];
//        }
//    }
    
    // Temporary
    if (indexPath.section == 0) {
        viewController = [[HomeViewController alloc] init];
    } else if (indexPath.section == 2) {
        if (indexPath.row == 0) {
            viewController = [[ArtistsViewController alloc] initWithType:LoadingListTypeSaved];
        }
        else if (indexPath.row == 1) {
            viewController = [[AllAlbumsViewController alloc] initWithType:LoadingListTypeSaved];
        }
        else if (indexPath.row == 2) {
            viewController = [[TracksViewController alloc] initWithType:LoadingListTypeSaved];
        }
        else if (indexPath.row == 3)
        {
            viewController = [[GenresViewController alloc] initWithType:LoadingListTypeSaved];
        }
    } else if (indexPath.section == 3) {
        if (indexPath.row == 0) {
            viewController = [[ArtistsViewController alloc] initWithType:LoadingListTypeFavorites];
        }
        else if (indexPath.row == 1) {
            viewController = [[AllAlbumsViewController alloc] initWithType:LoadingListTypeFavorites];
        }
        else if (indexPath.row == 2) {
            viewController = [[TracksViewController alloc] initWithType:LoadingListTypeFavorites];
        }
        else if (indexPath.row == 3)
        {
            viewController = [[GenresViewController alloc] initWithType:LoadingListTypeFavorites];
        }
    } else if (indexPath.section == 5){
        if (indexPath.row == 0) {
            viewController = [[TermsViewController alloc] init];
        } else {
            viewController = [[AboutViewController alloc] init];
        }
    }
    
    if (viewController == nil) {
        return nil;
    }
    
    viewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_menu.png"]
                                                                                       style:UIBarButtonItemStylePlain
                                                                                      target:self
                                                                                      action:@selector(leftBarButtonItemTapped:)];
    
    return [[BaseNavigationController alloc] initWithRootViewController:viewController];
}

#pragma mark - NSArray

- (NSArray *)menuItems {
    _menuItems = @[@[@{@"icon": @"ic_browse.png", @"title": NSLocalizedString(@"browse", nil)}],
                   @[],
                   @[
                     @{@"icon": @"", @"title": NSLocalizedString(@"artists", nil)},
                     @{@"icon": @"", @"title": NSLocalizedString(@"albums", nil)},
                     @{@"icon": @"", @"title": NSLocalizedString(@"tracks", nil)},
                     @{@"icon": @"", @"title": NSLocalizedString(@"genres", nil)}
                     ],
                   @[
                       @{@"icon": @"", @"title": NSLocalizedString(@"artists", nil)},
                       @{@"icon": @"", @"title": NSLocalizedString(@"albums", nil)},
                       @{@"icon": @"", @"title": NSLocalizedString(@"tracks", nil)},
                       @{@"icon": @"", @"title": NSLocalizedString(@"genres", nil)}
                       ],
                   @[],
                   @[
                       @{@"icon": @"ic_terms_privacy.png", @"title": NSLocalizedString(@"terms_privacy", nil)},
                       @{@"icon": @"ic_about.png", @"title": [NSString stringWithFormat:NSLocalizedString(@"about", nil), APP_NAME]},
                       @{@"icon": @"ic_terms_privacy.png", @"title": NSLocalizedString(@"logout", nil)}
                    ]
                 ];

        
        
    if ([User currentUser].accessToken.length <= 0) {
        _menuItems = @[@[@{@"icon": @"ic_browse.png", @"title": NSLocalizedString(@"browse", nil)}],
                       @[],
                       @[
                           @{@"icon": @"", @"title": NSLocalizedString(@"artists", nil)},
                           @{@"icon": @"", @"title": NSLocalizedString(@"albums", nil)},
                           @{@"icon": @"", @"title": NSLocalizedString(@"tracks", nil)},
                           @{@"icon": @"", @"title": NSLocalizedString(@"genres", nil)}
                           ],
                       @[
                           @{@"icon": @"", @"title": NSLocalizedString(@"artists", nil)},
                           @{@"icon": @"", @"title": NSLocalizedString(@"albums", nil)},
                           @{@"icon": @"", @"title": NSLocalizedString(@"tracks", nil)},
                           @{@"icon": @"", @"title": NSLocalizedString(@"genres", nil)}
                           ],
                       @[],
                       @[
                           @{@"icon": @"ic_terms_privacy.png", @"title": NSLocalizedString(@"terms_privacy", nil)},
                           @{@"icon": @"ic_about.png", @"title": [NSString stringWithFormat:NSLocalizedString(@"about", nil), APP_NAME]},
                           ]
                       ];
    }
    return _menuItems;
}

#pragma mark - UINavigationBar

- (void)leftBarButtonItemTapped:(id)sender {
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [appDelegate.containerViewController.view endEditing:YES];
    [appDelegate.containerViewController presentMenuViewController];
}

#pragma mark - UIView

- (UIView *)topView {
    if (_topView == nil) {
        _topView = [[UIView alloc] init];
        _topView.translatesAutoresizingMaskIntoConstraints = NO;
        _topView.backgroundColor = [[BaseTheme currentTheme] tintColor];
    }
    
    return _topView;
}

#pragma mark - UITableView

- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[STCollapseTableView alloc] initWithFrame:CGRectZero
                                                  style:UITableViewStylePlain];
        _tableView.translatesAutoresizingMaskIntoConstraints = NO;
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.contentInset = UIEdgeInsetsMake(_tableView.contentInset.top, _tableView.contentInset.left, kAppVersionLabelHeight, _tableView.contentInset.right);
        _tableView.dataSource = self;
        _tableView.delegate = self;
        [_tableView registerClass:[MenuTableViewCell class]
           forCellReuseIdentifier:self.cellReuseIdentifier];
        _tableView.exclusiveSections = YES;
    }
    
    return _tableView;
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.menuItems.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.menuItems[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *menuItem = self.menuItems[indexPath.section][indexPath.row];
    
    MenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:self.cellReuseIdentifier
                                                                 forIndexPath:indexPath];
    cell.iconImageView.image = [UIImage imageNamed:menuItem[@"icon"]];
    cell.titleLabel.text = menuItem[@"title"];
    
    return cell;
}

#pragma mark - UITableViewDataDelegate

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return nil;
    }
    
    UIView *headerView = [[UIView alloc] init];

    if (section == 1) {
        headerView.backgroundColor = [UIColor colorWithRed:255.0f / 255
                                                     green:255.0f / 255
                                                      blue:255.0f / 255
                                                     alpha:1.0f];
        
        UILabel *titleLabel = [[UILabel alloc] init];
        titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        titleLabel.font = [UIFont systemFontOfSize:17.0f];
        titleLabel.textColor = [UIColor blackColor];
        titleLabel.text = NSLocalizedString(@"your_music", nil);
        
        [headerView addSubview:titleLabel];
        
        [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-space-[titleLabel]-space-|"
                                                                           options:0
                                                                           metrics:@{@"space": @(15.0f)}
                                                                             views:@{@"titleLabel": titleLabel}]];
        
        [headerView addConstraint:[NSLayoutConstraint constraintWithItem:titleLabel
                                                               attribute:NSLayoutAttributeCenterY
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:headerView
                                                               attribute:NSLayoutAttributeCenterY
                                                              multiplier:1.0f
                                                                constant:0.0f]];
    }
    if (section == 2) {
        headerView.backgroundColor = [UIColor colorWithRed:255.0f / 255
                                                     green:255.0f / 255
                                                      blue:255.0f / 255
                                                     alpha:1.0f];
        
        UILabel *titleLabel = [[UILabel alloc] init];
        titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        titleLabel.font = [UIFont systemFontOfSize:17.0f];
        titleLabel.textColor = [UIColor blackColor];
        titleLabel.text = NSLocalizedString(@"Saved", nil);
        
        [headerView addSubview:titleLabel];
        
        UIImageView *iconView = [[UIImageView alloc] init];
        iconView.translatesAutoresizingMaskIntoConstraints = NO;
        iconView.image = [UIImage imageNamed:@"Save"];
        [headerView addSubview:iconView];
        
        [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-space-[iconImageView(width)]-space-[titleLabel]-space-|"
                                                                                 options:0
                                                                                 metrics:@{@"space": @(15.0f),
                                                                                           @"width": @(25.0f)}
                                                                                   views:@{@"iconImageView": iconView, @"titleLabel": titleLabel}]];
        
        [headerView addConstraint:[NSLayoutConstraint constraintWithItem:iconView
                                                                     attribute:NSLayoutAttributeCenterY
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:headerView
                                                                     attribute:NSLayoutAttributeCenterY
                                                                    multiplier:1.0f
                                                                      constant:0.0f]];
        
        [headerView addConstraint:[NSLayoutConstraint constraintWithItem:titleLabel
                                                                     attribute:NSLayoutAttributeCenterY
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:headerView
                                                                     attribute:NSLayoutAttributeCenterY
                                                                    multiplier:1.0f
                                                                      constant:0.0f]];

        return headerView;
    }
    if (section == 3) {
        headerView.backgroundColor = [UIColor colorWithRed:255.0f / 255
                                                     green:255.0f / 255
                                                      blue:255.0f / 255
                                                     alpha:1.0f];
        
        UILabel *titleLabel = [[UILabel alloc] init];
        titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        titleLabel.font = [UIFont systemFontOfSize:17.0f];
        titleLabel.textColor = [UIColor blackColor];
        titleLabel.text = NSLocalizedString(@"Favorites", nil);;
        
        [headerView addSubview:titleLabel];
        
        UIImageView *iconView = [[UIImageView alloc] init];
        iconView.translatesAutoresizingMaskIntoConstraints = NO;
        iconView.image = [UIImage imageNamed:@"Like"];
        [headerView addSubview:iconView];
        
        [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-space-[iconImageView(width)]-space-[titleLabel]-space-|"
                                                                           options:0
                                                                           metrics:@{@"space": @(15.0f),
                                                                                     @"width": @(25.0f)}
                                                                             views:@{@"iconImageView": iconView, @"titleLabel": titleLabel}]];
        
        [headerView addConstraint:[NSLayoutConstraint constraintWithItem:iconView
                                                               attribute:NSLayoutAttributeCenterY
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:headerView
                                                               attribute:NSLayoutAttributeCenterY
                                                              multiplier:1.0f
                                                                constant:0.0f]];
        
        [headerView addConstraint:[NSLayoutConstraint constraintWithItem:titleLabel
                                                               attribute:NSLayoutAttributeCenterY
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:headerView
                                                               attribute:NSLayoutAttributeCenterY
                                                              multiplier:1.0f
                                                                constant:0.0f]];
        
    }
    if (section == 4) {
        headerView.backgroundColor = [UIColor colorWithRed:255.0f / 255
                                                     green:255.0f / 255
                                                      blue:255.0f / 255
                                                     alpha:1.0f];
        
        UILabel *titleLabel = [[UILabel alloc] init];
        titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        titleLabel.font = [UIFont systemFontOfSize:17.0f];
        titleLabel.textColor = [UIColor blackColor];
        titleLabel.text = NSLocalizedString(@"playlists", nil);;
        
        [headerView addSubview:titleLabel];
        
        UIImageView *iconView = [[UIImageView alloc] init];
        iconView.translatesAutoresizingMaskIntoConstraints = NO;
        iconView.image = [UIImage imageNamed:@"Playlist"];
        [headerView addSubview:iconView];
        
        [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-space-[iconImageView(width)]-space-[titleLabel]-space-|"
                                                                           options:0
                                                                           metrics:@{@"space": @(15.0f),
                                                                                     @"width": @(25.0f)}
                                                                             views:@{@"iconImageView": iconView, @"titleLabel": titleLabel}]];
        
        [headerView addConstraint:[NSLayoutConstraint constraintWithItem:iconView
                                                               attribute:NSLayoutAttributeCenterY
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:headerView
                                                               attribute:NSLayoutAttributeCenterY
                                                              multiplier:1.0f
                                                                constant:0.0f]];
        
        [headerView addConstraint:[NSLayoutConstraint constraintWithItem:titleLabel
                                                               attribute:NSLayoutAttributeCenterY
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:headerView
                                                               attribute:NSLayoutAttributeCenterY
                                                              multiplier:1.0f
                                                                constant:0.0f]];
        UITapGestureRecognizer *tap =
        [[UITapGestureRecognizer alloc] initWithTarget:self
                                                action:@selector(goPlayList:)];
        [headerView addGestureRecognizer:tap];
    }

    
    UIView *separatorView = [[UIView alloc] init];
    separatorView.translatesAutoresizingMaskIntoConstraints = NO;
    separatorView.backgroundColor = [UIColor colorWithRed:224.0f / 255
                                                    green:224.0f / 255
                                                     blue:224.0f / 255
                                                    alpha:1.0f];
    
    [headerView addSubview:separatorView];
    
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[separatorView]|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:@{@"separatorView": separatorView}]];
    
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[separatorView(height)]"
                                                                       options:0
                                                                       metrics:@{@"height": @(1.0f)}
                                                                         views:@{@"separatorView": separatorView}]];
    
    return headerView;
}


- (void)goPlayList:(UITapGestureRecognizer *)recognizer
{
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    if ([self.delegate respondsToSelector:@selector(menuViewControllerDidSelectViewController:)]) {
        [self.delegate menuViewControllerDidSelectViewController:self];
    }

    PlaylistsViewController *viewController = [[PlaylistsViewController alloc] init];
    viewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_menu.png"]
                                                                                       style:UIBarButtonItemStylePlain
                                                                                      target:self
                                                                                      action:@selector(leftBarButtonItemTapped:)];



    _selectedViewController =  [[BaseNavigationController alloc] initWithRootViewController:viewController];
    appDelegate.containerViewController.contentViewController = self.selectedViewController;
    [appDelegate.containerViewController hideMenuViewController];

}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return 0.0f;
    } else if (section == 1) {
        return 40.0f;
    } else if (section == 2) {
        return 60.0f;
    } else if (section == 3) {
        return 60.0f;
    } else if (section == 4) {
    return 60.0f;
    }
    return 1.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60.0f;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([indexPath isEqual:self.selectedIndexPath]) {
        [self.tableView selectRowAtIndexPath:indexPath
                                    animated:NO
                              scrollPosition:UITableViewScrollPositionNone];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.selectedIndexPath = indexPath;
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    if (indexPath.row == [_menuItems[5] count] - 1 && indexPath.section == 5) {
        AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [tableView deselectRowAtIndexPath:indexPath animated:YES];;
        [delegate logout];
        return;
    }
    _selectedViewController = [self viewControllerAtIndexPath:self.selectedIndexPath];
    
    if (_selectedViewController == nil) {
        [appDelegate.containerViewController hideMenuViewController];
        return;
    }
    
    if ([self.delegate respondsToSelector:@selector(menuViewControllerDidSelectViewController:)]) {
        [self.delegate menuViewControllerDidSelectViewController:self];
    }
    
    
    appDelegate.containerViewController.contentViewController = self.selectedViewController;
    [appDelegate.containerViewController hideMenuViewController];
}

#pragma mark - UILabel

- (UILabel *)appVersionLabel {
    if (_appVersionLabel == nil) {
        _appVersionLabel = [[UILabel alloc] init];
        _appVersionLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _appVersionLabel.backgroundColor = [[BaseTheme currentTheme] tintColorWithAlpha:0.9f];
        _appVersionLabel.font = [UIFont systemFontOfSize:12.0f
                                                  weight:UIFontWeightSemibold];
        _appVersionLabel.textColor = [UIColor whiteColor];
        _appVersionLabel.textAlignment = NSTextAlignmentCenter;
        _appVersionLabel.text = [NSString stringWithFormat:@"%@ %@ (%@)", APP_NAME, APP_VERSION, APP_BUILD];
    }
    
    return _appVersionLabel;
}

@end
