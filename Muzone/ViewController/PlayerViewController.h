//
//  PlayerViewController.h
//  Muzone
//
//  Created by Sukhrob Khakimov on 5/23/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import "BaseViewController.h"

extern NSString *__nonnull const PlayerViewControllerWillPlay;
extern NSString *__nonnull const PlayerViewControllerWillPause;
extern NSString *__nonnull const PlayerViewControllerWillStop;
extern NSString *__nonnull const PlayerViewControllerWillDispose;
extern NSString *__nonnull const PlayerViewControllerDidChangeCurrentTrack;
extern NSString *__nonnull const PlayerViewControllerWillAppear;
extern NSString *__nonnull const PlayerViewControllerDidDisappear;
extern NSString *__nonnull const PlayerViewControllerShouldHandleError;

extern NSString *__nonnull const PlayerViewControllerErrorKey;

@class Track;

@interface PlayerViewController : BaseViewController


@property (nonnull, nonatomic, strong) Track *currentTrack;
@property (nonatomic, assign, readonly, getter = isRunning) BOOL running;

- (nonnull instancetype)initWithTracks:(nonnull NSArray<Track *> *)tracks currentTrack:(nonnull Track *)currentTrack;

- (void)play;
- (void)pause;
- (void)stop;
- (void)dispose;
- (void)first;
- (void)last;
- (void)previous;
- (void)next;
- (void)shuffle;

@end
