//
//  PlayerViewController.m
//  Muzone
//
//  Created by Sukhrob Khakimov on 5/23/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MPNowPlayingInfoCenter.h>
#import <MediaPlayer/MPMediaItem.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <SDWebImage/UIImageView+WebCache.h>

#import "AppDelegate.h"
#import "STKAudioPlayer.h"
#import "Constants.h"
#import "BaseTheme.h"
#import "SessionManager.h"
#import "ErrorHandler.h"
#import "TimeHelper.h"

#import "Album.h"
#import "Artist.h"
#import "Genre.h"
#import "Track.h"

#import "PlayerViewController.h"

#import "MarqueeLabel.h"
#import "PlayerSlider.h"

// Testing
#import "LyricsViewController.h"

#import "TrackRepository.h"
#import "FavoriteManager.h"
#import "SelectPlaylistViewController.h"

#define kButtonSize CGSizeMake(32.0f, 32.0f)

static CGFloat const horizontalSpace = 25.0f;

NSString *const PlayerViewControllerWillPlay = @"PlayerViewControllerWillPlay";
NSString *const PlayerViewControllerWillPause = @"PlayerViewControllerWillPause";
NSString *const PlayerViewControllerWillStop = @"PlayerViewControllerWillStop";
NSString *const PlayerViewControllerWillDispose = @"PlayerViewControllerWillDispose";
NSString *const PlayerViewControllerDidChangeCurrentTrack = @"PlayerViewControllerDidChangeCurrentTrack";
NSString *const PlayerViewControllerWillAppear = @"PlayerViewControllerWillAppear";
NSString *const PlayerViewControllerDidDisappear = @"PlayerViewControllerDidDisappear";
NSString *const PlayerViewControllerShouldHandleError = @"PlayerViewControllerShouldHandleError";

NSString *const PlayerViewControllerErrorKey = @"PlayerViewControllerErrorKey";

typedef NS_ENUM (NSInteger, MUZRepeatMode) {
    MUZRepeatModeNone,
    MUZRepeatModeAll,
    MUZRepeatModeSingle
};

@interface PlayerViewController () <STKAudioPlayerDelegate>

@property (nonnull, nonatomic, strong) STKAudioPlayer *audioPlayer;
@property (nullable, nonatomic, strong) NSTimer *timer;

@property (nonatomic, assign) BOOL currentTrackDidChange;
@property (nonatomic, getter = isSeeking) BOOL seeking;
@property (nonatomic, assign) BOOL shuffleEnabled;
@property (nonatomic, assign) NSInteger selectedTrackIndex;
@property (nonatomic, assign) MUZRepeatMode repeatMode;

@property (nonnull, nonatomic, strong) NSMutableArray<Track *> *tracks;
@property (nonnull, nonatomic, strong) NSMutableArray *trackIndexes;

@property (nonnull, nonatomic, strong) UIView *backgroundView;
@property (nonnull, nonatomic, strong) UIVisualEffectView *visualEffectView;
@property (nonnull, nonatomic, strong) UIImageView *backgroundImageView;

@property (nonnull, nonatomic, strong) UIView *topView;
@property (nonnull, nonatomic, strong) UIButton *closeButton;
@property (nonnull, nonatomic, strong) UIButton *lyricsButton;
@property (nonnull, nonatomic, strong) UIButton *favoriteButton;
@property (nonnull, nonatomic, strong) UIButton *playlistButton;

@property (nonnull, nonatomic, strong) UIActivityIndicatorView *activityIndicator;

@property (nonnull, nonatomic, strong) UIImage *placeholderImage;
@property (nonnull, nonatomic, strong) UIImageView *albumCoverImageView;

@property (nonnull, nonatomic, strong) PlayerSlider *slider;
@property (nonnull, nonatomic, strong) UILabel *elapsedTimeLabel;
@property (nonnull, nonatomic, strong) UILabel *totalTimeLabel;
@property (nonnull, nonatomic, strong) MarqueeLabel *artistNameLabel;
@property (nonnull, nonatomic, strong) MarqueeLabel *trackTitleLabel;
@property (nonnull, nonatomic, strong) UILabel *stateLabel;

@property (nonnull, nonatomic, strong) UIButton *playPauseButton;
@property (nonnull, nonatomic, strong) UIButton *previousButton;
@property (nonnull, nonatomic, strong) UIButton *nextButton;
@property (nonnull, nonatomic, strong) UIButton *repeatButton;
@property (nonnull, nonatomic, strong) UIButton *shuffleButton;

@property (nonnull, nonatomic, strong) TrackRepository *trackRepository;
@property (nonnull, nonatomic, strong) FavoriteManager *favoriteManager;
@property (nonnull, nonatomic, strong) NSManagedObjectContext *mainManagedObjectContext;
@property (nonnull, nonatomic, strong) NSManagedObjectContext *workerManagedObjectContext;

@end

@implementation PlayerViewController

- (instancetype)initWithTracks:(NSArray<Track *> *)tracks currentTrack:(Track *)currentTrack {
    self = [super init];
    
    if (self) {
        _tracks = [NSMutableArray arrayWithArray:tracks];
        _currentTrack = currentTrack;
        
        [self resetTrackIndexes];
        self.currentTrackDidChange = YES;
        [self updateSelectedTrackIndex];
    }
    
    return self;
}

- (void)loadView {
    [super loadView];
    
    SessionManager *sessionManager = [SessionManager sharedManager];
    self.repeatMode = sessionManager.repeatMode;
    self.shuffleEnabled = sessionManager.shuffle;
    
    [self updateCurrentTrack];
    
    CGSize constrainedSize = CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX);
    CGRect timeLabelRect = [self.totalTimeLabel.text boundingRectWithSize:constrainedSize
                                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                                               attributes:@{NSFontAttributeName: self.totalTimeLabel.font}
                                                                  context:nil];
    CGFloat timeLabelWidth = ceil(timeLabelRect.size.width) + 5.0f;
    
    CGRect stateLabelRect = [@"" boundingRectWithSize:constrainedSize
                                              options:NSStringDrawingUsesLineFragmentOrigin
                                           attributes:@{NSFontAttributeName: self.stateLabel.font}
                                              context:nil];
    CGFloat stateLabelHeight = ceil(stateLabelRect.size.height);
    
    [self.view addSubview:self.backgroundView];
    [self.backgroundView addSubview:self.backgroundImageView];
    
    [self.view addSubview:self.topView];
    [self.topView addSubview:self.closeButton];
    
    [self.view addSubview:self.albumCoverImageView];
    
    [self.view addSubview:self.slider];
    
    [self.view addSubview:self.elapsedTimeLabel];
    [self.view addSubview:self.totalTimeLabel];
    [self.view addSubview:self.artistNameLabel];
    [self.view addSubview:self.trackTitleLabel];
    [self.view addSubview:self.stateLabel];
    [self.view addSubview:self.lyricsButton];
    [self.view addSubview:self.favoriteButton];
    [self.view addSubview:self.playlistButton];
    [self.view addSubview:self.activityIndicator];
    
    [self.view addSubview:self.playPauseButton];
    [self.view addSubview:self.previousButton];
    [self.view addSubview:self.nextButton];
    [self.view addSubview:self.repeatButton];
    [self.view addSubview:self.shuffleButton];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[backgroundView]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:@{@"backgroundView": self.backgroundView}]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[backgroundView]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:@{@"backgroundView": self.backgroundView}]];
    
    [self.backgroundView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[backgroundImageView]|"
                                                                                options:0
                                                                                metrics:nil
                                                                                  views:@{@"backgroundImageView": self.backgroundImageView}]];
    
    [self.backgroundView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[backgroundImageView]|"
                                                                                options:0
                                                                                metrics:nil
                                                                                  views:@{@"backgroundImageView": self.backgroundImageView}]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[topView]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:@{@"topView": self.topView}]];
    
    [self.topView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-space-[closeButton(width)]"
                                                                         options:0
                                                                         metrics:@{@"space": @(horizontalSpace),
                                                                                   @"width": @(kButtonSize.width)}
                                                                           views:@{@"closeButton": self.closeButton}]];
    
    [self.topView addConstraint:[NSLayoutConstraint constraintWithItem:self.closeButton
                                                             attribute:NSLayoutAttributeCenterY
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.topView
                                                             attribute:NSLayoutAttributeCenterY
                                                            multiplier:1.0f
                                                              constant:0.0f]];
    
    [self.closeButton addConstraint:[NSLayoutConstraint constraintWithItem:self.closeButton
                                                                 attribute:NSLayoutAttributeHeight
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:nil
                                                                 attribute:NSLayoutAttributeNotAnAttribute
                                                                multiplier:1.0f
                                                                  constant:kButtonSize.height]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-space-[albumCoverImageView]-space-|"
                                                                      options:0
                                                                      metrics:@{@"space": @(horizontalSpace)}
                                                                        views:@{@"albumCoverImageView": self.albumCoverImageView}]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-space-[elapsedTimeLabel(width)]-space-[slider]-space-[totalTimeLabel(width)]-space-|"
                                                                      options:0
                                                                      metrics:@{@"space": @(horizontalSpace),
                                                                                @"width": @(timeLabelWidth)}
                                                                        views:@{@"slider": self.slider,
                                                                                @"elapsedTimeLabel": self.elapsedTimeLabel,
                                                                                @"totalTimeLabel": self.totalTimeLabel}]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.elapsedTimeLabel
                                                          attribute:NSLayoutAttributeCenterY
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.slider
                                                          attribute:NSLayoutAttributeCenterY
                                                         multiplier:1.0f
                                                           constant:0.0f]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.totalTimeLabel
                                                          attribute:NSLayoutAttributeCenterY
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.slider
                                                          attribute:NSLayoutAttributeCenterY
                                                         multiplier:1.0f
                                                           constant:0.0f]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-space-[artistNameLabel]-space-|"
                                                                      options:0
                                                                      metrics:@{@"space": @(horizontalSpace)}
                                                                        views:@{@"artistNameLabel": self.artistNameLabel}]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-space-[trackTitleLabel]-space-|"
                                                                      options:0
                                                                      metrics:@{@"space": @(horizontalSpace)}
                                                                        views:@{@"trackTitleLabel": self.trackTitleLabel}]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-space-[stateLabel]-space-|"
                                                                      options:0
                                                                      metrics:@{@"space": @(horizontalSpace)}
                                                                        views:@{@"stateLabel": self.stateLabel}]];
    
    [self.playlistButton addConstraint:[NSLayoutConstraint constraintWithItem:self.playlistButton
                                                          attribute:NSLayoutAttributeWidth
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                          attribute:NSLayoutAttributeNotAnAttribute
                                                         multiplier:1.0f
                                                           constant:kButtonSize.width]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.playlistButton
                                                          attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.totalTimeLabel
                                                          attribute:NSLayoutAttributeCenterX
                                                         multiplier:1.0f
                                                           constant:0.0f]];

    [self.favoriteButton addConstraint:[NSLayoutConstraint constraintWithItem:self.favoriteButton
                                                                  attribute:NSLayoutAttributeWidth
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:nil
                                                                  attribute:NSLayoutAttributeNotAnAttribute
                                                                 multiplier:1.0f
                                                                   constant:kButtonSize.width]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.favoriteButton
                                                          attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.elapsedTimeLabel
                                                          attribute:NSLayoutAttributeCenterX
                                                         multiplier:1.0f
                                                           constant:0.0f]];

    [self.activityIndicator addConstraint:[NSLayoutConstraint constraintWithItem:self.activityIndicator
                                                                    attribute:NSLayoutAttributeWidth
                                                                    relatedBy:NSLayoutRelationEqual
                                                                       toItem:nil
                                                                    attribute:NSLayoutAttributeNotAnAttribute
                                                                   multiplier:1.0f
                                                                     constant:kButtonSize.width]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.activityIndicator
                                                          attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.elapsedTimeLabel
                                                          attribute:NSLayoutAttributeCenterX
                                                         multiplier:1.0f
                                                           constant:0.0f]];

    
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[playlistButton(height)]-space-[totalTimeLabel]"
                                                                      options:0
                                                                      metrics:@{@"space": @(10.0),
                                                                                @"height": @(kButtonSize.height)}
                                                                        views:@{@"playlistButton": self.playlistButton,
                                                                                @"totalTimeLabel": self.totalTimeLabel}]];

    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[favoriteButton(height)]-space-[elapsedTimeLabel]"
                                                                      options:0
                                                                      metrics:@{@"space": @(10.0),
                                                                                @"height": @(kButtonSize.height)}
                                                                        views:@{@"favoriteButton": self.favoriteButton,
                                                                                @"elapsedTimeLabel": self.elapsedTimeLabel}]];

    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[activityIndicator(height)]-space-[elapsedTimeLabel]"
                                                                      options:0
                                                                      metrics:@{@"space": @(10.0),
                                                                                @"height": @(kButtonSize.height)}
                                                                        views:@{@"activityIndicator": self.activityIndicator,
                                                                                @"elapsedTimeLabel": self.elapsedTimeLabel}]];

    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.playPauseButton
                                                          attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeCenterX
                                                         multiplier:1.0f
                                                           constant:0.0f]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.lyricsButton
                                                          attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeCenterX
                                                         multiplier:1.0f
                                                           constant:0.0f]];

    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[previousButton(width)]-space-[playPauseButton]"
                                                                      options:0
                                                                      metrics:@{@"space": @(horizontalSpace),
                                                                                @"width": @(kButtonSize.width)}
                                                                        views:@{@"playPauseButton": self.playPauseButton,
                                                                                @"previousButton": self.previousButton}]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[favoriteButton(width)]-space-[lyricsButton]"
                                                                      options:0
                                                                      metrics:@{@"space": @(horizontalSpace),
                                                                                @"width": @(kButtonSize.width)}
                                                                        views:@{@"lyricsButton": self.lyricsButton,
                                                                                @"favoriteButton": self.favoriteButton}]];

    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.previousButton
                                                          attribute:NSLayoutAttributeCenterY
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.playPauseButton
                                                          attribute:NSLayoutAttributeCenterY
                                                         multiplier:1.0f
                                                           constant:0.0f]];
    
    [self.previousButton addConstraint:[NSLayoutConstraint constraintWithItem:self.previousButton
                                                                    attribute:NSLayoutAttributeHeight
                                                                    relatedBy:NSLayoutRelationEqual
                                                                       toItem:nil
                                                                    attribute:NSLayoutAttributeNotAnAttribute
                                                                   multiplier:1.0f
                                                                     constant:kButtonSize.height]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[playPauseButton]-space-[nextButton(width)]"
                                                                      options:0
                                                                      metrics:@{@"space": @(horizontalSpace),
                                                                                @"width": @(kButtonSize.width)}
                                                                        views:@{@"playPauseButton": self.playPauseButton,
                                                                                @"nextButton": self.nextButton}]];

    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[lyricsButton]-space-[playlistButton(width)]"
                                                                      options:0
                                                                      metrics:@{@"space": @(horizontalSpace),
                                                                                @"width": @(kButtonSize.width)}
                                                                        views:@{@"lyricsButton": self.lyricsButton,
                                                                                @"playlistButton": self.playlistButton}]];

    
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.nextButton
                                                          attribute:NSLayoutAttributeCenterY
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.playPauseButton
                                                          attribute:NSLayoutAttributeCenterY
                                                         multiplier:1.0f
                                                           constant:0.0f]];
    
    [self.nextButton addConstraint:[NSLayoutConstraint constraintWithItem:self.nextButton
                                                                attribute:NSLayoutAttributeHeight
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:nil
                                                                attribute:NSLayoutAttributeNotAnAttribute
                                                               multiplier:1.0f
                                                                 constant:kButtonSize.height]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-space-[repeatButton(width)]"
                                                                      options:0
                                                                      metrics:@{@"space": @(horizontalSpace),
                                                                                @"width": @(kButtonSize.width)}
                                                                        views:@{@"repeatButton": self.repeatButton}]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.repeatButton
                                                          attribute:NSLayoutAttributeCenterY
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.playPauseButton
                                                          attribute:NSLayoutAttributeCenterY
                                                         multiplier:1.0f
                                                           constant:0.0f]];
    
    [self.repeatButton addConstraint:[NSLayoutConstraint constraintWithItem:self.repeatButton
                                                                  attribute:NSLayoutAttributeHeight
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:nil
                                                                  attribute:NSLayoutAttributeNotAnAttribute
                                                                 multiplier:1.0f
                                                                   constant:kButtonSize.height]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[shuffleButton(width)]-space-|"
                                                                      options:0
                                                                      metrics:@{@"space": @(horizontalSpace),
                                                                                @"width": @(kButtonSize.width)}
                                                                        views:@{@"shuffleButton": self.shuffleButton}]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.shuffleButton
                                                          attribute:NSLayoutAttributeCenterY
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.playPauseButton
                                                          attribute:NSLayoutAttributeCenterY
                                                         multiplier:1.0f
                                                           constant:0.0f]];
    
    [self.shuffleButton addConstraint:[NSLayoutConstraint constraintWithItem:self.shuffleButton
                                                                   attribute:NSLayoutAttributeHeight
                                                                   relatedBy:NSLayoutRelationEqual
                                                                      toItem:nil
                                                                   attribute:NSLayoutAttributeNotAnAttribute
                                                                  multiplier:1.0f
                                                                    constant:kButtonSize.height]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[topLayoutGuide][topView(height1)]-space2-[albumCoverImageView]-space3-[trackTitleLabel]-space1-[artistNameLabel]-space1-[stateLabel(height2)]-space2-[lyricsButton(height1)]-0-[slider]-space2-[playPauseButton]-space2-|"
                                                                      options:0
                                                                      metrics:@{@"height1": @(44.0f),
                                                                                @"height2": @(stateLabelHeight),
                                                                                @"space1": @(5.0f),
                                                                                @"space2": @(25.0f),
                                                                                @"space3": @(15.0f)}
                                                                        views:@{@"topLayoutGuide": self.topLayoutGuide,
                                                                                @"topView": self.topView,
                                                                                @"albumCoverImageView": self.albumCoverImageView,
                                                                                @"trackTitleLabel": self.trackTitleLabel,
                                                                                @"artistNameLabel": self.artistNameLabel,
                                                                                @"stateLabel": self.stateLabel,
                                                                                @"lyricsButton": self.lyricsButton,
                                                                                @"slider": self.slider,
                                                                                @"playPauseButton": self.playPauseButton}]];
    
    [self.backgroundView addSubview:self.visualEffectView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Gesture
    UISwipeGestureRecognizer *swipeDownGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                                                                     action:@selector(handleSwipeDownGestureRecognizer:)];
    swipeDownGestureRecognizer.direction = UISwipeGestureRecognizerDirectionDown;
    [self.view addGestureRecognizer:swipeDownGestureRecognizer];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:PlayerViewControllerWillAppear
                                                        object:self];
}

- (void)viewDidDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] postNotificationName:PlayerViewControllerDidDisappear
                                                        object:self];
    
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -

- (NSURL *)fileUrlForCurrentTrack {
    NSURL *fileUrl;
    
    if (self.currentTrack.localFileName) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = paths.lastObject;
        NSString *directoryPath = [documentsDirectory stringByAppendingPathComponent:TRACKS_DIRECTORY];
        NSString *filePath = [directoryPath stringByAppendingPathComponent:self.currentTrack.localFileName];
        fileUrl= [NSURL fileURLWithPath:filePath];
    } else {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = paths.lastObject;
        NSString *directoryPath = [documentsDirectory stringByAppendingPathComponent:TRACKS_DIRECTORY];
        NSString *filePath = [directoryPath stringByAppendingPathComponent:[self.currentTrack.fileUrl lastPathComponent]];
        NSURL *url = [NSURL URLWithString:filePath];
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if (![fileManager fileExistsAtPath:[url path]]) {
            fileUrl = [NSURL URLWithString:self.currentTrack.fileUrl];
        }
        else
        {
            fileUrl= [NSURL fileURLWithPath:[url path]];
        }
        
    }
    
    return fileUrl;
}

- (BOOL)isRunning {
    if (self.audioPlayer.state == STKAudioPlayerStateRunning ||
        self.audioPlayer.state == STKAudioPlayerStateBuffering ||
        self.audioPlayer.state == STKAudioPlayerStatePlaying) {
        return YES;
    }
    
    return NO;
}

- (void)play {
    self.playPauseButton.selected = YES;
    [self updateNowPlayingInfo];
    
    if (self.audioPlayer.state == STKAudioPlayerStatePaused && !self.currentTrackDidChange) {
        [self.trackTitleLabel unpauseLabel];
        [self.artistNameLabel unpauseLabel];
        
        [self.audioPlayer resume];
    } else {
        self.currentTrackDidChange = NO;
        
        [self.trackTitleLabel restartLabel];
        [self.artistNameLabel restartLabel];
        
        [self.audioPlayer playURL:[self fileUrlForCurrentTrack]];
        
        NSMutableDictionary *dict = [NSMutableDictionary new];
        if (self.currentTrack.album.title.length> 0) {
            [dict setObject:self.currentTrack.album.title forKey:@"album"];
        }
        if (self.currentTrack.artist.name.length > 0) {
            [dict setObject:self.currentTrack.artist.name forKey:@"artist"];
        }
        if (self.currentTrack.genre.title.length > 0) {
            [dict setObject:self.currentTrack.genre.title forKey:@"genre"];
        }
        
        [Answers logContentViewWithName:self.currentTrack.title
                            contentType:@"Player"
                              contentId:[NSString stringWithFormat:@"%@", self.currentTrack.uid]
                       customAttributes:dict];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:PlayerViewControllerWillPlay
                                                        object:self];
    
}

- (void)pause {
    self.playPauseButton.selected = NO;
    
    [self.trackTitleLabel pauseLabel];
    [self.artistNameLabel pauseLabel];
    
    [self.audioPlayer pause];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:PlayerViewControllerWillPause
                                                        object:self];
}

- (void)stop {
    self.playPauseButton.selected = NO;
    
    [self.trackTitleLabel resetLabel];
    [self.artistNameLabel resetLabel];
    
    [self.audioPlayer stop];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:PlayerViewControllerWillStop
                                                        object:self];
}

- (void)dispose {
    self.playPauseButton.selected = NO;
    
    [self.trackTitleLabel resetLabel];
    [self.artistNameLabel resetLabel];
    
    [self.audioPlayer dispose];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:PlayerViewControllerWillDispose
                                                        object:self];
}

- (void)first {
    self.slider.value = 0;
    self.selectedTrackIndex = 0;
    
    NSInteger trackIndex = [self.trackIndexes[self.selectedTrackIndex] integerValue];
    self.currentTrack = _tracks[trackIndex];
    
    if (self.playPauseButton.selected) {
        [self play];
    }
}

- (void)last {
    self.slider.value = 0;
    self.selectedTrackIndex = _tracks.count - 1;
    
    NSInteger trackIndex = [self.trackIndexes[self.selectedTrackIndex] integerValue];
    self.currentTrack = _tracks[trackIndex];
    
    if (self.playPauseButton.selected) {
        [self play];
    }
}

- (void)previous {
    double progress = self.slider.value;
    self.slider.value = 0;
    
    if (progress < 1) {
        if (self.selectedTrackIndex > 0) {
            self.selectedTrackIndex--;
            
            NSInteger trackIndex = [self.trackIndexes[self.selectedTrackIndex] integerValue];
            self.currentTrack = _tracks[trackIndex];
            
            if (self.playPauseButton.selected) {
                [self play];
            }
        } else if (self.repeatMode == MUZRepeatModeAll) {
            [self last];
        }
    } else {
        if (self.playPauseButton.selected) {
            [self play];
        }
    }
}

- (void)next {
    NSInteger numberOfTracks = _tracks.count;
    
    if (self.selectedTrackIndex < numberOfTracks - 1) {
        self.slider.value = 0;
        self.selectedTrackIndex++;
        
        NSInteger trackIndex = [self.trackIndexes[self.selectedTrackIndex] integerValue];
        self.currentTrack = _tracks[trackIndex];
        
        if (self.playPauseButton.selected) {
            [self play];
        }
    } else if (self.repeatMode == MUZRepeatModeAll) {
        [self first];
    }
}

- (void)shuffle {
    [self shuffleTrackIndexes];
}

- (void)setCurrentTrack:(Track *)currentTrack {
    if (_currentTrack.uid != currentTrack.uid) {
        _currentTrack = currentTrack;
        self.currentTrackDidChange = YES;
        [self updateCurrentTrack];
    }
}

- (void)updateCurrentTrack {
    self.trackTitleLabel.text = _currentTrack.title;
    self.artistNameLabel.text = _currentTrack.artist.name;
    self.elapsedTimeLabel.text = [TimeHelper timeStringFromTotalSeconds:0];
    self.totalTimeLabel.text = [TimeHelper timeStringFromTotalSeconds:_currentTrack.duration];
    self.slider.maximumValue = [_currentTrack.duration floatValue];
    
    [self setFavorite:[self.favoriteManager isFavoriteTrack:_currentTrack] button:self.favoriteButton];
    
    self.lyricsButton.hidden = _currentTrack.lyrics.length == 0;
    [self updatePreviousNextButtonsState];
    
    __weak PlayerViewController *weakSelf = self;
    
    [self.albumCoverImageView sd_setImageWithURL:[NSURL URLWithString:_currentTrack.imageUrl] placeholderImage:self.placeholderImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (image) {
            if (cacheType == SDImageCacheTypeNone) {
                weakSelf.albumCoverImageView.alpha = 0.0f;
                [UIView animateWithDuration:0.25f animations:^{
                    weakSelf.albumCoverImageView.alpha = 1.0f;
                } completion:^(BOOL finished) {
                    weakSelf.albumCoverImageView.image = image;
                }];
            } else {
                weakSelf.albumCoverImageView.image = image;
            }
        }
    }];
    
    [self.backgroundImageView sd_setImageWithURL:[NSURL URLWithString:_currentTrack.imageUrl] placeholderImage:self.placeholderImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (image) {
            if (cacheType == SDImageCacheTypeNone) {
                weakSelf.backgroundImageView.alpha = 0.0f;
                [UIView animateWithDuration:0.25f animations:^{
                    weakSelf.backgroundImageView.alpha = 1.0f;
                } completion:^(BOOL finished) {
                    weakSelf.backgroundImageView.image = image;
                }];
            } else {
                weakSelf.backgroundImageView.image = image;
            }
        }
    }];
    
    [self updateNowPlayingInfo];
}

- (void)setShuffleEnabled:(BOOL)shuffleEnabled {
    _shuffleEnabled = shuffleEnabled;
    
    SessionManager *sessionManager = [SessionManager sharedManager];
    [sessionManager setShuffle:shuffleEnabled];
    [sessionManager saveSession];
    
    self.shuffleButton.enabled = NO;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void) {
        if (shuffleEnabled) {
            [self shuffleTrackIndexes];
        } else {
            [self resetTrackIndexes];
        }
        
        [self updateSelectedTrackIndex];
        
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            [self updatePreviousNextButtonsState];
            self.shuffleButton.selected = shuffleEnabled;
            self.shuffleButton.enabled = YES;
            
            if (shuffleEnabled) {
                self.shuffleButton.tintColor = [[BaseTheme currentTheme] controlTintColor];
            } else {
                self.shuffleButton.tintColor = [UIColor whiteColor];
            }
        });
    });
}

- (void)updateSelectedTrackIndex {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uid = %@", self.currentTrack.uid];
    NSArray *result = [_tracks filteredArrayUsingPredicate:predicate];
    
    if (result.count > 0) {
        Track *selectedTrack = result[0];
        self.selectedTrackIndex = [_tracks indexOfObject:selectedTrack];
    } else {
        self.selectedTrackIndex = 0;
    }
}

#pragma mark - NSMutableArray

- (NSMutableArray *)trackIndexes {
    if (_trackIndexes == nil) {
        _trackIndexes = [NSMutableArray new];
    }
    
    return _trackIndexes;
}

- (void)shuffleTrackIndexes {
    NSInteger count = _tracks.count;
    
    for (NSUInteger index = 0; index < count; ++index) {
        NSInteger numberOfItems = count - index;
        NSInteger newIndex = (arc4random() % numberOfItems) + index;
        [self.trackIndexes exchangeObjectAtIndex:index
                           withObjectAtIndex:newIndex];
    }
}

- (void)resetTrackIndexes {
    [self.trackIndexes removeAllObjects];
    
    for (NSInteger index = 0; index < _tracks.count; index++) {
        [self.trackIndexes addObject:@(index)];
    }
}

#pragma mark - MUZRepeatMode

- (void)setRepeatMode:(MUZRepeatMode)repeatMode {
    _repeatMode = repeatMode;
    
    SessionManager *sessionManager = [SessionManager sharedManager];
    [sessionManager setRepeatMode:repeatMode];
    [sessionManager saveSession];
    
    if (repeatMode == MUZRepeatModeAll) {
        [self.repeatButton setImage:[[UIImage imageNamed:@"ic_repeat_all.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]
                           forState:UIControlStateNormal];
        self.repeatButton.tintColor = [[BaseTheme currentTheme] controlTintColor];
    } else if (repeatMode == MUZRepeatModeSingle) {
        [self.repeatButton setImage:[[UIImage imageNamed:@"ic_repeat_single.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]
                           forState:UIControlStateNormal];
        self.repeatButton.tintColor = [[BaseTheme currentTheme] controlTintColor];
    } else {
        [self.repeatButton setImage:[[UIImage imageNamed:@"ic_repeat_all.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]
                           forState:UIControlStateNormal];
        self.repeatButton.tintColor = [UIColor whiteColor];
    }
}

#pragma mark - NSTimer

- (void)startTimer {
    if (_timer == nil) {
        _timer = [NSTimer timerWithTimeInterval:0.001f
                                         target:self
                                       selector:@selector(tick)
                                       userInfo:nil
                                        repeats:YES];
        
        [[NSRunLoop currentRunLoop] addTimer:_timer
                                     forMode:NSRunLoopCommonModes];
    }
}

- (void)stopTimer {
    [_timer invalidate];
    _timer = nil;
}

- (void)tick {
    if (!self.isSeeking) {
        self.slider.value = self.audioPlayer.progress;
        self.elapsedTimeLabel.text = [TimeHelper timeStringFromTotalSeconds:@(self.slider.value)];
    }
}

#pragma mark - UIGestureRecognizer

- (void)handleSwipeDownGestureRecognizer:(UISwipeGestureRecognizer *)swipeDownGestureRecognizer {
    [self dismissViewControllerAnimated:YES
                             completion:nil];
}

#pragma mark - UIView

- (UIView *)backgroundView {
    if (_backgroundView == nil) {
        _backgroundView = [[UIView alloc] init];
        _backgroundView.translatesAutoresizingMaskIntoConstraints = NO;
    }
    
    return _backgroundView;
}

- (UIView *)topView {
    if (_topView == nil) {
        _topView = [[UIView alloc] init];
        _topView.translatesAutoresizingMaskIntoConstraints = NO;
        [_topView setContentHuggingPriority:UILayoutPriorityDefaultHigh
                                    forAxis:UILayoutConstraintAxisVertical];
        [_topView setContentCompressionResistancePriority:UILayoutPriorityDefaultHigh
                                                  forAxis:UILayoutConstraintAxisVertical];
    }
    
    return _topView;
}

#pragma mark - UIVisualEffectView

- (UIVisualEffectView *)visualEffectView {
    if (_visualEffectView == nil) {
        UIVisualEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        _visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        _visualEffectView.frame = self.view.bounds;
    }
    
    return _visualEffectView;
}

#pragma mark - UIImage

- (UIImage *)placeholderImage {
    if (_placeholderImage == nil) {
        _placeholderImage = [UIImage imageNamed:@"img_placeholder.png"];
    }
    
    return _placeholderImage;
}

#pragma mark - UIImageView

- (UIImageView *)backgroundImageView {
    if (_backgroundImageView == nil) {
        _backgroundImageView = [[UIImageView alloc] init];
        _backgroundImageView.translatesAutoresizingMaskIntoConstraints = NO;
        _backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
    }
    
    return _backgroundImageView;
}

- (UIImageView *)albumCoverImageView {
    if (_albumCoverImageView == nil) {
        _albumCoverImageView = [[UIImageView alloc] init];
        _albumCoverImageView.translatesAutoresizingMaskIntoConstraints = NO;
        [_albumCoverImageView setContentHuggingPriority:UILayoutPriorityDefaultLow
                                                forAxis:UILayoutConstraintAxisVertical];
        [_albumCoverImageView setContentCompressionResistancePriority:UILayoutPriorityDefaultLow
                                                              forAxis:UILayoutConstraintAxisVertical];
        _albumCoverImageView.contentMode = UIViewContentModeScaleAspectFit;
        _albumCoverImageView.layer.cornerRadius = 3.0f;
        _albumCoverImageView.clipsToBounds = YES;
    }
    
    return _albumCoverImageView;
}

#pragma mark - PlayerSlider

- (PlayerSlider *)slider {
    if (_slider == nil) {
        _slider = [[PlayerSlider alloc] init];
        _slider.translatesAutoresizingMaskIntoConstraints = NO;
        [_slider setContentHuggingPriority:UILayoutPriorityDefaultHigh
                                   forAxis:UILayoutConstraintAxisVertical];
        [_slider setContentCompressionResistancePriority:UILayoutPriorityDefaultHigh
                                                 forAxis:UILayoutConstraintAxisVertical];
        [_slider addTarget:self
                    action:@selector(sliderTouchDown)
          forControlEvents:UIControlEventTouchDown];
        [_slider addTarget:self
                    action:@selector(sliderValueChanged)
          forControlEvents:UIControlEventValueChanged];
        [_slider addTarget:self
                    action:@selector(sliderTouchUpInside)
          forControlEvents:UIControlEventTouchUpInside];
        _slider.minimumTrackTintColor = [[BaseTheme currentTheme] controlTintColor];
        _slider.maximumTrackTintColor = [UIColor whiteColor];
        _slider.minimumValue = 0;
    }
    
    return _slider;
}

- (void)sliderTouchDown {
    self.seeking = YES;
}

- (void)sliderValueChanged {
    self.elapsedTimeLabel.text = [TimeHelper timeStringFromTotalSeconds:@(self.slider.value)];
}

- (void)sliderTouchUpInside {
    self.seeking = NO;
    [self.audioPlayer seekToTime:self.slider.value];
}

#pragma mark - UILabel

- (MarqueeLabel *)trackTitleLabel {
    if (_trackTitleLabel == nil) {
        _trackTitleLabel = [[MarqueeLabel alloc] initWithFrame:CGRectZero
                                                          rate:20.0f
                                                 andFadeLength:10.0f];
        _trackTitleLabel.marqueeType = MLLeftRight;
        _trackTitleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [_trackTitleLabel setContentHuggingPriority:UILayoutPriorityDefaultHigh
                                            forAxis:UILayoutConstraintAxisVertical];
        [_trackTitleLabel setContentCompressionResistancePriority:UILayoutPriorityDefaultHigh
                                                          forAxis:UILayoutConstraintAxisVertical];
        _trackTitleLabel.font = [UIFont systemFontOfSize:17.0f];
        _trackTitleLabel.textColor = [UIColor whiteColor];
        _trackTitleLabel.textAlignment = NSTextAlignmentCenter;
    }
    
    return _trackTitleLabel;
}

- (MarqueeLabel *)artistNameLabel {
    if (_artistNameLabel == nil) {
        _artistNameLabel = [[MarqueeLabel alloc] initWithFrame:CGRectZero
                                                          rate:20.0f
                                                 andFadeLength:10.0f];
        _artistNameLabel.marqueeType = MLLeftRight;
        _artistNameLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [_artistNameLabel setContentHuggingPriority:UILayoutPriorityDefaultHigh
                                            forAxis:UILayoutConstraintAxisVertical];
        [_artistNameLabel setContentCompressionResistancePriority:UILayoutPriorityDefaultHigh
                                                          forAxis:UILayoutConstraintAxisVertical];
        _artistNameLabel.font = [UIFont systemFontOfSize:14.0f];
        _artistNameLabel.textColor = [UIColor whiteColor];
        _artistNameLabel.textAlignment = NSTextAlignmentCenter;
    }
    
    return _artistNameLabel;
}

- (UILabel *)stateLabel {
    if (_stateLabel == nil) {
        _stateLabel = [[UILabel alloc] init];
        _stateLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _stateLabel.font = [UIFont systemFontOfSize:12.0f];
        _stateLabel.textColor = [UIColor whiteColor];
        _stateLabel.textAlignment = NSTextAlignmentCenter;
    }
    
    return _stateLabel;
}

- (UILabel *)createTimeLabel {
    UILabel *timeLabel = [[UILabel alloc] init];
    timeLabel.translatesAutoresizingMaskIntoConstraints = NO;
    timeLabel.font = [UIFont systemFontOfSize:12.0f];
    timeLabel.textColor = [UIColor whiteColor];
    
    return timeLabel;
}

- (UILabel *)elapsedTimeLabel {
    if (_elapsedTimeLabel == nil) {
        _elapsedTimeLabel = [self createTimeLabel];
    }
    
    return _elapsedTimeLabel;
}

- (UILabel *)totalTimeLabel {
    if (_totalTimeLabel == nil) {
        _totalTimeLabel = [self createTimeLabel];
    }
    
    return _totalTimeLabel;
}

#pragma mark - UIButton

- (UIButton *)closeButton {
    if (_closeButton == nil) {
        _closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _closeButton.translatesAutoresizingMaskIntoConstraints = NO;
        [_closeButton addTarget:self
                         action:@selector(closeButtonTapped:)
               forControlEvents:UIControlEventTouchUpInside];
        [_closeButton setImage:[UIImage imageNamed:@"ic_arrow_down.png"]
                      forState:UIControlStateNormal];
        [_closeButton setImage:[[UIImage imageNamed:@"ic_arrow_down.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]
                      forState:UIControlStateHighlighted];
        _closeButton.tintColor = [[BaseTheme currentTheme] controlTintColor];
    }
    
    return _closeButton;
}

- (void)closeButtonTapped:(id)sender {
    [self dismissViewControllerAnimated:YES
                             completion:nil];
}

- (UIButton *)lyricsButton {
    if (_lyricsButton == nil) {
        _lyricsButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _lyricsButton.translatesAutoresizingMaskIntoConstraints = NO;
        [_lyricsButton addTarget:self
                         action:@selector(lyricsButtonTapped:)
               forControlEvents:UIControlEventTouchUpInside];
        [_lyricsButton setImage:[UIImage imageNamed:@"ic_lyrics.png"]
                      forState:UIControlStateNormal];
        [_lyricsButton setImage:[[UIImage imageNamed:@"ic_lyrics.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]
                      forState:UIControlStateHighlighted];
        _lyricsButton.tintColor = [[BaseTheme currentTheme] controlTintColor];
    }
    
    return _lyricsButton;
}

- (void)lyricsButtonTapped:(id)sender {
    LyricsViewController *viewController = [[LyricsViewController alloc] initWithTrack:self.currentTrack];
    [self presentViewController:viewController
                       animated:YES
                     completion:nil];
}


-(FavoriteManager*) favoriteManager
{
    return [FavoriteManager sharedManager];
}

- (UIButton *)favoriteButton {
    if (_favoriteButton == nil) {
        _favoriteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _favoriteButton.translatesAutoresizingMaskIntoConstraints = NO;
        [_favoriteButton addTarget:self
                          action:@selector(favoriteButtonTapped:)
                forControlEvents:UIControlEventTouchUpInside];
        [self setFavorite:[self.favoriteManager isFavoriteTrack:self.currentTrack] button:_favoriteButton];
    }
    
    return _favoriteButton;
}

- (UIButton *)playlistButton {
    if (_playlistButton == nil) {
        _playlistButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _playlistButton.translatesAutoresizingMaskIntoConstraints = NO;
        [_playlistButton addTarget:self
                            action:@selector(addToPlaylistButtonTapped:)
                  forControlEvents:UIControlEventTouchUpInside];
        [_playlistButton setImage:[UIImage imageNamed:@"add_to_playlist"]
                      forState:UIControlStateNormal];
        [_playlistButton setImage:[[UIImage imageNamed:@"add_to_playlist"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]
                      forState:UIControlStateHighlighted];
        _playlistButton.tintColor = [[BaseTheme currentTheme] controlTintColor];

    }
    
    return _playlistButton;
}


- (void)addToPlaylistButtonTapped:(id)sender {
    SelectPlaylistViewController *sv = [[SelectPlaylistViewController alloc] init];
    sv.track = self.currentTrack;
    
    AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    if ([delegate.window.rootViewController presentedViewController]) {
        [delegate.window.rootViewController dismissViewControllerAnimated:NO completion:^{}];
    }
    [delegate.window.rootViewController presentViewController:sv animated:YES completion:^{}];
}


-(UIActivityIndicatorView*)activityIndicator
{
    if (_activityIndicator == nil) {
        _activityIndicator = [[UIActivityIndicatorView alloc] init];
        _activityIndicator.backgroundColor = [UIColor clearColor];
        _activityIndicator.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _activityIndicator;
}


- (void)favoriteButtonTapped:(id)sender {
    
    if ([User currentUser].accessToken == nil)
    {
        [[User currentUser] showLoginForm];
    }
    else
    {
        [self showLoadingIndicator];
        [self.activityIndicator startAnimating];
        self.favoriteButton.hidden = YES;
        WEAKIFY_SELF;
        if ([self.favoriteManager isFavoriteTrack:self.currentTrack]) {
            [self.favoriteManager deleteFromFavoriteTrack:self.currentTrack complete:^(BOOL success) {
                STRONGIFY_SELF;
                [self hideLoadingIndicator];
                [self.activityIndicator stopAnimating];
                self.favoriteButton.hidden = NO;
                if (success)
                {
                 
                    [self setFavorite:NO button:self.favoriteButton];

                }
                else
                {
                    [self setFavorite:YES button:self.favoriteButton];
                }
            }];
        }
        else
        {
            [self.favoriteManager addToFavoriteTrack:self.currentTrack complete:^(BOOL success) {
                STRONGIFY_SELF;
                [self hideLoadingIndicator];
                [self.activityIndicator stopAnimating];
                self.favoriteButton.hidden = NO;
                if (success)
                {
                    
                    [self setFavorite:YES button:self.favoriteButton];
                    
                }
                else
                {
                    [self setFavorite:NO button:self.favoriteButton];
                }

            }];
        }
    }
}

-(void)deleteTrack:(Track*) track
{
    [self.workerManagedObjectContext performBlock:^{
        [self.workerManagedObjectContext deleteObject:track];
        
        NSError *error;
        
        if (![self.workerManagedObjectContext save:&error]) {
            [self.mainManagedObjectContext performBlock:^{
                [ErrorHandler handleError:error];
            }];
        } else {
            [self.mainManagedObjectContext performBlock:^{
                NSError *error;
                
                if (![self.mainManagedObjectContext save:&error]) {
                    [self.mainManagedObjectContext performBlock:^{
                        [ErrorHandler handleError:error];
                    }];
                } else {
                    [self.mainManagedObjectContext.parentContext performBlock:^{
                        NSError *error;
                        
                        if (![self.mainManagedObjectContext.parentContext save:&error]) {
                            [self.mainManagedObjectContext performBlock:^{
                                [ErrorHandler handleError:error];
                            }];
                        } else {
                        }
                    }];
                }
            }];
        }
    }];
    
}


-(void)setFavorite:(BOOL)favorite button:(UIButton*) button
{
    if (favorite) {
        [button setImage:[UIImage imageNamed:@"ic_favorite_white"]
                         forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:@"ic_favorite_white"]
                         forState:UIControlStateHighlighted];
    }
    else
    {
        [button setImage:[UIImage imageNamed:@"ic_favorite_border_white"]
                         forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:@"ic_favorite_border_white"]
                         forState:UIControlStateHighlighted];
    }
}


- (UIButton *)playPauseButton {
    if (_playPauseButton == nil) {
        _playPauseButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _playPauseButton.translatesAutoresizingMaskIntoConstraints = NO;
        [_playPauseButton setContentHuggingPriority:UILayoutPriorityDefaultHigh
                                            forAxis:UILayoutConstraintAxisVertical];
        [_playPauseButton setContentCompressionResistancePriority:UILayoutPriorityDefaultHigh
                                                          forAxis:UILayoutConstraintAxisVertical];
        [_playPauseButton addTarget:self
                             action:@selector(playPauseButtonTapped:)
                   forControlEvents:UIControlEventTouchUpInside];
        [_playPauseButton setImage:[UIImage imageNamed:@"ic_play.png"]
                          forState:UIControlStateNormal];
        [_playPauseButton setImage:[UIImage imageNamed:@"ic_pause.png"]
                          forState:UIControlStateSelected];
    }
    
    return _playPauseButton;
}

- (void)playPauseButtonTapped:(id)sender {
    if (self.playPauseButton.selected) {
        [self pause];
    } else {
        [self play];
    }
}

- (UIButton *)previousButton {
    if (_previousButton == nil) {
        _previousButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _previousButton.translatesAutoresizingMaskIntoConstraints = NO;
        [_previousButton addTarget:self
                            action:@selector(previousButtonTapped:)
                  forControlEvents:UIControlEventTouchUpInside];
        [_previousButton setImage:[UIImage imageNamed:@"ic_previous.png"]
                         forState:UIControlStateNormal];
    }
    
    return _previousButton;
}

- (void)previousButtonTapped:(id)sender {
    [self previous];
    [[NSNotificationCenter defaultCenter] postNotificationName:PlayerViewControllerDidChangeCurrentTrack
                                                        object:self];
}

- (UIButton *)nextButton {
    if (_nextButton == nil) {
        _nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _nextButton.translatesAutoresizingMaskIntoConstraints = NO;
        [_nextButton addTarget:self
                        action:@selector(nextButtonTapped:)
              forControlEvents:UIControlEventTouchUpInside];
        [_nextButton setImage:[UIImage imageNamed:@"ic_next.png"]
                     forState:UIControlStateNormal];
    }
    
    return _nextButton;
}

- (void)nextButtonTapped:(id)sender {
    [self next];
    [[NSNotificationCenter defaultCenter] postNotificationName:PlayerViewControllerDidChangeCurrentTrack
                                                        object:self];
}

- (void)updatePreviousNextButtonsState {
    NSInteger count = _tracks.count;
    
    if (self.repeatMode == MUZRepeatModeAll && count > 1) {
        self.previousButton.enabled = YES;
        self.nextButton.enabled = YES;
    } else {
        self.previousButton.enabled = self.selectedTrackIndex != 0;
        self.nextButton.enabled = self.selectedTrackIndex != count - 1;
    }
}

- (UIButton *)repeatButton {
    if (_repeatButton == nil) {
        _repeatButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _repeatButton.translatesAutoresizingMaskIntoConstraints = NO;
        [_repeatButton addTarget:self
                          action:@selector(repeatButtonTapped:)
                forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _repeatButton;
}

- (void)repeatButtonTapped:(id)sender {
    if (self.repeatMode == MUZRepeatModeNone) {
        self.repeatMode = MUZRepeatModeAll;
    } else if (self.repeatMode == MUZRepeatModeAll) {
        self.repeatMode = MUZRepeatModeSingle;
    } else {
        self.repeatMode = MUZRepeatModeNone;
    }
    
    [self updatePreviousNextButtonsState];
}

- (UIButton *)shuffleButton {
    if (_shuffleButton == nil) {
        _shuffleButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _shuffleButton.translatesAutoresizingMaskIntoConstraints = NO;
        [_shuffleButton addTarget:self
                           action:@selector(shuffleButtonTapped:)
                 forControlEvents:UIControlEventTouchUpInside];
        [_shuffleButton setImage:[[UIImage imageNamed:@"ic_shuffle.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]
                        forState:UIControlStateNormal];
        _shuffleButton.tintColor = [UIColor whiteColor];
    }
    
    return _shuffleButton;
}

- (void)shuffleButtonTapped:(id)sender {
    self.shuffleEnabled = !self.shuffleEnabled;
}

#pragma mark - CoreData

- (NSManagedObjectContext *)mainManagedObjectContext {
    if (_mainManagedObjectContext == nil) {
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        _mainManagedObjectContext = appDelegate.mainManagedObjectContext;
    }
    
    return _mainManagedObjectContext;
}

- (NSManagedObjectContext *)workerManagedObjectContext {
    if (_workerManagedObjectContext == nil) {
        _workerManagedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        _workerManagedObjectContext.parentContext = self.mainManagedObjectContext;
    }
    
    return _workerManagedObjectContext;
}

- (TrackRepository *)trackRepository {
    if (_trackRepository == nil) {
        _trackRepository = [[TrackRepository alloc] initWithManagedObjectContext:self.workerManagedObjectContext];
    }
    
    return _trackRepository;
}


#pragma mark - MPNowPlayingInfoCenter

- (void)updateNowPlayingInfo {
    CGFloat rate;
    
    if (self.playPauseButton.selected) {
        rate = 1.0f;
    } else {
        rate = 0.0f;
    }
    
    UIImage *image = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:_currentTrack.imageUrl];
    BOOL shouldLoad = NO;
    
    if (!image) {
        image = self.placeholderImage;
        shouldLoad = YES;
    }
    
    NSMutableDictionary *info = [self getInfodictionary:image rate:rate];
    
    [MPNowPlayingInfoCenter defaultCenter].nowPlayingInfo = info;
    
    if (shouldLoad) {
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        [manager downloadImageWithURL:[NSURL URLWithString:self.currentTrack.imageUrl] options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
            if (image) {
                [[SDImageCache sharedImageCache] storeImage:image
                                                     forKey:self.currentTrack.imageUrl];
                
                MPMediaItemArtwork *artwork = [[MPMediaItemArtwork alloc] initWithImage:image];
                [info setObject:artwork
                         forKey:MPMediaItemPropertyArtwork];
                [MPNowPlayingInfoCenter defaultCenter].nowPlayingInfo = info;
            }
        }];
    }
}

-(NSMutableDictionary*)getInfodictionary:(UIImage*)image rate:(CGFloat)rate
{
    NSMutableDictionary *info = [NSMutableDictionary new];
    if (_currentTrack.title.length > 0) {
        [info setObject:_currentTrack.title forKey:MPMediaItemPropertyTitle];
    }
    if (image != nil) {
        [info setObject:[[MPMediaItemArtwork alloc] initWithImage:image] forKey:MPMediaItemPropertyArtwork];
    }
    if (_currentTrack.album.title.length > 0) {
        [info setObject:_currentTrack.album.title forKey:MPMediaItemPropertyAlbumTitle];
    }
    if (_currentTrack.album.tracksCount != nil) {
        [info setObject:_currentTrack.album.tracksCount forKey:MPMediaItemPropertyAlbumTrackCount];
    }
    if (self.selectedTrackIndex >= 0 ) {
        [info setObject:@(self.selectedTrackIndex) forKey:MPMediaItemPropertyAlbumTrackNumber];
    }
    if (_currentTrack.artist.name.length > 0) {
        [info setObject:_currentTrack.artist.name forKey:MPMediaItemPropertyArtist];
    }
    if (_currentTrack.genre.title.length > 0) {
        [info setObject:_currentTrack.genre.title forKey:MPMediaItemPropertyGenre];
    }
    if (_currentTrack.duration != nil) {
        [info setObject:_currentTrack.duration forKey:MPMediaItemPropertyPlaybackDuration];
    }
    if (rate >= 0.0) {
        [info setObject:@(rate) forKey:MPNowPlayingInfoPropertyPlaybackRate];
    }
    if (self.slider.value >= 0.0) {
        [info setObject:@(self.slider.value) forKey:MPNowPlayingInfoPropertyElapsedPlaybackTime];
    }
    if (self.selectedTrackIndex >= 0) {
        [info setObject:@(self.selectedTrackIndex) forKey:MPNowPlayingInfoPropertyPlaybackQueueIndex];
    }
    if (_tracks != nil ) {
        [info setObject:@(_tracks.count) forKey:MPNowPlayingInfoPropertyPlaybackQueueCount];
    }
    return info;
}

#pragma mark - Loading indicator

- (void)showLoadingIndicator {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)hideLoadingIndicator {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

#pragma mark - STKAudioPlayer

- (STKAudioPlayer *)audioPlayer {
    if (_audioPlayer == nil) {
        _audioPlayer = [[STKAudioPlayer alloc] init];
        _audioPlayer.delegate = self;
    }
    
    return _audioPlayer;
}

#pragma mark - STKAudioPlayerDelegate

- (void)audioPlayer:(STKAudioPlayer *)audioPlayer didFinishBufferingSourceWithQueueItemId:(NSObject *)queueItemId {
    // Not implemented
}

- (void)audioPlayer:(STKAudioPlayer *)audioPlayer didStartPlayingQueueItemId:(NSObject *)queueItemId {
    // Not implemented
}

- (void)audioPlayer:(STKAudioPlayer *)audioPlayer didCancelQueuedItems:(NSArray *)queuedItems {
    // Not implemented
}

- (void)audioPlayer:(STKAudioPlayer *)audioPlayer didFinishPlayingQueueItemId:(NSObject *)queueItemId withReason:(STKAudioPlayerStopReason)stopReason andProgress:(double)progress andDuration:(double)duration {
    if (stopReason == STKAudioPlayerStopReasonEof && (NSInteger)duration > 0 && (NSInteger)self.slider.value == (NSInteger)duration) {
        if (self.repeatMode == MUZRepeatModeNone) {
            if (self.selectedTrackIndex == _tracks.count - 1) {
                self.playPauseButton.selected = NO;
            } else {
                [self next];
                [[NSNotificationCenter defaultCenter] postNotificationName:PlayerViewControllerDidChangeCurrentTrack
                                                                    object:self];
            }
        } else if (self.repeatMode == MUZRepeatModeAll) {
            if (self.selectedTrackIndex == _tracks.count - 1) {
                [self first];
                [[NSNotificationCenter defaultCenter] postNotificationName:PlayerViewControllerDidChangeCurrentTrack
                                                                    object:self];
            } else {
                [self next];
                [[NSNotificationCenter defaultCenter] postNotificationName:PlayerViewControllerDidChangeCurrentTrack
                                                                    object:self];
            }
        } else {
            [self play];
        }
    }
}

- (void)audioPlayer:(STKAudioPlayer *)audioPlayer stateChanged:(STKAudioPlayerState)state previousState:(STKAudioPlayerState)previousState {
    [self hideLoadingIndicator];
    self.stateLabel.text = @"";
    
    if (state == STKAudioPlayerStatePlaying) {
        [self startTimer];
    } else {
        [self stopTimer];
        
        if (state == STKAudioPlayerStateBuffering) {
            [self showLoadingIndicator];
            self.stateLabel.text = NSLocalizedString(@"buffering", nil);
            [self updateNowPlayingInfo];
        } else if (state == STKAudioPlayerStateError) {
            NSString *message = NSLocalizedString(@"error.loading", nil);
            self.stateLabel.text = message;
        }
    }
}

- (void)audioPlayer:(STKAudioPlayer *)audioPlayer unexpectedError:(STKAudioPlayerErrorCode)errorCode {
    [self hideLoadingIndicator];
    
    NSString *message = NSLocalizedString(@"error.unexpected", nil);
    self.stateLabel.text = message;
    
    // Uncomment to handle errors
//    NSError *error = [ErrorHandler errorWithCode:errorCode
//                                        userInfo:@{NSLocalizedDescriptionKey: message}];
//    [[NSNotificationCenter defaultCenter] postNotificationName:PlayerViewControllerShouldHandleError
//                                                        object:self
//                                                      userInfo:@{PlayerViewControllerErrorKey: error}];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)showAlertController:(NSString*) text {
    UIAlertAction *doneAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"done", nil)
                                                           style:UIAlertActionStyleDefault
                                                         handler:nil];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"track", nil)
                                                                             message:text
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:doneAction];
    [self presentViewController:alertController
                       animated:YES
                     completion:nil];
}



@end
