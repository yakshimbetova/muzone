//
//  PlaylistTracksViewController.h
//  Muzone
//
//  Created by Macbook on 5/1/17.
//  Copyright © 2017 Muzone LLC. All rights reserved.
//

#import "BaseViewController.h"

#import "Muzone-Swift.h"

@interface PlaylistTracksViewController : BaseViewController <UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate>
@property (nonnull, nonatomic, strong) Playlist *playlist;
@end
