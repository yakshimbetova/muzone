//
//  PlaylistTracksViewController.m
//  Muzone
//
//  Created by Macbook on 5/1/17.
//  Copyright © 2017 Muzone LLC. All rights reserved.
//

#import "PlaylistTracksViewController.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

#import "AppDelegate.h"
#import "Constants.h"
#import "BaseTheme.h"
#import "ErrorHandler.h"
#import "TrackRepository.h"

#import "Track.h"
#import "Artist.h"
#import "Album.h"

#import "SZKMessageView.h"
#import "SZKProgressView.h"

#import "TracksTableViewCell.h"

#import "PlaylistManager.h"

#import "Muzone-Swift.h"

@interface PlaylistTracksViewController () <SZKMessageViewDelegate>

@property (nonnull, nonatomic, strong) NSManagedObjectContext *mainManagedObjectContext;
@property (nonnull, nonatomic, strong) NSManagedObjectContext *workerManagedObjectContext;
@property (nonnull, nonatomic, strong) TrackRepository *trackRepository;
@property (nonnull, nonatomic, strong) TrackManager *trackManager;

@property (nonnull, nonatomic, copy) NSString *cellReuseIdentifier;

@property (nonnull, nonatomic, strong) NSMutableArray *tracks;

@property (nonnull, nonatomic, strong) UIRefreshControl *refreshControl;
@property (nonnull, nonatomic, strong) UITableView *tableView;
@property (nonnull, nonatomic, strong) SZKMessageView *messageView;
@property (nonnull, nonatomic, strong) SZKProgressView *progressView;
@end

@implementation PlaylistTracksViewController

- (instancetype)init {
    self = [super init];
    
    if (self) {
        self.cellReuseIdentifier = NSStringFromClass([TracksTableViewCell class]);
    }
    
    return self;
}

- (void)loadView {
    [super loadView];
    
    [self.view addSubview:self.tableView];
    
    [Answers logContentViewWithName:@"Playlist tracks"
                        contentType:@"Playlist"
                          contentId:[NSString stringWithFormat:@"%@", self.playlist.uid]
                   customAttributes:nil];

    
    [self.tableView insertSubview:self.refreshControl
                          atIndex:0];
    
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[tableView]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:@{@"tableView": self.tableView}]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[tableView]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:@{@"tableView": self.tableView}]];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = self.playlist.title;
    [self styleRightBarButtonItem];
    
    [self reloadPlaylists];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.tableView.contentInset = [self tableViewContentInset];
    
    // NSNotificationCenter
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    
    [notificationCenter addObserver:self
                           selector:@selector(playerViewControllerWillPlay:)
                               name:PlayerViewControllerWillPlay
                             object:nil];
    
    [notificationCenter addObserver:self
                           selector:@selector(playerViewControllerWillPause:)
                               name:PlayerViewControllerWillPause
                             object:nil];
    
    [notificationCenter addObserver:self
                           selector:@selector(playerViewControllerWillStop:)
                               name:PlayerViewControllerWillStop
                             object:nil];
    
    [notificationCenter addObserver:self
                           selector:@selector(playerViewControllerDidChangeCurrentTrack:)
                               name:PlayerViewControllerDidChangeCurrentTrack
                             object:nil];
    
    [notificationCenter addObserver:self
                           selector:@selector(playerViewControllerShouldHandleError:)
                               name:PlayerViewControllerShouldHandleError
                             object:nil];
    
    [notificationCenter addObserver:self
                           selector:@selector(downloadManagerProcessDidEnd:)
                               name:DownloadManagerProcessDidEnd
                             object:nil];
//    [notificationCenter addObserver:self
//                           selector:@selector(reloadTracks:)
//                               name:FavoriteManagerUpdateTracksNotification
//                             object:nil];
    
    
    [self.tableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    // NSNotificationCenter
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    
    [notificationCenter removeObserver:self
                                  name:PlayerViewControllerWillPlay
                                object:nil];
    
    [notificationCenter removeObserver:self
                                  name:PlayerViewControllerWillPause
                                object:nil];
    
    [notificationCenter removeObserver:self
                                  name:PlayerViewControllerWillStop
                                object:nil];
    
    [notificationCenter removeObserver:self
                                  name:PlayerViewControllerDidChangeCurrentTrack
                                object:nil];
    
    [notificationCenter removeObserver:self
                                  name:PlayerViewControllerShouldHandleError
                                object:nil];
    
    [notificationCenter removeObserver:self
                                  name:DownloadManagerProcessDidEnd
                                object:nil];
    
    [super viewWillDisappear:animated];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - NSNotificationCenter

- (void)playerViewControllerWillPlay:(NSNotification *)notification {
    self.tableView.contentInset = [self tableViewContentInset];
    [self.tableView reloadData];
}

- (void)playerViewControllerWillPause:(NSNotification *)notification {
    [self.tableView reloadData];
}

- (void)playerViewControllerWillStop:(NSNotification *)notification {
    self.tableView.contentInset = [self tableViewContentInset];
    [self.tableView reloadData];
}

- (void)playerViewControllerDidChangeCurrentTrack:(NSNotification *)notification {
    [self.tableView reloadData];
}

- (void)playerViewControllerShouldHandleError:(NSNotification *)notification {
    [self.tableView reloadData];
    
    NSError *error = notification.userInfo[PlayerViewControllerErrorKey];
    [ErrorHandler handleError:error];
}

- (void)downloadManagerProcessDidEnd:(NSNotification *)notification {
    NSError *error = notification.userInfo[DownloadManagerErrorKey];
    
    if (error == nil) {
        [self reloadPlaylists];
    }
}

- (void)reloadTracks:(NSNotification *)notification
{
    [self reloadPlaylists];
}

#pragma mark - NSMutableArray

- (NSMutableArray *)tracks {
    if (_tracks == nil) {
        _tracks = [NSMutableArray new];
    }
    
    return _tracks;
}

#pragma mark - UINavigationBar

- (void)rightBarButtonItemTapped:(id)sender {
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    if (appDelegate.playerViewController.isRunning) {
        [appDelegate.playerViewController stop];
    }
    
    self.editing = !self.editing;
    [self styleRightBarButtonItem];
    self.tableView.allowsSelection = !self.editing;
    self.tableView.editing = self.editing;
    
    NSArray *indexPaths = [self.tableView indexPathsForVisibleRows];
    
    for (NSIndexPath *indexPath in indexPaths) {
        TracksTableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        cell.editing = self.editing;
        [cell styleAnimated:YES];
    }
}


- (void)styleRightBarButtonItem {
    if (self.editing) {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"done", nil)
                                                                                  style:UIBarButtonItemStyleDone
                                                                                 target:self
                                                                                 action:@selector(rightBarButtonItemTapped:)];
    } else {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"edit", nil)
                                                                                  style:UIBarButtonItemStylePlain
                                                                                 target:self
                                                                                 action:@selector(rightBarButtonItemTapped:)];
    }
}

#pragma mark - UIRefreshControl

- (UIRefreshControl *)refreshControl {
    if (_refreshControl == nil) {
        _refreshControl = [[UIRefreshControl alloc] init];
        _refreshControl.backgroundColor = [UIColor whiteColor];
        [_refreshControl addTarget:self
                            action:@selector(refresh)
                  forControlEvents:UIControlEventValueChanged];
    }
    
    return _refreshControl;
}

- (void)refresh {
    [self reloadPlaylists];
}

#pragma mark - UITableView

- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero
                                                  style:UITableViewStylePlain];
        _tableView.translatesAutoresizingMaskIntoConstraints = NO;
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        _tableView.sectionIndexColor = [[BaseTheme currentTheme] tintColor];
        _tableView.sectionIndexBackgroundColor = [UIColor clearColor];
        _tableView.allowsSelection = !self.editing;
        _tableView.dataSource = self;
        _tableView.delegate = self;
        [_tableView registerClass:[TracksTableViewCell class]
           forCellReuseIdentifier:self.cellReuseIdentifier];
    }
    
    return _tableView;
}


- (UIEdgeInsets)tableViewContentInset {
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    return [appDelegate contentInsetForScrollView:self.tableView];
}

- (void)deselectSelectedRow {
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    
    if (indexPath) {
        [self.tableView deselectRowAtIndexPath:indexPath
                                      animated:YES];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.tracks count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Track *track = self.tracks[indexPath.row];
    
    TracksTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:self.cellReuseIdentifier
                                                                forIndexPath:indexPath];
    cell.editing = self.editing;
    cell.titleLabel.text = track.title;
    cell.artistNameLabel.text = track.artist.name;
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    PlayerViewController *playerViewController = appDelegate.playerViewController;
    
    if (playerViewController.isRunning && [track.uid isEqual:playerViewController.currentTrack.uid]) {
        cell.iconImageView.image = [UIImage imageNamed:@"ic_pause_green.png"];
    } else {
        cell.iconImageView.image = [UIImage imageNamed:@"ic_play_green.png"];
    }
    
    [cell.deleteButton addTarget:self
                          action:@selector(deleteButtonTapped:)
                forControlEvents:UIControlEventTouchUpInside];
    [cell.deleteButton setImage:[UIImage imageNamed:@"ic_delete.png"]
                       forState:UIControlStateNormal];
    
    [cell styleAnimated:NO];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Track *track = self.tracks[indexPath.row];
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    if (appDelegate.playerViewController) {
        if ([track.album.uid isEqual:appDelegate.playerViewController.currentTrack.album.uid]) {
            if (appDelegate.playerViewController.isRunning) {
                if ([track.uid isEqual:appDelegate.playerViewController.currentTrack.uid]) {
                    [appDelegate.playerViewController pause];
                } else {
                    [appDelegate.playerViewController pause];
                    appDelegate.playerViewController.currentTrack = track;
                    [appDelegate.playerViewController play];
                }
            } else {
                if ([track.uid isEqual:appDelegate.playerViewController.currentTrack.uid]) {
                    [appDelegate.playerViewController play];
                } else {
                    appDelegate.playerViewController.currentTrack = track;
                    [appDelegate.playerViewController play];
                }
            }
        } else {
            [appDelegate.playerViewController pause];
            appDelegate.playerViewController = [[PlayerViewController alloc] initWithTracks:self.tracks
                                                                               currentTrack:track];
            [appDelegate.playerViewController play];
        }
    } else {
        appDelegate.playerViewController = [[PlayerViewController alloc] initWithTracks:self.tracks
                                                                           currentTrack:track];
        [appDelegate.playerViewController play];
    }
    
    [self.tableView reloadData];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewCellEditingStyleNone;
}

- (BOOL)tableView:(UITableView *)tableview shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}


- (BOOL)tableView:(UITableView *)tableview canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}


//- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath
//{
//    NSString *stringToMove = [self.tracks objectAtIndex:sourceIndexPath.row];
//    [self.tracks removeObjectAtIndex:sourceIndexPath.row];
//    [self.tracks insertObject:stringToMove atIndex:destinationIndexPath.row];
//
//}


#pragma mark - UIAlertController

- (void)showAlertControllerForIndexPath:(nonnull NSIndexPath *)indexPath {
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"cancel", nil)
                                                           style:UIAlertActionStyleCancel
                                                         handler:nil];
    UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"delete", nil)
                                                           style:UIAlertActionStyleDestructive
                                                         handler:^(UIAlertAction *action) {
                                                             [self deleteTrackAtIndexPath:indexPath];
                                                         }];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"track", nil)
                                                                             message:NSLocalizedString(@"track.delete_message", nil)
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:cancelAction];
    [alertController addAction:deleteAction];
    [self presentViewController:alertController
                       animated:YES
                     completion:nil];
}

- (void)showAlertController:(NSString*) text {
    UIAlertAction *doneAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"done", nil)
                                                         style:UIAlertActionStyleDefault
                                                       handler:nil];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"track", nil)
                                                                             message:text
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:doneAction];
    [self presentViewController:alertController
                       animated:YES
                     completion:nil];
}


#pragma mark - UIButton

- (void)deleteButtonTapped:(UIButton *)button {
    CGPoint position = [button convertPoint:CGPointZero
                                     toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:position];
    [self showAlertControllerForIndexPath:indexPath];
}

#pragma mark - SZKMessageView

- (SZKMessageView *)messageView {
    if (_messageView == nil) {
        _messageView = [[SZKMessageView alloc] init];
        _messageView.delegate = self;
    }
    
    return _messageView;
}

- (void)messageView:(SZKMessageView *)messageView didTapActionButton:(UIButton *)actionButton {
    [self reloadPlaylists];
}

#pragma mark - UIActivityIndicatorView

- (SZKProgressView *)progressView {
    if (_progressView == nil) {
        _progressView = [[SZKProgressView alloc] init];
    }
    
    return _progressView;
}

#pragma mark - Loading indicator

- (void)showLoadingIndicator {
    if (self.tracks.count == 0) {
        [self.messageView hide];
        [self.progressView showInView:self.view];
    }
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)hideLoadingIndicator {
    [self.refreshControl endRefreshing];
    [self.progressView hide];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

#pragma mark - CoreData

- (NSManagedObjectContext *)mainManagedObjectContext {
    if (_mainManagedObjectContext == nil) {
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        _mainManagedObjectContext = appDelegate.mainManagedObjectContext;
    }
    
    return _mainManagedObjectContext;
}

- (NSManagedObjectContext *)workerManagedObjectContext {
    if (_workerManagedObjectContext == nil) {
        _workerManagedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        _workerManagedObjectContext.parentContext = self.mainManagedObjectContext;
    }
    
    return _workerManagedObjectContext;
}

- (TrackRepository *)trackRepository {
    if (_trackRepository == nil) {
        _trackRepository = [[TrackRepository alloc] initWithManagedObjectContext:self.workerManagedObjectContext];
    }
    
    return _trackRepository;
}

- (TrackManager *)trackManager {
    if (_trackManager == nil) {
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        _trackManager = appDelegate.trackManager;
    }
    
    return _trackManager;
}

-(void)loadPlaylist
{
    [self showLoadingIndicator];
    
    PlaylistManager *pManager = [PlaylistManager sharedManager];
    WEAKIFY_SELF;
    [pManager getAllTracksInPlaylist:self.playlist complete:^(BOOL success, NSArray * _Nullable result) {
        STRONGIFY_SELF;
        if (success) {
            [self hideLoadingIndicator];
            [self workWithPlaylist:result];
        }
        else
        {
            [self hideLoadingIndicator];
            self.tableView.hidden = YES;
            self.tableView.contentOffset = CGPointMake(0.0f, 0.0f - self.tableView.contentInset.top);
            [self.tableView reloadData];
            
            [self.messageView showInView:self.view
                               iconImage:nil
                                   title:nil
                                 message:NSLocalizedString(@"no_data", nil)
                       actionButtonTitle:NSLocalizedString(@"reload", nil)];
            
        }
    }];
    
}


-(void)workWithPlaylist:(NSArray*)playlistTracks
{
    [self.tracks removeAllObjects];
    NSInteger totalResult = playlistTracks.count;
    
    if (totalResult > 0) {
        
        [self.tracks addObjectsFromArray:playlistTracks];
        [self hideLoadingIndicator];
        
        self.tableView.hidden = NO;
        self.tableView.contentOffset = CGPointMake(0.0f, 0.0f - self.tableView.contentInset.top);
        [self.tableView reloadData];
    } else {
        [self hideLoadingIndicator];
    
        self.tableView.hidden = YES;
        self.tableView.contentOffset = CGPointMake(0.0f, 0.0f - self.tableView.contentInset.top);
        [self.tableView reloadData];
        
        [self.messageView showInView:self.view
                           iconImage:nil
                               title:nil
                             message:NSLocalizedString(@"no_data", nil)
                   actionButtonTitle:NSLocalizedString(@"reload", nil)];
    }
}

- (void)reloadPlaylists {
    [self loadPlaylist];
}

- (void)deleteTrackAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([User currentUser].accessToken == nil)
    {
        [[User currentUser] showLoginForm];
        return;
    }
    else
    {
        Track *track = self.tracks[indexPath.row];
        WEAKIFY_SELF;
        PlaylistManager *pManager = [PlaylistManager sharedManager];
        [pManager deleteTrackFromPlaylist:track playlist:self.playlist complete:^(BOOL success) {
            STRONGIFY_SELF;
            if (success) {
                [self reloadPlaylists];
            }
            else
            {
                [self showAlertController:NSLocalizedString(@"error.unexpected", nil)];
            }
        }];
    }
}



@end
