//
//  PlaylistsViewController.swift
//  Muzone
//
//  Created by Sukhrob Khakimov on 10/18/17.
//  Copyright © 2017 Muzone LLC. All rights reserved.
//

import CoreData
import UIKit

class PlaylistsViewController: BaseViewController {
    enum PlaylistType: String {
        case personal
        case following

        var title: String {
            switch self {
            case .personal: return "your_playlists".localized
            case .following: return "following_playlists".localized
            }
        }

        static func at(index: Int) -> PlaylistType? {
            let types = all()

            if index < types.count {
                return types[index]
            }

            return nil
        }

        static func all() -> [PlaylistType] {
            return [personal, following]
        }
    }

    fileprivate let numberOfPages: Int
    fileprivate let scrollableMenuViewHeight = CGFloat(44.0)
    fileprivate let tableViewRowHeight = CGFloat(75.0)
    fileprivate let cellReuseIdentifier = String(describing: PlaylistTableViewCell.self)
    fileprivate let addButtonSize = CGSize(width: 56.0, height: 56.0)

    fileprivate var allowPageSelection = true

    lazy fileprivate var allPlaylists: [[Playlist]] = []
    lazy fileprivate var tableViews: [UITableView] = []
    lazy fileprivate var refreshControls: [UIRefreshControl] = []
    lazy fileprivate var activityIndicatorViews: [UIActivityIndicatorView] = []
    lazy fileprivate var emptyStateViews: [EmptyStateView] = []

    lazy fileprivate var actionPopupItems: [[String: String]] = [
        ["icon": "ic_popup_play.png", "title": "play_all".localized],
        ["icon": "ic_popup_star.png", "title": "follow".localized + "/" + "unfollow".localized],
        ["icon": "ic_popup_add.png", "title": "add_to_queue".localized],
        ["icon": "ic_popup_download.png", "title": "save_all".localized],
        ["icon": "ic_popup_share.png", "title": "share".localized]
    ]

    lazy fileprivate var temporaryContext: NSManagedObjectContext = {
        let appDelegate: AppDelegate =  UIApplication.shared.delegate as! AppDelegate
        var temporaryContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        temporaryContext.parent = appDelegate.masterManagedObjectContext

        return temporaryContext
    }()

    lazy fileprivate var playlistManager = PlaylistManager(client: ApiClient.shared())

    lazy fileprivate var scrollableMenuView: ScrollableMenuView = {
        var scrollableMenuView = ScrollableMenuView()
        scrollableMenuView.translatesAutoresizingMaskIntoConstraints = false
        scrollableMenuView.backgroundColor = BaseTheme.currentTheme().tintColor()
        scrollableMenuView.selectionIndicatorViewBackgroundColor = .white
        scrollableMenuView.dataSource = self
        scrollableMenuView.delegate = self

        return scrollableMenuView
    }()

    lazy fileprivate var contentScrollView: UIScrollView = {
        var contentScrollView = UIScrollView()
        contentScrollView.backgroundColor = .white
        contentScrollView.showsVerticalScrollIndicator = false
        contentScrollView.showsHorizontalScrollIndicator = false
        contentScrollView.bounces = false
        contentScrollView.isPagingEnabled = true
        contentScrollView.delegate = self

        return contentScrollView
    }()

    lazy fileprivate var addButtonContainerView: UIView = {
        var addButtonContainerView = UIButton(type: .custom)
        addButtonContainerView.translatesAutoresizingMaskIntoConstraints = false
        addButtonContainerView.layer.shadowColor = UIColor.black.cgColor
        addButtonContainerView.layer.shadowOpacity = 0.6
        addButtonContainerView.layer.shadowRadius = 4.0
        addButtonContainerView.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)

        return addButtonContainerView
    }()

    lazy fileprivate var addButton: UIButton = {
        var addButton = UIButton(type: .custom)
        addButton.translatesAutoresizingMaskIntoConstraints = false
        addButton.setBackgroundImage(
            UIImage(color: .rgb(red: 192.0, green: 202.0, blue: 51.0)),
            for: .normal
        )
        addButton.setBackgroundImage(
            UIImage(color: .rgb(red: 192.0, green: 202.0, blue: 51.0, alpha: 0.5)),
            for: .highlighted
        )
        addButton.setImage(
            UIImage(named: "ic_add.png")?.withRenderingMode(.alwaysTemplate),
            for: .normal
        )
        addButton.addTarget(
            self,
            action: #selector(touchUpInside(addButton:)),
            for: .touchUpInside
        )
        addButton.imageView?.tintColor = .white
        addButton.layer.cornerRadius = self.addButtonSize.width / 2
        addButton.clipsToBounds = true

        return addButton
    }()

    init() {
        numberOfPages = PlaylistType.all().count

        super.init(nibName: nil, bundle: nil)

        for _ in 0..<numberOfPages {
            let playlists: [Playlist] = []
            allPlaylists.append(playlists)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func loadView() {
        super.loadView()

        var bindings: [String: AnyObject] = [
            "topLayoutGuide": topLayoutGuide,
            "scrollableMenuView": scrollableMenuView,
            "contentScrollView": contentScrollView,
            "addButtonContainerView": addButtonContainerView,
            "addButton": addButton
        ]

        view.addSubview(scrollableMenuView)
        view.addSubview(contentScrollView)
        view.addSubview(addButtonContainerView)
        addButtonContainerView.addSubview(addButton)

        populateContentOfScrollView()

        view.addConstraints(
            NSLayoutConstraint.constraints(
                withVisualFormat: "H:|[scrollableMenuView]|",
                options: NSLayoutFormatOptions(rawValue: 0),
                metrics: nil,
                views: bindings
            )
        )

        view.addConstraints(
            NSLayoutConstraint.constraints(
                withVisualFormat: "H:|[contentScrollView]|",
                options: NSLayoutFormatOptions(rawValue: 0),
                metrics: nil,
                views: bindings
            )
        )

        view.addConstraints(
            NSLayoutConstraint.constraints(
                withVisualFormat: "V:[topLayoutGuide][scrollableMenuView(height)][contentScrollView]|",
                options: NSLayoutFormatOptions(rawValue: 0),
                metrics: ["height": scrollableMenuViewHeight],
                views: bindings
            )
        )

        var visualFormatForTableViewsInsideScrollView = "H:|"

        for (index, tableView) in tableViews.enumerated() {
            let tableViewName = "tableView" + "\(index)"
            visualFormatForTableViewsInsideScrollView.append("[" + tableViewName + "(width)]")
            bindings[tableViewName] = tableView
        }

        visualFormatForTableViewsInsideScrollView.append("|")

        contentScrollView.addConstraints(
            NSLayoutConstraint.constraints(
                withVisualFormat: visualFormatForTableViewsInsideScrollView,
                options: NSLayoutFormatOptions(rawValue: 0),
                metrics: ["width": view.bounds.size.width],
                views: bindings
            )
        )

        for (index, tableView) in tableViews.enumerated() {
            let tableViewName = "tableView" + "\(index)"

            contentScrollView.addConstraints(
                NSLayoutConstraint.constraints(
                    withVisualFormat: "V:|[" + tableViewName + "]|",
                    options: NSLayoutFormatOptions(rawValue: 0),
                    metrics: nil,
                    views: bindings
                )
            )

            view.addConstraint(
                NSLayoutConstraint(
                    item: tableView,
                    attribute: .height,
                    relatedBy: .equal,
                    toItem: contentScrollView,
                    attribute: .height,
                    multiplier: 1.0,
                    constant: 0.0
                )
            )

            let activityIndicatorView = activityIndicatorViews[index]

            contentScrollView.addConstraint(
                NSLayoutConstraint(
                    item: activityIndicatorView,
                    attribute: .centerX,
                    relatedBy: .equal,
                    toItem: tableView,
                    attribute: .centerX,
                    multiplier: 1.0,
                    constant: 0.0
                )
            )

            contentScrollView.addConstraint(
                NSLayoutConstraint(
                    item: activityIndicatorView,
                    attribute: .centerY,
                    relatedBy: .equal,
                    toItem: tableView,
                    attribute: .centerY,
                    multiplier: 1.0,
                    constant: 0.0
                )
            )
        }

        view.addConstraints(
            NSLayoutConstraint.constraints(
                withVisualFormat: "H:[addButtonContainerView(width)]-space-|",
                options: NSLayoutFormatOptions(rawValue: 0),
                metrics: [
                    "space": 30.0,
                    "width": addButtonSize.width
                ],
                views: bindings
            )
        )

        view.addConstraints(
            NSLayoutConstraint.constraints(
                withVisualFormat: "V:[addButtonContainerView(height)]-space-|",
                options: NSLayoutFormatOptions(rawValue: 0),
                metrics: [
                    "space": 30.0,
                    "height": addButtonSize.height
                ],
                views: bindings
            )
        )

        addButtonContainerView.addConstraints(
            NSLayoutConstraint.constraints(
                withVisualFormat: "H:|[addButton]|",
                options: NSLayoutFormatOptions(rawValue: 0),
                metrics: nil,
                views: bindings
            )
        )

        addButtonContainerView.addConstraints(
            NSLayoutConstraint.constraints(
                withVisualFormat: "V:|[addButton]|",
                options: NSLayoutFormatOptions(rawValue: 0),
                metrics: nil,
                views: bindings
            )
        )
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        automaticallyAdjustsScrollViewInsets = false

        loadAllPlaylists()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: -

    func populateContentOfScrollView() { // FIXME: refactor
        for _ in 0..<numberOfPages {
            let tableView = createTableView()
            let refreshControl = createRefreshControl()
            let emptyStateView = createEmptyStateView()
            let activityIndicatorView = createActivityIndicatorView()

            tableViews.append(tableView)
            refreshControls.append(refreshControl)
            emptyStateViews.append(emptyStateView)
            activityIndicatorViews.append(activityIndicatorView)

            contentScrollView.addSubview(tableView)
            contentScrollView.addSubview(activityIndicatorView)

            tableView.translatesAutoresizingMaskIntoConstraints = false
            refreshControl.translatesAutoresizingMaskIntoConstraints = false
            activityIndicatorView.translatesAutoresizingMaskIntoConstraints = false
        }

        contentScrollView.translatesAutoresizingMaskIntoConstraints = false
    }

    func updateState(forPage page: Int) {
        let playlists = self.allPlaylists[page]

        let tableView = self.tableViews[page]
        let refreshControl = self.refreshControls[page]
        let emptyStateView = self.emptyStateViews[page]

        if playlists.count == 0 {
            let message: String
            let actionButtonTitle: String

            if page == 0 {
                message = "no_personal_playlists".localized
                actionButtonTitle = "create_playlist".localized
            } else {
                message = "no_following_playlists"
                actionButtonTitle = "see_public_playlists".localized
            }

            remove(refreshControl: refreshControl)
            emptyStateView.show(
                in: tableView.backgroundView!,
                iconImage: UIImage(named: "ic_music_queue.png"),
                message: message,
                actionTitle: actionButtonTitle
            )
        } else {
            add(refreshControl: refreshControl, to: tableView)
            emptyStateView.hide()
        }
    }

    // MARK: - PlaylistPopupView

    func showPlaylistPopupView(playlist: Playlist, isNew: Bool) {
        let playlistPopupView = PlaylistPopupView()
        playlistPopupView.isNew = isNew
        playlistPopupView.delegate = self
        playlistPopupView.show(playlist: playlist, in: view)
    }

    // MARK: - ActionPopupView

    func showActionPopupView(forView view: UIView) {
        let actionPopupView = ActionPopupView()
        actionPopupView.dataSource = self
        actionPopupView.delegate = self
        actionPopupView.show(in: self.view, for: view)
    }

    // MARK: - UIButton

    func touchUpInside(addButton: UIButton) {
        let playlist = Playlist(title: "", context: temporaryContext)
        showPlaylistPopupView(playlist: playlist, isNew: true)
    }

    func touchUpInside(actionButton: UIButton) {
        showActionPopupView(forView: actionButton)
    }

    // MARK: - UITableView

    func createTableView() -> UITableView {
        let tableView = UITableView(
            frame: .zero,
            style: .plain
        )
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = .white
        tableView.backgroundView = UIView()
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(
            PlaylistTableViewCell.self,
            forCellReuseIdentifier: self.cellReuseIdentifier
        )

        return tableView
    }

    func deselectSelectedRow() {
        for (index, _) in PlaylistType.all().enumerated() {
            let tableView = tableViews[index]

            if let indexPath = tableView.indexPathForSelectedRow {
                tableView.deselectRow(at: indexPath, animated: true)
            }
        }
    }

    // MARK: - UIRefreshControl

    func createRefreshControl() -> UIRefreshControl {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(
            self, action:
            #selector(valueChanged(refreshControl:)),
            for: .valueChanged
        )

        return refreshControl
    }

    func add(refreshControl: UIRefreshControl, to tableView: UITableView) {
        if !tableView.subviews.contains(refreshControl) {
            tableView.insertSubview(refreshControl, at: 0)
        }
    }

    func remove(refreshControl: UIRefreshControl) {
        refreshControl.removeFromSuperview()
    }

    func valueChanged(refreshControl: UIRefreshControl) {
        if let index = refreshControls.index(of: refreshControl) {
            let plalistType = PlaylistType.all()[index]
            loadPlaylists(type: plalistType)
        }
    }

    // MARK: - UIActivityIndicatorView

    func createActivityIndicatorView() -> UIActivityIndicatorView {
        return UIActivityIndicatorView(activityIndicatorStyle: .gray)
    }

    // MARK: - EmptyStateView

    func createEmptyStateView() -> EmptyStateView {
        let emptyStateView = EmptyStateView()
        emptyStateView.delegate = self

        return emptyStateView
    }

    // MARK: - Loading indicator

    func showLoadingIndicator(page: Int) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true

        let playlists = allPlaylists[page]

        if playlists.count == 0 {
            let activityIndicatorView = activityIndicatorViews[page]
            activityIndicatorView.startAnimating()
        }
    }

    func hideLoadingIndicator(page: Int) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false

        let activityIndicatorView = activityIndicatorViews[page]
        activityIndicatorView.stopAnimating()

        let refreshControl = refreshControls[page]
        refreshControl.endRefreshing()
    }

    // MARK: - ApiClient

    func loadAllPlaylists() {
        for (_, type) in PlaylistType.all().enumerated() {
            loadPlaylists(type: type)
        }
    }

    func loadPlaylists(type: PlaylistType) {
        guard let page = PlaylistType.all().index(of: type) else {
            return
        }

        showLoadingIndicator(page: page)
        _ = ApiClient.shared().myPlaylists(withParameters: nil, success: { (task, responseObject) in
            self.hideLoadingIndicator(page: page)

            let responseData = responseObject as! [[String: Any]]
            var playlists = self.allPlaylists[page]
            let tableView = self.tableViews[page]

            playlists.removeAll()

            for dictionary in responseData {
                let playlist = Playlist(dictionary: dictionary, context: self.temporaryContext)
                playlists.append(playlist)
            }

            self.allPlaylists[page] = playlists

            self.updateState(forPage: page)
            tableView.reloadData()
        }, failure: { (task, error) in
            self.hideLoadingIndicator(page: page)

            let playlists = self.allPlaylists[page]
            let tableView = self.tableViews[page]
            let emptyStateView = self.emptyStateViews[page]

            if let error = error {
                if playlists.count == 0 {
                    emptyStateView.show(
                        in: tableView.backgroundView!,
                        iconImage: UIImage(named: "ic_music_queue.png"),
                        message: error.localizedDescription,
                        actionTitle: "Reload" // FIXME: localize
                    )
                } else {
//                    ErrorHandler.handle(error: error) // FIXME: handle error
                }
            }
        })
    }
}

// MARK: - ScrollableMenuViewDataSource

extension PlaylistsViewController: ScrollableMenuViewDataSource {
    func numberOfItems(in scrollableMenuView: ScrollableMenuView) -> Int {
        return numberOfPages
    }

    func scrollableMenuView(_ scrollableMenuView: ScrollableMenuView, viewForItemAt index: Int, isSelected: Bool) -> UIView {
        let view = UIView()

        let titleLabel = UILabel()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.font = UIFont(name: "Roboto-Medium", size: 12.0) // FIXME: load from theme
        titleLabel.textAlignment = .center

        if let title = PlaylistType.at(index: index)?.title {
            titleLabel.text = title.uppercased(with: Locale.current)
        }

        if isSelected {
            titleLabel.textColor = .white
        } else {
            titleLabel.textColor = .rgb(value: 255.0, alpha: 0.7)
        }

        let bindings = ["titleLabel": titleLabel]

        view.addSubview(titleLabel)

        view.addConstraints(
            NSLayoutConstraint.constraints(
                withVisualFormat: "H:|-space-[titleLabel]-space-|",
                options: NSLayoutFormatOptions(rawValue: 0),
                metrics: ["space": 10.0],
                views: bindings
            )
        )

        view.addConstraints(
            NSLayoutConstraint.constraints(
                withVisualFormat: "V:|[titleLabel]|",
                options: NSLayoutFormatOptions(rawValue: 0),
                metrics: nil,
                views: bindings
            )
        )

        return view
    }
}

// MARK: - ScrollableMenuViewDelegate

extension PlaylistsViewController: ScrollableMenuViewDelegate {
    func scrollableMenuView(_ scrollableMenuView: ScrollableMenuView, sizeForItemAt index: Int) -> CGSize {
        let width = scrollableMenuView.bounds.size.width / CGFloat(numberOfPages)

        return CGSize(
            width: width,
            height: scrollableMenuViewHeight
        )
    }

    func scrollableMenuView(_ scrollableMenuView: ScrollableMenuView, willSelectItemAt index: Int) {
        allowPageSelection = false
        contentScrollView.setContentOffset(
            CGPoint(x: contentScrollView.bounds.size.width * CGFloat(index), y: 0.0),
            animated: true
        )
    }

    func scrollableMenuView(_ scrollableMenuView: ScrollableMenuView, didSelectItemAt index: Int) {
        allowPageSelection = true
    }
}

// MARK: - UIScrollViewDelegate

extension PlaylistsViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == contentScrollView && allowPageSelection {
            scrollView.contentOffset = CGPoint(x: scrollView.contentOffset.x, y: 0.0) // Disable vertical scrolling
            var page = Int(round(scrollView.contentOffset.x / scrollView.bounds.size.width))

            if page < 0 {
                page = 0
            } else {
                let lastPage = numberOfPages - 1

                if page > lastPage {
                    page = lastPage
                }
            }

            scrollableMenuView.selectItem(
                at: page,
                animated: true,
                completion: nil
            )
        }
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == contentScrollView {
            allowPageSelection = true
        }
    }

    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        if scrollView == contentScrollView {
            allowPageSelection = true
        }
    }
}

// MARK: - UITableViewDataSource

extension PlaylistsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let page = tableViews.index(of: tableView) {
            return allPlaylists[page].count
        }

        return 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(
            withIdentifier: cellReuseIdentifier,
            for: indexPath
        ) as! PlaylistTableViewCell

        cell.actionButton.addTarget(
            self,
            action: #selector(touchUpInside(actionButton:)),
            for: .touchUpInside
        )

        if let page = tableViews.index(of: tableView) {
            let playlist = allPlaylists[page][indexPath.row]
            cell.updateContent(playlist: playlist)
        }

        return cell
    }
}

// MARK: - UITableViewDelegate

extension PlaylistsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableViewRowHeight
    }
}

// MARK: - EmptyStateViewDelegate

extension PlaylistsViewController: EmptyStateViewDelegate {
    func emptyStateView(_ emptyStateView: EmptyStateView, didTap actionButton: UIButton) {
        emptyStateView.hide()

        if let index = emptyStateViews.index(of: emptyStateView) {
            let plalistType = PlaylistType.all()[index]
            loadPlaylists(type: plalistType)
        }
    }
}

// MARK: - PlaylistPopupViewDelegate

extension PlaylistsViewController: PlaylistPopupViewDelegate {
    func playlistPopupViewDidFinish(_ playlistPopupView: PlaylistPopupView) {
        if let playlist = playlistPopupView.playlist {
            if playlistPopupView.isNew {
                playlistManager.createPlaylist(playlist, completionHandler: { (error) in
                    if let error = error {
                        print("error: \(error)")
                        // FIXME: handle error
                    } else {
                        print("create")
                    }
                })
            } else {
                playlistManager.updatePlaylist(playlist, completionHandler: { (error) in
                    if let error = error {
                        print("error: \(error)")
                        // FIXME: handle error
                    } else {
                        print("update")
                    }
                })
            }
        }
    }
}

// MARK: - ActionPopupViewDataSource

extension PlaylistsViewController: ActionPopupViewDataSource {
    func numberOfActionItems(in actionPopupView: ActionPopupView) -> Int {
        return actionPopupItems.count
    }

    func actionPopupView(_ actionPopupView: ActionPopupView, iconImageForActionItemAt index: Int) -> UIImage? {
        let actionPopupItem = actionPopupItems[index]

        if let iconName = actionPopupItem["icon"] {
            return UIImage(named: iconName)
        }

        return nil
    }

    func actionPopupView(_ actionPopupView: ActionPopupView, titleForActionItemAt index: Int) -> String? {
        let actionPopupItem = actionPopupItems[index]

        return actionPopupItem["title"]
    }
}

// MARK: - ActionPopupViewDelegate

extension PlaylistsViewController: ActionPopupViewDelegate {
    func actionPopupView(_ actionPopupView: ActionPopupView, didSelectActionItemAt index: Int) {
        // FIXME: implement
    }
}
