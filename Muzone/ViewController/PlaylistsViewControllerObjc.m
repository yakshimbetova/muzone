//
//  PlaylistsViewControllerObjc.m
//  Muzone
//
//  Created by Sukhrob Khakimov on 5/27/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "SZKProgressView.h"
#import "SZKMessageView.h"
#import "Constants.h"

#import "PlaylistTableViewCellObjc.h"
#import "BaseTheme.h"
#import "AppDelegate.h"

#import "PlaylistManager.h"
#import "PlaylistsViewControllerObjc.h"

#import "TrackRepository.h"
#import "TrackManager.h"

#import "PlaylistTracksViewController.h"

#import "Muzone-Swift.h"


@interface PlaylistsViewControllerObjc ()  <SZKMessageViewDelegate, UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate, UIGestureRecognizerDelegate>
@property (nonatomic, retain) NSMutableArray * playlists;

@property (nonnull, nonatomic, strong) NSManagedObjectContext *mainManagedObjectContext;
@property (nonnull, nonatomic, strong) NSManagedObjectContext *workerManagedObjectContext;

@property (nonnull, nonatomic, strong) TrackRepository *trackRepository;
@property (nonnull, nonatomic, strong) TrackManager *trackManager;


@property (nonnull, nonatomic, strong) SZKProgressView *progressView;
@property (nonnull, nonatomic, strong) SZKMessageView *messageView;

@property (nonnull, nonatomic, strong) UITableView *tableView;
@property (nonnull, nonatomic, strong) UIButton *addPlaylist;

@property (nonnull, nonatomic, strong) NSMutableArray *sectionTitles;
@property (nonnull, nonatomic, strong) NSMutableArray *numberOfRowsInSections;
@property (nonnull, nonatomic, strong) NSMutableArray *lastIndexesInSections;


@end

@implementation PlaylistsViewControllerObjc

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = NSLocalizedString(@"playlists", nil);
    
    [self styleRightBarButtonItem];
    
    [Answers logContentViewWithName:@"Playlists"
                        contentType:@"Menu"
                          contentId:nil
                   customAttributes:nil];
    
    [self reloadData];
}

-(void)loadView
{
    [super loadView];
    
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.addPlaylist];
    
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[tableView]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:@{@"tableView": self.tableView}]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[addPlaylist]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:@{@"addPlaylist": self.addPlaylist}]];
    
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[tableView][addPlaylist(height)]|"
                                                                      options:0
                                                                      metrics:@{@"height": @(44.0)}
                                                                        views:@{@"tableView": self.tableView,
                                                                                @"addPlaylist": self.addPlaylist}]];
    
}

#pragma mark - UINavigationBar

- (void)rightBarButtonItemTapped:(id)sender {
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    if (appDelegate.playerViewController.isRunning) {
        [appDelegate.playerViewController stop];
    }
    
    self.editing = !self.editing;
    [self styleRightBarButtonItem];
    self.tableView.allowsSelection = !self.editing;
    
    NSArray *indexPaths = [self.tableView indexPathsForVisibleRows];
    
    for (NSIndexPath *indexPath in indexPaths) {
        PlaylistTableViewCellObjc *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        cell.editing = self.editing;
        [cell styleAnimated:YES];
    }
}


- (void)styleRightBarButtonItem {
    if (self.editing) {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"done", nil)
                                                                                  style:UIBarButtonItemStyleDone
                                                                                 target:self
                                                                                 action:@selector(rightBarButtonItemTapped:)];
    } else {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"edit", nil)
                                                                                  style:UIBarButtonItemStylePlain
                                                                                 target:self
                                                                                 action:@selector(rightBarButtonItemTapped:)];
    }
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // NSNotificationCenter
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    
    
    [notificationCenter addObserver:self
                           selector:@selector(reloadData)
                               name:PlaylistManagerUpdatePlaylistsNotification
                             object:nil];
}


-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)createPlaylist:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"New playlist", nil)
                                                    message:NSLocalizedString(@"Enter a name for the new playlist", nil)
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                                          otherButtonTitles:NSLocalizedString(@"done", nil), nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex != [alertView cancelButtonIndex]) {
        NSString *name = [alertView textFieldAtIndex:0].text;
        [self createNewPlaylist:name];
    }
}

-(void)createNewPlaylist:(NSString*)name
{
    [self showLoadingIndicator];
    PlaylistManager *pManager = [PlaylistManager sharedManager];
    [pManager createNewPlaylist:name complete:^(BOOL success) {
        [self hideLoadingIndicator];
    }];
}

- (void)reloadData
{
    [self showLoadingIndicator];
    WEAKIFY_SELF;
    PlaylistManager *pManager = [PlaylistManager sharedManager];
    [pManager getAllPlaylists:^(BOOL success, NSArray * _Nullable result) {
        STRONGIFY_SELF;
        if (success)
        {
            [self reloadTable:result];
            [self hideLoadingIndicator];
        }
        else
        {
            [self.tableView reloadData];
            self.tableView.hidden = YES;
            [self hideLoadingIndicator];
            [self.messageView showInView:self.view
                               iconImage:nil
                                   title:nil
                                 message:NSLocalizedString(@"no_data", nil)
                       actionButtonTitle:NSLocalizedString(@"reload", nil)];
        }
        
    }];
}


-(void)reloadTable:(NSArray*)artists
{
    [self.sectionTitles removeAllObjects];
    [self.numberOfRowsInSections removeAllObjects];
    [self.lastIndexesInSections removeAllObjects];
    [self.playlists removeAllObjects];
    
    
    NSArray *data = artists;
    NSInteger numberOfResults = data.count;
    
    if (numberOfResults > 0) {
        NSInteger numberOfRowsInSection = 0;
        
        for (NSInteger index = 0; index < numberOfResults; index++) {
            Playlist *playlist = data[index];
            [self.playlists addObject:playlist];
            
            NSString *sectionTitle = [playlist.title substringToIndex:1];
            
            if (![self.sectionTitles.lastObject isEqual:sectionTitle]) {
                numberOfRowsInSection = 0;
                
                [self.sectionTitles addObject:sectionTitle];
                [self.numberOfRowsInSections addObject:@(numberOfRowsInSection + 1)];
                
                NSInteger lastIndexInSection = [self.lastIndexesInSections.lastObject integerValue];
                [self.lastIndexesInSections addObject:@(lastIndexInSection + numberOfRowsInSection + 1)];
            } else {
                NSInteger numberOfRowsInLastSection = [self.numberOfRowsInSections.lastObject integerValue];
                [self.numberOfRowsInSections replaceObjectAtIndex:self.sectionTitles.count - 1
                                                       withObject:@(numberOfRowsInLastSection + numberOfRowsInSection + 1)];
                
                NSInteger lastIndexInSection = [self.lastIndexesInSections.lastObject integerValue];
                [self.lastIndexesInSections replaceObjectAtIndex:self.lastIndexesInSections.count - 1
                                                      withObject:@(lastIndexInSection + numberOfRowsInSection + 1)];
            }
        }
        self.tableView.hidden = NO;
        
    }
    else
    {
        self.tableView.hidden = YES;
        [self.messageView showInView:self.view
                           iconImage:nil
                               title:nil
                             message:NSLocalizedString(@"no_data", nil)
                   actionButtonTitle:NSLocalizedString(@"reload", nil)];

    
    }
    [self.tableView reloadData];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - NSMutableArray

- (NSMutableArray *)playlists {
    if (_playlists == nil) {
        _playlists = [NSMutableArray new];
    }
    
    return _playlists;
}


#pragma mark - UITableView

- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero
                                                  style:UITableViewStylePlain];
        _tableView.translatesAutoresizingMaskIntoConstraints = NO;
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        [_tableView registerClass:[PlaylistTableViewCellObjc class]
           forCellReuseIdentifier:NSStringFromClass([PlaylistTableViewCellObjc class])];
        _tableView.sectionIndexColor = [[BaseTheme currentTheme] tintColor];
        _tableView.sectionIndexBackgroundColor = [UIColor clearColor];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        
    }
    
    return _tableView;
}

-(UIButton*)addPlaylist {
    if (_addPlaylist == nil) {
        _addPlaylist = [[UIButton alloc] init];
        _addPlaylist.translatesAutoresizingMaskIntoConstraints = NO;
        _addPlaylist.backgroundColor = [[BaseTheme currentTheme] tintColorWithAlpha:0.9f];
        _addPlaylist.titleLabel.font = [UIFont systemFontOfSize:14.0f
                                                  weight:UIFontWeightSemibold];
        _addPlaylist.titleLabel.textColor = [UIColor whiteColor];
        _addPlaylist.titleLabel.textAlignment = NSTextAlignmentCenter;
        [_addPlaylist setTitle:NSLocalizedString(@"Add playlist", nil) forState:UIControlStateNormal];
        [_addPlaylist setTitle:NSLocalizedString(@"Add playlist", nil) forState:UIControlStateHighlighted];
        [_addPlaylist addTarget:self action:@selector(createPlaylist:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _addPlaylist;
}

- (NSMutableArray *)sectionTitles {
    if (_sectionTitles == nil) {
        _sectionTitles = [NSMutableArray new];
    }
    
    return _sectionTitles;
}

- (NSMutableArray *)numberOfRowsInSections {
    if (_numberOfRowsInSections == nil) {
        _numberOfRowsInSections = [NSMutableArray new];
    }
    
    return _numberOfRowsInSections;
}

- (NSMutableArray *)lastIndexesInSections {
    if (_lastIndexesInSections == nil) {
        _lastIndexesInSections = [NSMutableArray new];
    }
    
    return _lastIndexesInSections;
}



- (NSInteger)indexFromIndexPath:(NSIndexPath *)indexPath {
    return [self.lastIndexesInSections[indexPath.section] integerValue] - [self.numberOfRowsInSections[indexPath.section] integerValue] + indexPath.row;
}

#pragma mark - UITableViewDataSource

- (NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return self.sectionTitles;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.sectionTitles.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.numberOfRowsInSections[section] integerValue];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger index = [self indexFromIndexPath:indexPath];
    Playlist *genre = self.playlists[index];
    
    PlaylistTableViewCellObjc *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PlaylistTableViewCellObjc class])
                                                                 forIndexPath:indexPath];
    cell.titleLabel.text = genre.title;
    cell.editing = self.editing;
    
    [cell.deleteButton addTarget:self
                          action:@selector(deleteButtonTapped:)
                forControlEvents:UIControlEventTouchUpInside];
    [cell.deleteButton setImage:[UIImage imageNamed:@"ic_delete"]
                       forState:UIControlStateNormal];
    
    UILongPressGestureRecognizer *lpgr
    = [[UILongPressGestureRecognizer alloc]
       initWithTarget:self action:@selector(editButtonTapped:)];
    lpgr.delegate = self;
    lpgr.delaysTouchesBegan = YES;
    [cell addGestureRecognizer:lpgr];
    
    [cell styleAnimated:NO];

    return cell;
}

- (void)deleteButtonTapped:(UIButton *)button {
    CGPoint position = [button convertPoint:CGPointZero
                                     toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:position];
    [self showAlertControllerForIndexPath:indexPath];
}

- (void)editButtonTapped:(UILongPressGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer.state != UIGestureRecognizerStateEnded) {
        return;
    }
    CGPoint p = [gestureRecognizer locationInView:self.tableView];
    
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:p];
    [self showEditAlertControllerForIndexPath:indexPath];
}



#pragma mark - UITableViewDelegate

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [[BaseTheme currentTheme] sectionBackgroundColor];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    titleLabel.font = [UIFont boldSystemFontOfSize:17.0f];
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.text = self.sectionTitles[section];
    
    [headerView addSubview:titleLabel];
    
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-space-[titleLabel]-space-|"
                                                                       options:0
                                                                       metrics:@{@"space": @(15.0f)}
                                                                         views:@{@"titleLabel": titleLabel}]];
    
    [headerView addConstraint:[NSLayoutConstraint constraintWithItem:titleLabel
                                                           attribute:NSLayoutAttributeCenterY
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:headerView
                                                           attribute:NSLayoutAttributeCenterY
                                                          multiplier:1.0f
                                                            constant:0.0f]];
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 32.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger index = [self indexFromIndexPath:indexPath];
    Playlist *playlist = self.playlists[index];
    
    PlaylistTracksViewController *pv = [[PlaylistTracksViewController alloc] init];
    pv.playlist = playlist;
    
    [self.navigationController pushViewController:pv animated:YES];

}

#pragma mark - UIAlertController

- (void)showAlertControllerForIndexPath:(nonnull NSIndexPath *)indexPath {
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"cancel", nil)
                                                           style:UIAlertActionStyleCancel
                                                         handler:nil];
    UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"delete", nil)
                                                           style:UIAlertActionStyleDestructive
                                                         handler:^(UIAlertAction *action) {
                                                             [self deletePlaylistAtIndexPath:indexPath];
                                                         }];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Playlist", nil)
                                                                             message:NSLocalizedString(@"track.delete_playlist", nil)
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:cancelAction];
    [alertController addAction:deleteAction];
    [self presentViewController:alertController
                       animated:YES
                     completion:nil];
}

- (void)showEditAlertControllerForIndexPath:(nonnull NSIndexPath *)indexPath {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Playlist", nil)
                                                                             message:NSLocalizedString(@"Enter title of playlist", nil)
                                                                      preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"cancel", nil)
                                                           style:UIAlertActionStyleCancel
                                                         handler:nil];
    UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"edit", nil)
                                                           style:UIAlertActionStyleDestructive
                                                         handler:^(UIAlertAction *action) {
                                                             UITextField *textField = alertController.textFields[0];
                                                             [self editPlaylistAtIndexPath:indexPath title:textField.text];
                                                         }];
    
    [alertController addAction:cancelAction];
    [alertController addAction:deleteAction];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = NSLocalizedString(@"Title", nil);
    }];
    [self presentViewController:alertController
                       animated:YES
                     completion:nil];
}


- (void)showAlertController:(NSString*) text {
    UIAlertAction *doneAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"done", nil)
                                                         style:UIAlertActionStyleDefault
                                                       handler:nil];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"track", nil)
                                                                             message:text
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:doneAction];
    [self presentViewController:alertController
                       animated:YES
                     completion:nil];
}


- (void)deletePlaylistAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([User currentUser].accessToken == nil)
    {
        [[User currentUser] showLoginForm];
        return;
    }
    else
    {
        NSInteger index = [self indexFromIndexPath:indexPath];
        Playlist *playlist = self.playlists[index];
        
        WEAKIFY_SELF;
        PlaylistManager *pManager = [PlaylistManager sharedManager];
        [pManager deletePlaylist:playlist complete:^(BOOL success) {
            STRONGIFY_SELF;
            if (success) {
                [self reloadData];
            }
            else
            {
                [self showAlertController:NSLocalizedString(@"error.unexpected", nil)];
            }
        }];
    }
}

- (void)editPlaylistAtIndexPath:(NSIndexPath *)indexPath title:(NSString*)title {
    
    if ([User currentUser].accessToken == nil)
    {
        [[User currentUser] showLoginForm];
        return;
    }
    else
    {
        NSInteger index = [self indexFromIndexPath:indexPath];
        Playlist *playlist = self.playlists[index];
        
        WEAKIFY_SELF;
        PlaylistManager *pManager = [PlaylistManager sharedManager];
        [pManager renamePlaylist:title playlist:playlist complete:^(BOOL success) {
            STRONGIFY_SELF;
            if (success) {
                [self reloadData];
            }
            else
            {
                [self showAlertController:NSLocalizedString(@"error.unexpected", nil)];
            }
        }];
    }
}




#pragma mark - Loading indicator

- (void)showLoadingIndicator {
    if (self.playlists.count == 0) {
        [self.messageView hide];
        [self.progressView showInView:self.view];
    }
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)hideLoadingIndicator {
    [self.progressView hide];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

#pragma mark - CoreData

- (NSManagedObjectContext *)mainManagedObjectContext {
    if (_mainManagedObjectContext == nil) {
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        _mainManagedObjectContext = appDelegate.mainManagedObjectContext;
    }
    
    return _mainManagedObjectContext;
}

- (NSManagedObjectContext *)workerManagedObjectContext {
    if (_workerManagedObjectContext == nil) {
        _workerManagedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        _workerManagedObjectContext.parentContext = self.mainManagedObjectContext;
    }
    
    return _workerManagedObjectContext;
}

- (TrackRepository *)trackRepository {
    if (_trackRepository == nil) {
        _trackRepository = [[TrackRepository alloc] initWithManagedObjectContext:self.workerManagedObjectContext];
    }
    
    return _trackRepository;
}

- (TrackManager *)trackManager {
    if (_trackManager == nil) {
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        _trackManager = appDelegate.trackManager;
    }
    
    return _trackManager;
}


#pragma mark - SZKProgressView

- (SZKProgressView *)progressView {
    if (_progressView == nil) {
        _progressView = [[SZKProgressView alloc] init];
    }
    
    return _progressView;
}

#pragma mark - SZKMessageView

- (SZKMessageView *)messageView {
    if (_messageView == nil) {
        _messageView = [[SZKMessageView alloc] init];
        _messageView.delegate = self;
    }
    
    return _messageView;
}

#pragma mark - SZKMessageViewDelegate

- (void)messageView:(SZKMessageView *)messageView didTapActionButton:(UIButton *)actionButton {
    [self reloadData];
}



@end
