//
//  SearchResultsViewController.h
//  Muzone
//
//  Created by Sukhrob Khakimov on 6/18/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import "BaseViewController.h"

@interface SearchResultsViewController : BaseViewController <UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate>

@end
