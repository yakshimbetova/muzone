//
//  SearchResultsViewController.m
//  Muzone
//
//  Created by Sukhrob Khakimov on 6/18/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <SDWebImage/UIImageView+WebCache.h>

#import "AppDelegate.h"
#import "BaseTheme.h"
#import "ApiClient.h"
#import "ErrorHandler.h"
#import "SearchHistoryRepository.h"

#import "SearchHistory.h"
#import "Album.h"
#import "Artist.h"
#import "Track.h"

#import "BaseNavigationController.h"
#import "SearchResultsViewController.h"
#import "AlbumsViewController.h"
#import "AlbumViewController.h"

#import "SZKProgressView.h"
#import "SZKMessageView.h"

#import "SearchHistoriesTableViewCell.h"
#import "SearchResultsTableViewCell.h"

static CGFloat const topViewHeight = 44.0f;
static CGFloat const historiesTableViewRowHeight = 44.0f;

@interface SearchResultsViewController () <SZKMessageViewDelegate>

@property (nonnull, nonatomic, strong) NSURLSessionDataTask *loadingTask;
@property (nonnull, nonatomic, strong) NSManagedObjectContext *mainManagedObjectContext;
@property (nonnull, nonatomic, strong) NSManagedObjectContext *workerManagedObjectContext;
@property (nonnull, nonatomic, strong) SearchHistoryRepository *searchHistoryRepository;

@property (nonatomic, assign) BOOL allowLoading;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, assign) CGSize keyboardSize;
@property (nonatomic, assign) CGFloat margin;
@property (nonnull, nonatomic, strong) NSMutableArray *searchHistories;
@property (nonnull, nonatomic, strong) NSMutableArray *sections;
@property (nonnull, nonatomic, strong) NSMutableArray *albums;
@property (nonnull, nonatomic, strong) NSMutableArray *artists;
@property (nonnull, nonatomic, strong) NSMutableArray *tracks;

@property (nonnull, nonatomic, strong) SZKProgressView *progressView;
@property (nonnull, nonatomic, strong) SZKMessageView *messageView;
@property (nonnull, nonatomic, strong) UIView *titleView;
@property (nonnull, nonatomic, strong) UIButton *closeButton;
@property (nonnull, nonatomic, strong) UITextField *searchTextField;
@property (nonnull, nonatomic, strong) UITableView *historiesTableView;
@property (nonnull, nonatomic, strong) UITableView *resultsTableView;
@property (nonnull, nonatomic, strong) NSLayoutConstraint *historiesTableViewLeadingSpaceConstraint;
@property (nonnull, nonatomic, strong) NSLayoutConstraint *historiesTableViewTrailingSpaceConstraint;
@property (nonnull, nonatomic, strong) NSLayoutConstraint *historiesTableViewHeightConstraint;

@end

@implementation SearchResultsViewController

- (instancetype)init {
    self = [super init];
    
    if (self) {
        self.page = 1;
        self.margin = 0.0f;
    }
    
    return self;
}

- (void)loadView {
    [super loadView];
    
    // Navigation bar
    self.navigationItem.titleView = self.titleView;
    
    [self.view addSubview:self.historiesTableView];
    [self.view addSubview:self.resultsTableView];
    [self.titleView addSubview:self.closeButton];
    [self.titleView addSubview:self.searchTextField];
    
    self.historiesTableViewLeadingSpaceConstraint = [NSLayoutConstraint constraintWithItem:self.historiesTableView
                                                                                 attribute:NSLayoutAttributeLeading
                                                                                 relatedBy:NSLayoutRelationEqual
                                                                                    toItem:self.view
                                                                                 attribute:NSLayoutAttributeLeading
                                                                                multiplier:1.0f
                                                                                  constant:self.margin];
    [self.view addConstraint:self.historiesTableViewLeadingSpaceConstraint];
    
    self.historiesTableViewTrailingSpaceConstraint = [NSLayoutConstraint constraintWithItem:self.view
                                                                                  attribute:NSLayoutAttributeTrailing
                                                                                  relatedBy:NSLayoutRelationEqual
                                                                                     toItem:self.historiesTableView
                                                                                  attribute:NSLayoutAttributeTrailing
                                                                                 multiplier:1.0f
                                                                                   constant:self.margin];
    [self.view addConstraint:self.historiesTableViewTrailingSpaceConstraint];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.historiesTableView
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.resultsTableView
                                                          attribute:NSLayoutAttributeTop
                                                         multiplier:1.0f
                                                           constant:-2.0f]];
    
    self.historiesTableViewHeightConstraint = [NSLayoutConstraint constraintWithItem:self.historiesTableView
                                                                           attribute:NSLayoutAttributeHeight
                                                                           relatedBy:NSLayoutRelationEqual
                                                                              toItem:nil
                                                                           attribute:NSLayoutAttributeNotAnAttribute
                                                                          multiplier:1.0f
                                                                            constant:0.0f];
    [self.historiesTableView addConstraint:self.historiesTableViewHeightConstraint];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[resultsTableView]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:@{@"resultsTableView": self.resultsTableView}]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[resultsTableView]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:@{@"resultsTableView": self.resultsTableView}]];
    
    [self.titleView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[closeButton(width)][searchTextField]|"
                                                                           options:0
                                                                           metrics:@{@"width": @(topViewHeight)}
                                                                             views:@{@"closeButton": self.closeButton,
                                                                                     @"searchTextField": self.searchTextField}]];
    
    [self.titleView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[closeButton]|"
                                                                           options:0
                                                                           metrics:nil
                                                                             views:@{@"closeButton": self.closeButton}]];
    
    [self.titleView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[searchTextField]|"
                                                                           options:0
                                                                           metrics:nil
                                                                             views:@{@"searchTextField": self.searchTextField}]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // View
    self.view.backgroundColor = [UIColor colorWithRed:0.0f / 255
                                                green:0.0f / 255
                                                 blue:0.0f / 255
                                                alpha:0.4f];
    
    [Answers logContentViewWithName:@"Search"
                        contentType:nil
                          contentId:nil
                   customAttributes:nil];
    
    [self loadSearchHistoriesWithKey:nil
                              offset:0];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    self.margin = (self.view.bounds.size.width - self.titleView.bounds.size.width) / 2;
    self.historiesTableViewLeadingSpaceConstraint.constant = self.margin;
    self.historiesTableViewTrailingSpaceConstraint.constant = self.margin;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.resultsTableView.contentInset = [self resultsTableViewContentInset];
    
    // NSNotificationCenter
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    
    [notificationCenter addObserver:self
                           selector:@selector(playerViewControllerWillPlay:)
                               name:PlayerViewControllerWillPlay
                             object:nil];
    
    [notificationCenter addObserver:self
                           selector:@selector(playerViewControllerWillStop:)
                               name:PlayerViewControllerWillStop
                             object:nil];
    
    if (self.sections.count > 0) {
        ((BaseNavigationController *)self.navigationController).statusBarStyle = UIStatusBarStyleDefault;
        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"bg_nav_white.png"]
                                                      forBarMetrics:UIBarMetricsDefault];
    }
    
    [self registerForKeyboardNotifications];
    [self deselectSelectedRow];
}

- (void)viewWillDisappear:(BOOL)animated {
    // NSNotificationCenter
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    
    [notificationCenter removeObserver:self
                                  name:PlayerViewControllerWillPlay
                                object:nil];
    
    [notificationCenter removeObserver:self
                                  name:PlayerViewControllerWillStop
                                object:nil];
    
    [self.loadingTask cancel];
    [self dismissKeyboard];
    [self unregisterFromKeyboardNotifications];
    
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - NSMutableArray

- (NSMutableArray *)searchHistories {
    if (_searchHistories == nil) {
        _searchHistories = [NSMutableArray new];
    }
    
    return _searchHistories;
}

- (NSArray *)sections {
    if (_sections == nil) {
        _sections = [NSMutableArray new];
    }
    
    return _sections;
}

- (NSMutableArray *)albums {
    if (_albums == nil) {
        _albums = [NSMutableArray new];
    }
    
    return _albums;
}

- (NSMutableArray *)artists {
    if (_artists == nil) {
        _artists = [NSMutableArray new];
    }
    
    return _artists;
}

- (NSMutableArray *)tracks {
    if (_tracks == nil) {
        _tracks = [NSMutableArray new];
    }
    
    return _tracks;
}

#pragma mark - UIKeyboard

- (void)registerForKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)unregisterFromKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    NSDictionary *userInfo = [notification userInfo];
    self.keyboardSize = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    CGFloat height = [self historiesTableViewHeight];
    
    self.historiesTableViewHeightConstraint.constant = height;
    [self.historiesTableView setNeedsUpdateConstraints];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:[userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue]];
    [UIView setAnimationCurve:[userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue]];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [self.historiesTableView layoutIfNeeded];
    [UIView commitAnimations];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    NSDictionary *userInfo = [notification userInfo];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:[userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue]];
    [UIView setAnimationCurve:[userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue]];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView commitAnimations];
}

- (void)dismissKeyboard {
    [self.view endEditing:YES];
}

#pragma mark - NSNotificationCenter

- (void)playerViewControllerWillPlay:(NSNotification *)notification {
    self.resultsTableView.contentInset = [self resultsTableViewContentInset];
}

- (void)playerViewControllerWillStop:(NSNotification *)notification {
    self.resultsTableView.contentInset = [self resultsTableViewContentInset];
}

#pragma mark - UIView

- (UIView *)titleView {
    if (_titleView == nil) {
        _titleView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.view.bounds.size.width, 44.0f)];
        _titleView.backgroundColor = [UIColor whiteColor];
        _titleView.layer.borderWidth = 1.0f;
        _titleView.layer.borderColor = [[BaseTheme currentTheme] separatorColor].CGColor;
        _titleView.layer.cornerRadius = 2.0f;
    }
    
    return _titleView;
}

#pragma mark - UIButton

- (UIButton *)closeButton {
    if (_closeButton == nil) {
        _closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _closeButton.translatesAutoresizingMaskIntoConstraints = NO;
        _closeButton.tintColor = [UIColor darkGrayColor];
        [_closeButton setImage:[[UIImage imageNamed:@"ic_back.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]
                      forState:UIControlStateNormal];
        [_closeButton addTarget:self
                         action:@selector(closeButtonTapped:)
               forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _closeButton;
}

- (void)closeButtonTapped:(id)sender {
    [self dismissViewControllerAnimated:YES
                             completion:nil];
}

- (void)moreButtonTapped:(UIButton *)button {
    if (self.allowLoading) {
        NSString *type;
        NSInteger section = button.tag;
        
        if (section == 0) {
            if (self.albums.count > 0) {
                type = @"albums";
            } else if (self.artists.count > 0) {
                type = @"artists";
            } else {
                type = @"tracks";
            }
        } else if (section == 1) {
            if (self.artists.count > 0) {
                type = @"artists";
            } else {
                type = @"tracks";
            }
        } else {
            type = @"tracks";
        }
        
        [self.sections removeAllObjects];
        
        if ([type isEqualToString:@"albums"]) {
            [self.sections addObject:[NSLocalizedString(@"albums", nil) uppercaseStringWithLocale:[NSLocale currentLocale]]];
            [self.artists removeAllObjects];
            [self.tracks removeAllObjects];
        } else if ([type isEqualToString:@"artists"]) {
            [self.sections addObject:[NSLocalizedString(@"artists", nil) uppercaseStringWithLocale:[NSLocale currentLocale]]];
            [self.albums removeAllObjects];
            [self.tracks removeAllObjects];
        } else {
            [self.sections addObject:[NSLocalizedString(@"tracks", nil) uppercaseStringWithLocale:[NSLocale currentLocale]]];
            [self.albums removeAllObjects];
            [self.artists removeAllObjects];
        }
        
        [self.resultsTableView reloadData];
        [self searchWithType:type];
    }
}

#pragma mark - UITextField

- (UITextField *)searchTextField {
    if (_searchTextField == nil) {
        _searchTextField = [[UITextField alloc] init];
        _searchTextField.translatesAutoresizingMaskIntoConstraints = NO;
        _searchTextField.delegate = self;
        _searchTextField.font = [UIFont systemFontOfSize:14.0f];
        _searchTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _searchTextField.keyboardAppearance = UIKeyboardAppearanceDark;
        _searchTextField.returnKeyType = UIReturnKeySearch;
        _searchTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"search", nil)
                                                                                 attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
    }
    
    return _searchTextField;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    textField.text = @"";
    [self.messageView hide];
    [self loadSearchHistoriesWithKey:nil
                              offset:0];
    
    return NO;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    ((BaseNavigationController *)self.navigationController).statusBarStyle = UIStatusBarStyleDefault;
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"bg_nav_white.png"]
                                                  forBarMetrics:UIBarMetricsDefault];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.titleView.backgroundColor = [UIColor colorWithRed:250.0f / 255
                                                     green:250.0f / 255
                                                      blue:250.0f / 255
                                                     alpha:1.0f];
    
    [self.searchTextField resignFirstResponder];
    self.historiesTableView.hidden = YES;
    self.resultsTableView.hidden = YES;
    
    [self saveSearchKey:self.searchTextField.text];
    [self removeAllData];
    [self searchWithType:@"all"];
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *text = [textField.text stringByReplacingCharactersInRange:range
                                                             withString:string];
    [self.messageView hide];
    [self loadSearchHistoriesWithKey:text
                              offset:0];
    
    return YES;
}

#pragma mark - UITableView

- (UITableView *)historiesTableView {
    if (_historiesTableView == nil) {
        _historiesTableView = [[UITableView alloc] initWithFrame:CGRectZero
                                                           style:UITableViewStylePlain];
        _historiesTableView.translatesAutoresizingMaskIntoConstraints = NO;
        _historiesTableView.backgroundColor = [UIColor whiteColor];
        _historiesTableView.separatorColor = [[BaseTheme currentTheme] separatorColor];
        _historiesTableView.bounces = NO;
        _historiesTableView.layer.borderWidth = 1.0f;
        _historiesTableView.layer.borderColor = [[BaseTheme currentTheme] separatorColor].CGColor;
        _historiesTableView.layer.cornerRadius = 2.0f;
        _historiesTableView.dataSource = self;
        _historiesTableView.delegate = self;
        [_historiesTableView registerClass:[SearchHistoriesTableViewCell class]
                    forCellReuseIdentifier:NSStringFromClass([SearchHistoriesTableViewCell class])];
    }
    
    return _historiesTableView;
}

- (CGFloat)historiesTableViewHeight {
    CGFloat maxHeight = self.view.bounds.size.height - self.keyboardSize.height - self.margin;
    CGFloat height = self.searchHistories.count * historiesTableViewRowHeight;
    
    if (height > maxHeight) {
        return maxHeight;
    }
    
    return height;
}

- (UITableView *)resultsTableView {
    if (_resultsTableView == nil) {
        _resultsTableView = [[UITableView alloc] initWithFrame:CGRectZero
                                                         style:UITableViewStylePlain];
        _resultsTableView.translatesAutoresizingMaskIntoConstraints = NO;
        _resultsTableView.backgroundColor = [UIColor whiteColor];
        _resultsTableView.separatorColor = [[BaseTheme currentTheme] separatorColor];
        _resultsTableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
        _resultsTableView.dataSource = self;
        _resultsTableView.delegate = self;
        _resultsTableView.hidden = YES;
        
        for (NSInteger index = 0; index < 3; index++) {
            NSString *cellReuseIdentifier = [NSString stringWithFormat:@"%@%@", NSStringFromClass([SearchResultsTableViewCell class]), @(index)];
            [_resultsTableView registerClass:[SearchResultsTableViewCell class]
                      forCellReuseIdentifier:cellReuseIdentifier];
        }
    }
    
    return _resultsTableView;
}

- (UIEdgeInsets)resultsTableViewContentInset {
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    return [appDelegate contentInsetForScrollView:self.resultsTableView];
}

- (void)deselectSelectedRow {
    NSIndexPath *indexPath = [self.resultsTableView indexPathForSelectedRow];
    
    if (indexPath) {
        [self.resultsTableView deselectRowAtIndexPath:indexPath
                                             animated:YES];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (tableView == self.historiesTableView) {
        return 1;
    }
    
    return self.sections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.historiesTableView) {
        return self.searchHistories.count;
    }
    
    NSInteger numberOfAlbums = self.albums.count;
    NSInteger numberOfArtists = self.artists.count;
    NSInteger numberOfTracks = self.tracks.count;
    
    if (section == 0) {
        if (numberOfAlbums > 0) {
            return numberOfAlbums;
        } else if (numberOfArtists > 0) {
            return numberOfArtists;
        }
    } else if (section == 1) {
        if (numberOfAlbums > 0 && numberOfArtists > 0) {
            return numberOfArtists;
        }
    }
    
    return numberOfTracks;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.historiesTableView) {
        SearchHistory *searchHistory = self.searchHistories[indexPath.row];
        
        SearchHistoriesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([SearchHistoriesTableViewCell class])
                                                                                forIndexPath:indexPath];
        cell.titleLabel.text = searchHistory.key;
        
        return cell;
    }
    
    NSInteger numberOfAlbums = self.albums.count;
    NSInteger numberOfArtists = self.artists.count;
    NSString *reuseIdentifier;
    
    if (indexPath.section == 0) {
        if (numberOfAlbums > 0) {
            reuseIdentifier = [NSString stringWithFormat:@"%@%@", NSStringFromClass([SearchResultsTableViewCell class]), @(0)];
        } else if (numberOfArtists > 0) {
            reuseIdentifier = [NSString stringWithFormat:@"%@%@", NSStringFromClass([SearchResultsTableViewCell class]), @(1)];
        } else {
            reuseIdentifier = [NSString stringWithFormat:@"%@%@", NSStringFromClass([SearchResultsTableViewCell class]), @(2)];
        }
    } else if (indexPath.section == 1) {
        if (numberOfAlbums > 0 && numberOfArtists > 0) {
            reuseIdentifier = [NSString stringWithFormat:@"%@%@", NSStringFromClass([SearchResultsTableViewCell class]), @(1)];
        } else {
            reuseIdentifier = [NSString stringWithFormat:@"%@%@", NSStringFromClass([SearchResultsTableViewCell class]), @(2)];
        }
    } else {
        reuseIdentifier = [NSString stringWithFormat:@"%@%@", NSStringFromClass([SearchResultsTableViewCell class]), @(2)];
    }
    
    SearchResultsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier
                                                                          forIndexPath:indexPath];
    
    if (indexPath.section == 0) {
        if (numberOfAlbums > 0) {
            Album *album = self.albums[indexPath.row];
            [self coverImageViewForAlbum:album
                                 forCell:cell];
            cell.albumTitleLabel.text = album.title;
            cell.artistNameLabel.text = album.artist.name;
        } else if (numberOfArtists > 0) {
            Artist *artist = self.artists[indexPath.row];
            cell.artistNameLabel.text = artist.name;
        } else {
            Track *track = self.tracks[indexPath.row];
            [self coverImageViewForAlbum:track.album
                                 forCell:cell];
            cell.albumTitleLabel.text = track.album.title;
            cell.trackTitleLabel.text = track.title;
        }
    } else if (indexPath.section == 1) {
        if (numberOfAlbums > 0 && numberOfArtists > 0) {
            Artist *artist = self.artists[indexPath.row];
            cell.artistNameLabel.text = artist.name;
        } else {
            Track *track = self.tracks[indexPath.row];
            [self coverImageViewForAlbum:track.album
                                 forCell:cell];
            cell.albumTitleLabel.text = track.album.title;
            cell.trackTitleLabel.text = track.title;
        }
    } else {
        Track *track = self.tracks[indexPath.row];
        [self coverImageViewForAlbum:track.album
                             forCell:cell];
        cell.albumTitleLabel.text = track.album.title;
        cell.trackTitleLabel.text = track.title;
    }
    
    return cell;
}

- (void)coverImageViewForAlbum:(Album *)album forCell:(SearchResultsTableViewCell *)cell {
    __weak SearchResultsTableViewCell *weakCell = cell;
    [cell.activityIndicatorView startAnimating];
    [cell.albumCoverImageView sd_setImageWithURL:[NSURL URLWithString:album.imageUrl] placeholderImage:[UIImage imageNamed:@"img_placeholder.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [weakCell.activityIndicatorView stopAnimating];
        
        if (image) {
            if (cacheType == SDImageCacheTypeNone) {
                weakCell.albumCoverImageView.alpha = 0.0f;
                [UIView animateWithDuration:0.25f animations:^{
                    weakCell.albumCoverImageView.alpha = 1.0f;
                } completion:^(BOOL finished) {
                    weakCell.albumCoverImageView.image = image;
                }];
            } else {
                weakCell.albumCoverImageView.image = image;
            }
        }
    }];
}

#pragma mark - UITableViewDelegate

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (tableView == self.historiesTableView) {
        return nil;
    }
    
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor whiteColor];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    titleLabel.font = [UIFont systemFontOfSize:17.0f];
    titleLabel.textColor = [UIColor darkGrayColor];
    titleLabel.text = self.sections[section];
    
    [headerView addSubview:titleLabel];
    
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-space-[titleLabel]-space-|"
                                                                       options:0
                                                                       metrics:@{@"space": @(15.0f)}
                                                                         views:@{@"titleLabel": titleLabel}]];
    
    [headerView addConstraint:[NSLayoutConstraint constraintWithItem:titleLabel
                                                           attribute:NSLayoutAttributeCenterY
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:headerView
                                                           attribute:NSLayoutAttributeCenterY
                                                          multiplier:1.0f
                                                            constant:0.0f]];
    
    return headerView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    if (tableView == self.historiesTableView) {
        return nil;
    }
    
    UIView *footerView = [[UIView alloc] init];
    footerView.backgroundColor = [UIColor whiteColor];
    footerView.backgroundColor = [UIColor colorWithRed:255.0f / 255
                                                 green:255.0f / 255
                                                  blue:255.0f / 255
                                                 alpha:0.95f];
    
    UIButton *moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
    moreButton.translatesAutoresizingMaskIntoConstraints = NO;
    [moreButton addTarget:self
                   action:@selector(moreButtonTapped:)
         forControlEvents:UIControlEventTouchUpInside];
    moreButton.titleLabel.font = [UIFont systemFontOfSize:15.0f];
    moreButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [moreButton setTitleColor:[UIColor darkGrayColor]
                     forState:UIControlStateNormal];
    [moreButton setTitleColor:[UIColor grayColor]
                     forState:UIControlStateHighlighted];
    [moreButton setTitle:NSLocalizedString(@"load_more", nil)
                forState:UIControlStateNormal];
    moreButton.tag = section;
    
    [footerView addSubview:moreButton];
    
    [footerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-space-[moreButton]-space-|"
                                                                       options:0
                                                                       metrics:@{@"space": @(15.0f)}
                                                                         views:@{@"moreButton": moreButton}]];
    
    [footerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[moreButton]|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:@{@"moreButton": moreButton}]];
    
    return footerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (tableView == self.historiesTableView) {
        return 0.0f;
    }
    
    return 50.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (tableView == self.historiesTableView) {
        return 0.0f;
    }
    
    return 44.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.historiesTableView) {
        return historiesTableViewRowHeight;
    }
    
    return 60.0f;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.historiesTableView) {
        SearchHistory *searchHistory = self.searchHistories[indexPath.row];
        self.searchTextField.text = searchHistory.key;
        [self.searchTextField resignFirstResponder];
        
        ((BaseNavigationController *)self.navigationController).statusBarStyle = UIStatusBarStyleDefault;
        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"bg_nav_white.png"]
                                                      forBarMetrics:UIBarMetricsDefault];
        
        self.view.backgroundColor = [UIColor whiteColor];
        self.titleView.backgroundColor = [UIColor colorWithRed:250.0f / 255
                                                         green:250.0f / 255
                                                          blue:250.0f / 255
                                                         alpha:1.0f];
        self.historiesTableView.hidden = YES;
        self.resultsTableView.hidden = YES;
        
        [self removeAllData];
        [self searchWithType:@"all"];
    } else {
        NSInteger numberOfAlbums = self.albums.count;
        NSInteger numberOfArtists = self.artists.count;
        
        if (indexPath.section == 0) {
            if (numberOfAlbums > 0) {
                Album *album = self.albums[indexPath.row];
                AlbumViewController *viewController = [[AlbumViewController alloc] initWithAlbum:album];
                [self.navigationController pushViewController:viewController
                                                     animated:YES];
            } else if (numberOfArtists > 0) {
                Artist *artist = self.artists[indexPath.row];
                AlbumsViewController *viewController = [[AlbumsViewController alloc] initWithArtist:artist];
                [self.navigationController pushViewController:viewController
                                                     animated:YES];
            } else {
                Track *track = self.tracks[indexPath.row];
                AlbumViewController *viewController = [[AlbumViewController alloc] initWithAlbum:track.album];
                [self.navigationController pushViewController:viewController
                                                     animated:YES];
            }
        } else if (indexPath.section == 1) {
            if (numberOfAlbums > 0 && numberOfArtists > 0) {
                Artist *artist = self.artists[indexPath.row];
                AlbumsViewController *viewController = [[AlbumsViewController alloc] initWithArtist:artist];
                [self.navigationController pushViewController:viewController
                                                     animated:YES];
            } else {
                Track *track = self.tracks[indexPath.row];
                AlbumViewController *viewController = [[AlbumViewController alloc] initWithAlbum:track.album];
                [self.navigationController pushViewController:viewController
                                                     animated:YES];
            }
        } else {
            Track *track = self.tracks[indexPath.row];
            AlbumViewController *viewController = [[AlbumViewController alloc] initWithAlbum:track.album];
            [self.navigationController pushViewController:viewController
                                                 animated:YES];
        }
    }
}

#pragma mark - CoreData

- (NSManagedObjectContext *)mainManagedObjectContext {
    if (_mainManagedObjectContext == nil) {
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        _mainManagedObjectContext = appDelegate.mainManagedObjectContext;
    }
    
    return _mainManagedObjectContext;
}

- (NSManagedObjectContext *)workerManagedObjectContext {
    if (_workerManagedObjectContext == nil) {
        _workerManagedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        _workerManagedObjectContext.parentContext = self.mainManagedObjectContext;
    }
    
    return _workerManagedObjectContext;
}

- (SearchHistoryRepository *)searchHistoryRepository {
    if (_searchHistoryRepository == nil) {
        _searchHistoryRepository = [[SearchHistoryRepository alloc] initWithManagedObjectContext:self.workerManagedObjectContext];
    }
    
    return _searchHistoryRepository;
}

- (void)loadSearchHistoriesWithKey:(NSString *)key offset:(NSUInteger)offset {
    ((BaseNavigationController *)self.navigationController).statusBarStyle = UIStatusBarStyleLightContent;
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"bg_nav.png"]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.view.backgroundColor = [UIColor clearColor];
    self.titleView.backgroundColor = [UIColor whiteColor];
    self.historiesTableView.hidden = NO;
    self.resultsTableView.hidden = YES;
    
    [self.workerManagedObjectContext performBlock:^{
        NSArray *result = [self.searchHistoryRepository fetchAllByKey:key
                                                               offset:offset
                                                                limit:20];
        [self.searchHistories removeAllObjects];
        [self.searchHistories addObjectsFromArray:result];
        
        [self.mainManagedObjectContext performBlock:^{
            self.historiesTableViewHeightConstraint.constant = [self historiesTableViewHeight];
            [self.historiesTableView setNeedsUpdateConstraints];
            
            [UIView animateWithDuration:0.25f animations:^{
                [self.historiesTableView layoutIfNeeded];
            }];
            
            [self.searchTextField becomeFirstResponder];
            [self.historiesTableView reloadData];
        }];
    }];
}

- (void)saveSearchKey:(NSString *)key {
    SearchHistory *searchHistory = [self.searchHistoryRepository fetchOneByKey:key];
    
    if (!searchHistory) {
        SearchHistory *searchHistory = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([SearchHistory class])
                                                                     inManagedObjectContext:self.workerManagedObjectContext];
        searchHistory.key = key;
        searchHistory.createdAt = [NSDate date];
        
        NSError *error;
        
        if (![self.workerManagedObjectContext save:&error]) {
            [self.mainManagedObjectContext performBlock:^{
                [ErrorHandler handleError:error];
            }];
        } else {
            [self.mainManagedObjectContext performBlock:^{
                NSError *error;
                
                if (![self.mainManagedObjectContext save:&error]) {
                    [self.mainManagedObjectContext performBlock:^{
                        [ErrorHandler handleError:error];
                    }];
                } else {
                    [self.mainManagedObjectContext.parentContext performBlock:^{
                        NSError *error;
                        
                        if (![self.mainManagedObjectContext.parentContext save:&error]) {
                            [self.mainManagedObjectContext performBlock:^{
                                [ErrorHandler handleError:error];
                            }];
                        }
                    }];
                }
            }];
        }
    }
}

#pragma mark - SZKProgressView

- (SZKProgressView *)progressView {
    if (_progressView == nil) {
        _progressView = [[SZKProgressView alloc] init];
    }
    
    return _progressView;
}

#pragma mark - SZKMessageView

- (SZKMessageView *)messageView {
    if (_messageView == nil) {
        _messageView = [[SZKMessageView alloc] init];
        _messageView.delegate = self;
    }
    
    return _messageView;
}

#pragma mark - SZKMessageViewDelegate

- (void)messageView:(SZKMessageView *)messageView didTapActionButton:(UIButton *)actionButton {
    [self searchWithType:@"all"];
}

#pragma mark - Loading indicator

- (void)showLoadingIndicator {
    if (self.sections.count == 0) {
        [self.messageView hide];
        [self.progressView showInView:self.view];
    }
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    self.allowLoading = NO;
}

- (void)hideLoadingIndicator {
    [self.progressView hide];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    self.allowLoading = YES;
}

#pragma mark - ApiClient

- (void)searchWithType:(NSString *)type {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithDictionary:@{@"input": self.searchTextField.text}];
    
    if (![type isEqualToString:@"all"]) {
        [parameters setObject:@(self.page)
                       forKey:@"page"];
    }
    
    [Answers logSearchWithQuery:self.searchTextField.text
               customAttributes:nil];
    
    [self showLoadingIndicator];
    ApiClient *client = [ApiClient sharedClient];
    self.loadingTask = [client searchWithParameters:parameters type:type success:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        [self hideLoadingIndicator];
        
        if ([type isEqualToString:@"all"]) {
            self.resultsTableView.contentOffset = CGPointMake(0.0f, 0.0f - self.resultsTableView.contentInset.top);
            [self removeAllData];
        }
        
        NSMutableArray *indexPaths = [NSMutableArray new];
        
        if ([type isEqualToString:@"albums"]) {
            indexPaths = [NSMutableArray arrayWithArray:[self saveAlbumsFromData:responseObject]];
            self.page++;
        } else if ([type isEqualToString:@"artists"]) {
            indexPaths = [NSMutableArray arrayWithArray:[self saveArtistsFromData:responseObject]];
            self.page++;
        } else if ([type isEqualToString:@"tracks"]) {
            indexPaths = [NSMutableArray arrayWithArray:[self saveTracksFromData:responseObject]];
            self.page++;
        } else {
            [indexPaths addObjectsFromArray:[self saveAlbumsFromData:responseObject[@"albums"]]];
            [indexPaths addObjectsFromArray:[self saveArtistsFromData:responseObject[@"artists"]]];
            [indexPaths addObjectsFromArray:[self saveTracksFromData:responseObject[@"tracks"]]];
            self.page = 1;
        }
        
        NSInteger numberOfResults = indexPaths.count;
        
        if (numberOfResults > 0) {
            if ([type isEqualToString:@"all"]) {
                self.resultsTableView.hidden = NO;
                [self.resultsTableView reloadData];
            } else {
                [self.resultsTableView insertRowsAtIndexPaths:indexPaths
                                             withRowAnimation:UITableViewRowAnimationFade];
            }
        } else if (self.sections.count == 0) {
            self.resultsTableView.hidden = YES;
            [self.resultsTableView reloadData];
            [self.messageView showInView:self.view
                               iconImage:nil
                                   title:nil
                                 message:NSLocalizedString(@"no_data", nil)
                       actionButtonTitle:NSLocalizedString(@"reload", nil)];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error) {
        [self hideLoadingIndicator];
        
        if (error.code != NSURLErrorCancelled) {
            if (self.sections.count > 0) {
                [ErrorHandler handleError:error];
            } else {
                [self.messageView showInView:self.view
                                   iconImage:nil
                                       title:nil
                                     message:[ErrorHandler messageFromError:error]
                           actionButtonTitle:NSLocalizedString(@"reload", nil)];
            }
        } else {
            [self.messageView showInView:self.view
                               iconImage:nil
                                   title:nil
                                 message:nil
                       actionButtonTitle:NSLocalizedString(@"reload", nil)];
        }
    }];
}

- (nonnull NSArray *)saveAlbumsFromData:(nullable NSArray *)data {
    NSInteger numberOfResults = data.count;
    NSMutableArray *indexPaths = [NSMutableArray new];
    
    if (numberOfResults > 0) {
        NSInteger totalCount = self.albums.count;
        
        if (totalCount == 0) {
            [self.sections addObject:[NSLocalizedString(@"albums", nil) uppercaseStringWithLocale:[NSLocale currentLocale]]];
        }
        
        for (NSInteger index = 0; index < numberOfResults; index++) {
            Album *album = [[Album alloc] initWithDictionary:data[index]
                                                     context:self.workerManagedObjectContext];
            [self.albums addObject:album];
            
            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:totalCount + index
                                                         inSection:0];
            [indexPaths addObject:indexPath];
        }
    }
    
    return indexPaths;
}

- (nonnull NSArray *)saveArtistsFromData:(nullable NSArray *)data {
    NSInteger numberOfResults = data.count;
    NSMutableArray *indexPaths = [NSMutableArray new];
    
    if (numberOfResults > 0) {
        NSInteger totalCount = self.artists.count;
        
        if (totalCount == 0) {
            [self.sections addObject:[NSLocalizedString(@"artists", nil) uppercaseStringWithLocale:[NSLocale currentLocale]]];
        }
        
        NSInteger section = self.sections.count - 1;
        
        for (NSInteger index = 0; index < numberOfResults; index++) {
            Artist *artist = [[Artist alloc] initWithDictionary:data[index]
                                                        context:self.workerManagedObjectContext];
            [self.artists addObject:artist];
            
            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:totalCount + index
                                                         inSection:section];
            [indexPaths addObject:indexPath];
        }
    }
    
    return indexPaths;
}

- (nonnull NSArray *)saveTracksFromData:(nullable NSArray *)data {
    NSInteger numberOfResults = data.count;
    NSMutableArray *indexPaths = [NSMutableArray new];
    
    if (numberOfResults > 0) {
        NSInteger totalCount = self.tracks.count;
        
        if (totalCount == 0) {
            [self.sections addObject:[NSLocalizedString(@"tracks", nil) uppercaseStringWithLocale:[NSLocale currentLocale]]];
        }
        
        NSInteger section = self.sections.count - 1;
        
        for (NSInteger index = 0; index < numberOfResults; index++) {
            Track *track = [[Track alloc] initWithDictionary:data[index]
                                                     context:self.workerManagedObjectContext];
            [self.tracks addObject:track];
            
            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:totalCount + index
                                                         inSection:section];
            [indexPaths addObject:indexPath];
        }
    }
    
    return indexPaths;
}

- (void)removeAllData {
    [self.sections removeAllObjects];
    [self.albums removeAllObjects];
    [self.artists removeAllObjects];
    [self.tracks removeAllObjects];
    [self.resultsTableView reloadData];
}

@end
