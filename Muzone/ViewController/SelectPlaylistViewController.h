//
//  SelectPlaylistViewController.h
//  Muzone
//
//  Created by Macbook on 5/1/17.
//  Copyright © 2017 Muzone LLC. All rights reserved.
//

#import "Track.h"
#import "BaseViewController.h"

@interface SelectPlaylistViewController : BaseViewController
@property (nonnull, nonatomic, strong) Track *track;
@end
