//
//  SelectPlaylistViewController.m
//  Muzone
//
//  Created by Sukhrob Khakimov on 5/27/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "SZKProgressView.h"
#import "SZKMessageView.h"
#import "Constants.h"

#import "ArtistsTableViewCell.h"
#import "BaseTheme.h"
#import "AppDelegate.h"

#import "PlaylistManager.h"
#import "SelectPlaylistViewController.h"

#import "TrackRepository.h"
#import "TrackManager.h"

#import "Muzone-Swift.h"


@interface SelectPlaylistViewController ()  <SZKMessageViewDelegate, UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate>
@property (nonatomic, retain) NSMutableArray * playlists;

@property (nonnull, nonatomic, strong) NSManagedObjectContext *mainManagedObjectContext;
@property (nonnull, nonatomic, strong) NSManagedObjectContext *workerManagedObjectContext;

@property (nonnull, nonatomic, strong) TrackRepository *trackRepository;
@property (nonnull, nonatomic, strong) TrackManager *trackManager;


@property (nonnull, nonatomic, strong) SZKProgressView *progressView;
@property (nonnull, nonatomic, strong) SZKMessageView *messageView;

@property (nonnull, nonatomic, strong) UITableView *tableView;
@property (nonnull, nonatomic, strong) UIView *navigationView;

@property (nonnull, nonatomic, strong) NSMutableArray *sectionTitles;
@property (nonnull, nonatomic, strong) NSMutableArray *numberOfRowsInSections;
@property (nonnull, nonatomic, strong) NSMutableArray *lastIndexesInSections;



@end

@implementation SelectPlaylistViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [Answers logContentViewWithName:@"Add to playlist"
                        contentType:@"Player"
                          contentId:[NSString stringWithFormat:@"%@", self.track.uid]
                   customAttributes:nil];
    
    [self reloadData];
}

-(void)loadView
{
    [super loadView];
    [self.view addSubview:self.navigationView];
    [self.view addSubview:self.tableView];
    
    CGFloat navViewHeight = [UIApplication sharedApplication].statusBarFrame.size.height + 44.0;
    
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[navigationView]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:@{@"navigationView": self.navigationView}]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[tableView]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:@{@"tableView": self.tableView}]];
    
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[navigationView(height)][tableView]|"
                                                                      options:0
                                                                      metrics:@{@"height": @(navViewHeight)}
                                                                        views:@{@"navigationView": self.navigationView,
                                                                                @"tableView": self.tableView}]];
    
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // NSNotificationCenter
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    
    
    [notificationCenter addObserver:self
                           selector:@selector(reloadData)
                               name:PlaylistManagerUpdatePlaylistsNotification
                             object:nil];
}


-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)reloadData
{
    
    if ([PlaylistManager sharedManager].playlists.count > 0) {
        [self reloadTable:[PlaylistManager sharedManager].playlists];
    }
    else
    {
        [self showLoadingIndicator];
        WEAKIFY_SELF;
        PlaylistManager *pManager = [PlaylistManager sharedManager];
        [pManager getAllPlaylists:^(BOOL success, NSArray * _Nullable result) {
            STRONGIFY_SELF;
            if (success)
            {
                [self reloadTable:result];
                [self hideLoadingIndicator];
            }
            else
            {
                [self.tableView reloadData];
                self.tableView.hidden = YES;
                [self hideLoadingIndicator];
                [self.messageView showInView:self.view
                                   iconImage:nil
                                       title:nil
                                     message:NSLocalizedString(@"no_data", nil)
                           actionButtonTitle:NSLocalizedString(@"reload", nil)];
            }
            
        }];
    }
    
    
}


-(void)reloadTable:(NSArray*)artists
{
    [self.sectionTitles removeAllObjects];
    [self.numberOfRowsInSections removeAllObjects];
    [self.lastIndexesInSections removeAllObjects];
    [self.playlists removeAllObjects];
    
    
    NSArray *data = artists;
    NSInteger numberOfResults = data.count;
    
    if (numberOfResults > 0) {
        NSInteger numberOfRowsInSection = 0;
        
        for (NSInteger index = 0; index < numberOfResults; index++) {
            Playlist *playlist = data[index];
            [self.playlists addObject:playlist];
            
            NSString *sectionTitle = [playlist.title substringToIndex:1];
            
            if (![self.sectionTitles.lastObject isEqual:sectionTitle]) {
                numberOfRowsInSection = 0;
                
                [self.sectionTitles addObject:sectionTitle];
                [self.numberOfRowsInSections addObject:@(numberOfRowsInSection + 1)];
                
                NSInteger lastIndexInSection = [self.lastIndexesInSections.lastObject integerValue];
                [self.lastIndexesInSections addObject:@(lastIndexInSection + numberOfRowsInSection + 1)];
            } else {
                NSInteger numberOfRowsInLastSection = [self.numberOfRowsInSections.lastObject integerValue];
                [self.numberOfRowsInSections replaceObjectAtIndex:self.sectionTitles.count - 1
                                                       withObject:@(numberOfRowsInLastSection + numberOfRowsInSection + 1)];
                
                NSInteger lastIndexInSection = [self.lastIndexesInSections.lastObject integerValue];
                [self.lastIndexesInSections replaceObjectAtIndex:self.lastIndexesInSections.count - 1
                                                      withObject:@(lastIndexInSection + numberOfRowsInSection + 1)];
            }
        }
        self.tableView.hidden = NO;
        
    }
    else
    {
        self.tableView.hidden = YES;
        [self.messageView showInView:self.view
                           iconImage:nil
                               title:nil
                             message:NSLocalizedString(@"no_data", nil)
                   actionButtonTitle:NSLocalizedString(@"reload", nil)];
        
        
    }
    [self.tableView reloadData];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - NSMutableArray

- (NSMutableArray *)playlists {
    if (_playlists == nil) {
        _playlists = [NSMutableArray new];
    }
    
    return _playlists;
}


#pragma mark - UITableView

- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero
                                                  style:UITableViewStylePlain];
        _tableView.translatesAutoresizingMaskIntoConstraints = NO;
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        [_tableView registerClass:[ArtistsTableViewCell class]
           forCellReuseIdentifier:NSStringFromClass([ArtistsTableViewCell class])];
        _tableView.sectionIndexColor = [[BaseTheme currentTheme] tintColor];
        _tableView.sectionIndexBackgroundColor = [UIColor clearColor];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        
    }
    
    return _tableView;
}

- (UIView *)navigationView {
    if (_navigationView == nil) {
        _navigationView = [[UIView alloc] init];
        _navigationView.translatesAutoresizingMaskIntoConstraints = NO;
        _navigationView.backgroundColor = [[BaseTheme currentTheme] tintColorWithAlpha:0.9f];
        
        UILabel *title = [UILabel new];
        title.translatesAutoresizingMaskIntoConstraints = NO;
        title.backgroundColor = [UIColor clearColor];
        title.font = [UIFont systemFontOfSize:16.0f weight:UIFontWeightSemibold];
        title.textColor = [UIColor whiteColor];
        title.textAlignment = NSTextAlignmentCenter;
        title.text = NSLocalizedString(@"Select playlist", nil);
        [_navigationView addSubview:title];
        
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 20, 44, 44)];
        [button setImage:[UIImage imageNamed:@"ic_arrow_down"] forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:@"ic_arrow_down"] forState:UIControlStateHighlighted];
        [button addTarget:self action:@selector(dismiss:) forControlEvents:UIControlEventTouchUpInside];
        button.backgroundColor = [UIColor clearColor];
        [_navigationView addSubview:button];
        
        [_navigationView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[button][label]-44-|"
                                                                          options:0
                                                                          metrics:nil
                                                                            views:@{@"label": title,
                                                                                    @"button":button}]];
        
        [_navigationView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[label]|"
                                                                                options:0
                                                                                metrics:nil
                                                                                  views:@{@"label": title}]];
        

    }
    
    return _navigationView;
}

-(void) dismiss:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}


- (NSMutableArray *)sectionTitles {
    if (_sectionTitles == nil) {
        _sectionTitles = [NSMutableArray new];
    }
    
    return _sectionTitles;
}

- (NSMutableArray *)numberOfRowsInSections {
    if (_numberOfRowsInSections == nil) {
        _numberOfRowsInSections = [NSMutableArray new];
    }
    
    return _numberOfRowsInSections;
}

- (NSMutableArray *)lastIndexesInSections {
    if (_lastIndexesInSections == nil) {
        _lastIndexesInSections = [NSMutableArray new];
    }
    
    return _lastIndexesInSections;
}



- (NSInteger)indexFromIndexPath:(NSIndexPath *)indexPath {
    return [self.lastIndexesInSections[indexPath.section] integerValue] - [self.numberOfRowsInSections[indexPath.section] integerValue] + indexPath.row;
}

#pragma mark - UITableViewDataSource

- (NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return self.sectionTitles;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.sectionTitles.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.numberOfRowsInSections[section] integerValue];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger index = [self indexFromIndexPath:indexPath];
    Playlist *playlist = self.playlists[index];
    
    ArtistsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ArtistsTableViewCell class])
                                                                 forIndexPath:indexPath];
    cell.titleLabel.text = playlist.title;
    return cell;
}

#pragma mark - UITableViewDelegate

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [[BaseTheme currentTheme] sectionBackgroundColor];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    titleLabel.font = [UIFont boldSystemFontOfSize:17.0f];
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.text = self.sectionTitles[section];
    
    [headerView addSubview:titleLabel];
    
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-space-[titleLabel]-space-|"
                                                                       options:0
                                                                       metrics:@{@"space": @(15.0f)}
                                                                         views:@{@"titleLabel": titleLabel}]];
    
    [headerView addConstraint:[NSLayoutConstraint constraintWithItem:titleLabel
                                                           attribute:NSLayoutAttributeCenterY
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:headerView
                                                           attribute:NSLayoutAttributeCenterY
                                                          multiplier:1.0f
                                                            constant:0.0f]];
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 32.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger index = [self indexFromIndexPath:indexPath];
    Playlist *playlist = self.playlists[index];
    WEAKIFY_SELF;
    [self showLoadingIndicator];
    PlaylistManager *pManager = [PlaylistManager sharedManager];
    [pManager addTrackToPlaylist:self.track playlist:playlist complete:^(BOOL success) {
        STRONGIFY_SELF;
        [self hideLoadingIndicator];
        if (success) {
            [self dismissViewControllerAnimated:YES completion:^{}];
        }
    }];
    
}



#pragma mark - Loading indicator

- (void)showLoadingIndicator {
    if (self.playlists.count == 0) {
        [self.messageView hide];
        [self.progressView showInView:self.view];
    }
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)hideLoadingIndicator {
    [self.progressView hide];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

#pragma mark - CoreData

- (NSManagedObjectContext *)mainManagedObjectContext {
    if (_mainManagedObjectContext == nil) {
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        _mainManagedObjectContext = appDelegate.mainManagedObjectContext;
    }
    
    return _mainManagedObjectContext;
}

- (NSManagedObjectContext *)workerManagedObjectContext {
    if (_workerManagedObjectContext == nil) {
        _workerManagedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        _workerManagedObjectContext.parentContext = self.mainManagedObjectContext;
    }
    
    return _workerManagedObjectContext;
}

- (TrackRepository *)trackRepository {
    if (_trackRepository == nil) {
        _trackRepository = [[TrackRepository alloc] initWithManagedObjectContext:self.workerManagedObjectContext];
    }
    
    return _trackRepository;
}

- (TrackManager *)trackManager {
    if (_trackManager == nil) {
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        _trackManager = appDelegate.trackManager;
    }
    
    return _trackManager;
}


#pragma mark - SZKProgressView

- (SZKProgressView *)progressView {
    if (_progressView == nil) {
        _progressView = [[SZKProgressView alloc] init];
    }
    
    return _progressView;
}

#pragma mark - SZKMessageView

- (SZKMessageView *)messageView {
    if (_messageView == nil) {
        _messageView = [[SZKMessageView alloc] init];
        _messageView.delegate = self;
    }
    
    return _messageView;
}

#pragma mark - SZKMessageViewDelegate

- (void)messageView:(SZKMessageView *)messageView didTapActionButton:(UIButton *)actionButton {
    [self reloadData];
}



@end
