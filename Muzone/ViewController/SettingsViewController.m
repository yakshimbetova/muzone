//
//  SettingsViewController.m
//  Muzone
//
//  Created by Sukhrob Khakimov on 5/27/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

#import "SettingsViewController.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = NSLocalizedString(@"settings", nil);
    
    [Answers logContentViewWithName:@"Settings"
                        contentType:@"Menu"
                          contentId:nil
                   customAttributes:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
