//
//  TermsViewController.m
//  Muzone
//
//  Created by Sukhrob Khakimov on 5/27/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

#import "AppDelegate.h"
#import "Constants.h"
#import "ErrorHandler.h"

#import "TermsViewController.h"

#import "SZKMessageView.h"
#import "SZKProgressView.h"

@interface TermsViewController () <SZKMessageViewDelegate>

@property (nonnull, nonatomic, strong) UIWebView *webView;
@property (nonnull, nonatomic, strong) SZKMessageView *messageView;
@property (nonnull, nonatomic, strong) SZKProgressView *progressView;

@end

@implementation TermsViewController

- (void)loadView {
    [super loadView];
    
    [self.view addSubview:self.webView];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[webView]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:@{@"webView": self.webView}]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[webView]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:@{@"webView": self.webView}]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = NSLocalizedString(@"terms_privacy", nil);
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh
                                                                                           target:self
                                                                                           action:@selector(rightBarButtonItemTapped:)];
    
    [Answers logContentViewWithName:@"Terms & Privacy"
                        contentType:@"Menu"
                          contentId:nil
                   customAttributes:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.webView.scrollView.contentInset = [self scrollViewContentInset];
    
    // NSNotificationCenter
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    
    [notificationCenter addObserver:self
                           selector:@selector(playerViewControllerWillPlay:)
                               name:PlayerViewControllerWillPlay
                             object:nil];
    
    [notificationCenter addObserver:self
                           selector:@selector(playerViewControllerWillStop:)
                               name:PlayerViewControllerWillStop
                             object:nil];
    
    [self loadWebView];
}

- (void)viewWillDisappear:(BOOL)animated {
    // NSNotificationCenter
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    
    [notificationCenter removeObserver:self
                                  name:PlayerViewControllerWillPlay
                                object:nil];
    
    [notificationCenter removeObserver:self
                                  name:PlayerViewControllerWillStop
                                object:nil];
    
    [self.webView stopLoading];
    
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - NSNotificationCenter

- (void)playerViewControllerWillPlay:(NSNotification *)notification {
    self.webView.scrollView.contentInset = [self scrollViewContentInset];
}

- (void)playerViewControllerWillStop:(NSNotification *)notification {
    self.webView.scrollView.contentInset = [self scrollViewContentInset];
}

#pragma mark - UINavigationBar

- (void)rightBarButtonItemTapped:(id)sender {
    [self loadWebView];
}

#pragma mark - UIWebView

- (UIWebView *)webView {
    if (_webView == nil) {
        _webView = [[UIWebView alloc] init];
        _webView.translatesAutoresizingMaskIntoConstraints = NO;
        _webView.backgroundColor = self.view.backgroundColor;
        _webView.delegate = self;
    }
    
    return _webView;
}

- (UIEdgeInsets)scrollViewContentInset {
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    return [appDelegate contentInsetForScrollView:self.webView.scrollView];
}

- (void)loadWebView {
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:TERMS_URL]]];
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidStartLoad:(UIWebView *)webView {
    [self.messageView hide];
    self.webView.hidden = YES;
    [self showLoadingIndicator];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [self hideLoadingIndicator];
    self.webView.hidden = NO;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [self hideLoadingIndicator];
    [self.messageView showInView:self.view
                       iconImage:nil
                           title:nil
                         message:[ErrorHandler messageFromError:error]
               actionButtonTitle:NSLocalizedString(@"reload", nil)];
}

#pragma mark - SZKMessageView

- (SZKMessageView *)messageView {
    if (_messageView == nil) {
        _messageView = [[SZKMessageView alloc] init];
        _messageView.delegate = self;
    }
    
    return _messageView;
}

#pragma mark - SZKMessageViewDelegate

- (void)messageView:(SZKMessageView *)messageView didTapActionButton:(UIButton *)actionButton {
    [self loadWebView];
}

#pragma mark - SZKProgressView

- (SZKProgressView *)progressView {
    if (_progressView == nil) {
        _progressView = [[SZKProgressView alloc] init];
    }
    
    return _progressView;
}

#pragma mark - Loading indicator

- (void)showLoadingIndicator {
    [self.progressView showInView:self.view];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    self.navigationItem.rightBarButtonItem.enabled = NO;
}

- (void)hideLoadingIndicator {
    [self.progressView hide];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    self.navigationItem.rightBarButtonItem.enabled = YES;
}

@end
