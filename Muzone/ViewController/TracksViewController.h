//
//  TracksViewController.h
//  Muzone
//
//  Created by Sukhrob Khakimov on 5/27/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import "BaseViewController.h"

@interface TracksViewController : BaseViewController <UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate>
- (instancetype)initWithType:(LoadingListType)type;
@end
