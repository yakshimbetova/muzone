//
//  TracksViewController.m
//  Muzone
//
//  Created by Sukhrob Khakimov on 5/27/16.
//  Copyright © 2016 Muzone LLC. All rights reserved.
//

#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

#import "AppDelegate.h"
#import "Constants.h"
#import "BaseTheme.h"
#import "ErrorHandler.h"
#import "TrackRepository.h"

#import "Album.h"
#import "Artist.h"
#import "Track.h"

#import "TracksViewController.h"

#import "SZKMessageView.h"
#import "SZKProgressView.h"

#import "TracksTableViewCell.h"

#import "FavoriteManager.h"

static NSInteger const limit = 50;

@interface TracksViewController () <SZKMessageViewDelegate>

@property (nonnull, nonatomic, strong) NSManagedObjectContext *mainManagedObjectContext;
@property (nonnull, nonatomic, strong) NSManagedObjectContext *workerManagedObjectContext;
@property (nonnull, nonatomic, strong) TrackRepository *trackRepository;
@property (nonnull, nonatomic, strong) TrackManager *trackManager;

@property (nonatomic, assign) BOOL reloading;
@property (nonatomic, assign) BOOL allowLoading;

@property (nonnull, nonatomic, copy) NSString *cellReuseIdentifier;

@property (nonnull, nonatomic, strong) NSMutableArray *tracks;
@property (nonnull, nonatomic, strong) NSMutableArray *sectionTitles;
@property (nonnull, nonatomic, strong) NSMutableArray *numberOfRowsInSections;
@property (nonnull, nonatomic, strong) NSMutableArray *lastIndexesInSections;

@property (nonnull, nonatomic, strong) UISearchBar *searchBar;
@property (nonnull, nonatomic, strong) UIRefreshControl *refreshControl;
@property (nonnull, nonatomic, strong) UITableView *tableView;
@property (nonnull, nonatomic, strong) SZKMessageView *messageView;
@property (nonnull, nonatomic, strong) SZKProgressView *progressView;
@property (nonatomic) LoadingListType type;
@end

@implementation TracksViewController

- (instancetype)initWithType:(LoadingListType)type {
    self = [super init];
    
    if (self) {
        self.type = type;
        self.allowLoading = (type == LoadingListTypeFavorites) ? NO: YES;
        self.cellReuseIdentifier = NSStringFromClass([TracksTableViewCell class]);
    }
    
    return self;
}

- (void)loadView {
    [super loadView];
    
    [self.view addSubview:self.searchBar];
    [self.view addSubview:self.tableView];
    [self.tableView insertSubview:self.refreshControl
                          atIndex:0];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[searchBar]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:@{@"searchBar": self.searchBar}]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[tableView]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:@{@"tableView": self.tableView}]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[searchBar(height)][tableView]|"
                                                                      options:0
                                                                      metrics:@{@"height": @(44.0f)}
                                                                        views:@{@"searchBar": self.searchBar,
                                                                                @"tableView": self.tableView}]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = NSLocalizedString(@"tracks", nil);
    [self styleRightBarButtonItem];
    
    [self reloadTracks];
    
    [Answers logContentViewWithName:@"Tracks"
                        contentType:@"Menu"
                          contentId:nil
                   customAttributes:@{@"type": (self.type == LoadingListTypeSaved) ? @"Saved" : @"Favorites"}];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.tableView.contentInset = [self tableViewContentInset];
    
    // UIKeyboard
    [self registerForKeyboardNotifications];
    
    // NSNotificationCenter
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    
    [notificationCenter addObserver:self
                           selector:@selector(playerViewControllerWillPlay:)
                               name:PlayerViewControllerWillPlay
                             object:nil];
    
    [notificationCenter addObserver:self
                           selector:@selector(playerViewControllerWillPause:)
                               name:PlayerViewControllerWillPause
                             object:nil];
    
    [notificationCenter addObserver:self
                           selector:@selector(playerViewControllerWillStop:)
                               name:PlayerViewControllerWillStop
                             object:nil];
    
    [notificationCenter addObserver:self
                           selector:@selector(playerViewControllerDidChangeCurrentTrack:)
                               name:PlayerViewControllerDidChangeCurrentTrack
                             object:nil];
    
    [notificationCenter addObserver:self
                           selector:@selector(playerViewControllerShouldHandleError:)
                               name:PlayerViewControllerShouldHandleError
                             object:nil];
    
    [notificationCenter addObserver:self
                           selector:@selector(downloadManagerProcessDidEnd:)
                               name:DownloadManagerProcessDidEnd
                             object:nil];
    [notificationCenter addObserver:self
                           selector:@selector(reloadTracks:)
                               name:FavoriteManagerUpdateTracksNotification
                             object:nil];

    
    [self.tableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated {
    // UIKeyboard
    [self dismissKeyboard];
    [self unregisterFromKeyboardNotifications];
    
    // NSNotificationCenter
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    
    [notificationCenter removeObserver:self
                                  name:PlayerViewControllerWillPlay
                                object:nil];
    
    [notificationCenter removeObserver:self
                                  name:PlayerViewControllerWillPause
                                object:nil];
    
    [notificationCenter removeObserver:self
                                  name:PlayerViewControllerWillStop
                                object:nil];
    
    [notificationCenter removeObserver:self
                                  name:PlayerViewControllerDidChangeCurrentTrack
                                object:nil];
    
    [notificationCenter removeObserver:self
                                  name:PlayerViewControllerShouldHandleError
                                object:nil];
    
    [notificationCenter removeObserver:self
                                  name:DownloadManagerProcessDidEnd
                                object:nil];
    
    [super viewWillDisappear:animated];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - NSNotificationCenter

- (void)playerViewControllerWillPlay:(NSNotification *)notification {
    self.tableView.contentInset = [self tableViewContentInset];
    [self.tableView reloadData];
}

- (void)playerViewControllerWillPause:(NSNotification *)notification {
    [self.tableView reloadData];
}

- (void)playerViewControllerWillStop:(NSNotification *)notification {
    self.tableView.contentInset = [self tableViewContentInset];
    [self.tableView reloadData];
}

- (void)playerViewControllerDidChangeCurrentTrack:(NSNotification *)notification {
    [self.tableView reloadData];
}

- (void)playerViewControllerShouldHandleError:(NSNotification *)notification {
    [self.tableView reloadData];
    
    NSError *error = notification.userInfo[PlayerViewControllerErrorKey];
    [ErrorHandler handleError:error];
}

- (void)downloadManagerProcessDidEnd:(NSNotification *)notification {
    NSError *error = notification.userInfo[DownloadManagerErrorKey];
    
    if (error == nil) {
        [self reloadTracks];
    }
}

- (void)reloadTracks:(NSNotification *)notification
{
    [self reloadTracks];
}

#pragma mark - NSMutableArray

- (NSMutableArray *)tracks {
    if (_tracks == nil) {
        _tracks = [NSMutableArray new];
    }
    
    return _tracks;
}

- (NSMutableArray *)sectionTitles {
    if (_sectionTitles == nil) {
        _sectionTitles = [NSMutableArray new];
    }
    
    return _sectionTitles;
}

- (NSMutableArray *)numberOfRowsInSections {
    if (_numberOfRowsInSections == nil) {
        _numberOfRowsInSections = [NSMutableArray new];
    }
    
    return _numberOfRowsInSections;
}

- (NSMutableArray *)lastIndexesInSections {
    if (_lastIndexesInSections == nil) {
        _lastIndexesInSections = [NSMutableArray new];
    }
    
    return _lastIndexesInSections;
}

#pragma mark - UIKeyboard

- (void)registerForKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)unregisterFromKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)keyboardWillShow:(NSNotification *)aNotification {
    NSDictionary *userInfo = [aNotification userInfo];
    CGSize keyboardSize = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = self.tableView.contentInset;
    contentInsets.bottom = keyboardSize.height;
    
    [self.searchBar setShowsCancelButton:YES
                                animated:YES];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:[userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue]];
    [UIView setAnimationCurve:[userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue]];
    [UIView setAnimationBeginsFromCurrentState:YES];
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
    [UIView commitAnimations];
}

- (void)keyboardWillHide:(NSNotification *)aNotification {
    NSDictionary *userInfo = [aNotification userInfo];
    UIEdgeInsets contentInsets = self.tableView.contentInset;
    contentInsets.bottom = 0.0f;
    
    [self.searchBar setShowsCancelButton:NO
                                animated:YES];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:[userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue]];
    [UIView setAnimationCurve:[userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue]];
    [UIView setAnimationBeginsFromCurrentState:YES];
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
    [UIView commitAnimations];
}

- (void)dismissKeyboard {
    [self.view endEditing:YES];
}

#pragma mark - UINavigationBar

- (void)rightBarButtonItemTapped:(id)sender {
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    if (appDelegate.playerViewController.isRunning) {
        [appDelegate.playerViewController stop];
    }
    
    self.editing = !self.editing;
    [self styleRightBarButtonItem];
    self.tableView.allowsSelection = !self.editing;
    
    NSArray *indexPaths = [self.tableView indexPathsForVisibleRows];
    
    for (NSIndexPath *indexPath in indexPaths) {
        TracksTableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        cell.editing = self.editing;
        [cell styleAnimated:YES];
    }
}

- (void)styleRightBarButtonItem {
    if (self.editing) {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"done", nil)
                                                                                  style:UIBarButtonItemStyleDone
                                                                                 target:self
                                                                                 action:@selector(rightBarButtonItemTapped:)];
    } else {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"edit", nil)
                                                                                  style:UIBarButtonItemStylePlain
                                                                                 target:self
                                                                                 action:@selector(rightBarButtonItemTapped:)];
    }
}

#pragma mark - UISearchBar

- (UISearchBar *)searchBar {
    if (_searchBar == nil) {
        _searchBar = [[UISearchBar alloc] init];
        _searchBar.translatesAutoresizingMaskIntoConstraints = NO;
        _searchBar.searchBarStyle = UISearchBarStyleMinimal;
        _searchBar.keyboardAppearance = UIKeyboardAppearanceDark;
        _searchBar.delegate = self;
        _searchBar.placeholder = NSLocalizedString(@"search", nil);
    }
    
    return _searchBar;
}

#pragma mark - UISearchBarDelegate

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [self dismissKeyboard];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [self reloadTracks];
}

#pragma mark - UIRefreshControl

- (UIRefreshControl *)refreshControl {
    if (_refreshControl == nil) {
        _refreshControl = [[UIRefreshControl alloc] init];
        _refreshControl.backgroundColor = [UIColor whiteColor];
        [_refreshControl addTarget:self
                            action:@selector(refresh)
                  forControlEvents:UIControlEventValueChanged];
    }
    
    return _refreshControl;
}

- (void)refresh {
    if (self.allowLoading) {
        [self reloadTracks];
    } else {
        [self.refreshControl endRefreshing];
    }
}

#pragma mark - UITableView

- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero
                                                  style:UITableViewStylePlain];
        _tableView.translatesAutoresizingMaskIntoConstraints = NO;
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        _tableView.sectionIndexColor = [[BaseTheme currentTheme] tintColor];
        _tableView.sectionIndexBackgroundColor = [UIColor clearColor];
        _tableView.allowsSelection = !self.editing;
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.hidden = YES;
        [_tableView registerClass:[TracksTableViewCell class]
           forCellReuseIdentifier:self.cellReuseIdentifier];
    }
    
    return _tableView;
}

- (UIEdgeInsets)tableViewContentInset {
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    return [appDelegate contentInsetForScrollView:self.tableView];
}

- (void)deselectSelectedRow {
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    
    if (indexPath) {
        [self.tableView deselectRowAtIndexPath:indexPath
                                      animated:YES];
    }
}

- (NSInteger)indexFromIndexPath:(NSIndexPath *)indexPath {
    return [self.lastIndexesInSections[indexPath.section] integerValue] + 1 - [self.numberOfRowsInSections[indexPath.section] integerValue] + indexPath.row;
}

#pragma mark - UITableViewDataSource

- (NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return self.sectionTitles;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.sectionTitles.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.numberOfRowsInSections[section] integerValue];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger index = [self indexFromIndexPath:indexPath];
    Track *track = self.tracks[index];
    
    TracksTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:self.cellReuseIdentifier
                                                                forIndexPath:indexPath];
    cell.editing = self.editing;
    cell.titleLabel.text = track.title;
    cell.artistNameLabel.text = track.artist.name;
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    PlayerViewController *playerViewController = appDelegate.playerViewController;
    
    if (playerViewController.isRunning && [track.uid isEqual:playerViewController.currentTrack.uid]) {
        cell.iconImageView.image = [UIImage imageNamed:@"ic_pause_green.png"];
    } else {
        cell.iconImageView.image = [UIImage imageNamed:@"ic_play_green.png"];
    }
    
    [cell.deleteButton addTarget:self
                          action:@selector(deleteButtonTapped:)
                forControlEvents:UIControlEventTouchUpInside];
    [cell.deleteButton setImage:[UIImage imageNamed:@"ic_delete.png"]
                       forState:UIControlStateNormal];
    
    [cell styleAnimated:NO];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [[BaseTheme currentTheme] sectionBackgroundColor];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    titleLabel.font = [UIFont boldSystemFontOfSize:17.0f];
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.text = self.sectionTitles[section];
    
    [headerView addSubview:titleLabel];
    
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-space-[titleLabel]-space-|"
                                                                       options:0
                                                                       metrics:@{@"space": @(15.0f)}
                                                                         views:@{@"titleLabel": titleLabel}]];
    
    [headerView addConstraint:[NSLayoutConstraint constraintWithItem:titleLabel
                                                           attribute:NSLayoutAttributeCenterY
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:headerView
                                                           attribute:NSLayoutAttributeCenterY
                                                          multiplier:1.0f
                                                            constant:0.0f]];
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 32.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60.0f;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    NSInteger index = [self indexFromIndexPath:indexPath];
    
    if (self.allowLoading && index == self.tracks.count - 1) {
        if (self.type == LoadingListTypeSaved) {
            [self loadTracks];
        }
        else
        {
            [self loadFavorites];
        }
        
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger index = [self indexFromIndexPath:indexPath];
    Track *track = self.tracks[index];
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    if (appDelegate.playerViewController) {
        if ([track.album.uid isEqual:appDelegate.playerViewController.currentTrack.album.uid]) {
            if (appDelegate.playerViewController.isRunning) {
                if ([track.uid isEqual:appDelegate.playerViewController.currentTrack.uid]) {
                    [appDelegate.playerViewController pause];
                } else {
                    [appDelegate.playerViewController pause];
                    appDelegate.playerViewController.currentTrack = track;
                    [appDelegate.playerViewController play];
                }
            } else {
                if ([track.uid isEqual:appDelegate.playerViewController.currentTrack.uid]) {
                    [appDelegate.playerViewController play];
                } else {
                    appDelegate.playerViewController.currentTrack = track;
                    [appDelegate.playerViewController play];
                }
            }
        } else {
            [appDelegate.playerViewController pause];
            appDelegate.playerViewController = [[PlayerViewController alloc] initWithTracks:self.tracks
                                                                               currentTrack:track];
            [appDelegate.playerViewController play];
        }
    } else {
        appDelegate.playerViewController = [[PlayerViewController alloc] initWithTracks:self.tracks
                                                                           currentTrack:track];
        [appDelegate.playerViewController play];
    }
    
    [self.tableView reloadData];
}

#pragma mark - UIAlertController

- (void)showAlertControllerForIndexPath:(nonnull NSIndexPath *)indexPath {
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"cancel", nil)
                                                           style:UIAlertActionStyleCancel
                                                         handler:nil];
    UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"delete", nil)
                                                           style:UIAlertActionStyleDestructive
                                                         handler:^(UIAlertAction *action) {
                                                             [self deleteTrackAtIndexPath:indexPath];
                                                         }];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"track", nil)
                                                                             message:NSLocalizedString(@"track.delete_message", nil)
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:cancelAction];
    [alertController addAction:deleteAction];
    [self presentViewController:alertController
                       animated:YES
                     completion:nil];
}

- (void)showAlertController:(NSString*) text {
    UIAlertAction *doneAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"done", nil)
                                                         style:UIAlertActionStyleDefault
                                                       handler:nil];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"track", nil)
                                                                             message:text
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:doneAction];
    [self presentViewController:alertController
                       animated:YES
                     completion:nil];
}


#pragma mark - UIButton

- (void)deleteButtonTapped:(UIButton *)button {
    CGPoint position = [button convertPoint:CGPointZero
                                     toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:position];
    [self showAlertControllerForIndexPath:indexPath];
}

#pragma mark - SZKMessageView

- (SZKMessageView *)messageView {
    if (_messageView == nil) {
        _messageView = [[SZKMessageView alloc] init];
        _messageView.delegate = self;
    }
    
    return _messageView;
}

- (void)messageView:(SZKMessageView *)messageView didTapActionButton:(UIButton *)actionButton {
    [self reloadTracks];
}

#pragma mark - UIActivityIndicatorView

- (SZKProgressView *)progressView {
    if (_progressView == nil) {
        _progressView = [[SZKProgressView alloc] init];
    }
    
    return _progressView;
}

#pragma mark - Loading indicator

- (void)showLoadingIndicator {
    if (self.tracks.count == 0) {
        [self.messageView hide];
        [self.progressView showInView:self.view];
    }
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    self.allowLoading = NO;
}

- (void)hideLoadingIndicator {
    [self.refreshControl endRefreshing];
    [self.progressView hide];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    self.allowLoading = (self.type == LoadingListTypeFavorites) ? NO: YES;
}

#pragma mark - CoreData

- (NSManagedObjectContext *)mainManagedObjectContext {
    if (_mainManagedObjectContext == nil) {
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        _mainManagedObjectContext = appDelegate.mainManagedObjectContext;
    }
    
    return _mainManagedObjectContext;
}

- (NSManagedObjectContext *)workerManagedObjectContext {
    if (_workerManagedObjectContext == nil) {
        _workerManagedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        _workerManagedObjectContext.parentContext = self.mainManagedObjectContext;
    }
    
    return _workerManagedObjectContext;
}

- (TrackRepository *)trackRepository {
    if (_trackRepository == nil) {
        _trackRepository = [[TrackRepository alloc] initWithManagedObjectContext:self.workerManagedObjectContext];
    }
    
    return _trackRepository;
}

- (TrackManager *)trackManager {
    if (_trackManager == nil) {
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        _trackManager = appDelegate.trackManager;
    }
    
    return _trackManager;
}


- (void)loadTracks {
    [self showLoadingIndicator];
    [self.workerManagedObjectContext performBlock:^{
        if (self.reloading) {
            self.reloading = NO;
            
            [self.sectionTitles removeAllObjects];
            [self.numberOfRowsInSections removeAllObjects];
            [self.lastIndexesInSections removeAllObjects];
            [self.tracks removeAllObjects];
        }
        
        NSInteger totalCount = self.tracks.count;
        NSArray *result = [self.trackRepository fetchAllWithText:self.searchBar.text
                                                          offset:totalCount
                                                           limit:limit];
        NSInteger totalResult = result.count;
        
        if (totalResult > 0) {
            NSInteger numberOfNewSections = 0;
            NSMutableArray *indexPaths = [NSMutableArray new];
            
            for (NSInteger index = 0; index < totalResult; index++) {
                Track *track = result[index];
                
                if (!track.objectID.isTemporaryID) {
                    NSString *sectionTitle = [track.title substringToIndex:1];
                    
                    if (![self.sectionTitles.lastObject isEqualToString:sectionTitle]) {
                        numberOfNewSections++;
                        
                        [self.sectionTitles addObject:sectionTitle];
                        [self.numberOfRowsInSections addObject:@(1)];
                        
                        NSNumber *lastObject = self.lastIndexesInSections.lastObject;
                        NSInteger lastIndexInSection = 0;
                        
                        if (lastObject) {
                            lastIndexInSection = [lastObject integerValue] + 1;
                        }
                        
                        [self.lastIndexesInSections addObject:@(lastIndexInSection)];
                    } else {
                        NSInteger numberOfRowsInLastSection = [self.numberOfRowsInSections.lastObject integerValue];
                        [self.numberOfRowsInSections replaceObjectAtIndex:self.sectionTitles.count - 1
                                                               withObject:@(numberOfRowsInLastSection + 1)];
                        
                        NSInteger lastIndexInSection = [self.lastIndexesInSections.lastObject integerValue];
                        [self.lastIndexesInSections replaceObjectAtIndex:self.lastIndexesInSections.count - 1
                                                              withObject:@(lastIndexInSection + 1)];
                    }
                    
                    [self.tracks addObject:track];
                    
                    NSInteger numberOfRowsInLastSection = [self.numberOfRowsInSections.lastObject integerValue];
                    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:numberOfRowsInLastSection - 1
                                                                inSection:self.sectionTitles.count - 1];
                    [indexPaths addObject:indexPath];
                }
            }
            
            [self.mainManagedObjectContext performBlock:^{
                [self hideLoadingIndicator];
                
                if (indexPaths.count > 0) {
                    if (totalCount > 0) {
                        [self.tableView beginUpdates];
                        
                        if (numberOfNewSections > 0) {
                            NSInteger startIndex = self.sectionTitles.count - numberOfNewSections;
                            [self.tableView insertSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(startIndex, numberOfNewSections)]
                                          withRowAnimation:UITableViewRowAnimationNone];
                        }
                        
                        [self.tableView insertRowsAtIndexPaths:indexPaths
                                              withRowAnimation:UITableViewRowAnimationNone];
                        [self.tableView endUpdates];
                    } else {
                        self.tableView.hidden = NO;
                        self.tableView.contentOffset = CGPointMake(0.0f, 0.0f - self.tableView.contentInset.top);
                        [self.tableView reloadData];
                    }
                } else {
                    if (totalCount == 0) {
                        self.tableView.hidden = YES;
                        self.tableView.contentOffset = CGPointMake(0.0f, 0.0f - self.tableView.contentInset.top);
                        [self.tableView reloadData];
                        
                        [self.messageView showInView:self.view
                                           iconImage:nil
                                               title:nil
                                             message:NSLocalizedString(@"no_data", nil)
                                   actionButtonTitle:NSLocalizedString(@"reload", nil)];
                    }
                }
            }];
        } else {
            [self.mainManagedObjectContext performBlock:^{
                [self hideLoadingIndicator];
                
                if (totalCount == 0) {
                    self.tableView.hidden = YES;
                    self.tableView.contentOffset = CGPointMake(0.0f, 0.0f - self.tableView.contentInset.top);
                    [self.tableView reloadData];
                    
                    [self.messageView showInView:self.view
                                       iconImage:nil
                                           title:nil
                                         message:NSLocalizedString(@"no_data", nil)
                               actionButtonTitle:NSLocalizedString(@"reload", nil)];
                }
            }];
        }
    }];
}

-(void)loadFavorites
{
    [self showLoadingIndicator];

    FavoriteManager *fManager = [FavoriteManager sharedManager];
    WEAKIFY_SELF;
    [fManager getFavoriteTracks:^(BOOL success, NSArray * _Nullable result) {
        STRONGIFY_SELF;
        if (success) {
            [self hideLoadingIndicator];
            [self workWithFavorites:result];
        }
        else
        {
            [self hideLoadingIndicator];
            self.tableView.hidden = YES;
            self.tableView.contentOffset = CGPointMake(0.0f, 0.0f - self.tableView.contentInset.top);
            [self.tableView reloadData];
            
            [self.messageView showInView:self.view
                               iconImage:nil
                                   title:nil
                                 message:NSLocalizedString(@"no_data", nil)
                       actionButtonTitle:NSLocalizedString(@"reload", nil)];

        }
    }];
    
}


-(void)workWithFavorites:(NSArray*)favoriteTracks
{
    [self.workerManagedObjectContext performBlock:^{
        [self.sectionTitles removeAllObjects];
        [self.numberOfRowsInSections removeAllObjects];
        [self.lastIndexesInSections removeAllObjects];
        [self.tracks removeAllObjects];

        NSInteger totalResult = favoriteTracks.count;
        
        if (totalResult > 0) {
            NSMutableArray *indexPaths = [NSMutableArray new];
            
            for (NSInteger index = 0; index < totalResult; index++) {
                Track *track = favoriteTracks[index];
                
                
                if ([self.tracks indexOfObject:track] > self.tracks.count) {
                    NSString *sectionTitle = [track.title substringToIndex:1];
                    
                    if (![self.sectionTitles.lastObject isEqualToString:sectionTitle] ) {
                        
                        [self.sectionTitles addObject:sectionTitle];
                        [self.numberOfRowsInSections addObject:@(1)];
                        
                        NSNumber *lastObject = self.lastIndexesInSections.lastObject;
                        NSInteger lastIndexInSection = 0;
                        
                        if (lastObject) {
                            lastIndexInSection = [lastObject integerValue] + 1;
                        }
                        
                        [self.lastIndexesInSections addObject:@(lastIndexInSection)];
                    } else {
                        NSInteger numberOfRowsInLastSection = [self.numberOfRowsInSections.lastObject integerValue];
                        [self.numberOfRowsInSections replaceObjectAtIndex:self.sectionTitles.count - 1
                                                               withObject:@(numberOfRowsInLastSection + 1)];
                        
                        NSInteger lastIndexInSection = [self.lastIndexesInSections.lastObject integerValue];
                        [self.lastIndexesInSections replaceObjectAtIndex:self.lastIndexesInSections.count - 1
                                                              withObject:@(lastIndexInSection + 1)];
                    }
                   
                    [self.tracks addObject:track];
                
                    
                    
                    NSInteger numberOfRowsInLastSection = [self.numberOfRowsInSections.lastObject integerValue];
                    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:numberOfRowsInLastSection - 1
                                                                inSection:self.sectionTitles.count - 1];
                    [indexPaths addObject:indexPath];
                }
            }
            
            [self.mainManagedObjectContext performBlock:^{
                [self hideLoadingIndicator];
                
                if (self.tracks.count > 0) {
                        self.tableView.hidden = NO;
                        self.tableView.contentOffset = CGPointMake(0.0f, 0.0f - self.tableView.contentInset.top);
                        [self.tableView reloadData];
                } else {
                        self.tableView.hidden = YES;
                        self.tableView.contentOffset = CGPointMake(0.0f, 0.0f - self.tableView.contentInset.top);
                        [self.tableView reloadData];
                        
                        [self.messageView showInView:self.view
                                           iconImage:nil
                                               title:nil
                                             message:NSLocalizedString(@"no_data", nil)
                                   actionButtonTitle:NSLocalizedString(@"reload", nil)];
                }
            }];
        } else {
            [self.mainManagedObjectContext performBlock:^{
                [self hideLoadingIndicator];
                
                self.tableView.hidden = YES;
                self.tableView.contentOffset = CGPointMake(0.0f, 0.0f - self.tableView.contentInset.top);
                [self.tableView reloadData];
                
                [self.messageView showInView:self.view
                                   iconImage:nil
                                       title:nil
                                     message:NSLocalizedString(@"no_data", nil)
                           actionButtonTitle:NSLocalizedString(@"reload", nil)];
            }];
        }
    }];
}

- (void)reloadTracks {
    self.reloading = YES;
    if (self.type == LoadingListTypeSaved) {
        [self loadTracks];
    }
    else
    {
        [self loadFavorites];
    }
    
}

- (void)deleteTrackAtIndexPath:(NSIndexPath *)indexPath {
    if (self.type == LoadingListTypeFavorites) {
        
        if ([User currentUser].accessToken == nil)
        {
            [[User currentUser] showLoginForm];
            return;
        }
        else
        {
            NSInteger index = [self indexFromIndexPath:indexPath];
            Track *track = self.tracks[index];
            FavoriteManager *fManager = [FavoriteManager sharedManager];
            [fManager deleteFromFavoriteTrack:track complete:^(BOOL success) {
                if (!success) {
                    [self showAlertController:NSLocalizedString(@"error.unexpected", nil)];
                }
                
            }];
            return;
        }
    }
    NSInteger index = [self indexFromIndexPath:indexPath];
    Track *track = self.tracks[index];
    NSString *fileName = track.localFileName;
    [self.workerManagedObjectContext performBlock:^{
        [self.workerManagedObjectContext deleteObject:track];
        
        NSError *error;
        
        if (![self.workerManagedObjectContext save:&error]) {
            [self.mainManagedObjectContext performBlock:^{
                [ErrorHandler handleError:error];
            }];
        } else {
            [self.mainManagedObjectContext performBlock:^{
                NSError *error;
                
                if (![self.mainManagedObjectContext save:&error]) {
                    [self.mainManagedObjectContext performBlock:^{
                        [ErrorHandler handleError:error];
                    }];
                } else {
                    [self.mainManagedObjectContext.parentContext performBlock:^{
                        NSError *error;
                        
                        if (![self.mainManagedObjectContext.parentContext save:&error]) {
                            [self.mainManagedObjectContext performBlock:^{
                                [ErrorHandler handleError:error];
                            }];
                        } else {
                            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                            NSString *documentsDirectory = paths.lastObject;
                            NSString *sourcePath = [documentsDirectory stringByAppendingPathComponent:TRACKS_DIRECTORY];
                            NSString *filePath = [sourcePath stringByAppendingPathComponent:fileName];
                            
                            NSError *error;
                            
                            NSFileManager *fileManager = [NSFileManager defaultManager];
                            [fileManager removeItemAtPath:filePath
                                                    error:&error];
                            
                            if (error) {
                                [self.mainManagedObjectContext performBlock:^{
                                    [ErrorHandler handleError:error];
                                }];
                            }
                            
                            NSInteger numberOfRowsInSection = [self.numberOfRowsInSections[indexPath.section] integerValue];
                            
                            if (numberOfRowsInSection == 1) {
                                [self.sectionTitles removeObjectAtIndex:indexPath.section];
                                [self.numberOfRowsInSections removeObjectAtIndex:indexPath.section];
                                [self.lastIndexesInSections removeObjectAtIndex:indexPath.section];
                            } else {
                                [self.numberOfRowsInSections replaceObjectAtIndex:indexPath.section
                                                                       withObject:@(numberOfRowsInSection - 1)];
                            }
                            
                            for (NSInteger section = indexPath.section; section < self.sectionTitles.count; section++) {
                                NSInteger lastIndexInSection = [self.lastIndexesInSections[section] integerValue];
                                [self.lastIndexesInSections replaceObjectAtIndex:section
                                                                      withObject:@(lastIndexInSection - 1)];
                            }
                            
                            [self.tracks removeObjectAtIndex:index];
                            
                            [self.mainManagedObjectContext performBlock:^{
                                [CATransaction begin];
                                [CATransaction setCompletionBlock:^{
                                    if (self.tracks.count == 0) {
                                        self.tableView.hidden = YES;
                                        self.tableView.contentOffset = CGPointMake(0.0f, 0.0f - self.tableView.contentInset.top);
                                        [self.tableView reloadData];
                                        
                                        [self.messageView showInView:self.view
                                                           iconImage:nil
                                                               title:nil
                                                             message:NSLocalizedString(@"no_data", nil)
                                                   actionButtonTitle:NSLocalizedString(@"reload", nil)];
                                    }
                                }];
                                [self.tableView beginUpdates];
                                
                                if (numberOfRowsInSection == 1) {
                                    [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:indexPath.section]
                                                  withRowAnimation:UITableViewRowAnimationRight];
                                }
                                
                                [self.tableView deleteRowsAtIndexPaths:@[indexPath]
                                                      withRowAnimation:UITableViewRowAnimationRight];
                                [self.tableView endUpdates];
                                [CATransaction commit];
                            }];
                        }
                    }];
                }
            }];
        }
    }];
}

-(void)deleteTrack:(Track*) track index:(NSIndexPath *)indexPath
{
    [self.workerManagedObjectContext performBlock:^{
        [self.workerManagedObjectContext deleteObject:track];
        
        NSError *error;
        
        if (![self.workerManagedObjectContext save:&error]) {
            [self.mainManagedObjectContext performBlock:^{
                [ErrorHandler handleError:error];
            }];
        } else {
            [self.mainManagedObjectContext performBlock:^{
                NSError *error;
                
                if (![self.mainManagedObjectContext save:&error]) {
                    [self.mainManagedObjectContext performBlock:^{
                        [ErrorHandler handleError:error];
                    }];
                } else {
                    [self.mainManagedObjectContext.parentContext performBlock:^{
                        NSError *error;
                        
                        if (![self.mainManagedObjectContext.parentContext save:&error]) {
                            [self.mainManagedObjectContext performBlock:^{
                                [ErrorHandler handleError:error];
                            }];
                        } else {
                            
                            NSInteger numberOfRowsInSection = [self.numberOfRowsInSections[indexPath.section] integerValue];
                            
                            if (numberOfRowsInSection == 1) {
                                [self.sectionTitles removeObjectAtIndex:indexPath.section];
                                [self.numberOfRowsInSections removeObjectAtIndex:indexPath.section];
                                [self.lastIndexesInSections removeObjectAtIndex:indexPath.section];
                            } else {
                                [self.numberOfRowsInSections replaceObjectAtIndex:indexPath.section
                                                                       withObject:@(numberOfRowsInSection - 1)];
                            }
                            
                            for (NSInteger section = indexPath.section; section < self.sectionTitles.count; section++) {
                                NSInteger lastIndexInSection = [self.lastIndexesInSections[section] integerValue];
                                [self.lastIndexesInSections replaceObjectAtIndex:section
                                                                      withObject:@(lastIndexInSection - 1)];
                            }
                            
                            [self.tracks removeObject:track];
                            
                            [self.mainManagedObjectContext performBlock:^{
                                [CATransaction begin];
                                [CATransaction setCompletionBlock:^{
                                    if (self.tracks.count == 0) {
                                        self.tableView.hidden = YES;
                                        self.tableView.contentOffset = CGPointMake(0.0f, 0.0f - self.tableView.contentInset.top);
                                        [self.tableView reloadData];
                                        
                                        [self.messageView showInView:self.view
                                                           iconImage:nil
                                                               title:nil
                                                             message:NSLocalizedString(@"no_data", nil)
                                                   actionButtonTitle:NSLocalizedString(@"reload", nil)];
                                    }
                                }];
                                [self.tableView beginUpdates];
                                
                                if (numberOfRowsInSection == 1) {
                                    [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:indexPath.section]
                                                  withRowAnimation:UITableViewRowAnimationRight];
                                }
                                
                                [self.tableView deleteRowsAtIndexPaths:@[indexPath]
                                                      withRowAnimation:UITableViewRowAnimationRight];
                                [self.tableView endUpdates];
                                [CATransaction commit];
                            }];
                        }
                    }];
                }
            }];
        }
    }];

}

@end
